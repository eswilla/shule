
CREATE TABLE testing.subject_student
(
  subject_student_id serial NOT NULL,
  subject_id integer,
  student_id integer,
  created_at timestamp without time zone DEFAULT now(),
  CONSTRAINT pk_subject_student PRIMARY KEY (subject_student_id)
)

ALTER TABLE testing.book
   ALTER COLUMN book TYPE character varying;

ALTER TABLE testing.book
   ALTER COLUMN author TYPE character varying;

ALTER TABLE testing.book
   ALTER COLUMN edition TYPE character varying;

ALTER TABLE testing.student
   ALTER COLUMN library SET DEFAULT 0;

ALTER TABLE testing.student
   ALTER COLUMN hostel SET DEFAULT 0;

ALTER TABLE testing.student
   ALTER COLUMN transport SET DEFAULT 0;

ALTER TABLE testing.student
   ALTER COLUMN create_date SET DEFAULT now();

CREATE VIEW subject_count AS
SELECT count(DISTINCT a.subject_id) as core_subjects, b."studentID", count(DISTINCT c.subject_id) as option_subjects,(count(DISTINCT a.subject_id) + count(DISTINCT c.subject_id)) as total_subjects 
         FROM testing.subject_section a  
         JOIN 
              testing.student b ON b."sectionID"=a.section_id
         LEFT JOIN 
             testing.subject_student c ON c.student_id=b."studentID" GROUP BY "studentID";

ALTER TABLE testing.feetype ADD COLUMN bank_account_id bigint;

ALTER TABLE testing.bank_account ADD COLUMN bank_branch character varying;

ALTER TABLE testing.bank_account ADD COLUMN bank_swiftcode character varying;

ALTER TABLE testing.bank_account ADD COLUMN note text;

ALTER TABLE testing.bank_account ADD COLUMN created_at timestamp without time zone;
ALTER TABLE testing.bank_account ALTER COLUMN created_at SET DEFAULT now();

CREATE TABLE testing.subject_student
(
  subject_student_id serial NOT NULL,
  subject_id integer,
  student_id integer,
  created_at timestamp without time zone DEFAULT now(),
  CONSTRAINT pk_subject_student PRIMARY KEY (subject_student_id )
);

--Additional tables

--student archive table
CREATE TABLE public.student_archive
(
  id serial NOT NULL,
  student_id bigint NOT NULL,
  academic_year_id smallint NOT NULL,
  section_id smallint NOT NULL,
  created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone,
  updated_at timestamp(0) without time zone,
  CONSTRAINT student_archive_pkey PRIMARY KEY (id),
  CONSTRAINT student_archive_accademic_year_id_fkey FOREIGN KEY (academic_year_id)
      REFERENCES public.academic_year (id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT,
  CONSTRAINT student_archive_section_id_fkey FOREIGN KEY (section_id)
      REFERENCES public.section ("sectionID") MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT,
  CONSTRAINT student_archive_student_id_fkey FOREIGN KEY (student_id)
      REFERENCES public.student ("studentID") MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT
)

--accademic year table

CREATE TABLE testing.academic_year
(
  id serial NOT NULL,
  name character varying(100) NOT NULL,
  class_level_id smallint NOT NULL,
  created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone,
  updated_at timestamp(0) without time zone,
  CONSTRAINT accademic_year_pkey PRIMARY KEY (id),
  CONSTRAINT accademic_year_class_level_id_fkey FOREIGN KEY (class_level_id)
      REFERENCES testing.classlevel (classlevel_id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT,
  CONSTRAINT accademic_year_name_key UNIQUE (name)
)



ALTER TABLE testing.student ADD COLUMN academic_year_id bigint;

--run only once after adding academic year in student table then don't run again
UPDATE testing.student
   SET  academic_year_id=1;

--add this column in mark table for each query this must be specified
ALTER TABLE testing.mark ADD COLUMN academic_year_id smallint;


--academic year should be also in settings table, when user change academic year, system will change it in setting table
ALTER TABLE testing.setting ADD COLUMN academic_year_id integer;

--update mark table only once after adding academic year id
UPDATE testing.mark SET academic_year_id=1;

ALTER TABLE testing.student ADD COLUMN status integer;
ALTER TABLE testing.student ALTER COLUMN status SET DEFAULT 1;
COMMENT ON COLUMN testing.student.status IS '0=not active, 1=active, 2=finish school';

ALTER TABLE testing.parent ADD COLUMN status integer;
ALTER TABLE testing.parent ALTER COLUMN status SET DEFAULT 1;
COMMENT ON COLUMN testing.parent.status IS '0=not active, 1=active';


ALTER TABLE testing.teacher
   ADD COLUMN id_number character varying;


CREATE TABLE testing.special_promotion
(
  id serial NOT NULL,
  student_id bigint,
  from_academic_year_id bigint,
  to_academic_year_id bigint,
  pass_mark real,
  remark character varying,
  created_at timestamp without time zone DEFAULT now(),
  updated_at timestamp without time zone,
  status integer, -- 1=promoted...
  CONSTRAINT pk_special_promotion PRIMARY KEY (id),
  CONSTRAINT fk_special_promotion_ac FOREIGN KEY (id)
      REFERENCES testing.academic_year (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE SET NULL
)
WITH (
  OIDS=FALSE
);
ALTER TABLE testing.special_promotion
  OWNER TO postgres;
COMMENT ON COLUMN testing.special_promotion.status IS '1=promoted
2=not promoted
3=de promoted';


CREATE OR REPLACE VIEW testing.core_option_subject_count AS 
 SELECT count(DISTINCT a.subject_id) AS core_subjects, b."studentID", count(DISTINCT c.subject_id) AS option_subjects, count(DISTINCT a.subject_id) + count(DISTINCT c.subject_id) AS total_subjects
   FROM testing.subject_section a
   JOIN testing.student b ON b."sectionID" = a.section_id
   LEFT JOIN testing.subject_student c ON c.student_id = b."studentID"
  GROUP BY b."studentID";

ALTER TABLE testing.core_option_subject_count
  OWNER TO postgres;

CREATE OR REPLACE VIEW testing.mark_grade AS 
 SELECT total_mark."studentID", total_mark."classesID", total_mark.year, total_mark."examID", total_mark.total, row_number() OVER (ORDER BY total_mark.total DESC) AS rank
   FROM testing.total_mark;

ALTER TABLE testing.mark_grade
  OWNER TO postgres;

CREATE OR REPLACE VIEW testing.mark_ranking AS 
 SELECT mark."studentID", mark.mark, rank() OVER (PARTITION BY mark."subjectID" ORDER BY mark.mark DESC) AS dense_rank, mark."examID", mark."subjectID"
   FROM testing.mark;

ALTER TABLE testing.mark_ranking
  OWNER TO postgres;

CREATE OR REPLACE VIEW testing.payment_history AS 
 SELECT a."invoiceNO", b."paymentID", a."invoiceID", b."transactionID", b.paymentdate, c.name AS student_name, d.name AS parent_name, b.paymentamount, b.paymenttype, b.slipfile, b.approved, e.feetype
   FROM testing.invoice a
   JOIN testing.payment b ON b."invoiceID" = a."invoiceID"
   JOIN testing.student c ON c."studentID" = b."studentID"
   JOIN testing.parent d ON d."parentID" = c."parentID"
   JOIN testing.feetype e ON e."feetypeID" = a."feetypeID";

ALTER TABLE testing.payment_history
  OWNER TO postgres;

CREATE OR REPLACE VIEW testing.subject_count AS 
         SELECT m.subject_id, m.student_id, s.subject, 'Option'::character varying AS subject_type, s.is_counted, s.is_penalty, s.pass_mark, s.grade_mark
           FROM testing.subject_student m
      JOIN testing.subject s ON s."subjectID" = m.subject_id
UNION 
         SELECT a.subject_id, b."studentID" AS student_id, s.subject, 'Core'::character varying AS subject_type, s.is_counted, s.is_penalty, s.pass_mark, s.grade_mark
           FROM testing.subject_section a
      JOIN testing.student b ON b."sectionID" = a.section_id
   JOIN testing.subject s ON s."subjectID" = a.subject_id;

ALTER TABLE testing.subject_count
  OWNER TO postgres;

CREATE OR REPLACE VIEW testing.sum_rank AS 
 SELECT a."examID", a."classesID", a."studentID", a."subjectID", c.is_counted, c.is_penalty, sum(a.mark) AS sum, rank() OVER (ORDER BY sum(a.mark) DESC) AS rank
   FROM testing.mark a
   JOIN testing.subject c ON c."subjectID" = a."subjectID"
  WHERE a.mark IS NOT NULL
  GROUP BY a."examID", a."classesID", a."studentID", a."subjectID", c.is_counted, c.is_penalty;

ALTER TABLE testing.sum_rank
  OWNER TO postgres;

CREATE OR REPLACE VIEW testing.total_mark AS 
 SELECT sum(mark.mark) AS total, mark."studentID", mark."examID", mark.year, mark."classesID"
   FROM testing.mark
  GROUP BY mark."studentID", mark."examID", mark."classesID", mark.year;

ALTER TABLE testing.total_mark
  OWNER TO postgres;



CREATE OR REPLACE VIEW testing.users AS 
        (        (        (         SELECT a."userID" AS id, a.email, a.phone, a.username, a.usertype
                                   FROM testing."user" a
                        UNION ALL 
                                 SELECT t."teacherID" AS id, t.email, t.phone, t.username, 'teacher'::character varying AS usertype
                                   FROM testing.teacher t)
                UNION ALL 
                         SELECT s."studentID" AS id, s.email, s.phone, s.username, 'student'::character varying AS usertype
                           FROM testing.student s)
        UNION ALL 
                 SELECT p."parentID" AS id, p.email, p.phone, p.username, 'parent'::character varying AS usertype
                   FROM testing.parent p)
UNION ALL 
         SELECT b."settingID" AS id, b.email, b.phone, b.username, 'Admin'::character varying AS usertype
           FROM testing.setting b;

ALTER TABLE testing.users
  OWNER TO postgres;



ALTER TABLE testing.subject_student
   ADD COLUMN academic_year_id integer;

--updates for queries that create user roles
-- dated 17th Jan 2017


CREATE TABLE testing.role (
    id serial,
    name character varying NOT NULL,
    sys_role character(1) DEFAULT '0'::bpchar NOT NULL,
    is_super character(1) DEFAULT '0'::bpchar NOT NULL,
    created_at timestamp(0) without time zone DEFAULT ('now'::text)::timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone
);

CREATE TABLE testing.permission_group (
    id serial,
    name character varying NOT NULL,
    created_at timestamp(0) without time zone DEFAULT ('now'::text)::timestamp(0) without time zone NOT NULL
);

CREATE TABLE testing.permission (
    id serial,
    name character varying(80) NOT NULL,
    display_name character varying NOT NULL,
    description text NOT NULL,
    is_super character(1) DEFAULT '0'::bpchar NOT NULL,
    permission_group_id smallint NOT NULL,
    created_at timestamp(0) without time zone DEFAULT ('now'::text)::timestamp(0) without time zone NOT NULL
);


CREATE TABLE testing.role_permission (
    id serial,
    role_id smallint NOT NULL,
    permission_id smallint NOT NULL,
    created_at timestamp(0) without time zone DEFAULT ('now'::text)::timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone
);

CREATE TABLE testing.user_role (
    id serial,
    userID bigint NOT NULL,
    role_id smallint NOT NULL,
    created_at timestamp(0) without time zone DEFAULT ('now'::text)::timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone
);



--
-- Data for Name: testing.permission_group; Type: TABLE DATA; Schema: demo2; Owner: postgres
--

INSERT INTO testing.permission_group VALUES (1, 'Students', '2017-01-11 11:26:07');
INSERT INTO testing.permission_group VALUES (2, 'Teachers', '2017-01-11 11:26:31');
INSERT INTO testing.permission_group VALUES (3, 'Parents', '2017-01-11 11:27:03');
INSERT INTO testing.permission_group VALUES (4, 'Users', '2017-01-11 11:27:23');
INSERT INTO testing.permission_group VALUES (5, 'Class', '2017-01-11 11:27:30');
INSERT INTO testing.permission_group VALUES (6, 'Section', '2017-01-11 11:27:37');
INSERT INTO testing.permission_group VALUES (7, 'Library', '2017-01-11 11:27:52');
INSERT INTO testing.permission_group VALUES (8, 'Transport', '2017-01-11 11:28:01');
INSERT INTO testing.permission_group VALUES (9, 'Subject', '2017-01-11 11:28:21');
INSERT INTO testing.permission_group VALUES (10, 'Grade', '2017-01-11 11:28:29');
INSERT INTO testing.permission_group VALUES (11, 'Class level', '2017-01-11 11:28:38');
INSERT INTO testing.permission_group VALUES (12, 'Exam', '2017-01-11 11:28:47');
INSERT INTO testing.permission_group VALUES (13, 'Mark', '2017-01-11 11:29:06');
INSERT INTO testing.permission_group VALUES (14, 'Routine', '2017-01-11 11:29:14');
INSERT INTO testing.permission_group VALUES (15, 'Attendance', '2017-01-11 11:29:33');

SELECT pg_catalog.setval('testing.permission_group_id_seq', 15, true);

INSERT INTO testing.permission VALUES (2, 'add_student', 'Add student', '', '0', 1, '2017-01-11 11:50:16');
INSERT INTO testing.permission VALUES (3, 'edit_student', 'Edit student', '', '0', 1, '2017-01-11 11:50:33');
INSERT INTO testing.permission VALUES (4, 'delete_student', 'Delete student', '', '0', 1, '2017-01-11 11:54:07');
INSERT INTO testing.permission VALUES (6, 'print_student_info', 'Print student information', '', '0', 1, '2017-01-11 12:05:02');
INSERT INTO testing.permission VALUES (5, 'view_student_info', 'View student information', '', '0', 1, '2017-01-11 11:57:36');
INSERT INTO testing.permission VALUES (10, 'view_teachers', 'View teachers', '', '0', 2, '2017-01-11 12:17:01');
INSERT INTO testing.permission VALUES (11, 'add_teacher', 'Add teacher', '', '0', 2, '2017-01-11 12:17:22');
INSERT INTO testing.permission VALUES (12, 'edit_teacher', 'Edit teacher', '', '0', 2, '2017-01-11 12:17:44');
INSERT INTO testing.permission VALUES (13, 'delete_teacher', 'Delete teacher', '', '0', 2, '2017-01-11 12:19:50');
INSERT INTO testing.permission VALUES (14, 'view_teacher_info', 'View more one teacher information', '', '0', 2, '2017-01-11 12:49:23');
INSERT INTO testing.permission VALUES (16, 'print_teacher_id', 'Print teacher ID card', '', '0', 2, '2017-01-11 13:09:39');
INSERT INTO testing.permission VALUES (1, 'view_students', 'View Students', '', '0', 1, '2017-01-11 11:33:08');
INSERT INTO testing.permission VALUES (8, 'print_student_id', 'Print student ID card', '', '0', 1, '2017-01-11 12:10:44');
INSERT INTO testing.permission VALUES (7, 'student_pdf_preview', 'Student PDF Preview', '', '0', 1, '2017-01-11 12:09:40');
INSERT INTO testing.permission VALUES (82, 'view_teacher_attendance', 'View teachers attendance', '', '0', 15, '2017-01-11 16:59:24');
INSERT INTO testing.permission VALUES (15, 'teacher_pdf_preview', 'Teacher PDF Preview', '', '0', 2, '2017-01-11 13:03:34');
INSERT INTO testing.permission VALUES (18, 'view_parents', 'View list of parents', '', '0', 3, '2017-01-11 13:38:34');
INSERT INTO testing.permission VALUES (19, 'add_parent', 'Add parent', '', '0', 3, '2017-01-11 13:39:02');
INSERT INTO testing.permission VALUES (20, 'edit_parent', 'Edit parent', '', '0', 3, '2017-01-11 14:01:34');
INSERT INTO testing.permission VALUES (21, 'delete_parent', 'Delete parent', '', '0', 3, '2017-01-11 14:02:10');
INSERT INTO testing.permission VALUES (22, 'view_parent_info', 'View parent information', '', '0', 3, '2017-01-11 14:03:28');
INSERT INTO testing.permission VALUES (23, 'print_parent_info', 'Print parent information', '', '0', 3, '2017-01-11 14:04:10');
INSERT INTO testing.permission VALUES (24, 'parent_pdf_preview', 'Parent PDF preview', '', '0', 3, '2017-01-11 14:05:03');
INSERT INTO testing.permission VALUES (25, 'parent_pdf_email', 'Send Parent PDF to email', '', '0', 3, '2017-01-11 14:06:12');
INSERT INTO testing.permission VALUES (17, 'teacher_pdf_email', 'Send teacher PDF to email', '', '0', 2, '2017-01-11 13:23:53');
INSERT INTO testing.permission VALUES (9, 'student_pdf_email', 'Send Student PDF to email', '', '0', 1, '2017-01-11 12:15:39');
INSERT INTO testing.permission VALUES (26, 'view_users', 'View users ', '', '0', 4, '2017-01-11 14:34:07');
INSERT INTO testing.permission VALUES (27, 'add_user', 'Add new user', '', '0', 4, '2017-01-11 14:49:28');
INSERT INTO testing.permission VALUES (28, 'edit_user', 'Edit user', '', '0', 4, '2017-01-11 14:58:43');
INSERT INTO testing.permission VALUES (29, 'delete_user', 'Delete user', '', '0', 4, '2017-01-11 15:02:07');
INSERT INTO testing.permission VALUES (30, 'view_user_info', 'View user information', '', '0', 4, '2017-01-11 15:02:49');
INSERT INTO testing.permission VALUES (31, 'print_user_info', 'Print user information', '', '0', 4, '2017-01-11 15:06:13');
INSERT INTO testing.permission VALUES (32, 'print_user_id', 'Print user ID card', '', '0', 4, '2017-01-11 15:10:00');
INSERT INTO testing.permission VALUES (33, 'user_pdf_email', 'Send User PDF to email', '', '0', 4, '2017-01-11 15:12:00');
INSERT INTO testing.permission VALUES (34, 'download_user_info', 'Download User PDF information', '', '0', 4, '2017-01-11 15:14:28');
INSERT INTO testing.permission VALUES (35, 'add_class', 'Add class', '', '0', 5, '2017-01-11 15:15:44');
INSERT INTO testing.permission VALUES (36, 'edit_class', 'Edit class', '', '0', 5, '2017-01-11 15:16:02');
INSERT INTO testing.permission VALUES (37, 'delete_class', 'Delete class', '', '0', 5, '2017-01-11 15:16:21');
INSERT INTO testing.permission VALUES (38, 'add_section', 'Add section', '', '0', 6, '2017-01-11 15:29:19');
INSERT INTO testing.permission VALUES (39, 'print_exam_attendance', 'Print exam attendance', '', '0', 6, '2017-01-11 15:35:03');
INSERT INTO testing.permission VALUES (40, 'add_exam_attendance', 'Add exam attendance', '', '0', 6, '2017-01-11 15:38:03');
INSERT INTO testing.permission VALUES (41, 'view_library_members', 'View library members', '', '0', 7, '2017-01-11 15:40:26');
INSERT INTO testing.permission VALUES (42, 'add_library_member', 'Add library member', '', '0', 7, '2017-01-11 15:41:22');
INSERT INTO testing.permission VALUES (43, 'view_books', 'View books', '', '0', 7, '2017-01-11 15:47:27');
INSERT INTO testing.permission VALUES (44, 'add_book', 'Add book', '', '0', 7, '2017-01-11 15:48:11');
INSERT INTO testing.permission VALUES (45, 'edit_book', 'Edit book', '', '0', 7, '2017-01-11 15:49:34');
INSERT INTO testing.permission VALUES (46, 'delete_book', 'Delete book ', '', '0', 7, '2017-01-11 15:51:12');
INSERT INTO testing.permission VALUES (47, 'search_library_member', 'Search library member', '', '0', 7, '2017-01-11 15:52:34');
INSERT INTO testing.permission VALUES (48, 'view_issue_user', 'View issue user', '', '0', 7, '2017-01-11 15:54:30');
INSERT INTO testing.permission VALUES (49, 'return_book', 'Return a book', '', '0', 7, '2017-01-11 15:55:00');
INSERT INTO testing.permission VALUES (50, 'add_transport_routes', 'Add transport routes', '', '0', 8, '2017-01-11 15:57:06');
INSERT INTO testing.permission VALUES (51, 'add_transport_fee', 'Add transport fee', '', '0', 8, '2017-01-11 15:57:47');
INSERT INTO testing.permission VALUES (52, 'delete_transport', 'Delete transport', '', '0', 8, '2017-01-11 15:58:53');
INSERT INTO testing.permission VALUES (53, 'edit_transport', 'Edit transport', '', '0', 8, '2017-01-11 16:00:25');
INSERT INTO testing.permission VALUES (54, 'add_transport_member', 'Add transport member', '', '0', 8, '2017-01-11 16:01:42');
INSERT INTO testing.permission VALUES (55, 'delete_transport_member', 'Delete transport member', '', '0', 8, '2017-01-11 16:02:11');
INSERT INTO testing.permission VALUES (56, 'edit_transport_member', 'Edit transport member', '', '0', 8, '2017-01-11 16:02:51');
INSERT INTO testing.permission VALUES (57, 'edit_section', 'Edit section', '', '0', 6, '2017-01-11 16:04:24');
INSERT INTO testing.permission VALUES (58, 'delete_section', 'Delete section', '', '0', 6, '2017-01-11 16:04:48');
INSERT INTO testing.permission VALUES (59, 'add_subject', 'Add Subject', '', '0', 9, '2017-01-11 16:15:16');
INSERT INTO testing.permission VALUES (60, 'edit_subject', 'Edit Subject', '', '0', 9, '2017-01-11 16:16:21');
INSERT INTO testing.permission VALUES (61, 'delete_subject', 'Delete Subject', '', '0', 9, '2017-01-11 16:17:05');
INSERT INTO testing.permission VALUES (62, 'add_grade', 'Add Grade', '', '0', 10, '2017-01-11 16:18:17');
INSERT INTO testing.permission VALUES (63, 'edit_grade', 'Edit Grade', '', '0', 10, '2017-01-11 16:18:44');
INSERT INTO testing.permission VALUES (64, 'delete_grade', 'Delete Grade', '', '0', 10, '2017-01-11 16:19:19');
INSERT INTO testing.permission VALUES (65, 'add_class_level', 'Add Class level', '', '0', 11, '2017-01-11 16:23:16');
INSERT INTO testing.permission VALUES (66, 'edit_class_level', 'Edit Class Level', '', '0', 11, '2017-01-11 16:23:53');
INSERT INTO testing.permission VALUES (67, 'delete_class_level', 'Delete Class Level', '', '0', 11, '2017-01-11 16:26:26');
INSERT INTO testing.permission VALUES (68, 'add_exam', 'Add Exam', '', '0', 12, '2017-01-11 16:29:51');
INSERT INTO testing.permission VALUES (69, 'edit_exam', 'Edit Exam', '', '0', 12, '2017-01-11 16:30:27');
INSERT INTO testing.permission VALUES (70, 'delete_exam', 'Delete Exam', '', '0', 12, '2017-01-11 16:30:49');
INSERT INTO testing.permission VALUES (71, 'add_exam_schedule', 'Add Exam Schedule', '', '0', 12, '2017-01-11 16:31:29');
INSERT INTO testing.permission VALUES (72, 'edit_exam_schedule', 'Edit Exam Schedule', '', '0', 12, '2017-01-11 16:32:10');
INSERT INTO testing.permission VALUES (73, 'delete_exam_schedule', 'Delete Exam Schedule', '', '0', 12, '2017-01-11 16:35:15');
INSERT INTO testing.permission VALUES (74, 'view_student_all_report', 'View Student All Reports', '', '0', 12, '2017-01-11 16:38:41');
INSERT INTO testing.permission VALUES (75, 'view_student_single_report', 'View Student Single Report', '', '0', 12, '2017-01-11 16:43:19');
INSERT INTO testing.permission VALUES (76, 'create_official_report', 'Create Official Report', '', '0', 12, '2017-01-11 16:44:17');
INSERT INTO testing.permission VALUES (77, 'view_official_reports', 'View official Reports', '', '0', 12, '2017-01-11 16:44:59');
INSERT INTO testing.permission VALUES (78, 'add_mark', 'Add Mark', '', '0', 13, '2017-01-11 16:45:38');
INSERT INTO testing.permission VALUES (79, 'view_mark', 'View Mark', '', '0', 13, '2017-01-11 16:46:06');
INSERT INTO testing.permission VALUES (80, 'add_routine', 'Add routine', '', '0', 14, '2017-01-11 16:53:54');
INSERT INTO testing.permission VALUES (81, 'view_routine', 'View routine', '', '0', 14, '2017-01-11 16:54:33');
INSERT INTO testing.permission VALUES (83, 'print_teacher_attendance', 'Print teacher attendance', '', '0', 15, '2017-01-11 17:00:23');
INSERT INTO testing.permission VALUES (84, 'add_teacher_attendance', 'Add teachers attendance', '', '0', 15, '2017-01-11 17:01:05');
INSERT INTO testing.permission VALUES (85, 'view_student_attendance', 'View students attendance', '', '0', 15, '2017-01-11 17:01:59');
INSERT INTO testing.permission VALUES (86, 'add_student_attendance', 'Add students attendance', '', '0', 15, '2017-01-11 17:03:01');
INSERT INTO testing.permission VALUES (87, 'view_exam_attendance', 'View exam attendance', '', '0', 15, '2017-01-11 17:03:36');

SELECT pg_catalog.setval('testing.permission_id_seq', 87, true);

INSERT INTO testing.role VALUES (1, 'Admin', '0', '0', '2017-01-11 18:31:24', NULL);
INSERT INTO testing.role VALUES (2, 'Teacher', '0', '0', '2017-01-11 18:31:59', NULL);
INSERT INTO testing.role VALUES (3, 'Accountant', '0', '0', '2017-01-11 18:32:44', NULL);
INSERT INTO testing.role VALUES (4, 'Librarian', '0', '0', '2017-01-11 18:32:54', NULL);
INSERT INTO testing.role VALUES (5, 'Secretary', '0', '0', '2017-01-11 18:33:24', NULL);

SELECT pg_catalog.setval('testing.role_id_seq', 5, true);


--add start_date and end_date column
--18th jan


ALTER TABLE testing.academic_year ADD COLUMN start_date date;
ALTER TABLE testing.academic_year ADD COLUMN end_date date;