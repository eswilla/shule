<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Excel
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */

require_once __DIR__."../../third_party/PHPExcel.php";

class Excel extends PHPExcel {

    public function __construct() {
	parent::__construct();
    }

}

?>