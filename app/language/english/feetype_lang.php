<?php

/* List Language  */
$lang['panel_title'] = "Fee Type";
$lang['add_title'] = "Add a Fee Type";
$lang['slno'] = "#";
$lang['feetype_name'] = "Fee Type";
$lang['feetype_amount'] = "Amount";
$lang['feetype_is_repeative'] = "Is Repeative";
$lang['feetype_is_repeative_yes'] = "Yes";
$lang['feetype_is_repeative_no'] = "No";
$lang['feetype_startdate'] = "Start Date";
$lang['feetype_enddate'] = "End Date";
$lang['feetype_note'] = "Note";
$lang['action'] = "Action";
$lang['feetypecategory']="Fee Category";
$lang['feetpype_select_category']="feetype select category";
$lang['category_title']="Add feetype Category";
$lang['category_note']="Category Note";
$lang['feetype_category']="Feetype Category";
$lang['add_category']="Add Category";
$lang['select_feetype_category']="select feetype category";

$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';

/* Add Language */
$lang['add_feetype'] = 'Add Fee';
$lang['update_feetype'] = 'Update Fee Type';
$lang['feetype_account'] = 'Fee Type Account';
$lang['feetpype_select_account'] = 'Select account number';