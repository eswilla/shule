<?php

/**
 * Description of bankaccount_lang
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
/* List Language  */
$lang['panel_title'] = "Bank Account";
$lang['add_title'] = "Add New Bank Account";
$lang['slno'] = "#";
$lang['menu_bankaccount']='Bank Account';
$lang['bankaccount_name'] = "Bank Name";
$lang['bankaccount_branch'] = "Branch";
$lang['bankaccount_account'] = "Account Number";
$lang['bankaccount_account_name'] = "Account Name";
$lang['bankaccount_currency'] = "Currency";
$lang['bankaccount_swiftcode'] = "Swiftcode";
$lang['bankaccount_note'] = "Note";
$lang['action'] = "Action";

$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';

/* Add Language */
$lang['add_bankaccount'] = 'Add Bank Account';
$lang['update_bankaccount'] = 'Update Bank Account';
$lang['bankaccount_select_currency'] = 'Select Currency';