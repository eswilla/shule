<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of email_lang
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Joshua John
 */

/*
 * Student information
 */
$lang['parent_registered_student']="Hello %s,Your child "
        . "%s has been joined at %s ";
$lang['parent_edit_student']="Hello %s ,Informations of your child %s"
        . " has been modified";
$lang['parent_deleted_student']="Hello %s  Your child %student_name has been removed from %s";


/*
 * Parent information
 */
$lang['add_new_parent']="Hello %s ,You have been added at %s with username: %s and password: %s "
        . "to login to the system";
$lang['edit_parent']="Hello %s Your information has been modified ,Login with username: %s and password :%s";
$lang['delete_parent']="Hello %s You have been removed at %s";

/*
 * Teacher information
 */
$lang['add_new_teacher']="Hello %s ,You have been added as Teacher at %s with username: %s and password: %s "
    . "to login to the system";
$lang['edit_teacher']="Hello %s Your information has been modified ,Login with username: %s and password :%s";
$lang['delete_teacher']="Hello %s You have been removed at %s, Now you do not have any access at %s";

/*
 * User information
 */
$lang['add_new_user']="Hello %s ,You have been added as %s at %s with username: %s and password: %s "
    . "to login to the system";
$lang['edit_user']="Hello %s Your information has been modified ,Login with username: %s and password :%s";
$lang['delete_user']="Hello %s You have been removed at %s, Now you do not have any access at %s";

/*
 * Class information
 */
$lang['add_new_class']="Hello %s ,you have been added as %s of %s";
$lang['edit_class']="Hello %s ,you have been modified as %s of %s";
$lang['delete_class']="Hello %s,this class: %s has been deleted,So now you no longer a teacher of that %s";



/*
 * Section information
 */
$lang['add_new_section']="Hello %s ,you have been added as %s of section %s in the class:- %s and class teacher is %s";
$lang['edit_section']="Hello %s ,section %s in class: %s has been modified as %s of class %s and class teacher is %s";
$lang['delete_section']="Hello %s,this section: %s has been deleted,So now you no longer a teacher of that section  %s";
$lang['class_teacher_notify_edit']="Hello %s ,section %s in your class %s has been modified as %s and section teacher is %s";
$lang['class_teacher_section_deletion']="Hello %s ,section %s has been deleted in your class %s";

/*
 * Subject information
 */
$lang['add_new_subject']="Hello %s, you have been assigned to be a teacher of subject %s";
$lang['edit_subject']="Hello %s ,subject %s has been modified";
$lang['delete_subject']="Hello %s ,subject %s has been deleted";

/*
 * Grade information
 */

$lang['add_new_grade']="Hello %s ,new grading system has been added ,Please login to %s to view all";
$lang['edit_grade']="Hello %s ,grade has been modified please login to %s to view modifications";
$lang['delete_grade']="Hello %s ,Grade has been deleted ";


/*
 * Exam information
 */
$lang['add_new_exam']="Hello %s ,%s exam has been added";
$lang['edit_exam']="Hello %s ,%s exam has been modified so you can login to see changes";
$lang['delete_exam']="Hello %s, %s has been deleted";


/*
 * Exam Schedule information
 */
$lang['add_new_exam_schedule']="Hello %s ,new exam schedule has been added";
$lang['edit_exam_schedule']="Hello %s ,exam schedule has been modified so you can login to see changes";
$lang['delete_exam_schedule']="Hello %s, exam schedule has been deleted";


/*
 * Routine information
 */
$lang['add_new_routine']="Hello %s ,new class routine has been added";
$lang['edit_routine']="Hello %s ,class routine has been modified so you can login to see changes";
$lang['delete_routine']="Hello %s, class routine has been deleted";
