<?php

/* List Language  */
$lang['panel_title'] = "Expense";
$lang['panel_category_title'] = "Financial Category";
$lang['add_title'] = "Add a expense";
$lang['add_category'] = "Add Category";
$lang['slno'] = "#";
$lang['expense_expense'] = "Name";
$lang['expense_date'] = "Date";
$lang['expense_amount'] = "Amount";
$lang['expense_note'] = "Note";
$lang['expense_uname'] = "User";
$lang['expense_total'] = "Total";
$lang['action'] = "Action";
$lang['balance_sheet_report'] = "Balance sheet";
$lang['profit_loss_report'] = "Income Statement";
$lang['account_report'] = "Account Report";

// $lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';

/* Add Language */

$lang['add_expense'] = 'Add Expense';
$lang['category'] = 'Category';
$lang['add_category'] = 'Add Category';
$lang['subcategory'] = 'Name';
$lang['update_expense'] = 'Update Expense';
$lang['select_category'] = 'Select Category';