<?php

/**
 * Description of classlevel_lang
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
$lang['panel_title']='Class Levels';
$lang['add_title']='Add Level';
$lang['name']='Level Name';
$lang['start_date']='Start Date';
$lang['end_date']='End Date';
$lang['span_number']='Span Number';
$lang['note']='Notes';
$lang['action']='Action';
$lang['slno']='#';

$lang['action'] = "Action";
$lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';

/* Add Language */

$lang['add_level'] = 'Add Level';
$lang['result_format'] = 'Result Format';
$lang['update_level'] = 'Update Level';
