<?php

/* List Language  */
$lang['language'] = "Language";
$lang['profile'] = "Profile";
$lang['change_password'] = "Change Password";
$lang['panel_title'] = "Password";
$lang['old_password'] = "Old Password";
$lang['new_password'] = "New Password";
$lang['re_password'] = "Re-Password";
$lang['logout'] = "Log out";
$lang['language'] = "Language";
$lang['view_more'] = "See All Notifications";
$lang['la_fs'] = "You have";
$lang['la_ls'] = "notifications";
$lang['close'] = "Close";
$lang['username']="Username";
$lang['login_hint']="Welcome and login";
$lang['bulk_invoice'] = "Bulk Invoice";
$lang['bank_account'] = "Bank Account";

$lang['Admin'] = "Admin";
$lang['Student'] = "Student";
$lang['Parent'] = "Parents";
$lang['Teacher'] = "Teacher";
$lang['Accountant'] = "Accountant";
$lang['Librarian'] = "Librarian";

$lang['Laboratorian'] = "Laboratory Technician";
$lang['Driver'] = "Driver";
$lang['Guards'] = "Guards";
$lang['Matron'] = "Matron";
$lang['Patron'] = "Patron";
$lang['Cook'] = "Cook";
$lang['Nurse'] = "Nurse";
$lang['Other'] = "Other";

$lang['success'] = "Success!";
$lang['cmessage'] = "Your password is changed.";

/* Menu List */
$lang['menu_add'] = 'Add';
$lang['menu_edit'] = 'Edit';
$lang['menu_view'] = 'View';
$lang['menu_success'] = 'Success';
$lang['menu_dashboard'] = 'Dashboard';
$lang['menu_student'] = 'Students';
$lang['menu_parent'] = 'Parents';
$lang['menu_teacher'] = 'Teachers';
$lang['menu_user'] = 'User';
$lang['menu_classes'] = 'Class';
$lang['menu_section'] = 'Section';
$lang['menu_subject'] = 'Subject';
$lang['menu_grade'] = 'Grade';
$lang['menu_classlevel'] = 'Class Levels';
$lang['menu_exam'] = 'Exam';
$lang['menu_examschedule'] = 'Exam Schedule';
$lang['menu_library'] = 'Library';
$lang['menu_mark'] = 'Mark';
$lang['menu_routine'] = 'Routine';
$lang['menu_attendance'] = 'Attendance';
$lang['menu_member'] = 'Member';
$lang['menu_books'] = 'Books';
$lang['menu_issue'] = 'Issue';
$lang['menu_fine'] = 'Fine';
$lang['menu_profile'] = 'Profile';
$lang['menu_transport'] = 'Transport';
$lang['menu_hostel'] = 'Hostel';
$lang['menu_category'] = 'Category';
$lang['menu_account'] = 'Account';
$lang['menu_feetype'] = 'Fee Type';
$lang['menu_setfee'] = 'Set Fee';
$lang['menu_balance'] = 'Balance';
$lang['menu_paymentsettings'] = 'Payment Settings';
$lang['menu_expense'] = 'Expense';
$lang['menu_notice'] = 'Notice';
$lang['menu_report'] = 'Report';
$lang['menu_setting'] = 'Setting';
$lang['menu_signature'] = 'Signature';


$lang['accountant'] = 'Accountant';
$lang['librarian'] = 'Librarian';
$lang['help']='Help';

$lang['upload'] = "Upload";


/* Start Update Menu */
$lang['menu_sattendance'] = 'Student Attendance';
$lang['menu_tattendance'] = 'Teacher Attendance';
$lang['menu_eattendance'] = 'Exam Attendance';
$lang['menu_promotion'] = 'Promotion';
$lang['menu_media'] = 'Media';
$lang['menu_message'] = 'Message';
$lang['menu_smssettings'] = 'SMS Settings';
$lang['menu_mailandsms'] = 'Mail / SMS';
$lang['menu_mailandsmstemplate'] = 'Mail / SMS Template';
$lang['menu_invoice'] = 'Invoice';
$lang['menu_payment_history']='Payment History';
$lang['menu_combined_report']='Official Report';
$lang['menu_create_report']='Create Report';
$lang['menu_view_report']='View Reports';
/**
 * Inventory navigation
 */
$lang['menu_inventory'] = 'Inventory';
$lang['menu_vendor'] = 'Vendors';
$lang['menu_stockregister'] = 'Stock Register';
$lang['menu_message'] = 'Message';


/* End Update Menu */
