<?php

/**
 * Description of inventory_lang
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
/* List Language  */
$lang['panel_title'] = "List of Items";
$lang['add_title'] = "Add Item";
$lang['slno'] = "#";
$lang['item_name'] = "Name";
$lang['batch_number'] = "Batch Number";
$lang['item_status'] = "Item Status";
$lang['available_quantity'] = "Available Quantity";
$lang['vendor_name'] = "Vendor Name";

$lang['price'] = "Price";
$lang['contact_name']="Contact Person";


$lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';

/* Add Language */
$lang['inventory_title']='Add new Item';
$lang['name'] = 'Name';
$lang['email'] = 'Email';
$lang['phone_number'] = 'Phone';
$lang['batch_number']='Batch Number';
$lang['status'] = 'Item Status';
$lang['contact_person_number'] = 'Contact Person Phone';
$lang['contact_person_name'] = 'Contact Person Name';
$lang['depreciation'] = 'Depreciation';
$lang['add_item'] = 'Add Item';
$lang['quantity']='Quantity';
$lang['comments']='Comments';
$lang['date_purchased']='Date Purchased';
$lang['created_at']='Date Recorded';
$lang['view_vendor_indo']='Click to View Vendor Information';
$lang['update_item']='Update Item';