<?php
/* List Language  */
$lang['panel_title'] = "Wazazi";
$lang['add_title'] = "Ongeza wazazi";
$lang['slno'] = "#";
$lang['parentes_photo'] = "Picha";
$lang['parentes_name'] = "Jina";
$lang['parentes_email'] = "Barua pepe";
$lang['parentes_dob'] = "Tarehe ya Kuazaliwa";
$lang['parentes_sex'] = "Jinsia";
$lang['parentes_religion'] = "Dini";
$lang['parentes_phone'] = "Namba ya Simu";
$lang['parentes_address'] = "Anuani";
$lang['parentes_classes'] = "Darasa";
$lang['parentes_roll'] = "Namba ya Udahili";
$lang['parentes_photo'] = "Picha";
$lang['parentes_username'] = "Jina la Kutumia";
$lang['parentes_password'] = "Neno Siri";
$lang['parentes_select_class'] = "Chagua Darasa";

/* Parentes */
$lang['parentes_guargian_name'] = "Jina la Mlezi";
$lang['parentes_father_name'] = "Jina la Baba";
$lang['parentes_mother_name'] = "Jina la Mama";
$lang['parentes_father_profession'] = "Kazi ya Baba";
$lang['parentes_mother_profession'] = "Kazi ya Mama";


$lang['action'] = "Hatua";
$lang['view'] = 'Tazma';
$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';
$lang['pdf_preview'] = 'Onesha PDF ya Awali';
$lang['print'] = 'Chapa';
$lang["mail"] = "Tuma PDF kwa Barua pepe";

$lang['to'] = 'Kwenda';
$lang['subject'] = 'Kichwa cha Ujumbe';
$lang['message'] = 'Ujumbe';
$lang['send'] = 'Tuma';
$lang['mail_to'] = "Sehemu ya kwenda inahitajika";
$lang['mail_valid'] = "Sehemu ya kwenda lazima iwe na barua pepe halali";
$lang['mail_subject'] = "Sehemu ya somo inahitajika";
$lang['mail_success'] = 'Barua pepe imefanikiwa kutumwa';
$lang['mail_error'] = 'Barua pepe imeshindwa kutuma';

/* Add Language */

$lang['add_parentes'] = 'Ongeza Mzazi';
$lang['update_parentes'] = 'Sasisha Mzazi';

/* ini code starts here*/
$lang['personal_information'] = "Taarifa Binafsi";
$lang['parentess_information'] = "Taarifa za Wazazi";
