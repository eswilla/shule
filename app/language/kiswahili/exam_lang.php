<?php

/* List Language  */
$lang['panel_title'] = "Mtihani";
$lang['add_title'] = "Ongeza Mtihani";
$lang['slno'] = "#";
$lang['exam_name'] = "Jina la mtihani";
$lang['exam_date'] = "Tarehe";
$lang['exam_class']="Darasa";
$lang['exam_section']="Mkondo";
$lang['exam_select_section']="Chagua Mkondo";
$lang['exam_note'] = "Kumbuka";
$lang['exam_all'] = "Yote";


$lang['action'] = "Hatua";
$lang['view'] = 'Tazama';
$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';

/* Add Language */

$lang['add_exam'] = 'Ongeza Mtihani';
$lang['update_exam'] = 'Sasisha Mtihani';

/* List Language  */
$lang['exam'] = "Mitihani";
$lang['add_exam'] = "Ongeza mtihani";
$lang['add_title'] = 'Ongeza Maksi';
$lang['slno'] = "#";
$lang['mark_exam'] = "Mtihani";
$lang['mark_classes'] = "Darasa";
$lang['mark_student'] = "Mwanafunzi";
$lang['mark_subject'] = "Somo";
$lang['mark_photo'] = "Picha";
$lang['mark_name'] = "Jina";
$lang['mark_roll'] = "Namba ya udahili";
$lang['mark_phone'] = "Namba ya Simu ya mkononi";
$lang['mark_dob'] = "Tarehe ya kuzaliwa";
$lang['mark_sex'] = "Jinsia";
$lang['mark_religion'] = "Dini";
$lang['mark_email'] = "Barua pepe";
$lang['mark_address'] = "Anuani";
$lang['mark_username'] = "Jina la kutumia";

$lang['mark_subject'] = "Somo";
$lang['mark_mark'] = "Alama";
$lang['mark_point'] = "Pointi";
$lang['mark_grade'] = "Daraja";


$lang['mark_select_classes'] = "Chagua Darasa";
$lang['mark_select_exam'] = "Chagua Mtihani";
$lang['mark_select_subject'] = "Chagua Somo";
$lang['mark_select_student'] = "Chagua Mwanafunzi";
$lang['mark_success'] = "Fanikiwa";
$lang['personal_information'] = "Taarifa za Mtu";
$lang['mark_information'] = "Taarifa za Alama";
$lang['action'] = "Hatua";
$lang['view'] = 'Tazama';
$lang['pdf_preview'] = 'Onesha PDF ya awali';
$lang['print'] = 'Chapa';
$lang["mail"] = "Tuma Pdf kwa barua pepe";

// /* Add Language */
$lang['add_mark'] = 'Alama';
$lang['add_sub_mark'] = 'Ongeza Alama';

$lang['to'] = 'Kwenda';
$lang['subject'] = 'Kichwa cha ujumbe';
$lang['message'] = 'Ujumbe';
$lang['send'] = 'Tuma';
$lang['mail_to'] = "Eneo hili linahitajika.";
$lang['mail_valid'] = "Eneo hili linahitaji barua pepe liyo kweli";
$lang['mail_subject'] = "Eneo la somo linahitajika.";
$lang['mail_success'] = 'Barua pepe imefanikiwa kutumwa';
$lang['mail_error'] = 'Barua pepe haijatumwa';