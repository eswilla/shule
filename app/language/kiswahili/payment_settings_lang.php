<?php 

$lang['panel_title'] = 'Mpangilio wa Malipo';
$lang['paypal_email'] = 'Barua pepe ya paypal';
$lang['paypal_api_username'] = 'Paypal Api Username';
$lang['paypal_api_password'] = 'Paypal Api Password';
$lang['paypal_api_signature'] = 'Paypal Api Signature';
$lang['paypal_demo'] = 'Paypal Sandbox';
$lang['save'] = 'Save';

$lang['tab_paypal'] = 'Paypal';
$lang['tab_stripe'] = 'Stripe';
$lang['stripe_private_key'] = 'Stripe private key';
$lang['stripe_public_key'] = 'Stripe public key';