<?php

/* List Language  */
$lang['panel_title'] = "Alama";
$lang['add_title'] = 'Ongeza Alama';
$lang['slno'] = "#";
$lang['mark_exam'] = "Mtihani";
$lang['mark_classes'] = "Darasa";
$lang['mark_student'] = "Mwanafunzi";
$lang['mark_subject'] = "Somo";
$lang['mark_photo'] = "Picha";
$lang['mark_name'] = "Jina";
$lang['mark_roll'] = "Namba ya Udahili";
$lang['mark_phone'] = "Namba ya Simu";
$lang['mark_dob'] = "Tarehe ya Kuzaliwa";
$lang['mark_sex'] = "Jinsia";
$lang['mark_religion'] = "Dhehebu";
$lang['mark_email'] = "Barua pepe";
$lang['mark_address'] = "Anuani";
$lang['mark_username'] = "Jina la Mtumiaji";

$lang['mark_subject'] = "Somo";
$lang['mark_section'] = "Mkondo";
$lang['mark_mark'] = "Alama";
$lang['mark_point'] = "Pointi";
$lang['mark_grade'] = "Daraja";


$lang['mark_select_classes'] = "Chagua Darasa";
$lang['mark_select_exam'] = "Chagua Mtihani";
$lang['mark_select_subject'] = "Chagua Somo";
$lang['mark_select_section'] = "Chagua Mkondo";
$lang['mark_select_student'] = "Chagua Mwanafunzi";
$lang['mark_success'] = "Success";
$lang['personal_information'] = "Taarifa Binafsi";
$lang['mark_information'] = "Taarifa za Alama";
$lang['action'] = "Hatua";
$lang['view'] = 'Tazama';
$lang['pdf_preview'] = 'Onesha PDF ya awali';
$lang['print'] = 'Chapa';
$lang["mail"] = "Tuma PDF kwa barua pepe";

// /* Add Language */
$lang['add_mark'] = 'Alama';
$lang['add_sub_mark'] = 'Ongeza Alama';

$lang['to'] = 'Kwenda';
$lang['subject'] = 'Kichwa cha Ujumbe';
$lang['message'] = 'Ujumbe';
$lang['send'] = 'Tuma';
$lang['mail_to'] = "Sehemu ya kwenda inahitajika";
$lang['mail_valid'] = "Sehemu ya kwenda lazima iwe na barua pepe halali";
$lang['mail_subject'] = "Sehemu ya somo inahitajika";
$lang['mail_success'] = 'Barua pepe imefanikiwa kutumwa';
$lang['mail_error'] = 'Barua pepe imeshindwa kutuma';
$lang['report_information']='Ripoti ya Mitihani';