<?php
$lang['panel_title'] = "Dashibodi";
$lang['dashboard_notice'] = "Notisi";
$lang['dashboard_username'] = "Jina la kutumia";
$lang['dashboard_email'] = "Barua pepe";
$lang['dashboard_phone'] = "Namba ya simu";
$lang['dashboard_address'] = "Anuani";
$lang['dashboard_libraryfee'] = "Ada ya maktaba";
$lang['dashboard_transportfee'] = 'Ada ya usafiri';
$lang['dashboard_hostelfee'] = 'Ada ya Bweni';

$lang['dashboard_earning_graph'] = "Grafu ya Kupata";
$lang['dashboard_notpaid'] = "Haijalipwa";
$lang['dashboard_partially_paid'] = "Imelipwa kidogo";
$lang['dashboard_fully_paid'] = "Imelipwa kamili";
$lang['dashboard_cash'] = "Fedha";
$lang['dashboard_cheque'] = "Hundi";
$lang['dashboard_paypal'] = "Paypal";
$lang['dashboard_stripe'] = "Stripe";
$lang['dashboard_sample'] = "Mfano";
$lang['view'] = "Tazama";