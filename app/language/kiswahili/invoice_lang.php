<?php

/* List Language  */
$lang['panel_title'] = "Ankara";
$lang['add_title'] = "Ongeza Ankara";
$lang['slno'] = "#";
$lang['invoice_classesID'] = "Darasa";
$lang['invoice_studentID'] = "Mwanafunzi";
$lang['invoice_select_classes'] = "Chagua Darasa";
$lang['invoice_select_student'] = "Chagua Mwanafunzi";
$lang['invoice_select_paymentmethod'] = "Chagua njia ya malipo";
$lang['invoice_amount'] = "Kiasi";
$lang['invoice_student'] = "Mwanafunzi";
$lang['invoice_date'] = "Tarehe";
$lang['invoice_feetype'] = "Aina ya Ada";
$lang['invoice_invoice'] = "Ankara #";
$lang['invoice_pdate'] = "Tarehe ya malipo";
$lang['invoice_total'] = "Jumla";
$lang['invoice_from'] = "Kutoka";
$lang['invoice_to'] = "Kwenda";
$lang['invoice_create_date'] = "Tarehe ya Kutengeneza";
$lang['invoice_subtotal'] = "Jumla Ndogo";
$lang['invoice_sremove'] = "Ondoa Mwanafunzi";
$lang['invoice_roll'] = "Namba ya udahili";
$lang['invoice_phone'] = "Namba ya Simu ya mkononi";
$lang['invoice_email'] = "Barua pepe";
$lang['invoice_status'] = "Hali ya malipo";
$lang['invoice_number'] = "Mamba ya malipo";
$lang['invoice_notpaid'] = "Haijalipwa";
$lang['invoice_partially_paid'] = "Imelipwa kiasi";
$lang['invoice_fully_paid'] = "Imelipwa Kamili";
$lang['invoice_paymentmethod'] = 'Njia ya malipo';

$lang['invoice_cash'] = "Fedha";
$lang['invoice_cheque'] = "Hundi";
$lang['invoice_paypal'] = "Paypal";
$lang['invoice_stripe'] = "Stripe";


$lang['to'] = 'Kwenda';
$lang['subject'] = 'Kichwa cha ujumbe';
$lang['message'] = 'Ujumbe';
$lang['send'] = 'Tuma';
$lang['mail_to'] = "sehemu ya kutuma inahitajika ";
$lang['mail_valid'] = "Sehemu ya kutuma lazima iwe na barua pepe halali";
$lang['mail_subject'] = "Sehemu ya somo inahitajka";
$lang['mail_success'] = 'Barua pepe imefanikiwa kutumwa';
$lang['mail_error'] = 'Barua pepe imeshindwa kutumwa!';


$lang['pdf_preview'] = 'Onesha PDF kwa awali';
$lang['print'] = 'Chapa';
$lang["mail"] = "Tuma PDF kwa barua pepe";
$lang['payment'] = 'Malipo';
$lang['invoice_due'] = "Deni ";
$lang['invoice_made'] = "Malipo yamefanyika";

$lang['view'] = "Tazama";
$lang['add_invoice'] = 'Ongeza Ankara ';
$lang['update_invoice'] = 'Sasisha Ankara';
$lang['add_payment'] = 'Ongeza Malipo';
$lang['action'] = 'Hatua';
$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';