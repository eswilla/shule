<?php

/* List Language  */
$lang['language'] = "Lugha";
$lang['profile'] = "Ukurasa";
$lang['change_password'] = "Badili Nywila";
$lang['panel_title'] = "Nywila";
$lang['old_password'] = "Nywila ya Zamani";
$lang['new_password'] = "Nywila Mpya";
$lang['re_password'] = "Ingiza Nywila";
$lang['logout'] = "Logua";
$lang['language'] = "Lugha";
$lang['view_more'] = "Angalia Taarifa Zote";
$lang['la_fs'] = "Una";
$lang['la_ls'] = "taarifa";
$lang['close'] = "Funga";
$lang['username']="Jina Tumizi";
$lang['login_hint']="Karibu na ingia ndani";

$lang['Admin'] = "Admini";
$lang['Student'] = "Mwanafunzi";
$lang['Parent'] = "Mzazi";
$lang['Teacher'] = "Mwalimu";
$lang['Accountant'] = "Mhasibu";
$lang['Librarian'] = "Mkutubi";

$lang['Laboratorian'] = "Mtaalamu wa Maabara";
$lang['Driver'] = "Dereva";
$lang['Guards'] = "Walinzi";
$lang['Matron'] = "Matron";
$lang['Patron'] = "Patron";
$lang['Cook'] = "Mpishi";
$lang['Nurse'] = "Nesi";
$lang['Other'] = "Mwengine";

$lang['success'] = "Imefanikiwa!";
$lang['cmessage'] = "Nywila yako imebadilishwa.";

/* Menu List */
$lang['menu_add'] = 'Ongeza';
$lang['menu_edit'] = 'Hariri';
$lang['menu_view'] = 'Angalia';
$lang['menu_success'] = 'Imefanikiwa';
$lang['menu_dashboard'] = 'Dashibodi';
$lang['menu_student'] = 'Wanafunzi';
$lang['menu_parent'] = 'Wazazi';
$lang['menu_teacher'] = 'Walimu';
$lang['menu_user'] = 'Watu';
$lang['menu_classes'] = 'Madarasa';
$lang['menu_section'] = 'Mikondo';
$lang['menu_subject'] = 'Masomo';
$lang['menu_classlevel'] = 'Madaraja ya Madarasa';
$lang['menu_grade'] = 'Madaraja';
$lang['menu_exam'] = 'Mtihani';
$lang['menu_examschedule'] = 'Ratiba ya Mtihani';
$lang['menu_library'] = 'Maktaba';
$lang['menu_mark'] = 'Alama';
$lang['menu_routine'] = 'Ratiba';
$lang['menu_attendance'] = 'Mahudhurio';
$lang['menu_member'] = 'Mwanachama';
$lang['menu_books'] = 'Vitabu';
$lang['menu_issue'] = 'Maswala';
$lang['menu_fine'] = 'Faini';
$lang['menu_profile'] = 'Ukurasa';
$lang['menu_transport'] = 'Usafiri';
$lang['menu_hostel'] = 'Bweni';
$lang['menu_category'] = 'Kipengere';
$lang['menu_account'] = 'Akaunti';
$lang['menu_feetype'] = 'Aina ya Ada';
$lang['menu_setfee'] = 'Weka Ada';
$lang['menu_balance'] = 'Salio';
$lang['menu_paymentsettings'] = 'Mpangilio wa malipo';
$lang['menu_expense'] = 'Matumizi';
$lang['menu_notice'] = 'Notisi';
$lang['menu_report'] = 'Ripoti';
$lang['menu_setting'] = 'Mpangilio';
$lang['menu_signature'] = 'Saini';

$lang['accountant'] = 'Mhasibu';
$lang['librarian'] = 'mkutubi';

$lang['upload'] = "Pakia";
$lang['help']='Msaada';


/* Start Update Menu */
$lang['menu_sattendance'] = 'Mahudhurio ya Mwanafunzi';
$lang['menu_tattendance'] = 'Mahudhurio ya Mwalimu';
$lang['menu_eattendance'] = 'Mahudhurio ya Mtihani';
$lang['menu_promotion'] = 'Promotion';
$lang['menu_media'] = 'Media';
$lang['menu_message'] = 'Meseji';
$lang['menu_smssettings'] = 'Mpangilio wa SMS';
$lang['menu_mailandsms'] = 'Barua pepe / SMS';
$lang['menu_mailandsmstemplate'] = 'Barua pepe / SMS Template';
$lang['menu_invoice'] = 'Ankara';

/* End Update Menu */
/**
 * Inventory navigation
 */
$lang['menu_inventory'] = 'Vitu';
$lang['menu_vendor'] = 'Waujazi';
$lang['menu_stockregister'] = 'Msajiri Vitu';
$lang['menu_message'] = 'Ujumbe';
