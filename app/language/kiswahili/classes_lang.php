<?php

/* List Language  */
$lang['panel_title'] = "Darasa";
$lang['add_title'] = "Ongeza darasa";
$lang['slno'] = "#";
$lang['classes_name'] = "Darasa";
$lang['classes_numeric'] = "Namaba ya darasa";
$lang['teacher_name'] = "Jina la mwalimu";
$lang['classes_note'] = "Notisi";
$lang['action'] = "Hatua";

$lang['classes_select_teacher'] = "Chagua mwalimu";

$lang['view'] = 'Tazama';
$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';

/* Add Language */

$lang['add_class'] = 'Ongeza darasa';
$lang['update_class'] = 'Sasisha Darasa';
$lang['classlevel'] = 'Daraja la Darasa';