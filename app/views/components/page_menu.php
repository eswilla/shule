<div class="container body">
    <div class="main_container">
	<!--    if you uncomment this, delete the below div. This will make scrowbar fixed 
	<div class="col-md-3 left_col menu_fixed">-->
	<div class="col-md-3 left_col">
            <div class="left_col scroll-view">
		<?php $usertype = $this->session->userdata("usertype"); ?>
                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <div class="profile">
                    <div class="user-profile">
                        <img src="<?= base_url("uploads/images/" . $this->session->userdata('photo'));
		?>" class="img-circle profile_img" alt="User Image"/>
                    </div>
                    <div class="profile_info">
                        <span>Welcome,</span>
                        <h2><?= $this->lang->line($this->session->userdata("usertype")) ?></h2>
                    </div>
                </div>
                <!-- /menu profile quick info -->

                <br/>

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu" style="z-index: 6000;">
                    <div class="menu_section">
                        <h3>&nbsp;&nbsp;</h3>
                        <ul class="nav side-menu">
                            <li><?php
				echo anchor('dashboard/index', '<i class="fa fa-home"></i> <span>' . $this->lang->line('menu_dashboard') . '</span>');
				?>

                            </li>
                            <li><a><i class="fa fa-edit"></i> <?= $this->lang->line('menu_user'); ?> <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
				    <?php
				    if ($usertype == "Admin" || $usertype == "Teacher") {
					echo '<li>';
					echo anchor('student/index', '<i class="fa icon-student"></i><span>' . $this->lang->line('menu_student') . '</span>');
					echo '</li>';
				    }
				    ?>

				    <?php
				    if ($usertype == "Admin") {
					echo '<li>';
					echo anchor('parentes/index', '<i class="fa fa-user"></i><span>' . $this->lang->line('menu_parent') . '</span>');
					echo '</li>';
				    }
				    ?>


				    <?php
				    if ($usertype) {
					echo '<li>';
					echo anchor('teacher/index', '<i class="fa icon-teacher"></i><span>' . $this->lang->line('menu_teacher') . '</span>');
					echo '</li>';
				    }
				    ?>

				    <?php
				    if ($usertype == "Admin") {
					echo '<li>';
					echo anchor('user/index', '<i class="fa fa-users"></i><span>' . $this->lang->line('menu_user') . '</span>');
					echo '</li>';
				    }
				    ?>
				    <?php
				    /**
				     * This menu will load all type of user groups/roles (predifines/to be defined)
				     */
				    //echo '<li>';
				    //echo anchor('user/index', '<i class="fa fa-users"></i><span>' . $this->lang->line('menu_user') . '</span>');
				    //echo '</li>';
				    ?>

                                </ul>
                            </li>


                            <li><?php
				    if ($usertype == "Admin") {
					echo '<li>';
					echo anchor('classes/index', '<i class="fa fa-sitemap"></i><span>' . $this->lang->line('menu_classes') . '</span>');
					echo '</li>';
				    }
				    ?>
                            </li>
                            <li>
				<?php
				if ($usertype == "Admin") {
				    echo '<li>';
				    echo anchor('section/index', '<i class="fa fa-star"></i><span>' . $this->lang->line('menu_section') . '</span>');
				    echo '</li>';
				}
				?>

                            </li>
			    <?php
			    if ($usertype == "Admin" || $usertype == "Student" || $usertype == "Parent" || $usertype == "Librarian" || $usertype == "Teacher") {
				echo '<li>';
				echo anchor('subject/index', '<i class="fa icon-subject"></i><span>' . $this->lang->line('menu_subject') . '</span>');
				echo '</li>';
			    }
			    ?>

			    <?php
			    if ($usertype == "Admin") {
				echo '<li>';
				echo anchor('grade/index', '<i class="fa fa-signal"></i><span>' . $this->lang->line('menu_grade') . '</span>');
				echo '</li>';
				echo '<li>';
				echo anchor('classlevel/index', '<i class="fa fa-signal"></i><span>' . $this->lang->line('menu_classlevel') . '</span>');
				echo '</li>';
			    }
			    ?>


			    <?php if ($usertype == "Admin") { ?>
    			    <li>
    				<a>
    				    <i class="fa icon-exam"></i>
    				    <span><?= $this->lang->line('menu_exam'); ?></span>
    				    <span class=" fa fa-angle-down"></span>
    				</a>
    				<ul class="nav child_menu">
    				    <li>
					    <?php echo anchor('exam/index', '<i class="fa fa-pencil"></i><span>' . $this->lang->line('menu_exam') . '</span>'); ?>
    				    </li>
    				    <li>
					    <?php echo anchor('examschedule/index', '<i class="fa fa-puzzle-piece"></i><span>' . $this->lang->line('menu_examschedule') . '</span>'); ?>
    				    </li>
    				    <li>
					    <?php echo anchor('exam/report', '<i class="fa fa-clipboard"></i><span>' . $this->lang->line('menu_report') . '</span>'); ?>
    				    </li>
    				    <li class="active"><a>Official Reports<span class="fa fa-chevron-down"></span></a>
    					<ul class="nav child_menu" style="display: block;">
    					    <li class="sub_menu">
						    <?php echo anchor('exam/combined', '<i class="fa fa-clipboard"></i><span>' . $this->lang->line('menu_create_report') . '</span>'); ?>
    					    </li>

    					    <li>
						    <?php echo anchor('examreport/index', '<i class="fa fa-clipboard"></i><span>' . $this->lang->line('menu_view_report') . '</span>'); ?>
    					    </li>
    					</ul>
    				    </li>
    				    <li>

    				    </li>
    				</ul>
    			    </li>
			    <?php } ?>


			    <?php
			    if ($usertype == "Student" || $usertype == "Parent" || $usertype == "Teacher") {
				echo '<li>';
				echo anchor('examschedule/index', '<i class="fa fa-puzzle-piece"></i><span>' . $this->lang->line('menu_examschedule') . '</span>');
				echo '</li>';
			    }
			    ?>

			    <?php
			    if ($usertype == "Admin" || $usertype == "Teacher") {
				echo '<li>';
				echo anchor('mark/index', '<i class="fa fa-flask"></i><span>' . $this->lang->line('menu_mark') . '</span>');
				echo '</li>';
			    }
			    ?>

			    <?php
			    if ($usertype == "Student") {
				echo '<li>';
				echo anchor('mark/view', '<i class="fa fa-flask"></i><span>' . $this->lang->line('menu_mark') . '</span>');
				echo '</li>';
			    }
			    ?>

			    <?php
			    if ($usertype == "Parent") {
				echo '<li>';
				echo anchor('mark/index', '<i class="fa fa-flask"></i><span>' . $this->lang->line('menu_mark') . '</span>');
				echo '</li>';
			    }
			    ?>

			    <?php
			    if ($usertype == "Admin" || $usertype == "Student" || $usertype == "Parent" || $usertype == "Teacher") {
				echo '<li>';
				echo anchor('routine/index', '<i class="fa icon-routine"></i><span>' . $this->lang->line('menu_routine') . '</span>');
				echo '</li>';
			    }
			    ?>

			    <?php if ($usertype == "Admin" || $usertype == "Teacher") { ?>
    			    <li id="tattendance">
    				<a>
    				    <i class="fa icon-attendance"></i>
    				    <span><?= $this->lang->line('menu_attendance'); ?> </span>
    				    <span class=" fa fa-angle-down"></span>
    				</a>
    				<ul class="nav child_menu">
    				    <li>
					    <?php echo anchor('sattendance/index', '<i class="fa icon-sattendance"></i><span>' . $this->lang->line('menu_sattendance') . '</span>'); ?>
    				    </li>
					<?php if ($usertype == "Teacher") { ?>
					    <li>
						<?php echo anchor('tattendance/view', '<i class="fa icon-tattendance"></i><span>' . $this->lang->line('menu_tattendance') . '</span>'); ?>
					    </li>
					<?php } else { ?>
					    <li>
						<?php echo anchor('tattendance/index', '<i class="fa icon-tattendance"></i><span>' . $this->lang->line('menu_tattendance') . '</span>'); ?>
					    </li>
					<?php } ?>
    				    <li>
					    <?php echo anchor('eattendance/index', '<i class="fa icon-eattendance"></i><span>' . $this->lang->line('menu_eattendance') . '</span>'); ?>
    				    </li>
    				</ul>
    			    </li>
			    <?php } ?>

			    <?php
			    if ($usertype == "Student" || $usertype == "Parent") {
				echo '<li id="sattendance">';
				echo anchor('sattendance/view', '<i class="fa icon-sattendance"></i><span>' . $this->lang->line('menu_attendance') . '</span>');
				echo '</li>';
			    }
			    ?>

			    <?php if ($usertype == "Admin") { ?>
    			    <li>
    				<a>
    				    <i class="fa icon-library"></i>
    				    <span><?= $this->lang->line('menu_library'); ?></span>
    				    <span class=" fa fa-angle-down"></span>
    				</a>
    				<ul class="nav child_menu">
    				    <li>
					    <?php echo anchor('lmember/index', '<i class="fa icon-member"></i><span>' . $this->lang->line('menu_member') . '</span>'); ?>
    				    </li>
    				    <li>
					    <?php echo anchor('book/index', '<i class="fa icon-lbooks"></i><span>' . $this->lang->line('menu_books') . '</span>'); ?>
    				    </li>
    				    <li>
					    <?php echo anchor('issue/index', '<i class="fa icon-issue"></i><span>' . $this->lang->line('menu_issue') . '</span>'); ?>
    				    </li>
    				    <li>
					    <?php echo anchor('issue/fine', '<i class="fa icon-fine"></i><span>' . $this->lang->line('menu_fine') . '</span>'); ?>
    				    </li>
    				</ul>
    			    </li>
			    <?php } ?>

			    <?php
			    if ($usertype == "Librarian") {
				echo '<li>';
				echo anchor('lmember/index', '<i class="fa icon-member"></i><span>' . $this->lang->line('menu_member') . '</span>');
				echo '</li>';
			    }
			    ?>

			    <?php
			    if ($usertype == "Librarian" || $usertype == "Parent" || $usertype == "Teacher") {
				echo '<li>';
				echo anchor('book/index', '<i class="fa icon-lbooks"></i><span>' . $this->lang->line('menu_books') . '</span>');
				echo '</li>';
			    }
			    ?>

			    <?php
			    if ($usertype == "Librarian" || $usertype == "Parent") {
				echo '<li>';
				echo anchor('issue/index', '<i class="fa icon-issue"></i><span>' . $this->lang->line('menu_issue') . '</span>');
				echo '</li>';
			    }
			    ?>

			    <?php
			    if ($usertype == "Librarian") {
				echo '<li>';
				echo anchor('issue/fine', '<i class="fa icon-fine"></i><span>' . $this->lang->line('menu_fine') . '</span>');
				echo '</li>';
			    }
			    ?>

			    <?php if ($usertype == "Student") { ?>
    			    <li>
    				<a>
    				    <i class="fa icon-exam"></i>
    				    <span><?= $this->lang->line('menu_library'); ?></span>
    				    <span class=" fa fa-angle-down"></span>
    				</a>
    				<ul class="nav child_menu">

    				    <li>
					    <?php echo anchor('book/index', '<i class="fa icon-lbooks"></i><span>' . $this->lang->line('menu_books') . '</span>'); ?>
    				    </li>
    				    <li>
					    <?php echo anchor('issue/index', '<i class="fa icon-issue"></i><span>' . $this->lang->line('menu_issue') . '</span>'); ?>
    				    </li>

    				    <li>
					    <?php echo anchor('lmember/view', '<i class="fa fa-briefcase"></i><span>' . $this->lang->line('menu_profile') . '</span>'); ?>
    				    </li>

    				</ul>
    			    </li>
			    <?php } ?>

			    <?php if ($usertype == "Admin" || $usertype == "Accountant") { ?>
    			    <li>
    				<a>
    				    <i class="fa icon-bus"></i>
    				    <span><?= $this->lang->line('menu_transport'); ?></span>
    				    <span class=" fa fa-angle-down"></span>
    				</a>
    				<ul class="nav child_menu">
    				    <li>
					    <?php echo anchor('transport/index', '<i class="fa icon-sbus"></i><span>' . $this->lang->line('menu_transport') . '</span>'); ?>
    				    </li>
    				    <li>
					    <?php echo anchor('tmember/index', '<i class="fa icon-member"></i><span>' . $this->lang->line('menu_member') . '</span>'); ?>
    				    </li>

    				</ul>
    			    </li>
			    <?php } ?>

			    <?php if ($usertype == "Student") { ?>
    			    <li>
    				<a>
    				    <i class="fa icon-bus"></i>
    				    <span><?= $this->lang->line('menu_transport'); ?></span>
    				    <span class=" fa fa-angle-down"></span>
    				</a>
    				<ul class="nav child_menu">
    				    <li>
					    <?php echo anchor('transport/index', '<i class="fa icon-sbus"></i><span>' . $this->lang->line('menu_transport') . '</span>'); ?>
    				    </li>
    				    <li>
					    <?php echo anchor('tmember/view', '<i class="fa fa-briefcase"></i><span>' . $this->lang->line('menu_profile') . '</span>'); ?>
    				    </li>

    				</ul>
    			    </li>
			    <?php } ?>

			    <?php
			    if ($usertype == "Parent" || $usertype == "Teacher" || $usertype == "Librarian") {
				echo '<li>';
				echo anchor('transport/index', '<i class="fa icon-sbus"></i><span>' . $this->lang->line('menu_transport') . '</span>');
				echo '</li>';
			    }
			    ?>

			    <?php if ($usertype) { ?>
    			    <li>
    				<a>
    				    <i class="fa icon-hhostel"></i>
    				    <span><?= $this->lang->line('menu_hostel'); ?></span>
    				    <span class=" fa fa-angle-down"></span>
    				</a>
    				<ul class="nav child_menu">
    				    <li>
					    <?php echo anchor('hostel/index', '<i class="fa icon-hostel"></i><span>' . $this->lang->line('menu_hostel') . '</span>'); ?>
    				    </li>
    				    <li>
					    <?php echo anchor('category/index', '<i class="fa fa-leaf"></i><span>' . $this->lang->line('menu_category') . '</span>'); ?>
    				    </li>
    				    <li>
					    <?php
					    if ($usertype == "Admin" || $usertype == "Accountant") {
						echo anchor('hmember/index', '<i class="fa icon-member"></i><span>' . $this->lang->line('menu_member') . '</span>');
					    }
					    ?>
    				    </li>
    				    <li>
					    <?php
					    if ($usertype == "Student") {
						echo anchor('hmember/view', '<i class="fa fa-briefcase"></i><span>' . $this->lang->line('menu_profile') . '</span>');
					    }
					    ?>
    				    </li>
    				</ul>
    			    </li>
			    <?php } ?>

			    <?php if ($usertype == "Admin" || $usertype == "accountant") { ?>
    			    <li>
    				<a>
    				    <i class="fa icon-account"></i>
    				    <span><?= $this->lang->line('menu_account'); ?></span>
    				    <span class=" fa fa-angle-down"></span>
    				</a>
    				<ul class="nav child_menu">

    				    <li>
					    <?php echo anchor('bankaccount/index', '<i class="fa icon-feetype"></i><span>' . $this->lang->line('bank_account') . '</span>'); ?>
    				    </li>
    				    <li>
					    <?php echo anchor('feetype/index', '<i class="fa icon-feetype"></i><span>' . $this->lang->line('menu_feetype') . '</span>'); ?>
    				    </li>
    				    <li>
					    <?php echo anchor('invoice/index', '<i class="fa icon-invoice"></i><span>' . $this->lang->line('menu_invoice') . '</span>'); ?>
    				    </li>
    				    <li>
					    <?php echo anchor('invoice/bulk_index', '<i class="fa icon-invoice"></i><span>' . $this->lang->line('bulk_invoice') . '</span>'); ?>
    				    </li>

    				    <li>
					    <?php echo anchor('invoice/payment_history', '<i class="fa icon-payment"></i><span>' . $this->lang->line('menu_payment_history') . '</span>'); ?>
    				    </li>
    				    <li>
					    <?php echo anchor('balance/index', '<i class="fa icon-payment"></i><span>' . $this->lang->line('menu_balance') . '</span>'); ?>
    				    </li>
    				    <li>
					    <?php echo anchor('expense/index', '<i class="fa icon-expense"></i><span>' . $this->lang->line('menu_expense') . '</span>'); ?>
    				    </li>
    				    <li class="active"><a>Account Reports<span class="fa fa-chevron-down"></span></a>
    					<ul class="nav child_menu" style="display: block;">
    					    <li class="sub_menu">
						    <?php echo anchor('expense/financial_index/1', '<i class="fa fa-clipboard"></i><span>Profit & Loss</span>'); ?>
    					    </li>

    					    <li>
						    <?php echo anchor('expense/financial_index/2', '<i class="fa fa-clipboard"></i><span>Balance Sheet</span>'); ?>
    					    </li>
    					</ul>
    				    </li>

    				    <!--                                        <li>
					<?php //echo anchor('payment_settings/index', '<i class="fa icon-paymentsettings"></i><span>' . $this->lang->line('menu_paymentsettings') . '</span>');    ?>
    									    </li>-->
    				</ul>
    			    </li>
			    <?php } ?>

			    <?php if ($usertype == "Accountant") { ?>
    			    <li>
    				<a>
    				    <i class="fa icon-account"></i>
    				    <span><?= $this->lang->line('menu_account'); ?></span>
    				    <span class=" fa fa-angle-down"></span>
    				</a>
    				<ul class="nav child_menu">
    				    <li>
					    <?php echo anchor('bankaccount/index', '<i class="fa icon-feetype"></i><span>' . $this->lang->line('bank_account') . '</span>'); ?>
    				    </li>
    				    <li>
					    <?php echo anchor('feetype/index', '<i class="fa icon-feetype"></i><span>' . $this->lang->line('menu_feetype') . '</span>'); ?>
    				    </li>
    				    <li>
					    <?php echo anchor('invoice/index', '<i class="fa icon-invoice"></i><span>' . $this->lang->line('menu_invoice') . '</span>'); ?>
    				    </li>
    				    <li>
					    <?php echo anchor('invoice/bulk_index', '<i class="fa icon-invoice"></i><span>' . $this->lang->line('bulk_invoice') . '</span>'); ?>
    				    </li>

    				    <li>
					    <?php echo anchor('invoice/payment_history', '<i class="fa icon-payment"></i><span>' . $this->lang->line('menu_payment_history') . '</span>'); ?>
    				    </li>
    				    <li>
					    <?php echo anchor('balance/index', '<i class="fa icon-payment"></i><span>' . $this->lang->line('menu_balance') . '</span>'); ?>
    				    </li>
    				    <li>
					    <?php echo anchor('expense/index', '<i class="fa icon-expense"></i><span>' . $this->lang->line('menu_expense') . '</span>'); ?>
    				    </li>
    				    <li class="active"><a>Account Reports<span class="fa fa-chevron-down"></span></a>
    					<ul class="nav child_menu" style="display: block;">
    					    <li class="sub_menu">
						    <?php echo anchor('expense/financial_index/1', '<i class="fa fa-clipboard"></i><span>Profit & Loss</span>'); ?>
    					    </li>

    					    <li>
						    <?php echo anchor('expense/financial_index/2', '<i class="fa fa-clipboard"></i><span>Balance Sheet</span>'); ?>
    					    </li>
    					</ul>
    				    </li>

    				    <!--                                        <li>
					<?php //echo anchor('payment_settings/index', '<i class="fa icon-paymentsettings"></i><span>' . $this->lang->line('menu_paymentsettings') . '</span>');    ?>
    									    </li>-->
    				</ul>
    			    </li>

			    <?php } ?>

			    <?php if ($usertype == "Student" || $usertype == "Parent") { ?>
    			    <li>
				    <?php echo anchor('invoice/index', '<i class="fa icon-invoice"></i><span>' . $this->lang->line('menu_invoice') . '</span>'); ?>
    			    </li>
    			    <li>
				    <?php echo anchor('invoice/payment_history', '<i class="fa icon-payment"></i><span>' . $this->lang->line('menu_payment_history') . '</span>'); ?>
    			    </li>
			    <?php } ?>

			    <?php
			    if ($usertype == "Admin" || $usertype == "Teacher") {
				echo '<li>';
				echo anchor('promotion/index', '<i class="fa icon-promotion"></i><span>' . $this->lang->line('menu_promotion') . '</span>');
				echo '</li>';
			    }
			    ?>

			    <?php
			    if ($usertype == "Admin" || $usertype == "Student" || $usertype == "Teacher") {
				echo '<li>';
				echo anchor('media/index', '<i class="fa fa-film"></i><span>' . $this->lang->line('menu_media') . '</span>');
				echo '</li>';
			    }
			    ?>

			    <?php if ($usertype == "Admin") { ?>
    			    <li>
    				<a>
    				    <i class="fa icon-mailandsmstop"></i>
    				    <span><?= $this->lang->line('menu_mailandsms'); ?></span>
    				    <span class=" fa fa-angle-down"></span>
    				</a>
    				<ul class="nav child_menu">
    				    <li>
					    <?php echo anchor('mailandsmstemplate/index', '<i class="fa icon-template"></i><span>' . $this->lang->line('menu_mailandsmstemplate') . '</span>'); ?>
    				    </li>
    				    <li>
					    <?php echo anchor('mailandsms/index', '<i class="fa icon-mailandsms"></i><span>' . $this->lang->line('menu_mailandsms') . '</span>'); ?>
    				    </li>
    				    <li>
					    <?php echo anchor('mailandsms/parent_sms', '<i class="fa fa-envelope"></i><span>SMS Parents</span>'); ?>
    				    </li>
    				    <li>
					    <?php echo anchor('smssettings/index', '<i class="fa fa-wrench"></i><span>' . $this->lang->line('menu_smssettings') . '</span>'); ?>
    				    </li>
    				</ul>
    			    </li>
			    <?php } ?>

			    <?php if ($usertype) { ?>
    			    <li>
    				<a>
    				    <i class="fa fa-envelope"></i>
    				    <span><?= $this->lang->line('menu_message') ?></span>
    				    <span class=" fa fa-angle-down"></span>
    				</a>        
    				<ul class="nav child_menu">

    				    <li>
					    <?php echo anchor('message/index', '<i class="fa fa-envelope"></i><span>SMS Staff</span>'); ?>
    				    </li>
    				</ul>       






    			    </li>
			    <?php } ?>


			    <?php
			    if ($usertype) {
				echo '<li>';
				echo anchor('notice/index', '<i class="fa fa-calendar"></i><span>' . $this->lang->line('menu_notice') . '</span>');
				echo '</li>';
			    }
			    if ($this->session->userdata("usertype") == "Admin") {
				?>
				<?php if ($usertype == "Admin") { ?>
				    <li>
					<a>
					    <i class="fa icon-exam"></i>
					    <span><?= $this->lang->line('menu_inventory'); ?></span>
					    <span class=" fa fa-angle-down"></span>
					</a>
					<ul class="nav child_menu">
					    <li>
						<?php echo anchor('inventory/index', '<i class="fa fa-puzzle-piece"></i><span>' . $this->lang->line('menu_stockregister') . '</span>'); ?>
					    </li>
					    <li>
						<?php echo anchor('vendor/index', '<i class="fa fa-pencil"></i><span>' . $this->lang->line('menu_vendor') . '</span>'); ?>
					    </li>

					</ul>
				    </li>
				<?php } ?>
    			    <li>
    				<a>
    				    <i class="fa icon-exam"></i>
    				    <span><?= $this->lang->line('menu_report'); ?></span>
    				    <span class=" fa fa-angle-down"></span>
    				</a>
    				<ul class="nav child_menu">
    				    <li>
					    <?= anchor('report/index', '<i class="fa fa-clipboard"></i><span>' . $this->lang->line('menu_report') . '</span>'); ?>
    				    </li>

    				</ul>
    			    </li>
				<?php
			    }
			    if ($this->session->userdata("usertype") == "Admin") {
				echo '<li>';
				echo anchor('setting/index', '<i class="fa fa-gears"></i><span>' . $this->lang->line('menu_setting') . '</span>');
				echo '</li>';
			    }
			    if ($this->session->userdata("usertype") == "Admin" || $this->session->userdata("usertype") == "Teacher") {
				echo '<li>';
				echo anchor('setting/signature', '<i class="fa fa-pencil-square-o"></i><span>' . $this->lang->line('menu_signature') . '</span>');
				echo '</li>';
			    }
			    ?>


			    <?php
			    if ($this->session->userdata("usertype") == "Teacher") {
				echo '<li>';
				echo anchor('help/teacher_help', '<i  class="fa fa-info-circle"></i><span>' . $this->lang->line('help') . '</span>', ' target="_blank" ');
				echo '</li>';
			    }
			    ?>

			    <?php
			    if ($this->session->userdata("usertype") == "Admin") {
				echo '<li>';
				echo anchor('help', '<i  class="fa fa-info-circle"></i><span>' . $this->lang->line('help') . '</span>', ' target="_blank" ');
				echo '</li>';
			    }
			    ?>

			    <?php
			    if ($this->session->userdata("usertype") == "Student") {
				echo '<li>';
				echo anchor('help/student_help', '<i  class="fa fa-info-circle"></i><span>' . $this->lang->line('help') . '</span>', ' target="_blank" ');
				echo '</li>';
			    }
			    ?>

			    <?php
			    if ($this->session->userdata("usertype") == "Accountant") {
				echo '<li>';
				echo anchor('help/account_help', '<i  class="fa fa-info-circle"></i><span>' . $this->lang->line('help') . '</span>', ' target="_blank" ');
				echo '</li>';
			    }
			    ?>

			    <?php
			    if ($this->session->userdata("usertype") == "Librarian") {
				echo '<li>';
				echo anchor('help/library_help', '<i  class="fa fa-info-circle"></i><span>' . $this->lang->line('help') . '</span>', ' target="_blank" ');
				echo '</li>';
			    }
			    ?>
			    <?php
			    if ($this->session->userdata("usertype") == "Parent") {
				echo '<li>';
				echo anchor('help/parent_help', '<i  class="fa fa-info-circle"></i><span>' . $this->lang->line('help') . '</span>', ' target="_blank" ');
				echo '</li>';
			    }
			    ?>
                        </ul>
                    </div>
                </div>


                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" href="<?= base_url("profile/index") ?>" data-placement="top" title=""
                       data-original-title="<?= $this->lang->line("profile") ?>">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="" data-original-title="FullScreen">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Lock">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" href="<?= base_url("signin/signout") ?>" data-placement="top" title=""
                       data-original-title="<?= $this->lang->line("logout") ?>">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                </div>
            </div>
        </div>
        <div class="top_nav navbar-fixed-top">

            <div class="nav_menu">
                <nav class="" role="navigation">
                    <div class="nav toggle">
                        <a class="navbar-btn sidebar-toggle" id="menu_toggle" data-toggle="offcanvas" role="button">
                            <i class="fa fa-bars"></i>
                        </a>
                    </div>




                    <ul class="nav navbar-nav navbar-right">
			<!--			<div class="col-md-2 col-sm-2" style="margin-top: 2%;">
						    <select class="form-control">
							<option>Choose Academic Year</option>
							<option>2016 / 2017</option>
							<option>2017 / 2018</option>
							<option>2018 / 2019</option>
			
						    </select>
						</div>-->

                        <li style="right:5%; margin-left:8%">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown"
                               aria-expanded="false">
                                <img src="<?= base_url("uploads/images/" . $this->session->userdata('photo'));
			    ?>" class="img-circle responsive-img" alt="User Image"/>
				     <?php
				     $name = $this->session->userdata('name');
				     if (strlen($name) > 15) {
					 echo substr($name, 0, 15) . "..";
				     } else {
					 echo $name;
				     }
				     ?>
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li><a href="<?= base_url("profile/index") ?>">
                                        <i class="fa fa-briefcase"></i>
					<?= $this->lang->line("profile") ?>
                                    </a>
                                </li>
                                <li>

                                    <a href="<?= base_url("signin/cpassword") ?>">
                                        <i class="fa fa-lock"></i>

                                        <span><?= $this->lang->line("change_password") ?></span>
                                    </a>


                                </li>
				<!--                                <li>
								    <a href="<?= base_url("help") ?>" target="_blank">Help</a>
								</li>-->
                                <li>
                                    <a href="<?= base_url("signin/signout") ?>">
                                        <div><i class="fa fa-sign-out pull-right"></i></div>
					<?= $this->lang->line("logout") ?>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li role="presentation" class="dropdown notifications-menu">
                            <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown"
                               aria-expanded="false">
                                <span> <?= $this->lang->line("language") ?> </span>
				&nbsp;<img class="language-img" src="<?php
					$image = $this->session->userdata('lang');
					echo base_url('uploads/language_image/' . $image . '.png');
					?>"
					   />
                                <i class="fa fa-angle-down"></i>

                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <!-- inner menu: contains the actual data -->
                                    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: auto;"><ul class="menu" style="overflow: hidden;width: 100%;height: auto;">
                                            <li class="language" id="english">
                                                <a href="<?php echo base_url('language/index/english') ?>">
                                                    <div class="pull-left">
                                                        <img src="<?php echo base_url('uploads/language_image/english.png'); ?>">
                                                    </div>
                                                    <h4>
                                                        English
							<?php if ($image == 'english') echo " <i class='fa fa-check'></i>"; ?>                                            </h4>
                                                </a>
                                            </li>
                                            <li class="language" id="french">
                                                <a href="<?php echo base_url('language/index/kiswahili') ?>">
                                                    <div class="pull-left">
                                                        <img src="<?php echo base_url('uploads/language_image/kiswahili.png'); ?>">
                                                    </div>
                                                    <h4>
                                                        Kiswahili
							<?php if ($image == 'kiswahili') echo " <i class='fa fa-check'></i>"; ?>
                                                    </h4>
                                                </a>
                                            </li>
                                            <li class="language" id="french">
                                                <a href="<?php echo base_url('language/index/french') ?>">
                                                    <div class="pull-left">
                                                        <img src="<?php echo base_url('uploads/language_image/french.png'); ?>">
                                                    </div>
                                                    <h4>
                                                        French
							<?php if ($image == 'french') echo " <i class='fa fa-check'></i>"; ?>
                                                    </h4>
                                                </a>
                                            </li>



                                        </ul><div class="slimScrollBar" style="width: 3px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 0px; z-index: 99; right: 1px; height: auto; background: rgb(0, 0, 0);"></div>
                                        <div class="slimScrollRail" style="width: 3px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 0px; opacity: 0.2; z-index: 90; right: 1px; background: rgb(51, 51, 51);"></div></div>
                                </li>
                                <li class="footer"></li>
                            </ul>
			</li>
                        <!-- Messages: style can be found in dropdown.less-->
                        <li class="dropdown messages-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-bell-o"></i>
				<?php
				$alCounter = 0;
				if (count($alert) > 0) {
				    $alCounter += count($alert);
				    echo "<span class='label label-danger'>";
				    echo "<lable class='alert-image'>" . count($alert) . "</lable>";
				    echo "</span>";
				}
				?>
                            </a>
			    <?php
			    if (count($alert) > 0) {
				$pdate = date("Y-m-d H:i:s");
				$title = '';
				$discription = '';
				$i = 1;
				echo "<ul class='dropdown-menu'>";
				echo "<li class='header'>";
				echo $this->lang->line("la_fs") . " " . $alCounter . " " . $this->lang->line("la_ls");
				echo '</li>';
				echo '<li>';
				echo "<ul class='menu'>";
				foreach ($alert as $alt) {
				    if (strlen($alt->title) > 16) {
					$title = substr($alt->title, 0, 16) . "..";
				    } else {
					$title = $alt->title;
				    }
				    if (strlen($alt->notice) > 30) {
					$discription = substr($alt->notice, 0, 30) . "..";
				    } else {
					$discription = $alt->notice;
				    }
				    echo '<li>';
				    echo "<a href=" . base_url("notice/view") . "/" . $alt->noticeID . ">";
				    echo "<div class='pull-left'>";
				    echo "<img class='img-circle' src='" . base_url('uploads/images/' . $siteinfos->photo) . "'>";
				    echo '</div>';
				    echo '<h4>';
				    echo strip_tags($title);
				    echo "<small><i class='fa fa-clock-o'></i> ";
				    $dafstdate = $alt->create_date;
				    $first_date = new DateTime($alt->create_date);
				    $second_date = new DateTime($pdate);
				    $difference = $first_date->diff($second_date);
				    if ($difference->y >= 1) {
					$format = 'Y-m-d H:i:s';
					$date = DateTime::createFromFormat($format, $dafstdate);
					echo $date->format('M d Y');
				    } elseif ($difference->m == 1 && $difference->m != 0) {
					echo $difference->m . " month";
				    } elseif ($difference->m <= 12 && $difference->m != 0) {
					echo $difference->m . " months";
				    } elseif ($difference->d == 1 && $difference->d != 0) {
					echo "Yesterday";
				    } elseif ($difference->d <= 31 && $difference->d != 0) {
					echo $difference->d . " days";
				    } else if ($difference->h == 1 && $difference->h != 0) {
					echo $difference->h . " hr";
				    } else if ($difference->h <= 24 && $difference->h != 0) {
					echo $difference->h . " hrs";
				    } elseif ($difference->i <= 60 && $difference->i != 0) {
					echo $difference->i . " mins";
				    } elseif ($difference->s <= 10) {
					echo "Just Now";
				    } elseif ($difference->s <= 60 && $difference->s != 0) {
					echo $difference->s . " sec";
				    }
				    echo '</small>';
				    echo '</h4>';
				    echo '<p>' . strip_tags($discription) . '</p>';
				    echo '</a>';
				    echo '</li>';
				}
				echo '</ul>';
				echo '</li>';
				echo "<li class='footer'>";
				echo "<a href=" . base_url("notice/index") . ">";
				echo $this->lang->line("view_more");
				echo '</a>';
				echo '</li>';
				echo '</ul>';
			    }
			    ?>
                        </li>


                </nav>
            </div>
	</div>

