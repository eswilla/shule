
</div><!-- ./wrapper -->


<!-- Bootstrap js -->
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/bootstrap.min.js'); ?>"></script>
<!-- Style js -->
<script type="text/javascript" src="<?php echo base_url('assets/shulesoft/style.js'); ?>"></script>

<!-- Jquery datatable js -->
<script type="text/javascript" src="<?php echo base_url('assets/datatables/jquery.dataTables.js'); ?>"></script>
<!-- Datatable js -->
<script type="text/javascript" src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js'); ?>"></script>

<script type="text/javascript" src="<?php echo base_url('assets/shulesoft/shulesoft.js'); ?>"></script>

<!-- autocomplete plugin select2 js -->
<script type="text/javascript" src="<?php echo base_url('assets/select2/select2.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/custom.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/datepicker/picker.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/datepicker/picker.date.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/datepicker/legacy.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/shulesoft/jquery.mCustomScrollbar.js'); ?>"></script>

<!-- Jquery UI jquery -->
<script src="<?php echo base_url('assets/jquery-ui/jquery-ui.min.js'); ?>" type="text/javascript" charset="utf-8" async defer></script>

<!-- Jquery gritter -->
<script src="<?php echo base_url()?>/assets/js/pnotify/pnotify.js"></script>
<script src="<?php echo base_url()?>/assets/js/pnotify/pnotify.buttons.js"></script>
<script src="<?php echo base_url()?>/assets/js/pnotify/pnotify.nonblock.js"></script>

<!-- Custom Theme Scripts -->



<script>

	function notify(message){
		new PNotify({
			title: 'Intructions',
			text: message,
			type: 'info',
			hide: 'false',
			styling: 'bootstrap3',
			addclass: 'dark'
		});
	}

    $(".select2").select2({placeholder: "Select username", maximumSelectionSize: 6});
    $(".guargianID").select2({placeholder: "Select Guardian", maximumSelectionSize: 6});

    $("button[data-select2-open]").click(function () {
	$("#" + $(this).data("select2-open")).select2("open");
    });
</script>


<?php if ($this->session->flashdata('success')): ?>
    <script type="text/javascript">
        toastr["success"]("<?= $this->session->flashdata('success'); ?>")
        toastr.options = {
    	"closeButton": true,
    	"debug": false,
    	"newestOnTop": false,
    	"progressBar": false,
    	"positionClass": "toast-top-right",
    	"preventDuplicates": false,
    	"onclick": null,
    	"showDuration": "500",
    	"hideDuration": "500",
    	"timeOut": "5000",
    	"extendedTimeOut": "1000",
    	"showEasing": "swing",
    	"hideEasing": "linear",
    	"showMethod": "fadeIn",
    	"hideMethod": "fadeOut"
        }
    </script>
<?php endif ?>
<?php if ($this->session->flashdata('error')): ?>
    <script type="text/javascript">
        toastr["error"]("<?= $this->session->flashdata('error'); ?>")
        toastr.options = {
    	"closeButton": true,
    	"debug": false,
    	"newestOnTop": false,
    	"progressBar": false,
    	"positionClass": "toast-top-right",
    	"preventDuplicates": false,
    	"onclick": null,
    	"showDuration": "500",
    	"hideDuration": "500",
    	"timeOut": "5000",
    	"extendedTimeOut": "1000",
    	"showEasing": "swing",
    	"hideEasing": "linear",
    	"showMethod": "fadeIn",
    	"hideMethod": "fadeOut"
        }
    </script>
<?php endif ?>


<!--Jquery Advanced Datatables-->
<script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script> <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet">
<script type="text/javascript">


    $(document).ready(function () {

		    $('.dataTable').DataTable({
			dom: 'Bfrtip',
			buttons: [
			    'copy', 'csv', 'excel', 'pdf', 'print'
			]
		    });
		    $('.buttons-html5,.dt-button').addClass('btn btn-success').removeClass('dt-button');
		   
		});
</script>
</body>
</html>