<!--/**-->
<!--* ------------------------------------------->
<!--*-->
<!--* ******* Address****************-->
<!--* INETS COMPANY LIMITED-->
<!--* P.O BOX 32258, DAR ES SALAAM-->
<!--* TANZANIA-->
<!--*-->
<!--*-->
<!--* *******Office Location *********-->
<!--* 11th block, Bima Road, Mikocheni B, Kinondoni, Dar es salaam-->
<!--*-->
<!--*-->
<!--* ********Contacts***************-->
<!--* Email: <info@inetstz.com>-->
<!--* Website: <www.inetstz.com>-->
<!--    * Mobile: <+255 655 406 004>-->
<!--    * Tel:    <+255 22 278 0228>-->
<!--    * ------------------------------------------->
<!--    */-->

<?php $this->load->view("components/page_header"); ?>
<?php $this->load->view("components/page_topbar"); ?>
<?php $this->load->view("components/page_menu"); ?>

<div class="container-fluid">

    <div class="row">
        <div class="col-lg-4"></div>
        <div class="col-lg-4 ">
            <h3>WELCOME TO ADMIN MANUAL HELP</h3>
            <h5>This is the manual help which demonstrate how a  teacher can use
                this system and interact with each feature</h5>
        </div>
        <div class="col-lg-4"></div>
    </div>
</div>

<?php $this->load->view("components/page_footer"); ?>