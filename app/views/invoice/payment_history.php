<?php
/**
 * Description of payment_history
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
$set = NULL;
?>

<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa icon-payment"></i> <?= $this->lang->line('payment_title') ?></h3>


        <ol class="breadcrumb">
            <li><a href="<?= base_url("dashboard/index") ?>"><i class="fa fa-laptop"></i> <?= $this->lang->line('menu_dashboard') ?></a></li>
            <li class="active"><?= $this->lang->line('menu_payment_history') ?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">

		<?php
		$usertype = $this->session->userdata("usertype");
		if($usertype=='Admin' || $usertype=='Accountant'|| $usertype=='Parent'){
		?>

                <div class="col-sm-6 col-sm-offset-3 list-group">
                    <div class="list-group-item list-group-item-warning">
                        <form style="" class="form-horizontal" role="form" method="post">
                           
                            
                            <?php
                   if($usertype=='Parent' || $usertype=='Student') {?>
                       <div class="form-group">
                                <label for="classesID" class="col-sm-2 col-sm-offset-2 control-label">
				    <?= $this->lang->line("student_classes") ?>
                                </label>
                              <div class="col-sm-6">
				    <?php
				   $array_class = array("0" => $this->lang->line("invoice_select_classes"));
				    foreach ($classes as $classa) {
					$array_class[$classa->classesID] = $classa->classes;
				    }
				    echo form_dropdown("classesID", $array_class, set_value("classesID", $set), "id='classesID' class='form-control'");
				    ?>
                                </div>
                            </div>
                                       
                  <div class="form-group">
                                <label for="classesID" class="col-sm-2 col-sm-offset-2 control-label">
				    <?= $this->lang->line("student_classes") ?>
                                </label>
                         <div class="col-sm-6">
                               <?php

                                $array = $array = array('0' => $this->lang->line("invoice_select_student"));
                                if($students != "empty") {
                                    foreach ($students as $student) {
                                        $array[$student->studentID] = $student->name;
                                    }
                                }

                                $stID = 0;
                                if($studentID == 0) {
                                    $stID = 0;
                                } else {
                                    $stID = $studentID;
                                }

                                echo form_dropdown("studentID", $array, set_value("studentID", $stID), "id='studentID' class='form-control'");
                            ?>
                        </div>
                  </div>
                        
                       
                  <?php }   else{?>
                            
                  <div class="form-group">
                                <label for="classesID" class="col-sm-2 col-sm-offset-2 control-label">
				    <?= $this->lang->line("student_classes") ?>
                                </label>
                              <div class="col-sm-6">
				    <?php
				   $array_class = array("0" => $this->lang->line("invoice_select_classes"));
				    foreach ($classes as $classa) {
					$array_class[$classa->classesID] = $classa->classes;
				    }
				    echo form_dropdown("classesID", $array_class, set_value("classesID", $set), "id='classesID' class='form-control'");
				    ?>
                                </div>
                            </div>
                                       
             
                       
                  <?php } ?>    
                          
                            
                   <div class="form-group">
                                <label for="classesID" class="col-sm-2 col-sm-offset-2 control-label">
				    <?= $this->lang->line("student_classes") ?>
                                </label>
                                <div class="col-sm-6">
				    <?php
				    $array['4'] =$this->lang->line("select_payment_type");
				    $array['3'] = 'All';
				    $array['0'] = 'Pending Approval';
				    $array['1'] = 'Approved Payments';
				    $array['2'] = 'Rejected Payments';

				    echo form_dropdown("payment_types", $array, set_value("payment_type", $set), "id='payment_types' class='form-control'");
				    ?>
                                </div>
                            </div> 
                                </div> 
                           
                           
                            
                        </form>
                    </div>
                </div>

		<?php
		
		}
		if ( isset($payment) && count($payment) > 0) {
		    ?>

    		<div class="col-sm-12">

    		    <div class="nav-tabs-custom">
    			<ul class="nav nav-tabs">
    			    <li class="active"><a data-toggle="tab" href="#all" aria-expanded="true"><?= $this->lang->line("student_all_students") ?></a></li>
				<?php
				foreach ($sections as $key => $section) {
                                    echo '<li class=""><a data-toggle="tab" href="#'. $section->sectionID .'" aria-expanded="false">'. $this->lang->line("student_section")." ".$section->section. " ( ". $section->category." )".'</a></li>';
                                } 
				?>
    			</ul>



    			<div class="tab-content">
    			    <div id="all" class="tab-pane active">
    				<div id="hide-table">
    				    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
    					<thead>
    					    <tr>
    						<th class="col-sm-1"><?= $this->lang->line('slno') ?></th>
    						<th class="col-sm-1"><?= $this->lang->line('invoice_invoice') ?></th>

    						<th class="col-sm-2"><?= $this->lang->line('payment_date') ?></th>
    						<th class="col-sm-2"><?= $this->lang->line('student_name') ?></th>
    						<th class="col-sm-1"><?= $this->lang->line('invoice_feetype') ?></th>
    						<th class="col-sm-1"><?= $this->lang->line('invoice_amount') ?></th>
    						<th class="col-sm-1"><?= $this->lang->line('bank_name') ?></th>
   
    						<th class="col-sm-2"><?= $this->lang->line('invoice_status') ?></th>

    						<th class="col-sm-6"><?= $this->lang->line('action') ?></th>
    					    </tr>
    					</thead>
    					<tbody>
						<?php
                                                                               
						if (count($payment)) {
						    $i = 1;
						    foreach ($payment as $pay) {
							?>
	    					    <tr>
	    						<td data-title="<?= $this->lang->line('slno') ?>">
								<?php echo $i; ?>
	    						</td>
	    						<td data-title="<?= $this->lang->line('invoice_invoice') ?>">
								<?php
								echo $pay->invoiceNO;
								?>
	    						</td>
	    						<td data-title="<?= $this->lang->line('payment_date') ?>">
								<?php echo $pay->paymentdate; ?>
	    						</td>
	    						<td data-title="<?= $this->lang->line('student_name') ?>">
								<?php echo $pay->name; ?>
	    						</td>
	    					
	    						<td data-title="<?= $this->lang->line('invoice_feetype') ?>">
								<?php echo $pay->feetype; ?>
	    						</td>
	    						<td data-title="<?= $this->lang->line('invoice_amount') ?>">
								<?php echo $pay->paymentamount; ?>
	    						</td>
	    						<td data-title="<?= $this->lang->line('bank_name') ?>">
								<?php echo $pay->paymenttype; ?>
	    						</td>
	    						<td data-title="<?= $this->lang->line('invoice_status') ?>">
								<?php
								if ((int) $pay->approved == 1) {
								    $status = 'Approved';
								    $btn = 'success';
								} else if ((int) $pay->approved == 2) {
								    $status = 'Rejected';
								    $btn = 'danger';
								} else if ((int) $pay->approved == 0 || (int) $pay->approved == NULL) {
								    $status = 'Pending';
								    $btn = 'info';
								}
								echo "<button class='btn btn-" . $btn . " btn-xs'>" . $status . "</button>";
								?>
	    						</td>
	    						<td data-title="<?= $this->lang->line('action') ?>">
								<?php
								if ($usertype == "Admin" || $usertype == 'Accountant') {
								    $file = 'uploads/bankslip/' . $pay->slipfile;
							

								    echo (int) $pay->approved == 1 ? btn_add_pdf('invoice/receipt/' . $pay->paymentID . "/" . $set, 'Receipt') : '';
								} elseif ($usertype == "Parent") {
								    // echo btn_view('student/view/'.$student->studentID."/".$set, $this->lang->line('view'));
								        echo (int) $pay->approved == 1 ? btn_add_pdf('invoice/receipt/' . $pay->paymentID . "/" . $set, 'Receipt') : '';
								}
								?>
	    						</td>
	    					    </tr>
							<?php
							$i++;
						    }
						}  else {?>
                                                <div class="col-sm-12">
		    <div class="alert alert-info">No Payment History</div>
		</div>    
                                               <?php }
						?>
    					</tbody>
    				    </table>
    				</div>

    			    </div>

    			</div>

    		    </div> <!-- nav-tabs-custom -->
    		</div> <!-- col-sm-12 for tab -->

		<?php }else{ ?>
		<div class="col-sm-12">
		    <div class="alert alert-info">No Payment History</div>
		</div>
		<?php } ?>
            </div> <!-- col-sm-12 -->

        </div><!-- row -->
    </div><!-- Body -->
</div><!-- /.box -->

<script type="text/javascript">
    
   $('#classesID').change(function() {     
    var classesID = $(this).val();
    if(classesID === '0') {
        $('#studentID').val(0);
    } else {
        $.ajax({
            type: 'POST',
            url: "<?=base_url('invoice/call_all_student')?>",
            data: {id:classesID,status:"parent"},
            dataType: "html",
            success: function(data) {
               $('#studentID').html(data);
            }
        });
    }
}); 
    
    
    $('#payment_types').change(function () {
	var type = $(this).val();
         var class_id = $('#classesID').val();
         if(class_id===0) {
         alert('select the Class');
         return false;
         }
	if (type === 0) {
	    $('#hide-table').hide();
	    $('.nav-tabs-custom').hide();
	} else {
	    $.ajax({
		type: 'POST',
		url: "<?= base_url('invoice/payment_data') ?>",
		data: {id: type,classesID:class_id },
		dataType: "html",
		success: function (data) {
                   
		    window.location.href = data;
		}
	    });
	}
    });
    
 
    

</script>