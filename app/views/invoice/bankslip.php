<?php

/**
 * Description of bankslip
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
?>

<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa icon-invoice"></i> <?= $this->lang->line('bankslip_title') ?></h3>


        <ol class="breadcrumb">
            <li><a href="<?= base_url("dashboard/index") ?>"><i class="fa fa-laptop"></i> <?= $this->lang->line('menu_dashboard') ?></a></li>
            <li><a href="<?= base_url("invoice/index") ?>"><?= $this->lang->line('menu_invoice') ?></a></li>
            <li class="active"><?= $this->lang->line('add_payment') ?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-8">

		<?php
		$usertype = $this->session->userdata("usertype");
		
		    /**
		     * ----------------------------------------------------------------
		     * In case a student bring cash, accountant or admin can manually
		     * add that amount to a student account. This is not much a good
		     * process just beacause it is associated with much security issues
		     * ----------------------------------------------------------------- 
		     * 
		     */
		    ?>
    		<form class="form-horizontal" role="form" method="post" enctype="multipart/form-data"> 

			<?php
			if (form_error('amount'))
			    echo "<div class='form-group has-error' >";
			else
			    echo "<div class='form-group' >";
			?>
    		    <label for="amount" class="col-sm-2 control-label">
			    <?= $this->lang->line("invoice_amount") ?>
    		    </label>
    		    <div class="col-sm-6">
    			<input type="text" class="form-control" id="amount" name="amount" value="" >
    		    </div>
    		    <span class="col-sm-4 control-label">
			    <?php if(isset($amount_error)) echo $amount_error; ?>
    		    </span>
    	    </div>
	    	<?php
		if (form_error('payment_method'))
		    echo "<div class='form-group has-error' >";
		else
		    echo "<div class='form-group' >";
		?>
    	    <label for="payment_method" class="col-sm-2 control-label">
		    <?= $this->lang->line("bank_name") ?>
    	    </label>
    	    <div class="col-sm-6">
		    <?php
		    $array = $array = array('0' => $this->lang->line("select_bank"));
		    foreach ($banknames as $bank) {
                                    $array[$bank->bank_name] =$bank->bank_name;
                                }
		    echo form_dropdown("bank_name", $array, set_value("bank_name"), "id='bank_name' class='form-control'");
		    ?>
    	    </div>
    	    <span class="col-sm-4 control-label">
		    <?php echo form_error('bank_name'); ?>
    	    </span>
    	</div>
	<?php
			if (form_error('amount'))
			    echo "<div class='form-group has-error' >";
			else
			    echo "<div class='form-group' >";
			?>
    		    <label for="amount" class="col-sm-2 control-label">
			    <?= $this->lang->line("transaction_id") ?>
    		    </label>
    		    <div class="col-sm-6">
    			<input type="text" class="form-control" id="amount" name="transaction_id" value="" >
    		    </div>
    		    <span class="col-sm-4 control-label">
			<?php if(isset($transaction_id)) echo $transaction_id; ?>
    		    </span>
    	    </div>
	   <?php 
                        if(isset($image)) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="photo" class="col-sm-2 control-label col-xs-8 col-md-2">
                            <?=$this->lang->line("bankslip_image")?> 
                        </label>
                        <div class="col-sm-4 col-xs-6 col-md-4">
                            <input class="form-control"  id="uploadFile" placeholder="Choose File" disabled />  
                        </div>

                        <div class="col-sm-2 col-xs-6 col-md-2">
                            <div class="fileUpload btn btn-success form-control">
                                <span class="fa fa-repeat"></span>
                                <span><?=$this->lang->line("upload")?></span>
                                <input id="uploadBtn" type="file" accept=".png,.jpg,.jpeg,.gif,.pdf" class="upload" name="image" />
                            </div>
                        </div>
                         <span class="col-sm-4 control-label col-xs-6 col-md-4">
                           
                            <?php if(isset($image)) echo $image; ?>
                        </span>
                    </div>

    	<div class="form-group">
    	    <div class="col-sm-offset-2 col-sm-8">
    		<input type="submit" class="btn btn-success" value="<?= $this->lang->line("add_payment") ?>" >
    	    </div>
    	</div>

    	</form>
</div>
</div>
</div>
</div>

<script type="text/javascript">

</script>