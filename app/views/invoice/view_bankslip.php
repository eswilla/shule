<?php
/**
 * Description of view_bankslip
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
?>
<?php
if (count($invoice)) {
    $usertype = $this->session->userdata("usertype");
    if ($usertype == "Admin" || $usertype == "Accountant" || $usertype == "Student" || $usertype == "Parent") {
	?>
	<?php if ($usertype == "Admin" || $usertype == "Accountant") { ?>
	    <div class="well">
	        <div class="row">

	    	<div class="col-sm-6">
	    	    <button class="btn-default btn-sm-cs" onclick="javascript:printDiv('printablediv')"><span class="fa fa-print"></span> <?= $this->lang->line('print') ?> </button>
			<?php if ((int) $invoice->approved != 1 && (int) $invoice->approved != 2) { ?>
			    <button class="btn-success btn-sm-cs" onclick="javascript:accept('<?= $invoice->invoiceID ?>', '<?= $invoice->paymentID ?>')"><span class="fa fa-check"></span> Accept</button>
			    <button class="btn-cs btn-sm-cs" onclick="javascript:reject('<?= $invoice->invoiceID ?>', '<?= $invoice->paymentID ?>')"><span class="fa fa-crosshairs"></span> Reject</button>
			<?php } ?>
	    	</div>

	    	<div class="col-sm-6">
	    	    <ol class="breadcrumb">
	    		<li><a href="<?= base_url("dashboard/index") ?>"><i class="fa fa-laptop"></i> <?= $this->lang->line('menu_dashboard') ?></a></li>
	    		<li><a href="<?= base_url("invoice/payment_history") ?>"><?= $this->lang->line('payment') ?></a></li>
	    		<li class="active"><?= $this->lang->line('view') ?></li>
	    	    </ol>
	    	</div>
	        </div>
	    </div>
	<?php } elseif ($usertype == "Student" || $usertype == "Parent") { ?>
	    <?php if ($invoice->paymentamount) { ?>
		<div class="well">
		    <div class="row">
			<div class="col-sm-6">

			    <?= btn_payment('invoice/payment/' . $invoice->invoiceID, $this->lang->line('payment')) ?>

			    <?= btn_sm_add('invoice/bankslip/' . $invoice->invoiceID, 'Submit Paying Slip') ?>
			</div>

			<div class="col-sm-6">
			    <ol class="breadcrumb">
				<li><a href="<?= base_url("dashboard/index") ?>"><i class="fa fa-laptop"></i> <?= $this->lang->line('menu_dashboard') ?></a></li>
				<li><a href="<?= base_url("invoice/index") ?>"><?= $this->lang->line('menu_invoice') ?></a></li>
				<li class="active"><?= $this->lang->line('view') ?></li>
			    </ol>
			</div>
		    </div>
		</div>
	    <?php } ?>
	<?php } ?>




	<div id="printablediv">
	    Bank Transaction ID: <?= $invoice->transactionID ?>

	    <section class="content invoice" >

		<div class="row text-center">
		    <!-- accepted payments column -->
		    <?php
		    $path = 'uploads/bankslip/' . $invoice->slipfile;
		    $ext = explode('.', basename($path))[1];
		    if ($ext == 'pdf') {
			echo '<a href="../../'.$path.'" target="_blank">Click Here to view PDF file</a>';
		    } else {
			$array = array(
			    "src" => base_url($path),
			);
			echo img($array);
		    }
		    ?>
		</div><!-- /.row -->

		<!-- this row will not appear when printing -->
	    </section><!-- /.content -->
	</div>

	<script language="javascript" type="text/javascript">
	    function printDiv(divID) {
		//Get the HTML of div
		var divElements = document.getElementById(divID).innerHTML;
		//Get the HTML of whole page
		var oldPage = document.body.innerHTML;

		//Reset the page's HTML with div's HTML only
		document.body.innerHTML =
			"<html><head><title></title></head><body>" +
			divElements + "</body>";

		//Print Page
		window.print();

		//Restore orignal HTML
		document.body.innerHTML = oldPage;
	    }
	    function closeWindow() {
		location.reload();
	    }
	    function accept(a, b) {
		$.ajax({
		    type: 'POST',
		    url: "<?= base_url('invoice/payment_accept') ?>",
		    data: {invoiceID: a, paymentID: b, status: 1, approved: 1},
		    dataType: "html",
		    success: function (data) {
			swal('success', 'Payment accepted successfully', 'success');
			window.location.reload();
		    }
		});
	    }
	    function reject(a, b) {
		$.ajax({
		    type: 'POST',
		    url: "<?= base_url('invoice/payment_accept') ?>",
		    data: {invoiceID: a, paymentID: b, status: 0, approved: 2},
		    dataType: "html",
		    success: function (data) {
			swal('success', 'Payment rejected successfully', 'warning')
			window.location.reload();
		    }
		});
	    }
	</script>
	<?php
    }
}
?>
