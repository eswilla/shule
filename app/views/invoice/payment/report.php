<?php
if (!empty($data)) {
    ?>
    <div class="row">
        <div class="col-md-12">
    	<div class="row">
    	    <div class="col-md-12 space20">
    		<div class="btn-group pull-right">
    		    <button data-toggle="dropdown" class="btn btn-green dropdown-toggle">
    			Export <i class="fa fa-angle-down"></i>
    		    </button>
    		    <ul class="dropdown-menu dropdown-light pull-right">
    			<li>
			    <a href="#" onclick="generate_pdf()" data-table="#sample-table-2"> Save as PDF </a>
    			</li>
    			<li>
    			    <a href="#" class="export-xml" data-table="#sample-table-2"> Save as XML </a>
    			</li>
    			<li>
    			    <a href="#" class="export-excel" data-table="#sample-table-2"> Export to Excel </a>
    			</li>

    		    </ul>
    		</div>
    	    </div>
    	</div>
	    <?php ob_start(); ?>
    	<!-- start: DYNAMIC TABLE PANEL -->
	<div id="table_report">
    	<div class="page-header">
    	    <h3>Payment Status Report</h3>
    	    <small><?= $method ?> Status Report </small>
    	</div>
    	<table class="table table-striped table-bordered table-hover table-full-width" id="sample-table-2" >
    	    <thead>
    		<tr>
    		    <th>Date</th>
    		    <th>Payer Name</th>
    		    <th>Business Name</th>
    		    <th>Amount</th>
    		    <th>Invoice </th>
    		    <th>Bank Name</th>
    		    <th>Mobile Transaction ID</th>
    		    <th>Bank Transaction ID</th>
    		    <th>Receipt</th>
    		    <th>Status</th>
    		</tr>
    	    </thead>
    	    <tbody>
		    <?php
		    $total = 0;
		    foreach ($data as $value) {
			$total+=$value->amount;
			?>
			<tr>
			    <td><?= $value->date; ?></td>
			    <td><?= $value->client_name; ?></td>
			    <td><?= $value->name; ?></td>
			    <td><?= $value->amount; ?></td>
			    <td><?= $value->invoice; ?></td>
			    <td><?= $value->bank_name; ?></td>
			    <td><?= $value->mobile_transaction_id; ?></td>
			    <td><?= $value->bank_transaction_id; ?></td>
			    <td><?= $value->code; ?></td>
			    <td><?= $value->status; ?></td>
			</tr>`
		    <?php } ?>
    	    </tbody>
    	    <tfoot>
    		<tr>
    		    <td></td>
    		    <td></td>
    		    <td></td>
    		    <td>Total: <?= $total ?></td>
    		    <td></td>
    		    <td></td>
    		    <td></td>
    		    <td></td>
    		    <td></td>
    		    <td></td>
    		</tr>`
    	    </tfoot>
    	</table>
	    </div>
    	<!-- end: DYNAMIC TABLE PANEL -->
	    <?php
	    $view = ob_get_clean();
	    echo $view;
	    ?>
        </div>
    </div>
    <!--    <div id="modified"></div>-->
    <script src="<?= site_url(); ?>assets/js/table-export.js"></script>
    <link rel="stylesheet" href="<?= site_url(); ?>assets/plugins/DataTables/media/css/DT_bootstrap.css" />
    <!-- common templates -->
    <div id="dialog_result"></div>
    <script>
        jQuery(document).ready(function () {
    	TableExport.init();
    	if ($(".tooltips").length) {
    	    $('.tooltips').tooltip();
    	}
    	generate_pdf = function () {
	    $.post(url+'payment/get_payment_table_report',{file:'file',cat_name:'<?=$cat_name?>'},function(data){
		console.log(data);
	    });
    	};
        });
    </script>
    <?php
} else {
    echo '<div class="alert alert-block alert-info fade in">
    <h4 class="alert-heading">Information</h4>
    <p>There is no records for this category</p>
</div>';
}
?>

