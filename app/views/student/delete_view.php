<?php
/**
 * Description of delete_view
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
	<div class="x_panel">
	    <div class="x_title">
		<h2>Delete Student</h2>
		<div class="clearfix"></div>
	    </div>
	    <div class="x_content">



		<div class="col-md-8 col-sm-8 col-xs-12" style="border:0px solid #e5e5e5;">

		    <h3 class="prod_title">Reasons</h3>

		    <p>You are about to delete <?= $student->name ?> Records. Please specify clearly reasons for you to delete this student.</p>
		    <br>
		    <form method="post">
			<div class="">
			    <h2>Reasons</h2>
			    <ul class="list-inline prod_color">
				<li>
				    <p>Left School</p>
				    <div class="color bg-green">
					<input type="radio" name="dlt" value="0"/>
				    </div>
				</li>
				<li>
				    <p>Suspension</p>
				    <div class="color bg-blue">
					<input type="radio" name="dlt" value="5"/>
				    </div>
				</li>
				<li>
				    <p>Entered Wrongly</p>
				    <div class="color bg-red">
					<input type="radio" name="dlt" value="3"/>
				    </div>
				</li>
				<li>
				    <p>Other</p>
				    <div class="color bg-orange">
					<input type="radio" name="dlt" value="4"/>
				    </div>
				</li>

			    </ul>
			</div>
			<br>

			<div class="">
			    <ul class="list-inline prod_size">

				<li>
				    <button type="submit" class="btn btn-danger btn-xs">Delete</button>
				</li>
			    </ul>
			</div>
		    </form>
		    <br>


		</div>

		<div class="col-md-4 col-sm-4 col-xs-12">
		    <div class="product-image">
			 <br>

			<?php
			$array = array(
			    "src" => base_url('uploads/images/' . $student->photo),
			    'width' => '90px',
			    'height' => '280px',
			    'class' => 'img-rounded'
			);
			echo img($array);
			?>
		    </div>
		</div>
	    </div>
	</div>
    </div>
</div>