<div class="panel panel-default">
    <div class="panel-heading-install">
		<ul class="nav nav-pills">
		  	<li class="active"><a href="<?=base_url('install/index')?>">Checklist</a></li>
      
		  	<li><a href="#">Database</a></li>
  
		  	<li><a href="#">School Config</a></li>
		  	<li><a href="#">Done!</a></li>
		</ul>
    </div>
    <div class="panel-body ins-bg-col">
    	<h4>Pre-Install Checklist</h4>
	<div class="alert alert-info"><span class="fa fa-check-circle"></span>Database for <b><?= str_replace('.','',set_schema_name())?>.shulesoft.com</b> has not been installed. Follow the following procedures to complete installation</div>
    <h4>Requirements</h4>
    <div class="alert alert-info"><span class="fa fa-check-circle"></span>
	<ul>
	    <li>School Information</li>
	    <li>Initial System Administrator Information</li>
	</ul>
    </div>
    	<div class="col-sm-12"><a href="<?=base_url('install/database')?>" class="btn btn-success pull-right">Next Step</a></div>
    </div>
</div>