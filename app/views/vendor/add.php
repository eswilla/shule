
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa fa-user"></i> <?=$this->lang->line('panel_title')?></h3>

       
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("parentes/index")?>"><?=$this->lang->line('menu_vendor')?></a></li>
            <li class="active"><?=$this->lang->line('menu_add')?> <?=$this->lang->line('panel_title')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-8">

                <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">

                    <?php 
                        if(form_error('name')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="name" class="col-sm-2 control-label">
                            <?=$this->lang->line("name")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="name" name="name" value="<?=set_value('name')?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('name'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('telephone_number')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="telephone_number" class="col-sm-2 control-label">
                            <?=$this->lang->line("telephone_number")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="telephone" name="telephone_number" value="<?=set_value('telephone_number')?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('telephone_number'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('phone_number')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="phone_number" class="col-sm-2 control-label">
                            <?=$this->lang->line("phone_number")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="phone_number" name="phone_number" value="<?=set_value('phone_number')?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('phone_number'); ?>
                        </span>
                    </div>
<?php 
                        if(form_error('email')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="email" class="col-sm-2 control-label">
                            <?=$this->lang->line("email")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="email" class="form-control" id="email" name="email" value="<?=set_value('email')?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('email'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('country')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="country" class="col-sm-2 control-label">
                            <?=$this->lang->line("country")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="country" name="country" value="<?=set_value('country')?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('country'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('city')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="city" class="col-sm-2 control-label">
                            <?=$this->lang->line("city")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="city" name="city" value="<?=set_value('city')?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('city'); ?>
                        </span>
                    </div>

                    

                    <?php 
                        if(form_error('location')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="location" class="col-sm-2 control-label">
                            <?=$this->lang->line("location")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="location" name="location" value="<?=set_value('location')?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('location'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('bank_name')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="bank_name" class="col-sm-2 control-label">
                            <?=$this->lang->line("bank_name")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="bank_name" name="bank_name" value="<?=set_value('bank_name')?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('bank_name'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('bank_branch')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="bank_branch" class="col-sm-2 control-label">
                            <?=$this->lang->line("bank_branch")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="bank_branch" name="bank_branch" value="<?=set_value('bank_branch')?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('bank_branch'); ?>
                        </span>
                    </div>

		      <?php 
                        if(form_error('account_number')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="account_number" class="col-sm-2 control-label">
                            <?=$this->lang->line("account_number")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="account_number" name="account_number" value="<?=set_value('account_number')?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('account_number'); ?>
                        </span>
                    </div>
		    
		        <?php 
                        if(form_error('contact_person_name')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="contact_person_name" class="col-sm-2 control-label">
                            <?=$this->lang->line("contact_person_name")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="contact_person_name" name="contact_person_name" value="<?=set_value('contact_person_name')?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('contact_person_name'); ?>
                        </span>
                    </div>
		    
		         <?php 
                        if(form_error('contact_person_phone')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="contact_person_phone" class="col-sm-2 control-label">
                            <?=$this->lang->line("contact_person_phone")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="contact_person_phone" name="contact_person_phone" value="<?=set_value('contact_person_phone')?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('contact_person_phone'); ?>
                        </span>
                    </div>
		    
		         <?php 
                        if(form_error('contact_person_email')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="contact_person_email" class="col-sm-2 control-label">
                            <?=$this->lang->line("contact_person_email")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="contact_person_email" name="contact_person_email" value="<?=set_value('contact_person_email')?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('contact_person_email'); ?>
                        </span>
                    </div>
		    
		         <?php 
                        if(form_error('contact_person_jobtitle')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="contact_person_jobtitle" class="col-sm-2 control-label">
                            <?=$this->lang->line("contact_person_jobtitle")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="contact_person_jobtitle" name="contact_person_jobtitle" value="<?=set_value('contact_person_jobtitle')?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('contact_person_jobtitle'); ?>
                        </span>
                    </div>
		        <?php 
                        if(form_error('service_product')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="service_product" class="col-sm-2 control-label">
                            <?=$this->lang->line("service_product")?>
                        </label>
                        <div class="col-sm-6">
                            <textarea  class="form-control" id="service_product" name="service_product" value="<?=set_value('service_product')?>" ></textarea>
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('service_product'); ?>
                        </span>
                    </div>
		    
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("add_vendor")?>" >
                        </div>
                    </div>

                </form>
      
            </div> <!-- col-sm-8 -->
            
        </div><!-- row -->
    </div><!-- Body -->
</div><!-- /.box -->

<script type="text/javascript">
document.getElementById("uploadBtn").onchange = function() {
    document.getElementById("uploadFile").value = this.value;
};

$(document).ready(function(){
    $('#show_password').click(function(){
        $('#password').attr('type',$(this).is(':checked')? 'text' : 'password');
    });

});
</script>

