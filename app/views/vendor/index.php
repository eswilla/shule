
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa fa-users"></i> <?=$this->lang->line('vendor_title')?></h3>

        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active"><?=$this->lang->line('menu_vendor')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">

                <?php 
                    $usertype = $this->session->userdata("usertype");
                ?>
                    <h5 class="page-header">
                        <a class="btn btn-success" href="<?php echo base_url('vendor/add') ?>">
                            <i class="fa fa-plus"></i> 
                            <?=$this->lang->line('add_vendor')?>
                        </a>
                    </h5>

                <div id="hide-table">
                    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead>
                            <tr>
                                <th class="col-lg-2"><?=$this->lang->line('slno')?></th>
                                <th class="col-lg-2"><?=$this->lang->line('vendor_name')?></th>
                                <th class="col-lg-2"><?=$this->lang->line('vendor_email')?></th>
                                <th class="col-lg-2"><?=$this->lang->line('vendor_phone')?></th>
                                <th class="col-lg-2"><?=$this->lang->line('vendor_location')?></th>
                                <th class="col-lg-2"><?=$this->lang->line('action')?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(count($vendors)) {$i = 1; foreach($vendors as $vendor) { ?>
                                <tr>
                                    <td data-title="<?=$this->lang->line('slno')?>">
                                        <?php echo $i; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('vendor_name')?>">
                                        <?php echo $vendor->name; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('vendor_email')?>">
                                        <?php echo $vendor->email; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('vendor_phone')?>">
                                        <?php echo $vendor->phone_number; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('vendor_location')?>">
                                        <?php echo $vendor->location; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('action')?>">
					       <?php echo btn_view('vendor/view/'.$vendor->vendor_id, $this->lang->line('view')) ?>
                                        <?php echo btn_edit('vendor/edit/'.$vendor->vendor_id, $this->lang->line('edit')) ?>
                                        <?php echo btn_delete('vendor/delete/'.$vendor->vendor_id, $this->lang->line('delete')) ?>
                                    </td>
                                </tr>
                            <?php $i++; }} ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
