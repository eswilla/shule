
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa fa-calendar"></i> <?= $this->lang->line('panel_title') ?></h3>


        <ol class="breadcrumb">
            <li><a href="<?= base_url("dashboard/index") ?>"><i class="fa fa-laptop"></i> <?= $this->lang->line('menu_dashboard') ?></a></li>
            <li><a href="<?= base_url("notice/index") ?>"><?= $this->lang->line('menu_notice') ?></a></li>
            <li class="active"><?= $this->lang->line('menu_add') ?> <?= $this->lang->line('menu_notice') ?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-9">
                <form class="form-horizontal" role="form" method="post">
		    <?php
		    if (form_error('title'))
			echo "<div class='form-group has-error' >";
		    else
			echo "<div class='form-group' >";
		    ?>
		    <label for="title" class="col-sm-1 control-label">
			<?= $this->lang->line("notice_title") ?>
		    </label>
		    <div class="col-sm-6">
			<input type="text" class="form-control" id="title" name="title" value="<?= set_value('title') ?>" >
		    </div>
		    <span class="col-sm-6 control-label">
			<?php echo form_error('title'); ?>
		    </span>
	    </div>


	    <?php
	    if (form_error('class'))
		echo "<div class='form-group has-error' >";
	    else
		echo "<div class='form-group' >";
	    ?>
	    <label for="classesID" class="col-sm-1 control-label">
		<?= $this->lang->line("notice_for") ?>
	    </label>
	    <div class="col-sm-6">
		<?php
		if (!empty($classes)) {
		    echo "<input type=\"checkbox\" id='checkboxs' onclick='check_all()'>", $this->lang->line("select_all"), "</option><br/>";

		    foreach ($classes as $class) {
			echo " <input type=\"checkbox\" class='check'  name=\"notice_for[]\"   value=\"$class->classes_numeric\">", $class->classes, "";
		    }
		} else {
		    echo '<span id="sectionID">';

		    echo '</span>';
		}
		?>
		<br/><input type="checkbox" class='check'  name="notice_for[]"   value="0"> Teachers
		<input type="checkbox" class='check'  name="notice_for[]"   value="100"> Staff
	    </div>
	    <span class="col-sm-6 control-label">
		<?php echo form_error('class'); ?>
	    </span>
	</div>

	<?php
	if (form_error('date'))
	    echo "<div class='form-group has-error' >";
	else
	    echo "<div class='form-group' >";
	?>
	<label for="date" class="col-sm-1 control-label">
	    <?= $this->lang->line("notice_date") ?>
	</label>
	<div class="col-sm-6">
	    <input type="text" class="form-control" id="date" name="date" value="<?= set_value('date') ?>" >
	</div>
	<span class="col-sm-6 control-label">
	    <?php echo form_error('date'); ?>
	</span>
    </div>

    <?php
    if (form_error('notice'))
	echo "<div class='form-group has-error' >";
    else
	echo "<div class='form-group' >";
    ?>
    <label for="notice" class="col-sm-1 control-label">
	<?= $this->lang->line("notice_notice") ?>
    </label>
    <div class="col-sm-8">
	<textarea class="form-control" id="notice" name="notice" ><?= set_value('notice') ?></textarea>
    </div>
    <span class="col-sm-3 control-label">
	<?php echo form_error('notice'); ?>
    </span>
</div>

<div class="form-group">
    <div class="col-sm-offset-1 col-sm-8">
	<input type="submit" class="btn btn-success" value="<?= $this->lang->line("add_class") ?>" >
    </div>
</div>

</form>

</div>
     <div class="col-sm-3"> 

    <div class="x_panel">
	<div class="x_title">
	    <h2>Upload School Calender From Excel</h2>
	    
	    <div class="clearfix"></div>
	
	</div>
	<p>Download <a href="<?=base_url('uploads/sample/sample_school_calender.xlsx')?>">Sample File here</a></p>
	<form id="demo-form2" action="<?= base_url('notice/uploadByFile') ?>" class="form-horizontal" method="POST" enctype="multipart/form-data">

	    <div class="form-group">

		<div class="col-md-6 col-sm-6 col-xs-12">
		    <input id="file" name="file"  type="file" required="required" accept=".xls,.xlsx,.csv,.odt">
		</div>
	    </div>
	    <div class="ln_solid"></div>
	    <div class="form-group">
		<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
		    <button type="submit" class="btn btn-success">Submit</button>
		</div>
	    </div>

	</form>
    </div>
</div>

</div>
</div>
</div>
<script type="text/javascript" src="<?php echo base_url('assets/editor/jquery-te-1.4.0.min.js'); ?>"></script>
<script type="text/javascript">
    $('#date').datepicker();
    $('#notice').jqte();
    function check_all() {
	$('.check').prop('checked', true);
    }
</script>
