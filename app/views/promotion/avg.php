<?php
/**
 * Description of avg
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
?>

<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa icon-promotion"></i> <?= $this->lang->line('panel_title') ?></h3>


        <ol class="breadcrumb">
            <li><a href="<?= base_url("dashboard/index") ?>"><i class="fa fa-laptop"></i> <?= $this->lang->line('menu_dashboard') ?></a></li>
            <li class="active"><?= $this->lang->line('menu_promotion') ?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="col-sm-6 col-sm-offset-3 list-group">
                    <div class="list-group-item list-group-item-warning">

                        <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
			    <?= $exams ?>
                            <div class="form-group">
                                <div class="col-sm-offset-4 col-sm-4">

                                    <input type="submit" id="save" class="btn btn-success" value="<?= $this->lang->line("add_mark_setting") ?>" >
                                </div>
                            </div>

                        </form>
			<div>
			</div>

		    </div> <!-- col-sm-12 -->

		</div><!-- row -->
	    </div><!-- Body -->
	</div><!-- /.box -->

        <script type="text/javascript">
	    $('#classesID').change(function () {
		var classesID = $(this).val();
		if (classesID == 0) {
		    $('#hide-table').hide();
		} else {
		    $.ajax({
			type: 'POST',
			url: "<?= base_url('promotion/promotion_list') ?>",
			data: "id=" + classesID,
			dataType: "html",
			success: function (data) {
			    window.location.href = data;
			}
		    });
		}
	    });

	   
        </script>
