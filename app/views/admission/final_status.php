<?php
/**
 * Description of final_status
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
?>
<div class="box">
    <div class="box-header">
	<h3 class="box-title"><i class="fa fa-home"></i> <a href="<?= base_url("/")	?>"> Home</a></h3>

	<ol class="breadcrumb">


	</ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
	<div class="row">
	    <div class="col-sm-12">
		<?php
		$value = $this->input->get_post('value');
		if ($value == 'success') {
		    ?>
    		<div class="alert alert-success"><p>Your submission form is submitted successfully. You will soon receive your application status in your email and your phone number</p></div>
		<?php } else { ?>
    		<div class="alert alert-warning"><p>Problem occurred, please try again later</p></div>

		<?php } ?>


	    </div> <!-- col-sm-8 -->

	</div><!-- row -->
    </div><!-- Body -->
</div><!-- /.box -->
