<?php
/**
 * Description of register_student
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
?>
<div class="box">
    <div class="box-header">
	<h3 class="box-title"><i class="fa icon-student"></i> <?= $this->lang->line('panel_title') ?></h3>

	<ol class="breadcrumb">
	    <li><a href="<?= base_url("/") ?>"><i class="fa fa-laptop"></i> <?= $this->lang->line('menu_dashboard') ?></a></li>

	</ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
	<div class="row">
	    <div class="col-sm-8">
		<form class="form-horizontal" role="form" method="post" action="<?= base_url("admission/addStudent") ?>" enctype="multipart/form-data">

		    <?php
		    if (form_error('name'))
			echo "<div class='form-group has-error' >";
		    else
			echo "<div class='form-group' >";
		    ?>
		    <label for="name_id" class="col-sm-2 control-label">
			<?= $this->lang->line("student_name") ?> *
		    </label>
		    <div class="col-sm-6">
			<input type="text" class="form-control" id="name_id" name="name" value="<?= set_value('name') ?>" >
		    </div>
		    <span class="col-sm-4 control-label">
			<?php echo form_error('name'); ?>
		    </span>
	    </div>


	    <div>
		<input type="hidden" value="<?= $parent_id ?>" name="guargianID" id="guargianID"/>
	    </div>



	    <?php
	    if (form_error('dob'))
		echo "<div class='form-group has-error' >";
	    else
		echo "<div class='form-group' >";
	    ?>
	    <label for="dob" class="col-sm-2 control-label">
		<?= $this->lang->line("student_dob") ?> *
	    </label>
	    <div class="col-sm-6">
		<input type="date" class="form-control" id="dob" name="dob" value="<?= set_value('dob') ?>" >
	    </div>
	    <span class="col-sm-4 control-label">
		<?php echo form_error('dob'); ?>
	    </span>
	</div>

	<?php
	if (form_error('sex'))
	    echo "<div class='form-group has-error' >";
	else
	    echo "<div class='form-group' >";
	?>
	<label for="sex" class="col-sm-2 control-label">
	    <?= $this->lang->line("student_sex") ?> *
	</label>
	<div class="col-sm-6">
	    <?php
	    echo form_dropdown("sex", array($this->lang->line('student_sex_male') => $this->lang->line('student_sex_male'), $this->lang->line('student_sex_female') => $this->lang->line('student_sex_female')), set_value("sex"), "id='sex' class='form-control'");
	    ?>
	</div>
	<span class="col-sm-4 control-label">
	    <?php echo form_error('sex'); ?>
	</span>
    </div>

    <?php
    if (form_error('religion'))
	echo "<div class='form-group has-error' >";
    else
	echo "<div class='form-group' >";
    ?>
    <label for="religion" class="col-sm-2 control-label">
	<?= $this->lang->line("student_religion") ?> *
    </label>
    <div class="col-sm-6">

	<select name="religion" id="religion" class="form-control">
	    <option value="Christian">Christian</option>
	    <option value="Muslim">Muslim</option>
	    <option value="Other">Other</option>
	</select>

    </div>
    <span class="col-sm-4 control-label">
	<?php echo form_error('religion'); ?>
    </span>
</div>

<?php
if (form_error('email'))
    echo "<div class='form-group has-error' >";
else
    echo "<div class='form-group' >";
?>
<label for="email" class="col-sm-2 control-label">
    <?= $this->lang->line("student_email") ?>
</label>
<div class="col-sm-6">
    <input type="text" class="form-control" id="email" name="email" value="<?= set_value('email') ?>" >
</div>
<span class="col-sm-4 control-label">
    <?php echo form_error('email'); ?>
</span>
</div>

<?php
if (form_error('phone'))
    echo "<div class='form-group has-error' >";
else
    echo "<div class='form-group' >";
?>
<label for="phone" class="col-sm-2 control-label">
    <?= $this->lang->line("student_phone") ?>
</label>
<div class="col-sm-6">
    <input type="text" class="form-control" id="phone" name="phone" value="<?= set_value('phone') ?>" >
</div>
<span class="col-sm-4 control-label">
    <?php echo form_error('phone'); ?>
</span>
</div>

<?php
if (form_error('address'))
    echo "<div class='form-group has-error' >";
else
    echo "<div class='form-group' >";
?>
<label for="address" class="col-sm-2 control-label">
    <?= $this->lang->line("student_address") ?> *
</label>
<div class="col-sm-6">
    <input type="text" class="form-control" id="address" name="address" value="<?= set_value('address') ?>" >
</div>
<span class="col-sm-4 control-label">
    <?php echo form_error('address'); ?> *
</span>
</div>

<?php
if (form_error('classesID'))
    echo "<div class='form-group has-error' >";
else
    echo "<div class='form-group' >";
?>
<label for="classesID" class="col-sm-2 control-label">
    <?= $this->lang->line("student_classes") ?> *
</label>
<div class="col-sm-6">
    <?php
    $array = array(0 => $this->lang->line("student_select_class"));
    foreach ($classes as $classa) {
	$array[$classa->classesID] = $classa->classes;
    }
    echo form_dropdown("classesID", $array, set_value("classesID"), "id='classesID' class='form-control'");
    ?>
</div>
<span class="col-sm-4 control-label">
    <?php echo form_error('classesID'); ?>
</span>
</div>

<input type="hidden" id="sectionID" name="sectionID" />
<input type="hidden" id="roll" name="roll" value="" >

<?php
if (isset($image))
    echo "<div class='form-group has-error' >";
else
    echo "<div class='form-group' >";
?>
<label for="photo" class="col-sm-2 control-label col-xs-8 col-md-2">
    <?= $this->lang->line("student_photo") ?>
</label>
<div class="col-sm-4 col-xs-6 col-md-4">
    <input class="form-control"  id="uploadFile" placeholder="Choose File" disabled />  
</div>

<div class="col-sm-2 col-xs-6 col-md-2">
    <div class="fileUpload btn btn-success form-control">
	<span class="fa fa-repeat"></span>
	<span><?= $this->lang->line("upload") ?></span>
	<input id="uploadBtn" type="file" class="upload" name="image" />
    </div>
</div>
<span class="col-sm-4 control-label col-xs-6 col-md-4">

    <?php if (isset($image)) echo $image; ?>
</span>
</div>

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-8">
	<input type="submit" class="btn btn-success" value="<?= $this->lang->line("add_student") ?>" >
    </div>
</div>
</form>
</div> <!-- col-sm-8 -->

</div><!-- row -->
</div><!-- Body -->
</div><!-- /.box -->

<script type="text/javascript">
    $('#dob').datepicker({
	maxDate: "-6y"
    });
    document.getElementById("uploadBtn").onchange = function () {
	document.getElementById("uploadFile").value = this.value;
    };

//    $('#classesID').change(function (event) {
//	var classesID = $(this).val();
//	if (classesID === '0') {
//	    $('#classesID').val(0);
//	} else {
//	    $.ajax({
//		type: 'POST',
//		url: "<?= base_url('student/sectioncall') ?>",
//		data: "id=" + classesID,
//		dataType: "html",
//		success: function (data) {
//		    $('#sectionID').html(data);
//		}
//	    });
//	}
//    });

    $(document).ready(function () {
	$('#show_password').click(function () {
	    $('#password').attr('type', $(this).is(':checked') ? 'text' : 'password');
	});

    });


</script>
