<?php
/**
 * Description of subject_subscriber
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
?>


<div class="col-sm-12 col-md-12">

    <div class="nav-tabs-custom">
	<ul class="nav nav-tabs">
	    <li class="active"><a data-toggle="tab" href="#all" aria-expanded="true"><?=$subject_name?> Subscribers</a></li>
	    <?php
	    foreach ($sections as $key => $section) {
		echo '<li class=""><a data-toggle="tab" href="#' . $section->sectionID . '" aria-expanded="false">' . $this->lang->line("student_section") . " " . $section->section . " ( " . $section->category . " )" . '</a></li>';
	    }
	    ?>
	</ul>



	<div class="tab-content">
	    <div id="all" class="tab-pane active">
		<div id="hide-table">
		    <!--Table one will be here with list of all student subscribe to that subject-->
		    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer tablesubscriber">
			<thead>
			    <tr>
				<th class="col-sm-2"><?= $this->lang->line('slno') ?></th>
				<th class="col-sm-2"><?= $this->lang->line('student_photo') ?></th>
				<th class="col-sm-2"><?= $this->lang->line('student_name') ?></th>
				<th class="col-sm-2"><?= $this->lang->line('student_roll') ?></th>
				<th class="col-sm-2"><?= $this->lang->line('student_section') ?></th>
				<th class="col-sm-2"><?= $this->lang->line('action') ?></th>
			    </tr>
			</thead>
			<tbody>
			    <?php
			    if (count($subject_students)) {
				$i = 1;
				foreach ($subject_students as $student) {
				    ?>
			    <tr id="std<?=$student->subject_student_id; ?>">
					<td data-title="<?= $this->lang->line('slno') ?>">
					    <?php echo $i; ?>
					</td>

					<td data-title="<?= $this->lang->line('promotion_photo') ?>">
					    <?php
					    $array = array(
						"src" => base_url('uploads/images/' . $student->photo),
						'width' => '35px',
						'height' => '35px',
						'class' => 'img-rounded'
					    );
					    echo img($array);
					    ?>
					</td>
					<td data-title="<?= $this->lang->line('student_name') ?>">
					    <?php echo $student->name; ?>
					</td>
					<td data-title="<?= $this->lang->line('student_roll') ?>">
					    <?php echo $student->roll; ?>
					</td>
					<td data-title="<?= $this->lang->line('student_section') ?>">
					    <?php echo $student->section; ?>
					</td>

					<td data-title="<?= $this->lang->line('action') ?>">
					    <a href="#" class="btn btn-danger btn-xs mrg" data-placement="top" data-toggle="tooltip" data-original-title="Delete" onmousedown="delete_subscriber(<?=$student->subject_student_id; ?>)"><i class="fa fa-trash-o"></i></a>
					</td>
				    </tr>
				    <?php
				    $i++;
				}
			    }
			    ?>
			</tbody>
		    </table>
		</div>

	    </div>

	    <?php foreach ($sections as $key => $section) { ?>
    	    <div id="<?= $section->sectionID ?>" class="tab-pane">
    		<div id="hide-table">
    		    <!--List of students in other sections, who are not in this course-->
    		    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer tablesubscriber">
    			<thead>
    			    <tr>
    				<th class="col-sm-2"><?= $this->lang->line('slno') ?></th>
    				<th class="col-sm-2"><?= $this->lang->line('student_photo') ?></th>
    				<th class="col-sm-2"><?= $this->lang->line('student_name') ?></th>
    				<th class="col-sm-2"><?= $this->lang->line('student_roll') ?></th>
    				<th class="col-sm-2"><?= $this->lang->line('student_section') ?></th>
    				<th class="col-sm-2"><?php
					echo '<input type="checkbox" class="all_promotion btn btn-warning" onclick="check_all(' . $section->sectionID . ')"> ' . $this->lang->line('action');
					?></th>
    			    </tr>
    			</thead>
    			<tbody>
				<?php
				if (count($allsection[$section->section])) {
				    $i = 1;
				    foreach ($allsection[$section->section] as $student) {
					?>
	    			    <tr>
	    				<td data-title="<?= $this->lang->line('slno') ?>">
						<?php echo $i; ?>
	    				</td>

	    				<td data-title="<?= $this->lang->line('promotion_photo') ?>">
						<?php
						$array = array(
						    "src" => base_url('uploads/images/' . $student->photo),
						    'width' => '35px',
						    'height' => '35px',
						    'class' => 'img-rounded'
						);
						echo img($array);
						?>
	    				</td>
	    				<td data-title="<?= $this->lang->line('student_name') ?>">
						<?php echo $student->name; ?>
	    				</td>
	    				<td data-title="<?= $this->lang->line('student_roll') ?>">
						<?php echo $student->roll; ?>
	    				</td>
	    				<td data-title="<?= $this->lang->line('student_section') ?>">
						<?php echo $student->section; ?>
	    				</td>

	    				<td data-title="<?= $this->lang->line('action') ?>">
					    <input type="checkbox" value="<?= $student->studentID; ?>" name="result<?= $student->studentID; ?>" id="" class="check<?= $section->sectionID ?>">

	    				</td>
	    			    </tr>
					<?php
					$i++;
				    }
				}
				?>
    			</tbody>
    		    </table>
    		    <button class="btn btn-success" onclick="subscribe(<?= $section->sectionID ?>)">Subscribe</button>
    		</div>
    	    </div>
	    <?php } ?>
	</div>

    </div> <!-- nav-tabs-custom -->
</div> <!-- col-sm-12 for tab -->
<!-- Jquery datatable js -->
<script type="text/javascript" src="<?php echo base_url('assets/datatables/jquery.dataTables.js'); ?>"></script>
<!-- Datatable js -->
<script type="text/javascript" src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js'); ?>"></script>

<script type="text/javascript">
			$(document).ready(function () {
			    $('.tablesubscriber').DataTable();
			    $('.input-sm').css({'width': '90%'})
			});
		
			function check_all(a) {
			    if ($('.check' + a).is(":checked")) {
				$('.check' + a).prop('checked', false);
			    } else {
				$('.check' + a).prop('checked', true);
			    }
			}
			delete_subscriber=function(a){
			     $.ajax({
				type: 'POST',
				url: "<?= base_url('subjectstudent/delete') ?>",
				data: "id=" + a,
				dataType: "html",
				success: function (data) {
				     swal('success',data,'success');
				     $('#std'+a).hide();
				}
			    });
			}
			subscribe = function (a) {
			    var subjectID = <?= $set ?>;
			    var result = $('.check'+a+':checked').map(function () {
				return this.value;
			    }).get();
			    $.ajax({
				type: 'POST',
				url: "<?= base_url('subjectstudent/add') ?>",
				data: "student_ids=" + result + "&subject_id=" + subjectID,
				dataType: "html",
				success: function (data) {
				    //window.location.replace($redirect);
				    swal('success',data,'success');
				}
			    });

			};

</script>