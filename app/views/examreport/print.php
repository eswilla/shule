<?php
/**
 * Description of print
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
require_once __DIR__ . '../../../../app/controllers/mark.php';
$usertype = $this->session->userdata("usertype");
$section_id = $this->input->get_post('section_id'); //otherwise when we specify to provide report by section and by class
?>
<!DOCTYPE html>
<html lang="en">
    <head>
	<meta charset="utf-8">
	<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<title>report</title>

	<link href="<?php echo base_url('assets/css/print_all/certificate.css'); ?>" rel="stylesheet" type="text/css">

	<link href="<?php echo base_url('assets/print.css'); ?>" rel="stylesheet" type="text/css">

	<link rel="shortcut" href="<?= base_url(); ?>assets/images/favicon.ico">

	<!-- Begin embedded font definitions -->
	<style id="fonts1" type="text/css" >

	    @font-face {
		font-family: DejaVuSans_b;
		src: url("../../assets/fonts/DejaVuSans_b.woff") format("woff");
	    }
	    table.table.table-striped td,table.table.table-striped th, table.table {
		border: 1px solid #000000;
		margin: 1px auto !important;
	    }

	    #report_footer{
		margin-left: 2em;
	    }
	    .headings{
		margin-left: 2em;
	    }
	    .school_logo{
		float: left; height: 150px; width: 20%
	    }
	    .head_letter_photo{
		float: right; height: 150px; width: 79%
	    }
	    .school_info_heading{
		float: left; text-align: center; width: 77%; height: 149px;
	    }
	    .student_photo{
		float: right; width: 22%; height: 149px;
	    }
	    @media print {
		/*.panel{*/
		/*padding-left: 10%;*/
		/*}*/
		/*table.table.table-striped td,table.table.table-striped th, table.table {*/
		/*border: 1px solid #000000 !important;*/
		/*margin: 1px auto !important;*/
		/*}*/
		/*.table-stripe {*/
		/*background-color: #dedede !important;*/
		/*border: 1px solid #000000 !important;*/
		/*}*/
		/*body, .panel {*/
		/*margin: 0;*/
		/*box-shadow: 0;*/
		/*}*/
		/*.headings{*/
		/*margin-left:2em;*/
		/*}*/
		/*#t7_1{*/
		/*width: 23em !important;*/
		/*}*/
		#wrappers{
		    page-break-before: always;
		}
		h3, h4 {
		    page-break-after: avoid;
		    font-size: 15px;
		    line-height: 18px;
		}

	    }
	    h3, h4 {
		page-break-after: avoid;
		font-size: 15px;
		line-height: 18px;
	    }
	</style>
	<?php if ($level->result_format == 'CSEE') { ?>
    	<style  type="text/css" >
    	    .panel{
    		color: #000000 !important;

    	    }
    	    table.table.table-striped td,table.table.table-striped th, table.table {
    		font-size: 12px !important;
    		margin-left: 10%;
    	    }
    	</style>
	<?php } ?>
    </head>
    <body onload="window.print()">
	<?php foreach ($students as $student) { ?>
    	<div class="row">
    	    <div class="col-md-12">

    		<div id="wrappers" style="overflow:hidden;" >


    		    <section class="panel">

    			<div >
				<?php
				$array = array(
				    "src" => base_url('uploads/images/' . $siteinfos->photo),
				    'width' => '100%',
				    'height' => '100%',
				    'class' => 'img-rounded',
				);
				$photo = array(
				    "src" => base_url('uploads/images/' . $student->photo),
				    'width' => '100%',
				    'height' => '100%',
				    'class' => 'img-rounded',
				);
				?>
    			    <div class="row">
    				<div class="school_logo">
					<?php echo img($array);
					?>
    				</div>
				<div class="head_letter_photo">
				    <div class="school_info_heading">
    					<br/>
					<h2 style="font-weight: bolder; font-size: 25px;" class="text-uppercase"><?= $siteinfos->sname ?></h2>
    					<h4><?= 'P.O BOX ' . $siteinfos->box . ', ' . $siteinfos->address ?>.</h4>
    					<h3>Cell: <?= str_replace(',', ' / ', $siteinfos->phone) ?></h3>
    					<h3> Email:<?= $siteinfos->email ?></h3>
    					<h3>Website: <?= $siteinfos->website ?></h3>
    				    </div>
				    <div class="student_photo">
					    <?php echo img($photo);
					    ?>
    				    </div>
    				    <div style="clear: both"></div>
    				</div>
    				<div style="clear: both"></div>
    			    </div>
    			    <hr/>

    			</div>

    			<div class="">
    			    <h1 align='center' style="font-size:20px;  font-weight: bolder;padding-top: 0%; ">STUDENT PROGRESSIVE REPORT (<?= strtoupper($classes->classes) . ' ' . $student->year ?>)</h1>
    			</div>

    			<div class="panel-body profile-view-dis" style="margin-top: .4em; padding: 0 5%;">
    			    <div class="row">
    				<div class="col-sm-9" style="float:left;" >
    				    <h4 style="font-weight: bolder;"> NAME: <?= strtoupper($student->name) ?></h4>
    				    <h4 style="margin-top: 2%;"> Admission No: <?= $student->roll ?></h4>
    				</div>
    				<div class="col-sm-3" style="float: right; font-weight: bolder;">
    				    <h6><?=
					    'STREAM: ' . $student->section
					    ?></h6>
    				</div>
    				<div style="clear:both;"></div>
    			    </div>

    			</div>

			    <?php if (count($exams) > 0 && !empty($exams)) {
				?>

				<div class="row"  style="margin: 2%;">
				    <?php
				    //print_r($marks);
				    if ($marks[$student->studentID] && $exams) {

					$examController = new Mark();
					?>

	    			    <div class="col-lg-12">

					    <?php
					    $map1 = function($r) {
						return intval($r->examID);
					    };
					    $marks_examsID = array_map($map1, $marks[$student->studentID]);
					    $max_semester = max($marks_examsID);

					    $map2 = function($r) {
						return intval($r->examID);
					    };
					    $examsID = array_map($map2, $exams);

					    $map3 = function($r) {
						return array("mark" => intval($r->mark), "semester" => $r->examID);
					    };
					    $all_marks = array_map($map3, $marks[$student->studentID]);

					    $map4 = function($r) {
						return array("gradefrom" => $r->gradefrom, "gradeupto" => $r->gradeupto);
					    };
					    $grades_check = array_map($map4, $grades);


					    echo "<table  class=\"table table-striped table-bordered\" style=\"margin-bottom: 6%;\">";
					    if ($exams->examID <= $max_semester) {

						$check = array_search($exams->examID, $marks_examsID);

						if ($check >= 0) {
						    $f = 0;
						    foreach ($grades_check as $key => $range) {
							foreach ($all_marks as $value) {
							    if ($value['semester'] == $exams->examID) {
								if ($value['mark'] >= $range['gradefrom'] && $value['mark'] <= $range['gradeupto']) {
								    $f = 1;
								}
							    }
							}
							if ($f == 1) {
							    break;
							}
						    }


						    echo "<thead>";
						    echo "<tr>";
						    echo "<th style='font-weight: bolder;'>";
						    echo strtoupper($this->lang->line("mark_subject"));
						    echo "</th>";
						    echo "<th style='font-weight: bolder;'>";
						    echo strtoupper($exams->exam);
						    echo "</th>";
						    if (count($grades) && $f == 1) {
							echo "<th style='font-weight: bolder;'>";
							echo strtoupper($this->lang->line("mark_point"));
							echo "</th>";
							echo "<th style='font-weight: bolder;'>";
							echo strtoupper($this->lang->line("mark_grade"));
							echo "</th>";
						    }
						    echo "<th style='font-weight: bolder;'>";
						    echo strtoupper('Grade Status');
						    echo "</th>";
						    echo "<th style='font-weight: bolder;'>";
						    echo strtoupper($section_id == '' ? 'Position in  Class:' : 'Position in  Stream:');
						    echo "</th>";
						    if (strtolower($usertype) == 'parent') {
							echo "<th style='font-weight: bolder;'>";
							echo 'Position in Stream ';
							echo "</th>";
						    }
						    echo "</tr>";

						    echo "</thead>";
						}
					    }

					    echo "<tbody>";

					    foreach ($marks[$student->studentID] as $mark) {
						if ($exams->examID == $mark->examID) {
						    echo "<tr>";
						    echo "<td data-title='" . $this->lang->line('mark_subject') . "' style='font-weight: bolder;'>";
						    echo strtoupper($mark->subject);
						    echo "</td>";
						    echo "<td data-title='" . $this->lang->line('mark_mark') . "'>";
						    echo $mark->mark;
						    echo "</td>";
						    if (count($grades)) {
							foreach ($grades as $grade) {
							    if ($grade->gradefrom <= $mark->mark && $grade->gradeupto >= $mark->mark) {
								echo "<td data-title='" . $this->lang->line('mark_point') . "'>";
								echo $grade->point;
								echo "</td>";
								echo "<td data-title='" . $this->lang->line('mark_grade') . "'>";
								echo $grade->grade;
								echo "</td>";
								break;
							    }
							}
						    }

						    echo "<td data-title='" . $this->lang->line('mark_mark') . "'>";
						    echo $grade->note;
						    echo "</td>";

						    echo "<td data-title='" . $this->lang->line('mark_mark') . "'>";

						    $subject_info = $examController->get_position($mark->studentID, $mark->examID, $mark->subjectID, $mark->classesID, $student->sectionID);
						    echo $subject_info->rank;
						    echo "</td>";
						    if (strtolower($usertype) == 'parent') {
							$subject_info_insection = $examController->get_position($mark->studentID, $mark->examID, $mark->subjectID, $mark->classesID, $student->sectionID);
							echo "<td data-title='" . $this->lang->line('mark_mark') . "'>";
							echo $subject_info_insection->rank;
							echo "</td>";
						    }
						    echo "</tr>";
						}
					    }
					    echo "</tbody>";
					    echo "</table>";
					    if (strtolower($usertype) == 'parent') {
						$sectionreport = $examController->get_rank_per_all_subjects($exams->examID, $student->classesID, $student->studentID, $student->sectionID);
						$totalstudent_insection = $examController->get_student_per_class($student->classesID, $student->sectionID);
					    }
					    ?>



	    				<table class="table table-striped table-bordered" style=" width: 100%; margin: 0%;">
	    				    <thead>
	    					<tr>
	    					    <th style="font-weight: bolder; width:25.4%;">TOTAL:
	    						<strong><?= $report[$student->studentID]->sum ?></strong></th>
	    					    <th style="font-weight: bolder;width:33.4%;">AVERAGE MARK:
	    						<strong><?= round($report[$student->studentID]->average, 1) ?></strong></th>
	    					    <th style="font-weight: bolder">Position
	    						In <?= $section_id == '' ? 'Class' : 'Stream' ?>
	    						<strong><?= $report[$student->studentID]->rank ?></strong> out of <?= count($students) ?></th>
							<?= strtolower($usertype) == 'parent' ? '<th style="font-weight: bolder">Position in Stream:<strong>' . $sectionreport->rank . '</strong> out of <strong>' . $totalstudent_insection->total_student . '</strong></th>' : '' ?>
	    					<!--								<th style="font-weight: bolder">Points</th>-->
	    					<!--								<th style="font-weight: bolder">Division</th>-->
	    					</tr>
	    				    </thead>
	    				    <tbody>
	    				    <!--                            <tr>-->
	    				    <!--                                <td><? //= $report->sum ?><!--</td>-->
	    				    <!--                                <td>--<? //= round($report->average, 1) ?><!--</td>-->
	    				    <!--                                <td>--<? //= count($total_students) ?><!--</td>-->
	    					<!--                                -<? //= strtolower($usertype) == 'parent' ? '<td>' . $totalstudent_insection->total_student . '</td>' : '' ?>
	    					<!--                                <td>-<? //= $report->rank ?><!--</td>-->
	    					<!--                                --<? //= strtolower($usertype) == 'parent' ? '<td>' . $sectionreport->rank . '</td>' : '' ?>
	    					<!--        <td>--<?php //	echo $points
							?><!--</td>--<td>--<?php //echo $division ?><!--<!--</td>-->
	    					<!---->
	    					<!--                            </tr>-->
	    				    </tbody>
	    				</table>

	    				<div style="float: left; width: 50%">
	    				    <table class="table table-striped table-bordered" style="  float: left; margin: 1% 0 0 0;">
	    					<thead>
	    					    <tr>
	    						<th style="font-weight: bolder; text-align: center; border-bottom: 1px solid #fff;">
	    						    GRADING
	    						</th>
	    					    </tr>
	    					</thead>
	    				    </table>

	    				    <table class="table table-striped table-bordered" style=" margin-bottom: 1%;">
	    					<thead>
	    					    <tr>
							    <?php foreach ($grades as $grade) { ?>
								<th style="font-weight: bolder; text-align: center;"><?= $grade->grade ?>
								</th>
							    <?php } ?>
	    					    </tr>
	    					</thead>
	    					<tbody>
	    					    <tr>
							    <?php foreach ($grades as $grade) { ?>
								<td style="font-size: smaller; text-align: left;background-color: white;color: #000;"><?= $grade->gradefrom ?>
								    - <?= $grade->gradeupto ?>
								</td>
							    <?php } ?>
	    					    </tr>
	    					</tbody>

	    				    </table>
	    				</div>
	    				<div style="float: right; width: 49.6%">
	    				    <table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
	    					<thead>
	    					    <tr>
	    						<th style="font-weight: bolder; "><strong>KEY:</strong></th>
	    					    </tr>
	    					</thead>
	    					<tbody>
	    					    <tr>
	    						<td style="background-color: white;color: #000;"><strong>NOTES:</strong> <span
	    							style="font-size: smaller">Here you will put notes to the key because</span
	    						</td>
	    					    </tr>
	    					</tbody>
	    				    </table>
	    				</div>
	    				<table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
	    				    <thead>
	    					<tr>
	    					    <th style="font-weight: bolder; width:56%;"><strong style="vertical-align: top;">General
	    						    Remarks:</strong><br> <?php
							    foreach ($grades as $grade) {
								if ($grade->gradefrom <= round($report->average, 0) && $grade->gradeupto >= round($report->average, 0)) {
								    echo "<span class='bolder' style='border-bottom: 1px solid black'>";
								    echo $grade->overall_note;
								    echo "</span>";
								    break;
								}
							    }
							    ?></th>
	    					    <th> Class Teacher's Signature<br><img src="<?= $class_teacher_signature ?>" width="54"
	    										   height="32"></th>
	    					</tr>
	    				    </thead>
	    				</table>
	    				<table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
	    				    <thead>
	    					<tr>
	    					    <th style="font-weight: bolder; width: 56%; "><strong style="vertical-align: top;">
	    						    Head Teacher's comments:</strong><br> <?php
							    foreach ($grades as $grade) {
								if ($grade->gradefrom <= round($report->average, 0) && $grade->gradeupto >= round($report->average, 0)) {
								    echo "<span class='bolder' style='border-bottom: 1px solid black'>";
								    echo $grade->overall_note;
								    echo "</span>";
								    break;
								}
							    }
							    ?></th>
	    					    <th style="width: 50%;">Signature<br></th>
	    					</tr>
	    				    </thead>
	    				</table>
	    				<table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
	    				    <thead>
	    					<tr>
	    					    <th style="text-align: center;">Date of Reporting:
	    						<b><?= $level->result_format != 'CSEE' ? '9th' : '3th' ?> January, 2017 </b>
	    					    </th>
	    					</tr>
	    				    </thead>
	    				</table>

	    			    </div>
				    <?php } ?>
				    <div style="margin-left: 3%; margin-top: 2%; font-size: 110%; line-height: 21px;">

					<div style="padding-left:5%;">

					    <div style="z-index: 4000">
						<div style="float: right; margin-right: 4%; margin-top: 4%;"></div>
						<div><img src="../../assets/images/stamp_<?=  set_schema_name()?>png"
							  width="100" height="100"
							  style="position:relative; margin:-19% 15% 0 0; float:right;"></div>

					    </div>

					</div>


				    </div>
				</div>


			    <?php } ?>
    		    </section>

    		</div>


    	    </div>
    	</div>

	    <?php
	}
	/*      $html = ob_get_clean();
	  $dompdf = new DOMPDF();
	  $dompdf->load_html($html);
	  $dompdf->render();
	  $dompdf->stream('output.pdf'); */
	?>
    </body>
</html>
<script>
    function nocontext(e) {
	var clickedTag = (e === null) ? event.srcElement.tagName : e.target.tagName;
	if (clickedTag === "IMG") {
	    alert(alertMsg);
	    return false;
	}
    }
    var alertMsg = "Please this document is copyrighted";
    document.oncontextmenu = nocontext;
    //]]>
</script></script>


