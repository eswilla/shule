
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa fa-star"></i> <?=$this->lang->line('panel_title')?></h3>

        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active"><?=$this->lang->line('examreport_section')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">

                <?php 
                    $usertype = $this->session->userdata("usertype");
                ?>
                    

                <div class="col-sm-6 col-sm-offset-3 list-group">
                    <div class="list-group-item list-group-item-warning">
                        <form style="" class="form-horizontal" role="form" method="post">  
                            <div class="form-group">              
                                <label for="classesID" class="col-sm-2 col-sm-offset-2 control-label">
                                    <?=$this->lang->line("subject_classes")?>
                                </label>
                                <div class="col-sm-6">
                                    <?php
                                        $array = array("0" => $this->lang->line("examreport_select_classes"));
                                        foreach ($classes as $classa) {
                                            $array[$classa->classesID] = $classa->classes;
                                        }
                                        echo form_dropdown("classesID", $array, set_value("classesID", $set), "id='classesID' class='form-control'");
                                    ?>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div id="hide-table">
                    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead>
                            <tr>
                                <th class="col-lg-2"><?=$this->lang->line('slno')?></th>
                                <th class="col-lg-2"><?=$this->lang->line('examreport_name')?></th>
                                <th class="col-lg-2"><?=$this->lang->line('examreport_date')?></th>
                                <th class="col-lg-2"><?=$this->lang->line('examreport_exams_combined')?></th>
                                <!--<th class="col-lg-2"><?=$this->lang->line('action')?></th>-->
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(isset($examreports)) {$i = 1; foreach($examreports as $report) { ?>
                                <tr>
                                    <td data-title="<?=$this->lang->line('slno')?>">
                                        <?php echo $i; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('examreport_name')?>">
                                        <?php echo $report->name; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('examreport_date')?>">
                                        <?php  echo date('d M Y',strtotime($report->regdate));  ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('examreport_exams_combined')?>">
                                        <?php 
					
					$exam_ids=  explode('_', $report->combined_exams);
					$name='';
					foreach ($exam_ids as $exam_id) {
					    $exam=new ExamReport();
					    $name .=$exam->getExamName($exam_id).', ';
					}
					echo rtrim($name,', ');
					?>
                                    </td>
                                    <!--<td data-title="<?=$this->lang->line('action')?>">-->
                                        <?php //echo btn_edit('section/edit/'.$section->sectionID.'/'.$set, $this->lang->line('edit')) ?>
                                        <?php //echo btn_delete('section/delete/'.$section->sectionID.'/'.$set, $this->lang->line('delete')) ?>
                                    <!--</td>-->
                                </tr>
                            <?php $i++; }} ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#classesID').change(function() {
        var classesID = $(this).val();
        if(classesID == 0) {
            $('#hide-table').hide();
        } else {
            $.ajax({
                type: 'POST',
                url: "<?=base_url('examreport/getReportList')?>",
                data: "id=" + classesID,
                dataType: "html",
                success: function(data) {
                    window.location.href = data;
                }
            });
        }
    });
</script>
