<?php
/**
 * Description of singlecombined
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
?>
<link href="<?php echo base_url('assets/print.css'); ?>" rel="stylesheet" type="text/css">
<?php
require_once __DIR__ . '../../../../app/controllers/mark.php';
$usertype = $this->session->userdata("usertype");

if ($usertype == "Admin") {
    ?>
    <div class="well">
        <div class="row">
    	<div class="col-sm-6">
    	    <button class="btn-cs btn-sm-cs" onclick="javascript:printDiv('printablediv')"><span
    		    class="fa fa-print"></span> <?= $this->lang->line('print') ?> </button>

    	    <a href="<?= base_url("exam/printall_combined/?class_id=" . $student->classesID . '&section_id=' . $student->sectionID) . '&exam=' . $this->input->get_post('exam') ?>"
    	       target="_blank"><i
    		    class="fa fa-laptop"></i><?= $this->lang->line('print') ?> all reports</a>


    	    <button class="btn-cs btn-sm-cs" data-toggle="modal" data-target="#mail"><span
    		    class="fa fa-envelope-o"></span> <?= $this->lang->line('mail') ?></button>
    	</div>
    	<div class="col-sm-6">
    	    <ol class="breadcrumb">
    		<li><a href="<?= base_url("dashboard/index") ?>"><i
    			    class="fa fa-laptop"></i> <?= $this->lang->line('menu_dashboard') ?></a></li>
    		<li><a href="<?= base_url("parentes/index") ?>"><?= $this->lang->line('menu_parent') ?></a></li>
    		<li class="active"><?= $this->lang->line('view') ?></li>
    	    </ol>
    	</div>
        </div>
    </div>
<?php } ?>

<div id="printablediv" class="page center">
    <section class="subpage">
	<div id="p1" style="overflow: hidden; left: 5%;" class="">


	    <!-- Begin inline CSS -->
	    <style type="text/css">

		.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td {
		    font-weight: bolder
		}

		#printablediv {
		    color: #000;
		}

	    </style>
	    <!-- End inline CSS -->

	    <!-- Begin embedded font definitions -->
	    <style id="fonts1" type="text/css">


		table.table.table-striped td, table.table.table-striped th, table.table {
		    border: 1px solid #000000;
		}


	    </style>


	    <!-- Begin page background -->


	    <?php
	    $array = array(
		"src" => base_url('uploads/images/' . $siteinfos->photo),
		'width' => '126em',
		'height' => '126em',
		'class' => 'img-rounded',
		'style' => 'margin-left:2em'
	    );
	    $photo = array(
		"src" => base_url('uploads/images/' . $student->photo),
		'width' => '128em',
		'height' => '128em',
		'class' => 'img-rounded',
		'style' => 'margin-left:2em'
	    );
	    ?>

	    <table class=" table-striped center"style="margin: 1px 2px 1px 0px;">
		<thead>
		    <tr>
			<th>
		    <?php echo img($array);
		    ?>
		</th>
		<th>
		<div class="col-md-12 text-center" style="font-size:17px;">
		    <h2 style="font-weight: bolder" class="text-uppercase"><?= $siteinfos->sname ?></h2>
		    <h4><?= 'P.O BOX ' . $siteinfos->box . ', ' . $siteinfos->address ?>.</h4>
		    <h3>Cell: <?= str_replace(',', ' / ', $siteinfos->phone) ?></h3>
		    <h3> Email:<?= $siteinfos->email ?></h3>
		    <h3>Website: <?= $siteinfos->website ?></h3>
		</div>
	    </th>
		<th>
		    <?php echo img($photo);
		    ?>
		</th>
		</tr>
		</thead>
	    </table>
	    <hr/>
	</div>

	<div class="">
	    <h1 align='center' style="font-size: 19px; margin-top: 0.7em; font-weight: bolder">STUDENT PROGRESSIVE
		REPORT (<?= strtoupper($classes->classes) . ' ' . $student->year ?>)</h1>
	</div>
	<div <?php if ($level->result_format != 'CSEE') { ?> class="panel-body profile-view-dis" <?php } ?>>
	    <div class="row" style="margin: 2%;">
		<div class="col-sm-6" style="float:left;">
		    <h1 style="font-weight: bolder;font-size: 1.4em;"> NAME: <?= $student->name ?></h1>
		    <h4 style="margin-top: 2%;"> Admission No: <?= $student->roll ?></h4>
		</div>
		<div class="col-sm-3" style="float: right;">
		    <h1 style="font-weight: bolder; font-size: 1.4em;"><?=
			(isset($section_id) && $section_id == '') ?
				'CLASS: ' . strtoupper($classes->classes) . '' :
				'STREAM: ' . $student->section
			?></h1>
		</div>
		<div style="clear:both;"></div>
	    </div>

	</div>

	<?php if (count($exams) > 0 && !empty($exams)) {
	    ?>

    	<div class="row" style="margin:0 2%; ">
		<?php
		if ($marks && $exams) {

		    $examController = new Exam();
		    ?>
		    <div class="col-lg-12">
			<table class="table table-striped table-bordered center" style="margin-bottom:-0.1%">
			    <thead>
				<tr>
				    <th style="font-weight: bolder">SUBJECT</th>

				    <?php foreach ($exams as $exam) { ?>
	    			    <th style="font-weight: bolder"><?= strtoupper($exam->exam) ?></th>
				    <?php } ?>

				    <th style="font-weight: bolder">AVERAGE</th>
				    <th style="font-weight: bolder">GRADE</th>
				    <?php if ($level->result_format == 'ACSEE') { ?>
	    			    <th style="font-weight: bolder">POINTS</th>
				    <?php } ?>
				    <th style="font-weight: bolder">REMARKS</th>
				</tr>
			    </thead>
			    <tbody>
				<?php
				$total_marks = 0;
				$total_subjects_done = 0;
				foreach ($subjects as $subject) {
				    ?>
	    			<tr>
	    			    <td style="font-weight: bolder"><?= strtoupper($subject->subject) ?></td>

					<?php
					$total = 0;
					$penalty = 0;
					$exam_done = 0;
					foreach ($exams as $exam) {

					    $mark = $examController->get_total_marks($student->studentID, $exam->examID, $subject->classesID, $subject->subjectID);
					    $total += $mark->total_mark;

					    /*
					     * check penalty for that subject
					     */
					    if ($subject->is_penalty == 1) {
//						foreach ($grades as $grade) {
//						    if ($grade->gradefrom == 0) {
//							if ($mark->total_mark < $grade->gradeupto) {
//							    $penalty = 1;
//							}
//						    }
//						}

						$penalty = $mark->total_mark < $subject->pass_mark ? 1 : 0;
					    }
					    ?>
					    <th><?= $mark->total_mark == 0 ? '-' : $mark->total_mark ?></th>
					    <?php
					    if ($mark->total_mark == '' || $mark->total_mark == 0) { //later we will change this condition to match exactly case for student who do the subject
					    } else {
						$exam_done++;
					    }
					}
					$avg = $exam_done != 0 ? round($total / $exam_done, 1) : 0;
					?>

	    			    <td><?= $avg == 0 ? '-' : $avg ?></td>

					<?php
					if ($avg == 0) {
					    //this is a short time solution, we put empty but
					    //later on we need to see if this student takes this subject or not
					    echo '<td></td><td></td>';
					} else {
					    echo return_grade($grades, round($avg), $level->result_format);
					}
					$total_marks += $avg;
					?>
	    			</tr>
				    <?php
				    is_float($avg) ? $total_subjects_done++ : 0;
				}

				/**
				 *
				 *
				 * ----------------------------------------------------------------
				 *
				 * We show Division & points only to A-level students. Those with
				 * grade format of ACSEE
				 */
				if ($level->result_format == 'ACSEE') {
				    ?>

	    			<tr>
	    			    <td style="font-weight: bolder">DIVISION</td>
					<?php
					$i = 0;
					$total_points = 0;
					foreach ($exams as $exam) {
					    $division[$i] = $examController->getDivision($student->studentID, $exam->examID);
					    $rank_info = $examController->get_rank_per_all_subjects($exam->examID, $student->classesID, $student->studentID);
					    ?>
					    <td><?= $division[$i][0] ?></td>
					    <?php
					    $total_points += $division[$i][1];
					    $i++;
					}
					$total_i = $i;
					$total_average_points = floor($total_points / $total_i);
					if ($level->result_format == 'CSEE') {
					    $div = cseeDivision($total_average_points, $penalty);
					} elseif ($level->result_format == 'ACSEE') {
					    $div = acseeDivision($total_average_points, $penalty);
					}
					?>
	    			    <td><?= $div ?></td>
	    			    <td></td>
	    			    <td></td>
	    			    <td></td>
	    			</tr>
	    			<tr>
	    			    <td style="font-weight: bolder">POINTS</td>
					<?php
					for ($i = 0; $i < $total_i; $i++) {
					    ?>
					    <td><?= $division[$i][1] ?></td>
					    <?php
					}
					?>
	    			    <td><?= $total_average_points ?></td>
	    			    <td></td>
	    			    <td></td>
	    			    <td></td>
	    			</tr>

				    <?php
				}

				/**
				 * Otherwise, we don't show points and division
				 * ------------------------------------------------------
				 */
				$section = isset($section_id) && $section_id == '' ? 'CLASS' : 'STREAM';
				$sectionID = $section == 'CLASS' ? NULL : $student->sectionID;
				?>
				<tr>
				    <td style="font-weight: bolder">POSITION IN <?= $section ?></td>
				    <?php
				    foreach ($exams as $exam) {

					$rank_info = $examController->get_rank_per_all_subjects($exam->examID, $student->classesID, $student->studentID, $sectionID);
					?>
	    			    <td><?= $rank_info->rank ?></td>
					<?php
				    }
				    ?>
				    <td><?= $rank ?></td>
				    <?= $level->result_format == 'ACSEE' ? "<td></td>" : '' ?>
				    <td></td>
				    <td></td>
				</tr>
			    </tbody>
			</table>


			<table class="table table-striped table-bordered" style=" width: 100%; margin: 0%;">
			    <thead>
				<tr>
				    <th style="font-weight: bolder; width:25.4%;">TOTAL:
					<strong><?= $total_marks ?></strong></th>
				    <th style="font-weight: bolder;width:33.4%;">AVERAGE MARK:
					<strong><?php
					    $total_average = round($total_marks / $total_subjects_done, 1);
					    echo $total_average;
					    ?></strong></th>
				    <th style="font-weight: bolder">Position
					In <?= isset($section_id) && $section_id == '' ? 'Class:' : 'Stream:' ?>
					<strong><?= $rank ?></strong> out of <?= count($students) ?></th>
				<!--								<th style="font-weight: bolder">Points</th>-->
				<!--								<th style="font-weight: bolder">Division</th>-->
				</tr>
			    </thead>
			    <tbody>
			    <!--             <tr>-->
			    <!--          <td><? $report->sum ?></td>-->
			    <!--    <td><? round($report->average, 1) ?><!--</td>-->
			    <!--     <td><? count($total_students) ?><!--</td>-->
				<!--         <? strtolower($usertype) == 'parent' ? '<td>' . $totalstudent_insection->total_student . '</td>' : '' ?>
				<!--    <td><? $report->rank ?><!--</td>-->
				<!--     <? strtolower($usertype) == 'parent' ? '<td>' . $sectionreport->rank . '</td>' : '' ?>
				<!--      <td>--><?php //echo $points  ?><!--<!--</td>-->
				<!--             <!--<td>-->
				<?php
				//                		echo $division
				//                               					
				// 
				?><!--</td>-->
				<!---->
				<!--                            </tr>-->
			    </tbody>
			</table>

			<div style="float: left; width: 50%">
			    <table class="table table-striped table-bordered" style="  float: left; margin: 1% 0 0 0;">
				<thead>
				    <tr>
					<th style="font-weight: bolder; text-align: center; border-bottom: 1px solid #fff;">
					    GRADING
					</th>
				    </tr>
				</thead>
			    </table>

			    <table class="table table-striped table-bordered" style=" margin-bottom: 1%;">
				<thead>
				    <tr>
					<?php foreach ($grades as $grade) { ?>
	    				<th style="font-weight: bolder; text-align: center;"><?= $grade->grade ?>
	    				</th>
	<?php } ?>
				    </tr>
				</thead>
				<tbody>
				    <tr>
					<?php foreach ($grades as $grade) { ?>
	    				<td style="font-weight: bold;font-size: smaller; text-align: center;background-color: white;color: #000;"><?= $grade->gradefrom ?>
	    				    - <?= $grade->gradeupto ?>
	    				</td>
	<?php } ?>
				    </tr>
				</tbody>

			    </table>
			</div>
			<div style="float: right; width: 49.6%">
			    <table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
				<thead>
				    <tr>
					<th style="font-weight: bolder; "><strong>KEY:</strong></th>
				    </tr>
				</thead>
				<tbody>
				    <tr>
					<td style="background-color: white;color: #000;"><strong>NOTES:</strong> <span
						style="font-size: smaller"></span
					</td>
				    </tr>
				</tbody>
			    </table>
			</div>
			<table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
			    <thead>
				<tr>
				    <th style="font-weight: bolder; width:56%;"><strong style="vertical-align: top;">General
					    Remarks:</strong><br> <?php
					foreach ($grades as $grade) {
					    if ($grade->gradefrom <= round($report->average, 0) && $grade->gradeupto >= round($report->average, 0)) {
						echo "<span class='bolder' style='border-bottom: 1px solid black'>";
						echo $grade->overall_note;
						echo "</span>";
						break;
					    }
					}
					?></th>
				    <th>Class Teacher's Signature<br><img src="<?= $class_teacher_signature ?>" width="54"
									  height="32"></th>
				</tr>
			    </thead>
			</table>
			<table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
			    <thead>
				<tr>
				    <th style="font-weight: bolder; width: 56%; "><strong style="vertical-align: top;">Head
					    Teacher's comments:</strong><br> <?php
					foreach ($grades as $grade) {
					    if ($grade->gradefrom <= round($report->average, 0) && $grade->gradeupto >= round($report->average, 0)) {
						echo "<span class='bolder' style='border-bottom: 1px solid black'>";
						echo $grade->overall_note;
						echo "</span>";
						break;
					    }
					}
					?></th>
				    <th style="width: 50%;">Signature<br></th>
				</tr>
			    </thead>
			</table>
			<table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
			    <thead>
				<tr>
				    <th style="text-align: center;">Date of Reporting:
					<b><?= $level->result_format != 'CSEE' ? '9th' : '3th' ?> January, 2017 </b>
				    </th>
				</tr>
			    </thead>
			</table>

		    </div>
    <?php } ?>

    	</div>
    	<div style="padding-left:5%;">

    	    <div style="z-index: 4000">
    		<div style="float: right; margin-right: 4%; margin-top: 4%;"></div>
		<div><img src="../../assets/images/stamp_<?=  set_schema_name()?>png"
    			  width="100" height="100"
    			  style="position:relative; margin:-17% 15% 0 0; float:right;"></div>

    	    </div>

    	</div>

<?php } ?>
    </section>
    <div class="page-break"></div>
</div>

<?php if ($usertype == "Admin") { ?>
    <!-- email modal starts here -->
    <form class="form-horizontal" role="form" action="<?= base_url('parentes/send_mail'); ?>" method="post">
        <div class="modal fade" id="mail">
    	<div class="modal-dialog">
    	    <div class="modal-content">
    		<div class="modal-header">
    		    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
    			    class="sr-only">Close</span></button>
    		    <h4 class="modal-title"><?= $this->lang->line('mail') ?></h4>
    		</div>
    		<div class="modal-body">

			<?php
			if (form_error('to'))
			    echo "<div class='form-group has-error' >";
			else
			    echo "<div class='form-group' >";
			?>
    		    <label for="to" class="col-sm-2 control-label">
    <?= $this->lang->line("to") ?>
    		    </label>
    		    <div class="col-sm-6">
    			<input type="email" class="form-control" id="to" name="to" value="<?= set_value('to') ?>">
    		    </div>
    		    <span class="col-sm-4 control-label" id="to_error">
    		    </span>
    		</div>

		    <?php
		    if (form_error('subject'))
			echo "<div class='form-group has-error' >";
		    else
			echo "<div class='form-group' >";
		    ?>
    		<label for="subject" class="col-sm-2 control-label">
    <?= $this->lang->line("subject") ?>
    		</label>
    		<div class="col-sm-6">
    		    <input type="text" class="form-control" id="subject" name="subject"
    			   value="<?= set_value('subject') ?>">
    		</div>
    		<span class="col-sm-4 control-label" id="subject_error">
    		</span>

    	    </div>

		<?php
		if (form_error('message'))
		    echo "<div class='form-group has-error' >";
		else
		    echo "<div class='form-group' >";
		?>
    	    <label for="message" class="col-sm-2 control-label">
    <?= $this->lang->line("message") ?>
    	    </label>
    	    <div class="col-sm-6">
    		<textarea class="form-control" id="message" style="resize: vertical;" name="message"
    			  value="<?= set_value('message') ?>"></textarea>
    	    </div>
    	</div>


        </div>
        <div class="modal-footer">
    	<button type="button" class="btn btn-default" style="margin-bottom:0px;"
    		data-dismiss="modal"><?= $this->lang->line('close') ?></button>
    	<input type="button" id="send_pdf" class="btn btn-success" value="<?= $this->lang->line("send") ?>"/>
        </div>
    </div>
    </div>
    </div>
    </form>
    <!-- email end here -->

    <script language="javascript" type="text/javascript">
        function printDiv(divID) {
    	//Get the HTML of div
    	var divElements = document.getElementById(divID).innerHTML;
    	//Get the HTML of whole page
    	var oldPage = document.body.innerHTML;

    	//Reset the page's HTML with div's HTML only
    	document.body.innerHTML =
    		"<html><head><title></title></head><body>" +
    		divElements + "</body>";

    	//Print Page
    	window.print();

    	//Restore orignal HTML
    	document.body.innerHTML = oldPage;
        }
        function closeWindow() {
    	location.reload();
        }

        function check_email(email) {
    	var status = false;
    	var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
    	if (email.search(emailRegEx) == -1) {
    	    $("#to_error").html('');
    	    $("#to_error").html("<?= $this->lang->line('mail_valid') ?>").css("text-align", "left").css("color", 'red');
    	} else {
    	    status = true;
    	}
    	return status;
        }


        $("#send_pdf").click(function () {
    	var to = $('#to').val();
    	var subject = $('#subject').val();
    	var message = $('#message').val();
    	var id = "<?php // $exams->examID                                ?>";
    	var error = 0;

    	if (to == "" || to == null) {
    	    error++;
    	    $("#to_error").html("");
    	    $("#to_error").html("<?= $this->lang->line('mail_to') ?>").css("text-align", "left").css("color", 'red');
    	} else {
    	    if (check_email(to) == false) {
    		error++
    	    }
    	}

    	if (subject == "" || subject == null) {
    	    error++;
    	    $("#subject_error").html("");
    	    $("#subject_error").html("<?= $this->lang->line('mail_subject') ?>").css("text-align", "left").css("color", 'red');
    	} else {
    	    $("#subject_error").html("");
    	}

    	if (error == 0) {
    	    $.ajax({
    		type: 'POST',
    		url: "<?= base_url('parentes/send_mail') ?>",
    		data: 'to=' + to + '&subject=' + subject + "&id=" + id + "&message=" + message,
    		dataType: "html",
    		success: function (data) {
    		    location.reload();
    		}
    	    });
    	}
        });
    </script>

<?php } ?>
