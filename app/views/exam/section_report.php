<?php
/**
 * Description of section_report
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
?>

<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa fa-book"></i> <?= $this->lang->line('panel_title') ?></h3>


        <ol class="breadcrumb">
            <li><a href="<?= base_url("dashboard/index") ?>"><i class="fa fa-laptop"></i> <?= $this->lang->line('menu_dashboard') ?></a></li>
            <li class="active"><?= $this->lang->line('menu_report') ?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">

<?php
$usertype = $this->session->userdata("usertype");
?>

                <div class="col-sm-6 col-sm-offset-3 list-group">
                    <div class="list-group-item list-group-item-warning">
                        <form class="form-horizontal" role="form" method="post">

<?php
if (form_error('classesID'))
    echo "<div class='form-group has-error' >";
else
    echo "<div class='form-group' >";
?>
			    <label for="classesID" class="col-sm-2 col-sm-offset-2 control-label">
			    <?= $this->lang->line('eattendance_classes') ?>
			    </label>
			    <div class="col-sm-6">
<?php
$array = array("0" => $this->lang->line("eattendance_select_classes"));
foreach ($classes as $classa) {
    $array[$classa->classesID] = $classa->classes;
}
echo form_dropdown("classesID", $array, set_value("classesID"), "id='classesID' class='form-control'");
?>
			    </div>
		    </div>
<?php
if (form_error('sectionID'))
    echo "<div class='form-group has-error' >";
else
    echo "<div class='form-group' >";
?>
		    <label for="sectionID" class="col-sm-2 col-sm-offset-2 control-label">
		    <?= $this->lang->line('exam_section') ?>
		    </label>
		    <div class="col-sm-6">
<?php
$array = array("0" => $this->lang->line("exam_select_section"));
if ($sections != "empty") {
    foreach ($sections as $section) {
	$array[$section->sectionID] = $section->section;
    }
}

echo form_dropdown("sectionID", $array, set_value("sectionID"), "id='sectionID' class='form-control'");
?>
		    </div>
		</div>
<?php
if (form_error('examID'))
    echo "<div class='form-group has-error' >";
else
    echo "<div class='form-group' >";
?>
		<label for="examID" class="col-sm-2 col-sm-offset-2 control-label">
		<?= $this->lang->line('eattendance_exam') ?>
		</label>
		<div class="col-sm-6">
<?php
$array = array("0" => $this->lang->line("eattendance_select_exam"));
foreach ($exams as $exam) {
    $array[$exam->examID] = $exam->exam;
}
echo form_dropdown("examID", $array, set_value("examID"), "id='examID' class='form-control'");
?>
		</div>
	    </div>



<?php
if (form_error('subjectID'))
    echo "<div class='form-group has-error' >";
else
    echo "<div class='form-group' >";
?>
	    <label for="subjectID" class="col-sm-2 col-sm-offset-2 control-label">
	    <?= $this->lang->line("eattendance_subject") ?>
	    </label>
	    <div class="col-sm-6">
<?php
$array = array('0' => $this->lang->line("eattendance_select_subject"));
if (!empty($subjects)) {
    foreach ($subjects as $subject) {
	$array[$subject->subjectID] = $subject->subject;
    }
}

$sID = 0;
if ($subjectID == 0) {
    $sID = 0;
} else {
    $sID = $subjectID;
}

echo form_dropdown("subjectID", $array, set_value("subjectID", $sID), "id='subjectID' class='form-control'");
?>
	    </div>
	</div>


	<div class="form-group">
	    <div class="col-sm-offset-4 col-sm-8">
		<input type="submit" class="btn btn-success" style="margin-bottom:0px" value="<?= $this->lang->line("view_report") ?>" >
	    </div>
	</div>
	</form>
    </div>
</div>

<?php
if (count($students) > 0) {
    ?>

    <div class="col-sm-12">

        <div class="nav-tabs-custom">
    	<ul class="nav nav-tabs">
    	    <li class="active"><a data-toggle="tab" href="#all" aria-expanded="true"><?= $this->lang->line("eattendance_all_students") ?></a></li>

    <!--		<a href="<?= base_url('uploads/report/exam' . $sID . '.xlsx') ?>"  style="float: right;" class="btn btn-success btn-xs mrg" data-placement="top" data-toggle="tooltip" data-original-title="Export"><i class="fa fa-cloud-download"></i></a>-->
    	    <a style="float: right;" onclick="javascript:printDiv('all')" class="btn btn-success btn-xs mrg" data-placement="top" data-toggle="tooltip" data-original-title="Print"><i class="fa fa-print"></i></a>
    	</ul>



    	<div class="tab-content">
    	    <div id="all" class="tab-pane active">
    		<div id="hide-table">
    		    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
    			<thead>
    			    <tr>
    				<th class="col-sm-2"><?= $this->lang->line('slno') ?></th>
    				<!--<th class="col-sm-2"><?= $this->lang->line('eattendance_photo') ?></th>-->
    				<th class="col-sm-2"><?= $this->lang->line('eattendance_name') ?></th>
    				<th class="col-sm-2"><?= $this->lang->line('eattendance_roll') ?></th>

    <?php
    if ($subjectID == 0) {
	$average = 'Student Average';
	//Loop in all subjects to show list of them here
	foreach ($subjects as $subject) {
	    echo '<th class="col-sm-2">' . substr($subject->subject, 0, 4) . '</th>';
	}
    } else {
	$average = 'Subject Average';
	echo '<th class="col-sm-2">Subject Name</th>';
    }
    ?>

    				<th class="col-sm-2">Total Marks</th>
    				<th class="col-sm-2"><?= $average ?></th>
				<?php if($userclass->classlevel_id==2  && $subjectID == 0){ ?>
    				<th class="col-sm-2">Division </th>
				<th class="col-sm-2">Points</th>
				<?php } ?>
    				<th class="col-sm-2">Rank</th>
    				<!--<th class="col-sm-2"><?= $this->lang->line('total_point') ?></th>-->
    				<th class="col-sm-2"><?= $this->lang->line('action') ?></th>
    			    </tr>
    			</thead>
    			<tbody>
    <?php
    if (count($students)) {
	$examController = new Exam();
	$i = 1;
	foreach ($students as $student) {
	    ?>
	    			    <tr>
	    				<td data-title="<?= $this->lang->line('slno') ?>">
	    <?php echo $i ?>
	    				</td>

	    	    <!--	    				    <td data-title="<?= $this->lang->line('eattendance_photo') ?>">
	    <?php
//						    $array = array(
//							"src" => base_url('uploads/images/' . $student->photo),
//							'width' => '35px',
//							'height' => '35px',
//							'class' => 'img-rounded'
//						    );
//						    echo img($array);
	    ?>
	    						    </td>-->
	    				<td data-title="<?= $this->lang->line('eattendance_name') ?>">
	    <?php echo $student->name; ?>
	    				</td>
	    				<td data-title="<?= $this->lang->line('eattendance_roll') ?>">
	    <?php echo $student->roll; ?>
	    				</td>
						<?php
						/*
						 * 
						 * -----------------------------------------
						 * This part show list of all subjects or ONE
						 *  subject depends on user selection
						 * -----------------------------------------
						 * 
						 */
						$subject_info = $examController->get_student_position_persubject($student->studentID, $examID, $subjectID, $classesID, $sectionID);

						$rank_info = $examController->get_rank_per_all_subjects($examID, $classesID, $student->studentID, $sectionID);
						if ($subjectID == 0) {
						    $sum = 0;
						    foreach ($subjects as $subject) {
							$total_mark = $examController->get_total_marks($student->studentID, $examID, $classesID, $subject->subjectID);
							$color = $total_mark->total_mark < $siteinfos->pass_mark ? "pink" : "";
							if($subject->is_penalty==1 && $subject->pass_mark >$total_mark->total_mark){
							    //this user is penaltized
							    $color='#ECC2BA';
							    
							}
							echo '<td data-title="" style="background: ' . $color . ';">';
							
							echo $total_mark->total_mark;
							$sum +=$total_mark->total_mark;
							echo '</td>';
						    }
						} else {
						    echo '<td>';
						    echo $subject_info->subject;
						    echo '</td>';
						}
						?>
	    				<td data-title="<?= $this->lang->line('eattendance_roll') ?>">
					    <?php
					    echo $subjectID == '0' ? $rank_info->sum : $subject_info->mark;
					    ?>
	    				</td>
						<?php
						if ($subjectID == '0') {

						    /**
						     * For the whole class, find total marks of that student
						     * and devide by total subjects taken by such student
						     */
						  
						    $color = $rank_info->average < $siteinfos->pass_mark ? "pink" : "";
						    echo '<td data-title="" style="background: ' . $color . ';">';
						    echo round($rank_info->average, 1);
						    echo '</td>';
						} else {
						    /*
						     * For one subject, just find total marks 
						     * for that subject and devide by number of students
						     */
						    $total_marks = 0;
						    foreach ($students as $student) {
							$total_mark = $examController->get_total_marks($student->studentID, $examID, $classesID, $subjectID);
							$total_marks +=$total_mark->total_mark;
						    }
						    $average_marks = $total_marks / count($students);
						    $color = $average_marks < $siteinfos->pass_mark ? "pink" : "";
						    echo '<td data-title="" style="background: ' . $color . ';">';
						    echo round($average_marks, 1);
						    echo '</td>';
						}
						if($userclass->classlevel_id==2 && $subjectID ==0){
						?>
	    				<td data-title="<?= $this->lang->line('eattendance_phone') ?>">
					    <?php
						    $division = $examController->getAcseeDivision($student->studentID, $examID, $classesID);
						    echo acseeDivision($division[0],$division[1]);
					    ?>
	    				</td>
					<td data-title="<?= $this->lang->line('eattendance_phone') ?>">
					    <?php
						   echo $division[0];
					    ?>
	    				</td>
						<?php } ?>
	    				<td data-title="<?= $this->lang->line('eattendance_roll') ?>">
	    <?php echo $subjectID == '0' ? $rank_info->rank : $subject_info->rank; ?>
	    				</td>

	    				<td data-title="<?= $this->lang->line('action') ?>">
	    				    <a href="<?= base_url('exam/singlereport/' . $student->studentID . '?exam=' . $examID.'&section_id=' . $sectionID) ?>" class="btn btn-success btn-xs mrg" data-placement="top" data-toggle="tooltip" data-original-title="Download Report"><i class="fa fa-download"></i></a>
	    <?php
//						    foreach ($eattendances as $eattendance) {
//							if ($eattendance->studentID == $student->studentID && $examID == $eattendance->examID && $classesID == $eattendance->classesID && $subjectID == $eattendance->subjectID) {
//
//							    if ($eattendance->eattendance == "Present") {
//								echo "<button class='btn btn-success btn-xs'>" . $eattendance->eattendance . "</button>";
//							    } elseif ($eattendance->eattendance == "") {
//								echo "<button class='btn btn-danger btn-xs'>" . "Absent" . "</button>";
//							    } else {
//								echo "<button class='btn btn-danger btn-xs'>" . $eattendance->eattendance . "</button>";
//							    }
//							    break;
//							}
//						    }
	    ?>
	    				</td>
	    			    </tr>
	    <?php
	    $i++;
	}
    }
    ?>
    			</tbody>
    		    </table>

    		</div>

    	    </div>

    	</div>

        </div> <!-- nav-tabs-custom -->
    </div> <!-- col-sm-12 for tab -->

    <?php
} else {
    ?>
    <div class="col-sm-12">

        <div class="nav-tabs-custom">
    	<ul class="nav nav-tabs">
    	    <li class="active"><a data-toggle="tab" href="#all" aria-expanded="true"><?= $this->lang->line("eattendance_all_students") ?></a></li>
    	</ul>

    	<div class="tab-content">
    	    <div id="all" class="tab-pane active">
    		<div id="hide-table">
    		    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
    			<thead>
    			    <tr>
    				<th class="col-sm-2"><?= $this->lang->line('slno') ?></th>
    				<th class="col-sm-2"><?= $this->lang->line('eattendance_photo') ?></th>
    				<th class="col-sm-2"><?= $this->lang->line('eattendance_name') ?></th>
    				<th class="col-sm-2"><?= $this->lang->line('eattendance_roll') ?></th>
    				<th class="col-sm-2"><?= $this->lang->line('eattendance_phone') ?></th>
    				<th class="col-sm-2"><?= $this->lang->line('action') ?></th>
    			    </tr>
    			</thead>
    			<tbody>
    <?php
    if (count($students)) {
	$i = 1;
	foreach ($students as $student) {
	    ?>
	    			    <tr>
	    				<td data-title="<?= $this->lang->line('slno') ?>">
	    <?php echo $i; ?>
	    				</td>

	    				<td data-title="<?= $this->lang->line('eattendance_photo') ?>">
	    <?php
	    $array = array(
		"src" => base_url('uploads/images/' . $student->photo),
		'width' => '35px',
		'height' => '35px',
		'class' => 'img-rounded'
	    );
	    echo img($array);
	    ?>
	    				</td>
	    				<td data-title="<?= $this->lang->line('eattendance_name') ?>">
	    <?php echo $student->name; ?>
	    				</td>
	    				<td data-title="<?= $this->lang->line('eattendance_roll') ?>">
	    <?php echo $student->roll; ?>
	    				</td>
	    				<td data-title="<?= $this->lang->line('eattendance_phone') ?>">
	    <?php echo $student->phone; ?>
	    				</td>
	    				<td data-title="<?= $this->lang->line('action') ?>">
	    <?php
	    foreach ($eattendances as $eattendance) {
		if ($eattendance->studentID == $student->studentID && $examID == $eattendance->examID && $classesID == $eattendance->classesID && $subjectID == $eattendance->subjectID) {

		    if ($eattendance->eattendance == "Present") {
			echo "<button class='btn btn-success btn-xs'>" . $eattendance->eattendance . "</button>";
		    } elseif ($eattendance->eattendance == "") {
			echo "<button class='btn btn-danger btn-xs'>" . "Absent" . "</button>";
		    } else {
			echo "<button class='btn btn-danger btn-xs'>" . $eattendance->eattendance . "</button>";
		    }
		    break;
		}
	    }
	    ?>
	    				</td>
	    			    </tr>
	    <?php
	    $i++;
	}
    }
    ?>
    			</tbody>
    		    </table>
    		</div>

    	    </div>
    	</div>
        </div> <!-- nav-tabs-custom -->
    </div>
<?php } ?>



</div> <!-- col-sm-12 -->
</div><!-- row -->
</div><!-- Body -->
</div><!-- /.box -->

<script type="text/javascript">
    $('#classesID').change(function (event) {
	var classesID = $(this).val();
	if (classesID === '0') {
	    $('#classesID').val(0);
	} else {
	    $.ajax({
		type: 'POST',
		url: "<?= base_url('student/sectioncall') ?>",
		data: "id=" + classesID,
		dataType: "html",
		success: function (data) {
		    $('#sectionID').html(data);
		}
	    });
	}
    });

    $('#sectionID').change(function (event) {
	var classesID = $(this).val();
	if (classesID === '0') {
	    $('#sectionID').val(0);
	} else {
	    $.ajax({
		type: 'POST',
		url: "<?= base_url('mark/getSubjectBySection') ?>",
		data: "id=" + classesID,
		dataType: "html",
		success: function (data) {
		    $('#subjectID').html(data);
		}
	    });
	}
    });

    function printDiv(divID) {
	//Get the HTML of div
	var divElements = document.getElementById(divID).innerHTML;
	//Get the HTML of whole page
	var oldPage = document.body.innerHTML;

	//Reset the page's HTML with div's HTML only
	document.body.innerHTML =
		"<html><head><title></title></head><body>" +
		divElements + "</body>";

	//Print Page
	window.print();

	//Restore orignal HTML
	document.body.innerHTML = oldPage;
    }
    function closeWindow() {
	location.reload();
    }

    $('#classesID').change(function (event) {

	var classesID = $(this).val();
	if (classesID === '0') {
	    $('#examID').val(0);
	} else {
	    $.ajax({
		type: 'POST',
		url: "<?= base_url('exam/getExamByClass') ?>",
		data: "id=" + classesID,
		dataType: "html",
		success: function (data) {
		    $('#examID').html(data);
		}
	    });
	}
    });
</script>