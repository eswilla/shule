<?php
require_once __DIR__ . '../../../../app/controllers/mark.php';
$usertype = $this->session->userdata("usertype");
?>
<link href="<?php echo base_url('assets/print.css'); ?>" rel="stylesheet" type="text/css">
<?php
if ($usertype == "Admin") {
    ?>
    <div class="well">
        <div class="row">
    	<div class="col-sm-6">
    	    <button class="btn-cs btn-sm-cs" onclick="javascript:printDiv('printablediv')"><span
    		    class="fa fa-print"></span> <?= $this->lang->line('print') ?> </button>
    	    <a href="<?= base_url("exam/printall/" . $exams->examID . '?class_id=' . $student->classesID . '&section_id=' . $student->sectionID) ?>"
    	       target="_blank"><i class="fa fa-laptop"></i><?= $this->lang->line('print') ?> all reports</a>
		   <?php
		   //echo btn_add_pdf('exam/print_preview/' . $exams->examID, $this->lang->line('pdf_preview'))
		   ?>
    	    <button class="btn-cs btn-sm-cs" data-toggle="modal" data-target="#mail"><span
    		    class="fa fa-envelope-o"></span> <?= $this->lang->line('mail') ?></button>
    	</div>
    	<div class="col-sm-6">
    	    <ol class="breadcrumb">
    		<li><a href="<?= base_url("dashboard/index") ?>"><i
    			    class="fa fa-laptop"></i> <?= $this->lang->line('menu_dashboard') ?></a></li>
    		<li><a href="<?= base_url("exam/index") ?>"><?= $this->lang->line('menu_parent') ?></a></li>
    		<li class="active"><?= $this->lang->line('view') ?></li>
    	    </ol>
    	</div>
        </div>

    </div>

<?php } else if (strtolower($usertype) == 'parent') { ?>
    <div class="well">
        <div class="row">
    	<div class="col-sm-6">
    	    <button class="btn-cs btn-sm-cs" onclick="javascript:printDiv('printablediv')"><span
    		    class="fa fa-print"></span> <?= $this->lang->line('print') ?> </button>
		    <?php
		    //echo btn_add_pdf('exam/print_preview/' . $exams->examID.'/'.$student->studentID, $this->lang->line('pdf_preview'))
		    ?>
    	</div>
        </div>
    </div>
<?php } ?>

<div id="printablediv" class="page center">

    <section class=" subpage">


	<div id="p1" style="overflow: hidden; left: 5%;" class="">


	    <!-- Begin inline CSS -->
	    <style type="text/css">

		.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td {
		    font-weight: bolder
		}

		#printablediv {
		    color: #000;
		}

	    </style>
	    <!-- End inline CSS -->

	    <!-- Begin embedded font definitions -->
	    <style id="fonts1" type="text/css">


		table.table.table-striped td, table.table.table-striped th, table.table {
		    border: 1px solid #000000;
		}
		@media print { 
		    table.table.table-striped td,table.table.table-striped th, table.table {
			border: 1px solid #000000 !important;
		    }
		    .table-stripe {
			background-color: #dedede !important;
			border: 1px solid #000000 !important;
		    }
		}

	    </style>


	    <!-- Begin page background -->


	    <?php
	    $array = array(
		"src" => base_url('uploads/images/' . $siteinfos->photo),
		'width' => '126em',
		'height' => '126em',
		'class' => 'img-rounded',
		'style' => 'margin-left:2em'
	    );
	    $photo = array(
		"src" => base_url('uploads/images/' . $student->photo),
		'width' => '128em',
		'height' => '128em',
		'class' => 'img-rounded',
		'style' => 'margin-left:2em'
	    );
	    ?>

	    <div class="row" style="margin-top: 1% ;">
<table class=" table-striped center"style="margin: 1px 2px 1px 0px;">
		<thead>
		    <tr>
			<th>
		    <?php echo img($array);
		    ?>
		</th>
		<th>
		<div class="col-md-12 text-center" style="font-size:17px;">
		    <h2 style="font-weight: bolder" class="text-uppercase"><?= $siteinfos->sname ?></h2>
		    <h4><?= 'P.O BOX ' . $siteinfos->box . ', ' . $siteinfos->address ?>.</h4>
		    <h3>Cell: <?= str_replace(',', ' / ', $siteinfos->phone) ?></h3>
		    <h3> Email:<?= $siteinfos->email ?></h3>
		    <h3>Website: <?= $siteinfos->website ?></h3>
		</div>
	    </th>
		<th>
		    <?php echo img($photo);
		    ?>
		</th>
		</tr>
		</thead>
	    </table>
		<hr>
	    </div>


	</div>

	<div class="">
	    <h1 align='center' style="font-size: 1.9em; font-weight: bolder;padding-top: 0%; ">STUDENT PROGRESSIVE
		REPORT (<?= strtoupper($classes->classes) . ' ' . $student->year ?>)</h1>
	</div>
	<div>
	    <!--        <div --><?php //if ($classlevel->result_format != 'CSEE') {  ?><!-- class="panel-body profile-view-dis" --><?php //}  ?><!-->
	    <div class="row" style="margin: 1% 2% 1% 2%;">
		    <div class="col-sm-6" style="float:left;">
			    <h1 style="font-weight: bolder;font-size: 1.4em;"> NAME: <?= $student->name ?></h1>
			    <h4 style="margin-top: 2%;"> Admission No: <?= $student->roll ?></h4>
		    </div>
		    <div class="col-sm-3" style="float: right;">
			    <h1 style="font-weight: bolder; font-size: 1.4em;"><?=
	    (isset($section_id) && $section_id == '') ?
		    'CLASS: ' . strtoupper($classes->classes) . '' :
		    'STREAM: ' . $student->section
	    ?></h1>
		    </div>
		    <div style="clear:both;"></div>
	    </div>

    </div>

	    <?php
	
	    if (empty($marks)) {
		echo '<p class="alert alert-info">Sorry, this Exam has not yet been published in this class</p>';
	    } elseif (count($exams) > 0 && !empty($exams)) {
		?>

    <div class="row table" style="margin: 2%;">
		<?php
		//print_r($marks);
		if ($marks && $exams) {

		    $examController = new Mark();
		    ?>

		<div class="col-lg-12">

		    <?php
		    $map1 = function ($r) {
			return intval($r->examID);
		    };
		    $marks_examsID = array_map($map1, $marks);
		    $max_semester = max($marks_examsID);

		    $map2 = function ($r) {
			return intval($r->examID);
		    };
		    $examsID = array_map($map2, $exams);

		    $map3 = function ($r) {
			return array("mark" => intval($r->mark), "semester" => $r->examID);
		    };
		    $all_marks = array_map($map3, $marks);

		    $map4 = function ($r) {
			return array("gradefrom" => $r->gradefrom, "gradeupto" => $r->gradeupto);
		    };
		    $grades_check = array_map($map4, $grades);


		    if (strtolower($usertype) == 'parent') {
			$sectionreport = $examController->get_rank_per_all_subjects($exams->examID, $student->classesID, $student->studentID, $student->sectionID);
			$totalstudent_insection = $examController->get_student_per_class($student->classesID, $student->sectionID);
		    }
		    ?>
		    <div class="row" style="margin: 2%;">
		    <?php
		    //print_r($marks);
		    if ($marks && $exams) {

			$examController = new Mark();
			?>

	    			    <div class="col-lg-12">

			<?php
			$map1 = function ($r) {
			    return intval($r->examID);
			};
			$marks_examsID = array_map($map1, $marks);
			$max_semester = max($marks_examsID);

			$map2 = function ($r) {
			    return intval($r->examID);
			};
			$examsID = array_map($map2, $exams);

			$map3 = function ($r) {
			    return array("mark" => intval($r->mark), "semester" => $r->examID);
			};
			$all_marks = array_map($map3, $marks);

			$map4 = function ($r) {
			    return array("gradefrom" => $r->gradefrom, "gradeupto" => $r->gradeupto);
			};
			$grades_check = array_map($map4, $grades);


			echo "<table  class=\"table table-striped table-bordered center\" style=\" margin-bottom: -.1%; \">";
			if ($exams->examID <= $max_semester) {

			    $check = array_search($exams->examID, $marks_examsID);

			    if ($check >= 0) {
				$f = 0;
				foreach ($grades_check as $key => $range) {
				    foreach ($all_marks as $value) {
					if ($value['semester'] == $exams->examID) {
					    if ($value['mark'] >= $range['gradefrom'] && $value['mark'] <= $range['gradeupto']) {
						$f = 1;
					    }
					}
				    }
				    if ($f == 1) {
					break;
				    }
				}


				echo "<thead style='background-color: #b3b3b3'>";
				echo "<tr>";
				echo "<th>";
				echo strtoupper($this->lang->line("mark_subject"));
				echo "</th>";
				echo "<th>";
				echo strtoupper($exams->exam);
				echo "</th>";
				if (count($grades) && $f == 1) {
				    echo "<th>";
				    echo strtoupper($this->lang->line("mark_point"));
				    echo "</th>";
				    echo "<th>";
				    echo strtoupper($this->lang->line("mark_grade"));
				    echo "</th>";
				}
				echo "<th>";
				echo strtoupper('Grade Status');
				echo "</th>";
				echo "<th>";
				echo strtoupper($section_id == '' ? 'Pos in  Class' : 'Pos in  Stream');
				echo "</th>";
				if (strtolower($usertype) == 'parent') {
				    echo "<th>";
				    echo 'Pos in Stream ';
				    echo "</th>";
				}
				echo "</tr>";

				echo "</thead>";
			    }
			}

			echo "<tbody>";

			foreach ($marks as $mark) {
			    if ($exams->examID == $mark->examID) {
				echo "<tr>";
				echo "<td data-title='" . $this->lang->line('mark_subject') . "'>";
				echo strtoupper($mark->subject);
				echo "</td>";
				echo "<td data-title='" . $this->lang->line('mark_mark') . "'>";
				echo $mark->mark;
				echo "</td>";
				if (count($grades)) {
				    foreach ($grades as $grade) {
					if ($grade->gradefrom <= $mark->mark && $grade->gradeupto >= $mark->mark) {
					    echo "<td data-title='" . $this->lang->line('mark_point') . "'>";
					    echo $grade->point;
					    echo "</td>";
					    echo "<td data-title='" . $this->lang->line('mark_grade') . "'>";
					    echo $grade->grade;
					    echo "</td>";
					    break;
					}
				    }
				}

				echo "<td data-title='" . $this->lang->line('mark_mark') . "'>";
				echo $grade->note;
				echo "</td>";

				echo "<td data-title='" . $this->lang->line('mark_mark') . "'>";

				$subject_info = $examController->get_position($mark->studentID, $mark->examID, $mark->subjectID, $mark->classesID, $section_id);
				echo $subject_info->rank;
				echo "</td>";
				if (strtolower($usertype) == 'parent') {
				    $subject_info_insection = $examController->get_position($mark->studentID, $mark->examID, $mark->subjectID, $mark->classesID, $student->sectionID);
				    echo "<td data-title='" . $this->lang->line('mark_mark') . "'>";
				    echo $subject_info_insection->rank;
				    echo "</td>";
				}
				echo "</tr>";
			    }
			}
			echo "</tbody>";
			echo "</table>";
			if (strtolower($usertype) == 'parent') {
			    $sectionreport = $examController->get_rank_per_all_subjects($exams->examID, $student->classesID, $student->studentID, $student->sectionID);
			    $totalstudent_insection = $examController->get_student_per_class($student->classesID, $student->sectionID);
			}
			?>



	    	    <!--						<h1 style="font-weight: bolder; margin-left: 2%;">Summary</h1>-->

	    	    <table class="table table-striped table-bordered" style=" width: 100%; margin: 0%;">
	    		<thead>
	    		    <tr>
	    			<th style="font-weight: bolder; width:26.2%;">TOTAL:
	    			    <strong><?= $report->sum ?></strong></th>
	    			<th style="font-weight: bolder;width:33.6%;">AVERAGE MARK:
	    			    <strong><?= round($report->average, 1) ?></strong></th>
	    			<th style="font-weight: bolder">Position
	    			    in <?= $section_id == '' ? 'Class' : 'Stream' ?>:
	    			    <strong><?= $report->rank ?></strong> out of <?= count($total_students) ?></th>
				    <?= strtolower($usertype) == 'parent' ? '<th style="font-weight: bolder">Position in Stream: <strong>' . $sectionreport->rank . '</strong>out of ' . count($total_students_section) . ' </th>' : '' ?>
	    		    <!--								<th style="font-weight: bolder">Points</th>-->
	    		    <!--								<th style="font-weight: bolder">Division</th>-->
	    		    </tr>
	    		</thead>
	    		<tbody>
	    		<!--                            <tr>-->
	    		<!--                                <td><? //= $report->sum ?><!--</td>-->
	    		<!--                                <td><? //= round($report->average, 1) ?><!--</td>-->
	    		<!--                                <td><? //= count($total_students) ?><!--</td>-->
	    		<!--                 <? //= strtolower($usertype) == 'parent' ? '<td>' . $totalstudent_insection->total_student . '</td>' : '' ?>
	    		<!--                                <td>-->
	    		<!--<? //= $report->rank ?></td>-->
	    		<!--       <? //= strtolower($usertype) == 'parent' ? '<td>' . $sectionreport->rank . '</td>' : '' ?>
	    		<!--                                <!--								<td>--><?php //echo $points ?><!--<!--</td>-->
	    		<!--    <td><? echo $division ?><!--<!--</td>-->
	    		    <!---->
	    		    <!--                            </tr>-->
	    		</tbody>
	    	    </table>
	    	    <div style="float: left; width: 50%">
	    		<table class="table table-striped table-bordered" style="  float: left; margin: 1% 0 0 0;">
	    		    <thead>
	    			<tr>
	    			    <th style="font-weight: bolder; text-align: center; border-bottom: 1px solid #fff;">
	    				GRADING
	    			    </th>
	    			</tr>
	    		    </thead>
	    		</table>

	    		<table class="table table-striped table-bordered" style=" margin-bottom: 1%;">
	    		    <thead>
	    			<tr>
					<?php foreach ($grades as $grade) { ?>
					    <th style="font-weight: bolder; text-align: center;"><?= $grade->grade ?>
					    </th>
					<?php } ?>
	    			</tr>
	    		    </thead>
	    		    <tbody>
	    			<tr>
					<?php foreach ($grades as $grade) { ?>
					    <td style="font-weight: bold;font-size: smaller; text-align: center;background-color: white;color: #000;"><?= $grade->gradefrom ?>
						- <?= $grade->gradeupto ?>
					    </td>
					<?php } ?>
	    			</tr>
	    		    </tbody>

	    		</table>
	    	    </div>
	    	    <div style="float: right; width: 49.6%">
	    		<table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
	    		    <thead>
	    			<tr>
	    			    <th style="font-weight: bolder; "><strong>KEY:</strong></th>
	    			</tr>
	    		    </thead>
	    		    <tbody>
	    			<tr>
	    			    <td style="background-color: white;color: #000;"><strong>NOTES:</strong> <span
	    				    style="font-size: smaller"></span
	    			    </td>
	    			</tr>
	    		    </tbody>
	    		</table>
	    	    </div>
	    	    <table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
	    		<thead>
	    		    <tr>
	    			<th style="font-weight: bolder; width:56%;"><strong style="vertical-align: top;">General
	    				Remarks:</strong><br> <?php
					foreach ($grades as $grade) {
					    if ($grade->gradefrom <= round($report->average, 0) && $grade->gradeupto >= round($report->average, 0)) {
						echo "<span class='bolder' style='border-bottom: 1px solid black'>";
						echo $grade->overall_note;
						echo "</span>";
						break;
					    }
					}
					?></th>
	    			<th>Class Teacher's Signature<br><img src="<?= $class_teacher_signature ?>" width="54"
	    							      height="32"></th>
	    		    </tr>
	    		</thead>
	    	    </table>
	    	    <table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
	    		<thead>
	    		    <tr>
	    			<th style="font-weight: bolder; width: 56%; "><strong style="vertical-align: top;">Head
	    				Teacher's comments:</strong><br> <?php
					foreach ($grades as $grade) {
					    if ($grade->gradefrom <= round($report->average, 0) && $grade->gradeupto >= round($report->average, 0)) {
						echo "<span class='bolder' style='border-bottom: 1px solid black'>";
						echo $grade->overall_note;
						echo "</span>";
						break;
					    }
					}
					?></th>
	    			<th style="width: 50%;">Signature<br></th>
	    		    </tr>
	    		</thead>
	    	    </table>
	    	    <table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
	    		<thead>
	    		    <tr>
	    			<th style="text-align: center;">Date of Reporting:
	    			    <b><?= $level->result_format != 'CSEE' ? '9th' : '3th' ?> January, 2017 </b>
	    			</th>
	    		    </tr>
	    		</thead>
	    	    </table>

	    	</div>
		<?php } ?>

	</div>
	<div style="padding-left:5%;">

	    <div style="z-index: 4000">
		<div style="float: right; margin-right: 4%; margin-top: 4%;"></div>
		<div><img src="../../assets/images/stamp_<?=  set_schema_name()?>png"
			  width="100" height="100"
			  style="position:relative; margin:-17% 15% 0 0; float:right;"></div>

	    </div>

	</div>

    <?php } ?>
    </section>

    </div>

    <?php if ($usertype == "Admin") { ?>
	<!-- email modal starts here -->
	<form class="form-horizontal" role="form" action="<?= base_url('exam/send_mail'); ?>" method="post">
	    <div class="modal fade" id="mail">
		<div class="modal-dialog">
		    <div class="modal-content">
			<div class="modal-header">
			    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
				    class="sr-only">Close</span></button>
			    <h4 class="modal-title"><?= $this->lang->line('mail') ?></h4>
			</div>
			<div class="modal-body">

			    <?php
			    if (form_error('to'))
				echo "<div class='form-group has-error' >";
			    else
				echo "<div class='form-group' >";
			    ?>
			    <label for="to" class="col-sm-2 control-label">
				<?= $this->lang->line("to") ?>
			    </label>
			    <div class="col-sm-6">
				<input type="email" class="form-control" id="to" name="to" value="<?= set_value('to') ?>">
			    </div>
			    <span class="col-sm-4 control-label" id="to_error">
			    </span>
			</div>

			<?php
			if (form_error('subject'))
			    echo "<div class='form-group has-error' >";
			else
			    echo "<div class='form-group' >";
			?>
			<label for="subject" class="col-sm-2 control-label">
			    <?= $this->lang->line("subject") ?>
			</label>
			<div class="col-sm-6">
			    <input type="text" class="form-control" id="subject" name="subject"
				   value="<?= set_value('subject') ?>">
			</div>
			<span class="col-sm-4 control-label" id="subject_error">
			</span>

		    </div>

		    <?php
		    if (form_error('message'))
			echo "<div class='form-group has-error' >";
		    else
			echo "<div class='form-group' >";
		    ?>
		    <label for="message" class="col-sm-2 control-label">
			<?= $this->lang->line("message") ?>
		    </label>
		    <div class="col-sm-6">

			<textarea class="form-control" id="message" style="resize: vertical;" name="message"
				  value="<?= set_value('message') ?>"></textarea>
		    </div>
		</div>


	    </div>
	    <div class="modal-footer">
		<button type="button" class="btn btn-default" style="margin-bottom:0px;"
			data-dismiss="modal"><?= $this->lang->line('close') ?></button>
		<input type="button" id="send_pdf" class="btn btn-success" value="<?= $this->lang->line("send") ?>"/>
	    </div>
	</div>
	</div>
	</div>
	</form>
	<!-- email end here -->


	<script language="javascript" type="text/javascript">


	    function closeWindow() {
		location.reload();
	    }

	    function check_email(email) {
		var status = false;
		var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
		if (email.search(emailRegEx) == -1) {
		    $("#to_error").html('');
		    $("#to_error").html("<?= $this->lang->line('mail_valid') ?>").css("text-align", "left").css("color", 'red');
		} else {
		    status = true;
		}
		return status;
	    }


	    $("#send_pdf").click(function () {
		var to = $('#to').val();
		var subject = $('#subject').val();
		var message = $('#message').val();
		var id = "<?= $exams->examID ?>";
		var error = 0;

		if (to == "" || to == null) {
		    error++;
		    $("#to_error").html("");
		    $("#to_error").html("<?= $this->lang->line('mail_to') ?>").css("text-align", "left").css("color", 'red');
		} else {
		    if (check_email(to) == false) {
			error++
		    }
		}

		if (subject == "" || subject == null) {
		    error++;
		    $("#subject_error").html("");
		    $("#subject_error").html("<?= $this->lang->line('mail_subject') ?>").css("text-align", "left").css("color", 'red');
		} else {
		    $("#subject_error").html("");
		}

		if (error == 0) {
		    $.ajax({
			type: 'POST',
			url: "<?= base_url('exam/send_mail') ?>",
			data: 'to=' + to + '&subject=' + subject + "&id=" + id + "&message=" + message,
			dataType: "html",
			success: function (data) {
			    location.reload();
			}
		    });
		}
	    });
	</script>

    <?php } ?>
    <script>
        function printDiv(divID) {

    	//Get the HTML of div
    	var divElements = document.getElementById(divID).innerHTML;
    	//Get the HTML of whole page
    	var oldPage = document.body.innerHTML;

    	//Reset the page's HTML with div's HTML only
    	document.body.innerHTML =
    		'<html><head><title></title></head><body>' +
    		divElements + '</body>';

    	//Print Page
    	window.print();
        }
    </script>

<?php } ?>


