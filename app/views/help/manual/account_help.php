<?php
$user = empty($usertype) ? 'Administrator' : $usertype;
?>

<!DOCTYPE html>
<html>
<head>
    <title>ShuleSoft Manual</title>
    <?php
    $css = base_url() . "/assets/manual_assets/";
    $js = base_url() . "/assets/manual_assets/";
    ?>
    <meta charset="utf-8">
    <!--        <link rel="canonical" href="http://www.templatemonster.com/help/quick-start-guide/website-templates/responsive-website-templates-v1-1/index_en.html"/> -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/3.2.1/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?= $css ?>css/bootstrap.css" media="screen">
    <link rel="stylesheet" href="<?= $css ?>css/responsive.css" media="screen">
    <link rel="stylesheet" href="<?= $css ?>css/prettify.css"  media="screen">
    <link rel="stylesheet" href="<?= $css ?>css/prettyPhoto.css"  media="screen">
    <link rel="stylesheet" href="<?= $css ?>css/idea.css"  media="screen">
    <link rel="stylesheet" href="<?= $css ?>css/style.css" media="screen">
    <link rel="stylesheet" href="<?= $css ?>css/contact-form.css" media="screen">
    <script src="<?= $js ?>js/jquery.js"></script>
    <script src="<?= $js ?>js/jquery.scrollTo.js"></script>
    <script src="<?= $js ?>js/jquery-migrate-1.1.0.js"></script>
    <script src="<?= $js ?>js/prettify.js"></script>
    <script src="<?= $js ?>js/bootstrap-affix.js"></script>
    <script src="<?= $js ?>js/jquery.prettyPhoto.js"></script>

    <script src="<?= $js ?>js/TMForm.js"></script>
    <script src="<?= $js ?>js/modal.js"></script>
    <script src="<?= $js ?>js/bootstrap-filestyle.js"></script>

    <script src="<?= $js ?>js/sForm.js"></script>
    <script src="<?= $js ?>js/jquery.countdown.min.js"></script>
    <script src="<?php echo $js ?>js/scripts.js"></script>
    <!--[if IE 8]>
    <link rel="stylesheet" href="css/ie.css" />
    <![endif]-->

    <!--[if lt IE 9]>
    <div style='text-align:center'><a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://www.theie6countdown.com/img/upgrade.jpg"border="0"alt=""/></a></div>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <![endif]-->
</head>

<body>

<!-- header
================================================== -->
<header class="header">
    <div class="container">
        <div class="row">
            <article class="span4"><a href="<?= base_url() ?>" target="_blank">Home</a></article>
            <article class="span6">
                <h1>
                    <i class="fa fa-book"></i>
                    ShuleSoft System Manual
                </h1>
            </article>
            <article class="span2">
                <!--<div id="languages" class="select-menu pull-right">
                                <div class="select-menu_icon">
                                <b>En</b>
                                <i class="icon-angle-down"></i>
                                </div>
                                <ul class="select-menu_list">

                                </ul>
                            </div>-->
                <div id="versions" class="select-menu pull-right">
                    <div class="select-menu_icon"><b>v1-0</b><i class="icon-angle-down"></i></div>
                    <!-- <ul class="select-menu_list">
                                    <li><a href="/help/quick-start-guide/website-templates/responsive-website-templates-v1-0/index_en.html"><span>v1-0</span></a></li>
                                    <li class="active"><a href="/help/quick-start-guide/website-templates/responsive-website-templates-v1-1/index_en.html"><span>v1-1</span></a></li>
                                    <li class="active"><a href="/help/quick-start-guide/website-templates/responsive-website-templates-v1-2/"><span>v1-2</span></a></li>
                                </ul>-->
                </div>
            </article>
        </div>
    </div>
</header>
<div id="content">
<div class="bg-content-top">
<div class="container">

<!-- Docs nav
    ================================================== -->
<div class="row">
<div class="span3">

    <div id="nav_container">
        <a href="javascript:;" id="affect_all">
                                    <span class="expand">
                                        <i class="icon-angle-down"></i>
                                    </span>            
                                    <span class="close">
                                        <i class="icon-angle-up"></i>
                                    </span>
        </a>
        <div class="clearfix"></div>
        <ul class="nav nav-list bs-docs-sidenav"  id="nav">

            <li class="nav-item item1">
                <dl class="slide-down">
                    <dt><a href="#introduction" class="icon-info-sign">Introduction</a></dt>
                    <dd></dd>
                </dl>
            </li>
            <li class="nav-item item2">
                <dl class="slide-down">
                    <dt><a href="#modules" class="icon-check">Accountant Modules</a></dt>
                    <dd></dd>
                </dl>
            </li>
            <li class="nav-item item3">
                <dl class="slide-down">
                    <dt><a href="#general-information" class="icon-play">Features</a></dt>
                    <dd></dd>
                </dl>
            </li>

            <li class="nav-item item6">
                <dl class="slide-down">
                    <dt><a href="#expenses" class="icon-time">Expenses</a></dt>
                    <dd></dd>
                </dl>
            </li>

            <li class="nav-item item5">
                <dl class="slide-down">
                    <dt><a href="#balance" class="icon-plus-sign">Balance</a></dt>
                    <dd></dd>
                </dl>
            </li>

            <li class="nav-item item5">
                <dl class="slide-down">
                    <dt><a href="#feetype" class="icon-plus-sign">FeeType</a></dt>
                    <dd></dd>
                </dl>
            </li>

            <li class="nav-item item10">
                <dl class="slide-down">
                    <dt><a href="#invoice" class="icon-money">Invoice</a></dt>
                    <dd></dd>
                </dl>
            </li>


            <li class="nav-item item12x">
                <dl class="slide-down">
                    <dt><a href="#notice" class="icon-play">Notice</a></dt>
                    <dd></dd>
                </dl>
            </li>
            <li class="nav-item item11">
                <dl class="slide-down">
                    <dt><a href="#transport" class="icon-bar-chart">Transport</a></dt>
                    <dd></dd>
                </dl>
            </li>


            <li class="nav-item item7">
                <dl class="slide-down">
                    <dt><a href="#hostel" class="icon-home">Hostel</a></dt>
                    <dd></dd>
                </dl>
            </li>






            <li class="nav-item item3">
                <dl class="slide-down">
                    <dt><a href="#conclusion" class="icon-question-sign">Conclusion</a></dt>
                    <dd></dd>
                </dl>
            </li>
        </ul>
    </div>
</div>


<div class="span9">

<!-- box-content
================================================== -->
<div class="box-content">
<!-- block-started
================================================== -->
<section class="block-started"  id="introduction">
    <h2 class="item1"><i class="icon-info-sign"></i> Introduction <small>
            <i>What is ShuleSoft ?</i></small></h2>
    <p>
        ShuleSoft is Cloud based management system for schools that brings efficiency in school management and interactions among students, teachers and parents. ShuleSoft School management system is used for managing education based organizations like schools, colleges, universities etc.
    <p>

</section>

<section class="block-templates" id="modules">
    <h2 class="item2"><i class="icon-check"></i>Accountant Modules Available</h2>

    <p>ShuleSoft has number of modules available.  Accountant can access the following modules</p>
    <ol>
        <li>Account module</li>

    </ol>
    <p>As Accountant, you can view and manage account modules for school operations.</p>
</section>

<section class="block-templates" id="general-information">
    <h2 class="item3"><i class="icon-play"></i>ShuleSoft Features </h2>


    <p>The following are some of the features available in shulesoft software. </p>

    <section id="features">
					<pre class="prettyprint lang-plain linenums">
Multiple Modules that link each other to bring more efficiency in school management
Cloud based solution. This means, the software will be accessed via the internet to enable each and every user to interact together and also to enable interlinking of system with payment systems
Linked with SMS platform to send SMS and Email notification on each major action
System support multiple accounts creation such that, each user (admin, teacher, student, parent etc) will have his/her own account and view his/her own information only 
Allow creation of Custom classes and sections.
Allow admin to create subjects and grading point as per country  or custom regulations
Allow creation of exams and exam schedule and also, teacher can mark a subject via the system and the system will help/her to generate all required reports
Allow registration of students in library, hostel and transport routes
Allow attendances (teacher, students and examination) to be taken and generate attendance reports
Allow simple creation of routines (classes and exams)
Accounting modules help a school to easily create invoices, accept electronic payments and manage all school accounting
Each user in the system have a unique profile to enable him/her to view and perform required task. These users include accountants, administrators, teachers, librarians, parents, students etc 
					</pre>
    </section>
</section>


<section class="block-templates" id="get-started">
    <h2 class="item4"><i class="icon-cog"></i>How to Get Started</h2>
    <p>This is the step by step guide on how to get started to use ShuleSoft in your school</p>
    <section id="how_to_start">
        <h4 id="add-teacher">View Teacher</h4>

					<pre class="prettyprint">
Login as Accountant
Select user menu in sidebar
Click  Teacher
Then List of teacher will be displayed
					</pre>
        <p>
            <img src="<?= base_url() ?>assets/images/account/teachers_view.png" width="1000" style="padding: 0px 20px;"/>
        </p>



    </section>

</section>

<section class="block-templates">
    <h2 class="item5"><i class="icon-play-circle"></i> How to View Expenses</h2>
    <section id="expenses">

        <section id="expenses">
            <h4 >Add  Expenses</h4>
					<pre class="prettyprint">
Login as parent
Select Expenses menu in sidebar
Click Add Expense button and fill data in the form displayed
Click add Expense

					</pre>
            <p>
                <img src="<?= base_url() ?>assets/images/account/add-expenses.png" width="1000" style="padding: 0px 20px;"/>
            </p>
        </section>

        <section id="expenses">
            <h4 >View  Expenses</h4>
					<pre class="prettyprint">
Login as Accountant
Select Expenses menu in sidebar
Then The list will be shown

					</pre>
            <p>
                <img src="<?= base_url() ?>assets/images/account/view_expenses.png" width="1000" style="padding: 0px 20px;"/>
            </p>
        </section>

        <section id="balance">
            <h4>Add Balance</h4>
					<pre class="prettyprint">
Login as Parent
Select Books menu in sidebar
Then list of books will be displayed

					</pre>
            <p>
                <img src="<?= base_url() ?>assets/images/account/check_balance.png" width="1000" style="padding: 0px 20px;"/>
            </p>        </section>

        <section id="feetype">
            <h4>ADD Feetype</h4>
					<pre class="prettyprint">
Login as Accountant
Select Feetype menu in sidebar
Click Add  feetype Button
 Then fill the form and click Add Fee

					</pre>
            <p>
                <img src="<?= base_url() ?>assets/images/account/add_feetype.png" width="1000" style="padding: 0px 20px;"/>
            </p>
            <p>
                <img src="<?= base_url() ?>assets/images/account/add_fee.png" width="1000" style="padding: 0px 20px;"/>
            </p>

            <h4>Edit Feetype</h4>
            <pre class="prettyprint">
Click ana icon for editing feetype
Then the form will appear and click update to update feetype

					</pre>
            <p>
                <img src="<?= base_url() ?>assets/images/account/edit_feetype.png" width="1000" style="padding: 0px 20px;"/>
            </p>
        </section>

        <section id="invoice">

            <h4> View Invoice</h4>
					<pre class="prettyprint">
Login as Accountant
Select Invoice menu in sidebar
Then select a class to view student with invoice

					</pre>
            <p>
                <img src="<?= base_url() ?>assets/images/account/view_invoices.png" width="1000" style="padding: 0px 20px;"/>
            </p>
            <h4>Edit Invoice</h4>
            <pre class="prettyprint">
Click ana icon for editing invoice
Then the form will appear and click update to update invoice

					</pre>
            <p>
                <img src="<?= base_url() ?>assets/images/account/edit_invoice.png" width="1000" style="padding: 0px 20px;"/>
            </p>

            <h4>View Student Invoice</h4>
            <p>
                <img src="<?= base_url() ?>assets/images/account/view_person_invoice.png" width="1000" style="padding: 0px 20px;"/>
            </p>

        </section>

        <section id="message">
            <h4>View Message</h4>
					<pre class="prettyprint">
Login as Acountant
Select Message menu in sidebar
Then message platform will be displayed and then you can compose and see the message

					</pre>
            <p>
                <img src="<?= base_url() ?>assets/images/account/message.png" width="1000" style="padding: 0px 20px;"/>
            </p>        </section>

        <section id="notice">

            <h4>View Notice</h4>
					<pre class="prettyprint">
Login as Accountant
Select Notice menu in sidebar
Then list of notices will be displayed

					</pre>
            <p>
                <img src="<?= base_url() ?>assets/images/account/notice_view.png" width="1000" style="padding: 0px 20px;"/>
            </p>        </section>

        <section id="transport">

            <h4>View Transport</h4>
					<pre class="prettyprint">
Login as Accountant
Select Transport menu in sidebar
Then list of transport routes will be displayed

					</pre>
            <p>
                <img src="<?= base_url() ?>assets/images/account/trasport1.png" width="1000" style="padding: 0px 20px;"/>
            </p>
            <h4>View Transport Member</h4>
            					<pre class="prettyprint">
Select a class to view member in transport

					</pre>
            <p>
                <img src="<?= base_url() ?>assets/images/account/trans2.png" width="1000" style="padding: 0px 20px;"/>
            </p>
            <h4>Add Transport Member</h4>
            					<pre class="prettyprint">
Select aN icon with plus sign to add member in transport
Then click add member

					</pre>
            <p>
                <img src="<?= base_url() ?>assets/images/account/trans3.png" width="1000" style="padding: 0px 20px;"/>
            </p>

        </section>
        <section id="hostel">

            <h4>View Hostels</h4>
					<pre class="prettyprint">
Login as Accountant
Select Hostel menu in sidebar
Then list of hostels will be displayed

					</pre>
            <p>
                <img src="<?= base_url() ?>assets/images/account/hostel_list.png" width="1000" style="padding: 0px 20px;"/>
            </p>

            <h4> Add Hostel Member</h4>
					<pre class="prettyprint">
Login as Accountant
Select Hostel menu in sidebar
Then select class to view list of hostel member
Click add button in the table with blue color
Form will be diplayed and then select hostel category and name then click add

					</pre>
            <p>
                <img src="<?= base_url() ?>assets/images/account/add_hostelmember.png" width="1000" style="padding: 0px 20px;"/>
            </p>

            <h4>View Hostel Member</h4>
					<pre class="prettyprint">
Login as Accountant
Select Hostel menu in sidebar
Then click member and list of memeber will be displayed

					</pre>
            <p>
                <img src="<?= base_url() ?>assets/images/account/add_hostelmember.png" width="1000" style="padding: 0px 20px;"/>
            </p>
            <h4>View Hostel Category</h4>
            <pre class="prettyprint">
   Click category menu in sidebar
    Then Click category to view all hostel categroy
            </pre>
            <p>
                <img src="<?= base_url() ?>assets/images/account/hostel_category.png" width="1000" style="padding: 0px 20px;"/>
            </p>

        </section>

    </section>


</div>
</div>
</div>

</div>
<footer id="conclusion">
    <div class="container">
        @ <a href="http://www.inetstz.com" target="_blank">INETS Company Limited </a>
    </div>
</footer>
</div>
</div>
<!-- Footer
 ================================================== -->


<div id="back-top">
    <a href="#"><i class="icon-double-angle-up"></i></a>
</div>
<!-- <script src="js/bootstrap-scrollspy.js"></script>  -->
<!--        <div class="language-modal">
        <div class="modal_bg"></div>
        <div class="modal">
        <div class="modal-header">
            <span class="modal_remove pull-right"><i class="icon-remove"></i></span>
            <h3>Choose language</h3>
        </div>
        <div class="modal-body">
            <ul id="modal_languages" class="nav nav-list bs-docs-sidenav"></ul>
        </div>
        </div>
    </div>-->
</body>
</html>
