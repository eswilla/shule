<?php


/**
 * Description of academicyear
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
?>
<div class="row">
    <p>Edit Academic Year</p>
	<form action="" method="post" id="academic_year">
	    <table  class="table table-striped table-bordered table-hover no-footer">
		<thead>
		    <tr>
			<th class="col-lg-2">
			    <input type="text" placeholder="Name" class="form-control" id="to" name="name" value="<?= set_value('name',$academic_year->name) ?>" ></th>
			<th class="col-lg-2">	<?php
			    $array = array(0 => 'select class level');
			    if (!empty($class_levels)) {
				foreach ($class_levels as $class_level) {
				    $array[$class_level->classlevel_id] = $class_level->name;
				}
			    }
			    echo form_dropdown("class_level_id", $array, set_value("class_level_id", $academic_year->id), "id='class_level_id' class='form-control'");
			    ?></th>
			<th class="col-lg-2">
			    <input  type="datetime" placeholder="Start Date" class="form-control date-picker" id="start_date" style="resize: vertical;" name="start_date" value="<?= set_value('start_date',$academic_year->start_date) ?>" />
			</th>
			<th class="col-lg-2">
			    <input  type="datetime" placeholder="End Date" class="form-control date-picker" id="end_date" style="resize: vertical;" name="end_date" value="<?= set_value('end_date',$academic_year->end_date) ?>" />
			</th>
			<th class="col-lg-2">
			    <input type="submit" id="send_pdf" class="btn btn-success" value="Update" /></th>
		    </tr>
		</thead>
	    </table>
	</form>
	<br/><br/>
    </div>
<script type="text/javascript">
    $(document).ready(function () {

	$(".date-picker").click(function () {
	    $(this).pickadate({
		selectYears: 50,
		selectMonths: true,
		max: new Date("<?= date('Y') + 1 ?>")
	    });
	});

    });
</script>