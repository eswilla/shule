<link href="<?php echo base_url('assets/custom_gante.css'); ?>" rel="stylesheet">
<link rel="stylesheet" href="<?= base_url()?>/assets/css/signature-pad.css">
<div class="box container">
    <div class="box-header">
        <h3 class="box-title"><i class="fa fa-gears"></i> <?=$this->lang->line('panel_title')?></h3>

<ol class="breadcrumb">
    <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
    <li class="active"><?=$this->lang->line('menu_setting')?></li>
</ol>
</div><!-- /.box-header -->
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="bs-example" data-example-id="simple-jumbotron">
            <div class="jumbotron">
                <div id="signature-pad" class="m-signature-pad col-md-12 col-sm-12">
                    <div class="m-signature-pad--body">
                        <canvas width="100" height="100"></canvas>
                    </div>
                    <div class="m-signature-pad--footer">
                        <div class="description">Sign above</div>
                        <button class="btn btn-warning pull-left" data-action="clear">Clear</button>
                        <button class="btn btn-success pull-right" data-action="save">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
<script src="<?= base_url()?>/assets/js/signature_pad.js"></script>
<script src="<?= base_url()?>/assets/js/app.js"></script>
<script>
    $(document).ready(function(){

    });

</script>