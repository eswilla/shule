
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa fa-gears"></i> <?= $this->lang->line('panel_title') ?></h3>


        <ol class="breadcrumb">
            <li><a href="<?= base_url("dashboard/index") ?>"><i class="fa fa-laptop"></i> <?= $this->lang->line('menu_dashboard') ?></a></li>
            <li class="active"><?= $this->lang->line('menu_setting') ?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">

	    <div class="x_panel">
		<div class="x_title">
                    <h2><i class="fa fa-bars"></i> System Settings </h2>
                    <div class="clearfix"></div>
		</div>
		<div class="x_content">

                    <div class="col-xs-3">
			<!-- required for floating -->
			<!-- Nav tabs -->
			<ul class="nav nav-tabs tabs-left">
			    <li class="active"><a href="#home" data-toggle="tab">General</a>
			    </li>
			    <li><a href="#profile" data-toggle="tab">Currency</a>
			    </li>
			    <li><a href="#messages" data-toggle="tab">Site Logo</a>
			    </li>
			    <li><a href="#settings" data-toggle="tab">SMS Settings</a>
			    </li>
			    <li><a href="#academic" data-toggle="tab">Academic Years</a>
			    </li>
			</ul>
                    </div>

                    <div class="col-xs-9">
			<!-- Tab panes -->
			<div class="tab-content">
			    <div class="tab-pane active" id="home">
				<p class="lead">General</p>
				<div class="col-sm-12">
				    <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
					<?php
					if (form_error('sname'))
					    echo "<div class='form-group has-error' >";
					else
					    echo "<div class='form-group' >";
					?>
					<label for="sname" class="col-sm-2 control-label">
					    <?= $this->lang->line("setting_school_name") ?>
					</label>
					<div class="col-sm-6">
					    <input type="text" class="form-control" id="sname" name="sname" value="<?= set_value('sname', $setting->sname) ?>" >
					</div>
					<span class="col-sm-4 control-label">
					    <?php echo form_error('sname'); ?>
					</span>
				</div>

				<?php
				if (form_error('phone'))
				    echo "<div class='form-group has-error' >";
				else
				    echo "<div class='form-group' >";
				?>
				<label for="phone" class="col-sm-2 control-label">
				    <?= $this->lang->line("setting_school_phone") ?>
				</label>
				<div class="col-sm-6">
				    <input type="text" class="form-control" id="phone" name="phone" value="<?= set_value('phone', $setting->phone) ?>" >
				</div>
				<span class="col-sm-4 control-label">
				    <?php echo form_error('phone'); ?>
				</span>
			    </div>

			    <?php
			    if (form_error('email'))
				echo "<div class='form-group has-error' >";
			    else
				echo "<div class='form-group' >";
			    ?>
			    <label for="email" class="col-sm-2 control-label">
				<?= $this->lang->line("setting_school_email") ?>
			    </label>
			    <div class="col-sm-6">
				<input type="text" class="form-control" id="email" name="email" value="<?= set_value('email', $setting->email) ?>" >
			    </div>
			    <span class="col-sm-4 control-label">
				<?php echo form_error('email'); ?>
			    </span>
			</div>

			<?php
			if (form_error('automation'))
			    echo "<div class='form-group has-error' >";
			else
			    echo "<div class='form-group' >";
			?>
                        <label for="automation" class="col-sm-2 control-label">
			    <?php //$this->lang->line("setting_school_automation")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="hidden"  class="form-control" id="automation" name="automation" value="26" >
                        </div>
                        <span class="col-sm-4 control-label">
			    <?php echo form_error('automation'); ?>
                        </span>
                    </div>



		    <?php
		    if (form_error('footer'))
			echo "<div class='form-group has-error' >";
		    else
			echo "<div class='form-group' >";
		    ?>
		    <label for="footer" class="col-sm-2 control-label">
			<?php //$this->lang->line("setting_school_footer")?>
		    </label>
		    <div class="col-sm-6">
			<input type="hidden" class="form-control" id="footer" name="footer" value="<?= set_value('footer', $setting->footer) ?>" >
		    </div>
		    <span class="col-sm-4 control-label">
			<?php echo form_error('footer'); ?>
		    </span>
		</div>

		<?php
		if (form_error('website'))
		    echo "<div class='form-group has-error' >";
		else
		    echo "<div class='form-group' >";
		?>
		<label for="address" class="col-sm-2 control-label">
		    <?= $this->lang->line("setting_school_website") ?>
		</label>
		<div class="col-sm-6">
		    <input type="text" class="form-control"  id="website" name="website" value="<?= set_value('website', $setting->website) ?>"/>
		</div>
		<span class="col-sm-4 control-label">
		    <?php echo form_error('website'); ?>
		</span>
	    </div>

	    <?php
	    if (form_error('pobox'))
		echo "<div class='form-group has-error' >";
	    else
		echo "<div class='form-group' >";
	    ?>
	    <label for="address" class="col-sm-2 control-label">
		<?= $this->lang->line("setting_school_box") ?>
	    </label>
	    <div class="col-sm-6">
		<input type="text" class="form-control"  id="box" name="box" value="<?= set_value('box', $setting->box) ?>"/>
	    </div>
	    <span class="col-sm-4 control-label">
		<?php echo form_error('pobox'); ?>
	    </span>
	</div>

	<?php
	if (form_error('address'))
	    echo "<div class='form-group has-error' >";
	else
	    echo "<div class='form-group' >";
	?>
	<label for="address" class="col-sm-2 control-label">
	    <?= $this->lang->line("setting_school_address") ?>
	</label>
	<div class="col-sm-6">
	    <textarea class="form-control" style="resize:none;" id="address" name="address"><?= set_value('address', $setting->address) ?></textarea>
	</div>
	<span class="col-sm-4 control-label">
	    <?php echo form_error('address'); ?>
	</span>
    </div>
    <?php
    if (form_error('pass_mark'))
	echo "<div class='form-group has-error' >";
    else
	echo "<div class='form-group' >";
    ?>
    <label for="address" class="col-sm-2 control-label">
	<?= $this->lang->line("setting_school_pass_mark") ?>
    </label>
    <div class="col-sm-6">
	<input type="text" class="form-control" id="pass_mark" name="pass_mark" value="<?= set_value('pass_mark', $setting->pass_mark) ?>"/>
    </div>
    <span class="col-sm-4 control-label">
	<?php echo form_error('pass_mark'); ?>
    </span>
</div>

<?php
if (form_error('motto'))
    echo "<div class='form-group has-error' >";
else
    echo "<div class='form-group' >";
?>
<label for="address" class="col-sm-2 control-label">
    <?= $this->lang->line("setting_school_motto") ?>
</label>
<div class="col-sm-6">
    <input type="text" class="form-control" id="motto" name="motto" value="<?= set_value('motto', $setting->motto) ?>"/>
</div>
<span class="col-sm-4 control-label">
    <?php echo form_error('motto'); ?>
</span>
</div>

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-8">
	<input type="submit" class="btn btn-success" value="<?= $this->lang->line("update_setting") ?>" >
    </div>
</div>

</form>
</div>
</div>
<div class="tab-pane" id="profile">
    <p class="lead">Currency Settings</p>
    <div class="col-sm-12">
	<form class="form-horizontal" role="form" method="post" >



	    <?php
	    if (form_error('currency_code'))
		echo "<div class='form-group has-error' >";
	    else
		echo "<div class='form-group' >";
	    ?>
	    <label for="currency_code" class="col-sm-2 control-label">
		<?= $this->lang->line("setting_school_currency_code") ?>
	    </label>
	    <div class="col-sm-6">
		<input type="text" class="form-control" id="currency_code" name="currency_code" value="<?= set_value('currency_code', $setting->currency_code) ?>" >
	    </div>
	    <span class="col-sm-4 control-label">
		<?php echo form_error('currency_code'); ?>
	    </span>
    </div>

    <?php
    if (form_error('currency_symbol'))
	echo "<div class='form-group has-error' >";
    else
	echo "<div class='form-group' >";
    ?>
    <label for="currency_symbol" class="col-sm-2 control-label">
	<?= $this->lang->line("setting_school_currency_symbol") ?>
    </label>
    <div class="col-sm-6">
	<input type="text" class="form-control" id="currency_symbol" name="currency_symbol" value="<?= set_value('currency_symbol', $setting->currency_symbol) ?>" >
    </div>
    <span class="col-sm-4 control-label">
	<?php echo form_error('currency_symbol'); ?>
    </span>
</div>


<div class="form-group">
    <div class="col-sm-offset-2 col-sm-8">
	<input type="submit" class="btn btn-success" value="<?= $this->lang->line("update_setting") ?>" >
    </div>
</div>

</form>
</div>
</div>
<div class="tab-pane" id="messages">
    <p class="lead">School Logo </p>
    <div class="col-sm-12">
	<form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">

	    <?php
	    if (isset($image))
		echo "<div class='form-group has-error' >";
	    else
		echo "<div class='form-group' >";
	    ?>
	    <label for="photo" class="col-sm-2 control-label">
		<?= $this->lang->line("setting_school_photo") ?>
	    </label>
	    <div class="col-sm-4">
		<input class="form-control" accept=".jpg,.png,.jpge"  id="uploadFile" placeholder="Choose File" disabled />  
	    </div>

	    <div class="col-sm-2">
		<div class="fileUpload btn btn-success form-control">
		    <span class="fa fa-repeat"></span>
		    <span><?= $this->lang->line("upload_setting") ?></span>
		    <input id="uploadBtn" type="file" class="upload" name="image" />
		</div>
	    </div>
	    <span class="col-sm-4 control-label">
		<?php if (isset($image)) echo $image; ?>
	    </span>
    </div>



    <div class="form-group">
	<div class="col-sm-offset-2 col-sm-8">
	    <input type="submit" class="btn btn-success" value="<?= $this->lang->line("update_setting") ?>" >
	</div>
    </div>

</form>
</div>

</div>
<div class="tab-pane" id="settings">
    <p class="lead">SMS Settings</p>
    <div class="col-sm-12">
	<form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">


	    <?php
	    if (form_error('api_key'))
		echo "<div class='form-group has-error' >";
	    else
		echo "<div class='form-group' >";
	    ?>
	    <label for="footer" class="col-sm-2 control-label">
		<?php //$this->lang->line("setting_school_footer")?>
		karibuSMS API KEY
	    </label>
	    <div class="col-sm-6">
		<input type="text" class="form-control" id="api_key" name="api_key" value="<?= set_value('api_key', $setting->api_key) ?>" >
	    </div>
	    <span class="col-sm-4 control-label">
		<?php echo form_error('api_key'); ?>
	    </span>
    </div>
    <?php
    if (form_error('api_secret'))
	echo "<div class='form-group has-error' >";
    else
	echo "<div class='form-group' >";
    ?>
    <label for="api_secret" class="col-sm-2 control-label">
	<?php //$this->lang->line("setting_school_footer")?>
	karibuSMS API SECRET
    </label>
    <div class="col-sm-6">
	<input type="text" class="form-control" id="api_secret" name="api_secret" value="<?= set_value('api_secret', $setting->api_secret) ?>" >
    </div>
    <span class="col-sm-4 control-label">
	<?php echo form_error('api_secret'); ?>
    </span>
</div>



<div class="form-group">
    <div class="col-sm-offset-2 col-sm-8">
	<input type="submit" class="btn btn-success" value="<?= $this->lang->line("update_setting") ?>" >
    </div>
</div>

</form>
</div>
</div>


<div class="tab-pane" id="academic">
    <p class="lead">Academic Year</p>
    <p align="right"><button class="btn btn-success" data-toggle="modal" data-target="#mail">Add New </button></p>

    <div>
	<form action="" method="post" id="academic_year">
	    <table  class="table table-striped table-bordered table-hover no-footer">
		<thead>
		    <tr>
			<th class="col-lg-2">
			    <input type="text" placeholder="Name" class="form-control" id="to" name="name" value="<?= set_value('name') ?>" ></th>
			<th class="col-lg-2">	<?php
			    $array = array(0 => 'select class level');
			    if (!empty($class_levels)) {
				foreach ($class_levels as $class_level) {
				    $array[$class_level->classlevel_id] = $class_level->name;
				}
			    }
			    echo form_dropdown("class_level_id", $array, set_value("class_level_id", 0), "id='class_level_id' class='form-control'");
			    ?></th>
			<th class="col-lg-2">
			    <input  type="datetime" placeholder="Start Date" class="form-control date-picker" id="start_date" style="resize: vertical;" name="start_date" value="<?= set_value('start_date') ?>" />
			</th>
			<th class="col-lg-2">
			    <input  type="datetime" placeholder="End Date" class="form-control date-picker" id="end_date" style="resize: vertical;" name="end_date" value="<?= set_value('end_date') ?>" />
			</th>
			<th class="col-lg-2">
			    <input type="button" id="send_pdf" class="btn btn-success" value="Add" onclick="add_year()" /></th>
		    </tr>
		</thead>
	    </table>
	</form>
	<br/><br/>
    </div>
    <div class="col-sm-12">
	<div id="hide-table">
	    <table  class="table table-striped table-bordered table-hover no-footer">
		<thead>
		    <tr>
			<th class="col-lg-2">No</th>
			<th class="col-lg-2">Name</th>
			<th class="col-lg-2">Level Name</th>
			<th class="col-lg-2">Start date</th>
			<th class="col-lg-2">End date</th>
			<!--<th class="col-lg-2">Status</th>-->
			<th class="col-lg-2">Action</th>
		    </tr>
		</thead>
		<tbody>
		    <?php
		    if (!empty($academic_years)) {
			$i = 1;
			foreach ($academic_years as $academic_year) {
			    ?>
			    <tr>
				<td data-title="<?= $this->lang->line('slno') ?>">
				    <?= $i ?>
				</td>
				<td data-title="<?= $this->lang->line('academic_name') ?>">
				    <?= $academic_year->name ?>
				</td>
				<td data-title="<?= $this->lang->line('level_name') ?>">
				    <?=  $array[$academic_year->class_level_id]; ?>
				</td>
				<td data-title="<?= $this->lang->line('start_date') ?>">
				    <?=$academic_year->start_date; ?>
				</td>
				<td data-title="<?= $this->lang->line('end_date') ?>">
				      <?=$academic_year->end_date; ?>
				</td>
<!--				<td data-title="<?= $this->lang->line('status') ?>">
				    <?php
//				    if ($academic_year->status == 1) {
//					echo 'Active';
//				    } else if ($academic_year->status = 2) {
//					echo 'Next Academic';
//				    }
				    ?>
				</td>-->
				<td data-title="<?= $this->lang->line('action') ?>">
				 
				    <?php echo btn_edit('setting/edit_academic/' . $academic_year->id, $this->lang->line('edit')) ?>
				</td>
			    </tr>
			    <?php
			    $i++;
			}
		    }
		    ?>
		<div id="add_year"></div>
		</tbody>
	    </table>
	</div>

    </div>

</div>
</div>

<div class="clearfix"></div>

</div>
</div>
</div>
</div>
</div>

<script type="text/javascript">
    document.getElementById("uploadBtn").onchange = function () {
	document.getElementById("uploadFile").value = this.value;
    };
    $(document).ready(function () {

	$(".date-picker").click(function () {
	    $(this).pickadate({
		selectYears: 50,
		selectMonths: true,
		max: new Date("<?= date('Y') + 2 ?>")
	    });
	});

    });
    add_year = function () {
	var form = $('#academic_year').serialize();
	$.ajax({
	    type: 'POST',
	    url: "<?= base_url('setting/academic_year') ?>",
	    data: {formdata: form},
	    dataType: "html",
	    success: function (data) {
		console.log(data);
		if (data == '1') {
		    window.location.reload();
		} else {
		    swal('warning', data, 'warning');
		}
	    }
	});
    }
</script>
