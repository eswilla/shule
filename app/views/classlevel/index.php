
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa fa-signal"></i> <?=$this->lang->line('panel_title')?></h3>

       
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active"><?=$this->lang->line('menu_classlevel')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">

                <?php 
                    $usertype = $this->session->userdata("usertype");
                    if($usertype == "Admin") {
                ?>
                    <h5 class="page-header">
                        <a class="btn btn-success" href="<?php echo base_url('classlevel/add') ?>">
                            <i class="fa fa-plus"></i> 
                            <?=$this->lang->line('add_title')?>
                        </a>
                    </h5>
                <?php } ?>

                 <div id="hide-table">
                    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead>
                            <tr>
                                <th class="col-lg-1"><?=$this->lang->line('slno')?></th>
                                <th class="col-lg-2"><?=$this->lang->line('name')?></th>
                                <th class="col-lg-2"><?=$this->lang->line('start_date')?></th>
                                <th class="col-lg-2"><?=$this->lang->line('end_date')?></th>
                                <th class="col-lg-1"><?=$this->lang->line('span_number')?></th>
				<th class="col-lg-1"><?=$this->lang->line('result_format')?></th>
                                <th class="col-lg-2"><?=$this->lang->line('note')?></th>
                                <th class="col-lg-3"><?=$this->lang->line('action')?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(count($classlevels)) {$i = 1; foreach($classlevels as $classlevel) { ?>
                                <tr>
                                    <td data-title="<?=$this->lang->line('slno')?>">
                                        <?php echo $i; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('name')?>">
                                        <?php echo $classlevel->name; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('start_date')?>">
                                        <?php echo $classlevel->start_date; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('end_date')?>">
                                        <?php echo $classlevel->end_date ; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('span_number')?>">
                                        <?php echo $classlevel->span_number; ?>
                                    </td>
				    <td data-title="<?=$this->lang->line('result_format')?>">
                                        <?php echo $classlevel->result_format; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('note')?>">
                                        <?php echo $classlevel->note; ?>
                                    </td>

                                    <td data-title="<?=$this->lang->line('action')?>">
                                        <?php echo btn_edit('classlevel/edit/'.$classlevel->classlevel_id, $this->lang->line('edit')) ?>
                                        <?php echo btn_delete('classlevel/delete/'.$classlevel->classlevel_id, $this->lang->line('delete')) ?>
                                    </td>
                                </tr>
                            <?php $i++; }} ?>
                        </tbody>
                    </table>
                </div>


            </div> <!-- col-sm-12 -->
        </div><!-- row -->
    </div><!-- Body -->
</div><!-- /.box -->
