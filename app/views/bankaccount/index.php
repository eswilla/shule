
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa icon-feetype"></i> <?=$this->lang->line('panel_title')?></h3>

       
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active"><?=$this->lang->line('menu_bankaccount')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">

                <h5 class="page-header">
                    <a class="btn btn-success" href="<?php echo base_url('bankaccount/add') ?>">
                        <i class="fa fa-plus"></i> 
                        <?=$this->lang->line('add_title')?>
                    </a>
                </h5>
                <div id="hide-table">
                    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead>
                            <tr>
                                <th class="col-sm-1"><?=$this->lang->line('slno')?></th>
                                <th class="col-sm-2"><?=$this->lang->line('bankaccount_name')?></th>
				<th class="col-sm-1"><?=$this->lang->line('bankaccount_branch')?></th>
				<th class="col-sm-1"><?=$this->lang->line('bankaccount_account_name')?></th>
				<th class="col-sm-1"><?=$this->lang->line('bankaccount_account')?></th>
				<th class="col-sm-1"><?=$this->lang->line('bankaccount_currency')?></th>
				<th class="col-sm-1"><?=$this->lang->line('bankaccount_swiftcode')?></th>
                                <th class="col-sm-4"><?=$this->lang->line('bankaccount_note')?></th>
                                <th class="col-sm-3"><?=$this->lang->line('action')?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(count($bankaccounts)) {$i = 1; foreach($bankaccounts as $account) { ?>
                                <tr>
                                    <td data-title="<?=$this->lang->line('slno')?>">
                                        <?php echo $i; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('bankaccount_name')?>">
                                        <?php echo $account->bank_name; ?>
                                    </td>
				    <td data-title="<?=$this->lang->line('bankaccount_branch')?>">
                                        <?php echo $account->bank_branch ?>
                                    </td>
				     <td data-title="<?=$this->lang->line('bankaccount_amount_name')?>">
                                        <?php echo $account->account_name; ?>
                                    </td>
				    <td data-title="<?=$this->lang->line('bankaccount_amount')?>">
                                        <?php echo $account->account_number; ?>
                                    </td>
				    
				    <td data-title="<?=$this->lang->line('bankaccount_currency')?>">
                                        <?php echo $account->currency ?>
                                    </td>
				    <td data-title="<?=$this->lang->line('bankaccount_swiftcode')?>">
                                        <?php echo $account->bank_swiftcode ?>
                                    </td>
				   
                                    <td data-title="<?=$this->lang->line('bankaccount_note')?>">
                                        <?php echo $account->note; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('action')?>">
                                        <?php echo btn_edit('bankaccount/edit/'.$account->bank_account_id, $this->lang->line('edit')) ?>
                                        <?php echo btn_delete('bankaccount/delete/'.$account->bank_account_id, $this->lang->line('delete')) ?>
                                    </td>
                                </tr>
                            <?php $i++; }} ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>