<?php

function btn_add($uri, $name) {
    return anchor($uri, "<i class='fa fa-plus'></i>", "class='btn btn-primary add_student btn-xs mrg' data-placement='top' data-toggle='tooltip' data-original-title='" . $name . "'");
}

function btn_view($uri, $name) {
    return anchor($uri, "<i class='fa fa-check-square-o'></i>", "class='btn btn-success btn-xs mrg' data-placement='top' data-toggle='tooltip' data-original-title='" . $name . "'");
}

function btn_edit($uri, $name) {
    return anchor($uri, "<i class='fa fa-edit'></i>", "class='btn btn-warning btn-xs mrg' data-placement='top' data-toggle='tooltip' data-original-title='" . $name . "'");
}

function btn_delete($uri, $name) {
    return anchor($uri, "<i class='fa fa-trash-o'></i>", array(
	'onclick' => "return confirm('you are about to delete a record. This cannot be undone. are you sure?')",
	'class' => 'btn btn-danger btn-xs mrg',
	'data-placement' => 'top',
	'data-toggle' => 'tooltip',
	'data-original-title' => $name
	    )
    );
}

function reset_keys($array) {
    $data = array();
    $i = 0;
    foreach ($array as $value) {
	$data[$i] = $value;
	$i++;
    }
    return $data;
}

function delete_file($uri, $id) {
    return anchor($uri, "<i class='fa fa-times '></i>", array(
	'onclick' => "return confirm('you are about to delete a record. This cannot be undone. are you sure?')",
	'id' => $id,
	'class' => "close pull-right"
	    )
    );
}

function share_file($uri, $id) {
    return anchor($uri, "<i class='fa fa-globe'></i>", array(
	'onclick' => "return confirm('you are about to delete a record. This cannot be undone. are you sure?')",
	'id' => $id,
	'class' => "pull-right"
	    )
    );
}

function btn_dash_view($uri, $name) {
    return anchor($uri, "<span class='fa fa-check-square-o'></span>", "class='btn btn-success btn-xs mrg' style='background-color:#1abc9c;color:#fff;border:1px solid #1abc9c' data-placement='top' data-toggle='tooltip' data-original-title='" . $name . "'");
}

function btn_invoice($uri, $name) {
    return anchor($uri, "<i class='fa fa-credit-card'></i>", "class='btn btn-primary btn-xs mrg' data-placement='top' data-toggle='tooltip' data-original-title='" . $name . "'");
}

function btn_return($uri, $name) {
    return anchor($uri, "<i class='fa fa-mail-forward'></i>", array(
	"onclick" => "return confirm('you are return the book . This cannot be undone. are you sure?')",
	"class" => 'btn btn-danger btn-xs',
	'data-placement' => 'top',
	'data-toggle' => 'tooltip',
	'data-original-title' => $name
	    )
    );
}

function btn_attendance($id, $method, $class, $name) {
    return "<input type='checkbox' class='" . $class . "' $method id='" . $id . "' data-placement='top' data-toggle='tooltip' data-original-title='" . $name . "' >  ";
    // return anchor($uri, "<i class='fa fa-credit-card'></i>", "class='btn btn-primary btn-xs mrg' data-placement='top' data-toggle='tooltip' data-original-title='".$name."'");
}

function btn_promotion($id, $class, $name) {
    $warning = '';
    return "<input type='checkbox' class='" . $class . "' id='" . $id . "' data-placement='top' data-toggle='tooltip' data-original-title='" . $name . "'  $warning >  ";
    // return anchor($uri, "<i class='fa fa-credit-card'></i>", "class='btn btn-primary btn-xs mrg' data-placement='top' data-toggle='tooltip' data-original-title='".$name."'");
}

if (!function_exists('dump')) {

    function dump($var, $label = 'Dump', $echo = TRUE) {
	// Store dump in variable 
	ob_start();
	var_dump($var);
	$output = ob_get_clean();

	// Add formatting
	$output = preg_replace("/\]\=\>\n(\s+)/m", "] => ", $output);
	$output = '<pre style="background: #FFFEEF; color: #000; border: 1px dotted #000; padding: 10px; margin: 10px 0; text-align: left;">' . $label . ' => ' . $output . '</pre>';

	// Output
	if ($echo == TRUE) {
	    echo $output;
	} else {
	    return $output;
	}
    }

}


if (!function_exists('dump_exit')) {

    function dump_exit($var, $label = 'Dump', $echo = TRUE) {
	dump($var, $label, $echo);
	exit;
    }

}

// infinite coding starts here..
function btn_add_pdf($uri, $name) {
    return anchor($uri, "<i class='fa fa-file'></i> " . $name, "class='btn-cs btn-sm-cs' style='text-decoration: none;' role='button' target='_blank'");
}

function btn_sm_edit($uri, $name) {
    return anchor($uri, "<i class='fa fa-edit'></i> " . $name, "class='btn-cs btn-sm-cs' style='text-decoration: none;' role='button'");
}

function btn_sm_add($uri, $name) {
    return anchor($uri, "<i class='fa fa-plus'></i> " . $name, "class='btn-cs btn-sm-cs' style='text-decoration: none;' role='button'");
}

function btn_payment($uri, $name) {
    return anchor($uri, "<i class='fa fa-credit-card'></i> " . $name, "class='btn-cs btn-sm-cs'style='text-decoration: none;' role='button'");
}

/**
 * @uses: This function will be used to detect aa schema name
 *         which is the username of that school
 * @return string
 */
function set_schema_name() {
    $server_name = $_SERVER['SERVER_NAME'];

    $domain = strtolower(str_replace('.shulesoft.com', '', $server_name));
    if ($domain == 'localhost') { // if condition for local IP will be here. If not localhost, put your server name
	$schema = LOCAL_SCHEMA;
    } else if ($domain == 'demo') {
	$schema = 'public.';
    } else {
	$schema = $domain . '.';
    }
    return $schema;
}

set_error_handler('errorHandler');

//this records errors that occurs in files or systems, its useful much

function errorHandler($errno, $errmsg, $filename, $linenum, $vars) {
    // timestamp for the error entry
    $dt = date("Y-M-d H:i:s");

    // define an assoc array of error string
    // in reality the only entries we should
    // consider are E_WARNING, E_NOTICE, E_USER_ERROR,
    // E_USER_WARNING and E_USER_NOTICE
    $errortype = array(
	E_ERROR => 'Error',
	E_WARNING => 'Warning',
	E_PARSE => 'Parsing Error',
	E_NOTICE => 'Notice',
	E_CORE_ERROR => 'Core Error',
	E_CORE_WARNING => 'Core Warning',
	E_COMPILE_ERROR => 'Compile Error',
	E_COMPILE_WARNING => 'Compile Warning',
	E_USER_ERROR => 'User Error',
	E_USER_WARNING => 'User Warning',
	E_USER_NOTICE => 'User Notice',
	E_STRICT => 'Runtime Notice',
	E_RECOVERABLE_ERROR => 'Catchable Fatal Error'
    );
    // set of errors for which a var trace will be saved
    $user_errors = array(E_USER_ERROR, E_USER_WARNING, E_USER_NOTICE);

    $object = new MY_Controller();

    $err = "<br/><hr/><ul>\n";
    $err .= "\t<li>date time " . $dt . "</li>\n";
    $err .= "\t<li>errornum " . $errno . "</li>\n";
    $err .= "\t<li>errortype " . $errortype[$errno] . "</li>\n";
    $err .= "\t<li>error msg: " . $errmsg . "</li>\n";
    $err .= "\t<li>request url: " . $_SERVER['REQUEST_URI'] . "</li>\n";
    $err .= "\t<li>File name: " . $filename . "</li>\n";
    $err .= "\t<li>Line no: " . $linenum . "</li>\n";
    $err .= "\t<li>Error user agent: " . $object->user_agent()->agent . "</li>\n";
    $err .= "\t<li>From which Computer Platform: " . $object->user_agent()->platform . "</li>\n";
    $err .= "\t<li>Error from which username: " . gethostname() . "</li>\n";

    if (in_array($errno, $user_errors)) {
	$err .= "\t<li>var trace: " . wddx_serialize_value($vars, "Variables") . "</li>\n";
    }
    $err .= "</ul>\n\n";


    // save to the error log, and e-mail me if there is a critical user error
    // Don't change this constant file path as is independend of root folder name
    $filename = set_schema_name() . '_' . str_replace('-', '_', date('Y-M-d')) . '.html';

    error_log($err, 3, dirname(__FILE__) . "/../../assets/errors/" . $filename);
}

/*
 * *  Function:   Convert number to string
 * *  Arguments:  int
 * *  Returns:    string
 * *  Description:
 * *      Converts a given integer (in range [0..1T-1], inclusive) into
 * *      alphabetical format ("one", "two", etc.).
 */

function number_to_words($number) {
    if (($number < 0) || ($number > 999999999)) {
	return "$number";
    }

    $Gn = floor($number / 1000000);  /* Millions (giga) */
    $number -= $Gn * 1000000;
    $kn = floor($number / 1000);     /* Thousands (kilo) */
    $number -= $kn * 1000;
    $Hn = floor($number / 100);      /* Hundreds (hecto) */
    $number -= $Hn * 100;
    $Dn = floor($number / 10);       /* Tens (deca) */
    $n = $number % 10; /* Ones */

    $res = "";

    if ($Gn) {
	$res .= number_to_words($Gn) . " Million";
    }

    if ($kn) {
	$res .= (empty($res) ? "" : " ") .
		number_to_words($kn) . " Thousand";
    }

    if ($Hn) {
	$res .= (empty($res) ? "" : " ") .
		number_to_words($Hn) . " Hundred";
    }

    $ones = array("", "One", "Two", "Three", "Four", "Five", "Six",
	"Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen",
	"Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eightteen",
	"Nineteen");
    $tens = array("", "", "Twenty", "Thirty", "Fourty", "Fifty", "Sixty",
	"Seventy", "Eigthy", "Ninety");

    if ($Dn || $n) {
	if (!empty($res)) {
	    $res .= " and ";
	}

	if ($Dn < 2) {
	    $res .= $ones[$Dn * 10 + $n];
	} else {
	    $res .= $tens[$Dn];

	    if ($n) {
		$res .= "-" . $ones[$n];
	    }
	}
    }

    if (empty($res)) {
	$res = "zero";
    }

    return $res;
}

if (!function_exists('return_grade')) {

    function return_grade($grades, $avg, $level_format = NULL) {
	$data = '';

	foreach ($grades as $grade) {
	    if ($grade->gradefrom <= $avg && $grade->gradeupto >= $avg) {
		$data .= "<td>" .
			$grade->grade .
			"</td>";
		$data .= $level_format == 'ACSEE' ? "<td>" .
			$grade->point .
			"</td>" : '';
		$data .='<td>' . $grade->note . '</td>';
		return $data;
	    }
	}
    }

}

function cseeDivision($total_point, $penalty) {
    if ($total_point >= 7 && $total_point <= 17 && $penalty == 0) {
	$division = 'I';
    } else if ($total_point >= 18 && $total_point <= 21 && $penalty == 0) {
	$division = 'II';
    } else if ($penalty == 1 && $total_point <= 25) {
	$division = 'III';
    } elseif ($total_point >= 18 && $total_point <= 25) {
	$division = 'III';
    } else if ($total_point >= 26 && $total_point <= 33) {
	$division = 'IV';
    } else if ($total_point >= 34 && $total_point <= 35) {
	$division = '0';
    } else {
	$division = '0';
    }
    return $division;
}

function acseeDivision($total_point, $penalty) {
    if ($total_point >= 3 && $total_point <= 9 && $penalty == 0) {
	$division = 'I';
    } else if ($total_point >= 10 && $total_point <= 12 && $penalty == 0) {
	$division = 'II';
    } else if ($penalty == 1 && $total_point <= 12) {
	$division = 'III';
    } elseif ($total_point >= 13 && $total_point <= 17) {
	$division = 'III';
    } else if ($total_point >= 18 && $total_point <= 19) {
	$division = 'IV';
    } else if ($total_point >= 20 && $total_point <= 21) {
	$division = '0';
    } else {
	$division = '0';
    }
    return $division;
}
