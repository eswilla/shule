<?php
/**
 * Description of print_combined
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
$usertype = $this->session->userdata("usertype");
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <META HTTP-EQUIV="Pragma" CONTENT="no-cache">
        <title>report</title>

 <!--<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/many_partners_certificate.css" >-->     
	<link href="<?php echo base_url('assets/css/print_all/print.css'); ?>" rel="stylesheet" type="text/css" media="print">
	<!--<link href="<?php echo base_url('assets/bootstrap/bootstrap.min.css'); ?>" rel="stylesheet">-->
	<link href="<?php echo base_url('assets/css/print_all/certificate.css'); ?>" rel="stylesheet" type="text/css">

        <link rel="shortcut" href="<?= base_url(); ?>assets/images/favicon.ico">
	<!-- Begin shared CSS values -->
	<style type="text/css" >
	    .t {
		-webkit-transform-origin: top left;
		-moz-transform-origin: top left;
		-o-transform-origin: top left;
		-ms-transform-origin: top left;
		-webkit-transform: scale(0.25);
		-moz-transform: scale(0.25);
		-o-transform: scale(0.25);
		-ms-transform: scale(0.25);
		z-index: 2;
		position: absolute;
		white-space: pre;
		overflow: visible;
	    }
	</style>
	<!-- End shared CSS values -->


	<!-- Begin inline CSS -->
	<style type="text/css" >

	    #t1_1{left:26%;top:47px;letter-spacing:0.2px;word-spacing:-0.4px;}
	    #t2_1{left:26%;top:92px;letter-spacing:-0.6px;word-spacing:1.4px; font-size: 4em;}
	    #t3_1{left:26%;top:120px; font-size: 4em;}
	    #t4_1{left:26%;top:142px; font-size: 4em;}
	    #t5_1{left: 31%;  top: 141.5px; font-size: 4em;}
	    #t6_1{left:26%;top:168px;letter-spacing:-0.4px;word-spacing:0.5px; font-size: 4em;font-size: 4em;}
	    #t7_1{left:4%;top:170px; border-bottom: 8px solid black;
		  width: 70em;}

	    .s4_1{
		FONT-SIZE: 93.3px;
		FONT-FAMILY: sans-serif;
		color: rgb(0,0,0);
	    }


	    .s2_1{
		FONT-SIZE: 60.2px;
		FONT-FAMILY: sans-serif;
		color: rgb(0,0,0);
	    }

	    .s1_1{
		FONT-SIZE: 8em;
		FONT-FAMILY: sans-serif;
		color: rgb(0,0,0);
	    }

	    .s3_1{
		FONT-SIZE: 67.2px;
		FONT-FAMILY: sans-serif;
		color: rgb(5,99,193);
	    }

	    #form1_1{	z-index:2;	cursor: pointer;	border-style:none;	position: absolute;	left:376px;	top:197px;	width:242px;	height:23px;	background: transparent;	font:normal 18px Arial, Helvetica, sans-serif;}

	</style>
	<!-- End inline CSS -->

	<!-- Begin embedded font definitions -->
	<style id="fonts1" type="text/css" >

	    @font-face {
		font-family: DejaVuSans_b;
		src: url("../../assets/fonts/DejaVuSans_b.woff") format("woff");
	    }
	    table.table.table-striped td,table.table.table-striped th, table.table {
		border: 1px solid #000000;
		margin: 0 auto !important;
	    }
	    #report_footer{
		margin-left: 2em;
	    }
	    .headings{
		margin-left: 2em;
	    }
	    @media print { 
		table.table.table-striped td,table.table.table-striped th, table.table {
		    border: 1px solid #000000 !important;

		}
		.table-stripe {
		    background-color: #dedede !important;
		    border: 1px solid #000000 !important;
		}
		body, .panel {
		    margin: 0;
		    box-shadow: 0;
		}


	    }

	</style>
	<?php if ($level->result_format == 'CSEE') { ?>
    	<style  type="text/css" >
    	    .panel{
    		color: #000000 !important;

    	    }
    	    .wrappers{
    		border: 1px solid red;
    	    }
    	    #p1{
    		height: 220px !important;
    	    }
    	    table.table.table-striped td,table.table.table-striped th, table.table {
    		font-size: 12px !important;
    		margin-left: 10%;
    	    }
    	    @media print { 
    		.panel{
    		    padding-left: 10%;
    		}
    		table.table.table-striped td,table.table.table-striped th, table.table {
    		    border: 1px solid #000000 !important;
    		    margin-left: 1% !important;
    		    font-size: 11px !important;
    		}
    		.table-stripe {
    		    background-color: #dedede !important;
    		    border: 1px solid #000000 !important;
    		}
    		body, .panel {
    		    margin: 0;
    		    box-shadow: 0;
    		}
    		table.table{
    		    width: 55em !important;
    		    max-width: 55em !important;
    		    margin-left:auto; 
    		    margin-right:auto;}
    		table.table.table-striped td,table.table.table-striped th, table.table td{
    		    padding: 4px !important;
    		}
    		.headings{
    		    margin-left:2em;
    		}
    		#t7_1{
    		    width: 23em !important;
    		}
    		#wrappers{ 
    		    page-break-before: always;
    		}
    		h3, h4 {
    		    page-break-after: avoid;
    		}

    	    }

    	</style>
	<?php } ?>
    </head>
    <body onload="window.print()">
	<?php
	foreach ($students as $student_info) {
	    $student = (object) $student_info;
	    ?>
    	<div class="rows">
    	    <div class="col-md-12">

    		<div id="wrappers" style="overflow:hidden;  height: 265mm;  height: 265mm; border: 1px solid white; margin: 10mm auto; line-break: auto" >

    		    <section class="panel">
    			<div id="p1" style="overflow: hidden; left: 5%; position: relative; width: 935px; height: 250px;">

    			    <!-- End embedded font definitions -->
    			    <div class="letterhead"
    				 <!-- Begin text definitions (Positioned/styled in CSS) -->
    				 <div id="t1_1" class="t s1_1"><?= $siteinfos->sname ?></div>
    				<div id="t2_1" class="t s2_1"><?= 'P.O BOX ' . $siteinfos->box . ' <br/>' . $siteinfos->address ?>.</div>
    				<div id="t3_1" class="t s2_1">Cell: <?= str_replace(',', ' / ', $siteinfos->phone) ?></div>
    				<div id="t4_1" class="t s2_1">Email: </div>
    				<div id="t5_1" class="t s3_1" ><?= $siteinfos->email ?></div>
    				<div id="t6_1" class="t s2_1">Website: <?= $siteinfos->website ?></div>
    			    </div>
    			    <div id="t7_1" class="t s4_1">____</div>

    			    <!-- End text definitions -->


    			    <!-- Begin Form Data -->
    			    <form>
    				<input type="button"  tabindex="1"  id="form1_1" data-objref="5 0 R" title="mailto:<?= $siteinfos->email ?>" onclick="window.location.href = 'mailto:<?= $siteinfos->email ?>'" />

    			    </form>
    			    <!-- End Form Data -->
    			    <!-- Begin page background -->
    			    <div id="pg1Overlay" style="width:100%; height:10%; position:absolute; z-index:1; background-color:rgba(0,0,0,0); -webkit-user-select: none;"></div>
    			    <div id="pg1"><?php
				    $array = array(
					"src" => base_url('uploads/images/' . $siteinfos->photo),
					'width' => '145px',
					'height' => '135px',
					'class' => 'img-rounded',
					'style' => 'margin-left:2em; padding-top: 5%;'
				    );
				    echo img($array);
				    ?></div>


    			</div>

    			<div class="">
    			    <h1 align='center' style="font-size: 23px; font-weight: bolder">STUDENT PROGRESSIVE REPORT (<?= strtoupper($classes->classes) . ' ' . $year ?>)</h1>
    			</div>
    			<div class="panel-body profile-view-dis" style="max-height: 40; margin-top: 2em; padding: 0 5%;">
    			    <div class="row">
    				<div class="col-sm-9" style="float:left;" >
    				    <h1 style="font-weight: bolder;"> NAME: <?= strtoupper($student->name) ?></h1>
    				</div>
    				<div class="col-sm-3" style="float: right;">
    				    <h1 style="font-weight: bolder; font-size: 14px;"><?=
					    (isset($section_id) && $section_id == '') ?
						    'CLASS: ' . strtoupper($classes->classes) . '' :
						    'SECTION ' . $section
					    ?></h1>
    				</div>
    				<div style="clear:both;"></div>
    			    </div>

    			</div>

			    <?php if (count($exams) > 0 && !empty($exams)) {
				?>

				<div class="row"  style="margin: 2%;">
				    <?php
				    if ($exams) {

					$examController = new Exam();
					?>
	    			    <div class="col-lg-12">
	    				<table class="table table-striped table-bordered">
	    				    <thead>
	    					<tr>
	    					    <th style="font-weight: bolder">SUBJECT</th>

							<?php foreach ($exams as $exam) { ?>
							    <th style="font-weight: bolder"><?= strtoupper($exam->exam) ?></th>
							<?php } ?>

	    					    <th style="font-weight: bolder">AVERAGE</th>
	    					    <th style="font-weight: bolder">GRADE</th>
							<?php if ($level->result_format == 'ACSEE') { ?>
							    <th style="font-weight: bolder">POINTS</th>
							<?php } ?>
	    					    <th style="font-weight: bolder">REMARKS</th>
	    					</tr>
	    				    </thead>
	    				    <tbody>
						    <?php
						    $total_marks = 0;
						    foreach ($subjects as $subject) {
							?>
							<tr>
							    <td style="font-weight: bolder"><?= strtoupper($subject->subject) ?></td>

							    <?php
							    $total = 0;
							    $penalty = 0;
							    foreach ($exams as $exam) {

								$mark = $examController->get_total_marks($student->studentID, $exam->examID, $classesID, $subject->subjectID);
								$total+=$mark->total_mark;

								/*
								 * check penalty for that subject
								 */
								if ($subject->is_penalty == 1) {
								    foreach ($grades as $grade) {
									if ($grade->gradefrom == 0) {
									    if ($mark->total_mark < $grade->gradeupto) {
										$penalty = 1;
									    }
									}
								    }
								}
								?>
		    					    <th><?= $mark->total_mark == 0 ? '' : $mark->total_mark ?></th>
								<?php
							    }
							    $avg = round($total / count($exams), 1);
							    ?>

							    <td><?= $avg == 0 ? '' : $avg ?></td>

							    <?php
							    if ($avg == 0) {
								//this is a short time solution, we put empty but
								//later on we need to see if this student takes this subject or not
								echo '<td></td><td></td>';
							    } else {
								echo return_grade($grades, round($avg), $level->result_format);
							    }
							    $total_marks+=$avg;
							    ?>
							</tr>
							<?php
						    }

						    /**
						     * 
						     * 
						     * ----------------------------------------------------------------
						     * 
						     * We show Division & points only to A-level students. Those with
						     * grade format of ACSEE
						     */
						    if ($level->result_format == 'ACSEE') {
							?>
							<tr>
							    <td style="font-weight: bolder">DIVISION</td>
							    <?php
							    $i = 0;
							    $total_points = 0;
							    foreach ($exams as $exam) {
								$division[$i] = $examController->getDivision($student->studentID, $exam->examID);
								?>
		    					    <td><?= $division[$i][0] ?></td>
								<?php
								$total_points+=$division[$i][1];
								$i++;
							    }
							    $total_i = $i;
							    $total_average_points = floor($total_points / $total_i);
							    if ($level->result_format == 'CSEE') {
								$div = cseeDivision($total_average_points, $penalty);
							    } elseif ($level->result_format == 'ACSEE') {
								$div = acseeDivision($total_average_points, $penalty);
							    }
							    ?>
							    <td><?= $div ?></td>
							    <td></td>
							    <td></td>
							    <td></td>
							</tr>
							<tr>
							    <td style="font-weight: bolder">POINTS</td>
							    <?php
							    for ($i = 0; $i < $total_i; $i++) {
								?>
		    					    <td><?= $division[$i][1] ?></td>
								<?php
							    }
							    ?>
							    <td><?= $total_average_points ?></td>
							    <td></td>
							    <td></td>
							    <td></td>
							</tr>
							<?php
						    }

						    /**
						     * Otherwise, we don't show points and division
						     * ------------------------------------------------------
						     */
						    $section = isset($section_id) && $section_id == '' ? 'CLASS' : 'SECTION';
						    $sectionID = $section == 'CLASS' ? NULL : $student->sectionID;
						    ?>
	    					<tr>
	    					    <td style="font-weight: bolder">POSITION IN <?= $section ?></td>
							<?php
							foreach ($exams as $exam) {

							    $rank_info = $examController->get_rank_per_all_subjects($exam->examID, $classesID, $student->studentID, $sectionID);
							    ?>
							    <td><?= $rank_info->rank ?></td>
							    <?php
							}
							?>
	    					    <td><?= $rank ?></td>
							<?= $level->result_format == 'ACSEE' ? "<td></td>" : '' ?>
	    					    <td></td>
	    					    <td></td>
	    					</tr>
	    				    </tbody>
	    				</table>
	    				<br/>
	    				<h2 style="font-weight: bolder; padding-left: 3%">SUMMARY</h2>

	    				<table class="table table-striped table-bordered">
	    				    <thead>
	    					<tr>
	    					    <th style="font-weight: bolder">Total Marks</th>
	    					    <th style="font-weight: bolder">Performance Average</th>
	    					    <th style="font-weight: bolder">Number of Students</th>
	    					    <th style="font-weight: bolder">Position In <?= isset($section_id) && $section_id == '' ? 'Class' : 'Section' ?></th>

	    					</tr>
	    				    </thead>
	    				    <tbody>
	    					<tr>
	    					    <td><?= $total_marks ?></td>
	    					    <td><?php
							    $total_avg = round($total_marks / count($subjects), 1);
							    echo $total_avg
							    ?></td>
	    					    <td><?= count($students) ?></td>
	    					    <td><?= $student->rank ?></td>

	    					</tr>
	    				    </tbody>
	    				</table>

	    				<br/>
	    				<table class="table table-striped table-bordered">
	    				    <thead>
	    					<tr>
	    					    <th style="font-weight: bolder">GRADES</th>
							<?php foreach ($grades as $grade) { ?>
							    <th><?= $grade->grade ?> = (<?= $grade->gradefrom ?>-<?= $grade->gradeupto ?>)</th>
	    <?php } ?>
	    					</tr>
	    				    </thead>
	    				</table>
	    			    </div>
	<?php } ?>
				</div>

    <?php } ?>
    			<div style="margin-left: 4%; margin-top: 2%; font-size: 110%; line-height: 21px;">


    			    <div>Class Teacher’s Signature: <img src="<?= $class_teacher_signature ?>" width="54" height="32"></div>

    			    <p>General Remarks:  <?php
				    foreach ($grades as $grade) {
					if ($grade->gradefrom <= round($total_avg, 0) && $grade->gradeupto >= round($total_avg, 0)) {
					    echo "<span class='bolder' style='border-bottom: 1px solid black'>";
					    echo $grade->overall_note;
					    echo "</span>";
					    break;
					}
				    }
				    ?>
    			    </p>

    			    <div>
    				<div style='font-weight: bolder;' id="republics" class="signtitle">Signature of Headmistress: <img src="<?= $signature ?>" width="54" height="32"></div>
    			    </div>		               	 
    			    <br/>

    			</div>
    		    </section>

    		</div>


    	    </div>
    	</div>
	    <?php
	}
	/*      $html = ob_get_clean();
	  $dompdf = new DOMPDF();
	  $dompdf->load_html($html);
	  $dompdf->render();
	  $dompdf->stream('output.pdf'); */
	?>
    </body>
</html>