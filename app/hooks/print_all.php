<?php


/**
 * Description of print_all
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */

?>
<?php
require_once __DIR__ . '../../../../app/controllers/mark.php';
$usertype = $this->session->userdata("usertype");
 ?>
<div id="printablediv" style="margin: 2%; font-size: 90%; max-height: 290mm;">
    <section class="panel">
	<div id="p1" style="overflow: hidden; left: 5%; position: relative; width: 935px; height: 270px;">

	    <!-- Begin shared CSS values -->
	    <style type="text/css" >
		.t {
		    -webkit-transform-origin: top left;
		    -moz-transform-origin: top left;
		    -o-transform-origin: top left;
		    -ms-transform-origin: top left;
		    -webkit-transform: scale(0.25);
		    -moz-transform: scale(0.25);
		    -o-transform: scale(0.25);
		    -ms-transform: scale(0.25);
		    z-index: 2;
		    position: absolute;
		    white-space: pre;
		    overflow: visible;
		}
	    </style>
	    <!-- End shared CSS values -->


	    <!-- Begin inline CSS -->
	    <style type="text/css" >

		#t1_1{left:223px;top:78px;letter-spacing:0.2px;word-spacing:-0.4px;    font-size: 138px;
		      font-weight: bolder;}
		#t2_1{left:252px;top:116px;letter-spacing:-0.6px;word-spacing:1.4px;}
		#tx_1{left:261px;top:136px;letter-spacing:-0.6px;word-spacing:1.4px;}
		#t3_1{left:290px;top:157px;}
		#t4_1{left:320px;top:178px;}
		#t5_1{left:377px;top:178px;}
		#t6_1{left:349px;top:199px;letter-spacing:-0.4px;word-spacing:0.5px;}
		#t7_1{left:10px;top:210px;}

		.s4_1{
		    FONT-SIZE: 93.3px;
		    FONT-FAMILY: DejaVuSans_b;
		    color: rgb(0,0,0);
		}

		.s2_1{
		    FONT-SIZE: 57.2px;
		    FONT-FAMILY: DejaVuSans_b;
		    color: rgb(0,0,0);
		}

		.s1_1{
		    FONT-SIZE: 158.9px;
		    FONT-FAMILY: DejaVuSans_b;
		    color: rgb(0,0,0);
		}

		.s3_1{
		    FONT-SIZE: 67.2px;
		    FONT-FAMILY: DejaVuSans_b;
		    color: rgb(5,99,193);
		}

		#form1_1{	z-index:2;	cursor: pointer;	border-style:none;	position: absolute;	left:376px;	top:197px;	width:242px;	height:23px;	text-align:left;	background: transparent;	font:normal 18px Arial, Helvetica, sans-serif;}

	    </style>
	    <!-- End inline CSS -->

	    <!-- Begin embedded font definitions -->
	    <style id="fonts1" type="text/css" >

		@font-face {
		    font-family: DejaVuSans_b;
		    src: url("../../assets/fonts/DejaVuSans_b.woff") format("woff");
		}

	    </style>
	    <!-- End embedded font definitions -->
	    <div class="letterhead"
		 <!-- Begin text definitions (Positioned/styled in CSS) -->
		 <div id="t1_1" class="t s1_1"><?= $siteinfos->sname ?></div>
		<div id="t2_1" class="t s2_1"><?= $siteinfos->address ?>.</div>
		<div id="tx_1" class="t s2_1"><?= 'P.O BOX ' . $siteinfos->box ?>.</div>
		<div id="t3_1" class="t s2_1">Cell: <?= str_replace(',', ' / ', $siteinfos->phone) ?></div>
		<div id="t4_1" class="t s2_1">Email: </div>
		<div id="t5_1" class="t s3_1" ><?= $siteinfos->email ?></div>
		<div id="t6_1" class="t s2_1">Website: <?= $siteinfos->website ?></div>
	    </div>
	    <div id="t7_1" class="t s4_1">_______________________________________________________________________________________________________________</div>

	    <!-- End text definitions -->


	    <!-- Begin Form Data -->
	    <form>
		<input type="button"  tabindex="1"  id="form1_1" data-objref="5 0 R" title="mailto:<?= $siteinfos->email ?>" onclick="window.location.href = 'mailto:<?= $siteinfos->email ?>'" />

	    </form>
	    <!-- End Form Data -->
	    <!-- Begin page background -->
	    <div id="pg1Overlay" style="width:100%; height:10%; position:absolute; z-index:1; background-color:rgba(0,0,0,0); -webkit-user-select: none;"></div>
	    <div id="pg1"><?php
		$array = array(
		    "src" => base_url('uploads/images/' . $siteinfos->photo),
		    'width' => '185px',
		    'height' => '230px',
		    'class' => 'img-rounded',
		    'style' => 'margin-left:2em; padding-top: 5%;'
		);
		echo img($array);
		?></div>


	</div>

	<div class="">
	    <h1 align='center' style="font-size: 23px; font-weight: bolder">STUDENT PROGRESSIVE REPORT (<?= strtoupper($classes->classes) . ' ' . $student->year ?>)</h1>
	</div>
	<div class="panel-body profile-view-dis">
	    <div class="row" style="margin: 2%;">
		<div class="col-sm-9" style="float:left;" >
		    <h1 style="font-weight: bolder;"> NAME: <?= $student->name ?></h1>
		</div>
		<div class="col-sm-3" style="float: right;">
		    <h1 style="font-weight: bolder; font-size: 14px;"><?=
			(isset($section_id) && $section_id == '') ?
				'CLASS: ' . strtoupper($classes->classes) . '' :
				'SECTION ' . $student->section
			?></h1>
		</div>
		<div style="clear:both;"></div>
	    </div>

	</div>

<?php if (count($exams) > 0 && !empty($exams)) {
    ?>

    	<div class="row"  style="margin: 2%;">
		<?php
		if ($marks && $exams) {

		    $examController = new Exam();
		    ?>
		    <div class="col-lg-12">
			<table class="table table-striped table-bordered">
			    <thead>
				<tr>
				    <th style="font-weight: bolder">SUBJECT</th>

				    <?php foreach ($exams as $exam) { ?>
	    			    <th style="font-weight: bolder"><?= strtoupper($exam->exam) ?></th>
	<?php } ?>

				    <th style="font-weight: bolder">AVERAGE</th>
				    <th style="font-weight: bolder">GRADE</th>
				    <th style="font-weight: bolder">POINTS</th>
				    <th style="font-weight: bolder">REMARKS</th>
				</tr>
			    </thead>
			    <tbody>
				<?php
				$total_marks = 0;
				foreach ($subjects as $subject) {
				    ?>
	    			<tr>
	    			    <td style="font-weight: bolder"><?= strtoupper($subject->subject) ?></td>

					<?php
					$total = 0;
					foreach ($exams as $exam) {

					    $mark = $examController->get_total_marks($student->studentID, $exam->examID, $subject->classesID, $subject->subjectID);
					    $total+=$mark->total_mark;
					    ?>
					    <th><?= $mark->total_mark ?></th>
					    <?php
					}
					$avg = round($total / count($exams), 1);
					?>

	    			    <td><?= $avg ?></td>

					<?php
					echo return_grade($grades, $avg);
					$total_marks+=$avg;
					?>
	    			</tr>
				    <?php } ?>
				<tr>
				    <td style="font-weight: bolder">DIVISION</td>
				    <?php
				    $i = 0;
				    $total_points = 0;
				    foreach ($exams as $exam) {
					$division[$i] = $examController->getDivision($student->studentID, $exam->examID);
					?>
	    			    <td><?= $division[$i][0] ?></td>
					<?php
					$total_points+=$division[$i][1];
					$i++;
				    }
				    $total_i = $i;
				    $total_average_points = round($total_points / $total_i, 0);
				    if ($level->result_format == 'CSEE') {
					$div = cseeDivision($total_average_points, 0);
				    } elseif ($level->result_format == 'ACSEE') {
					$div = acseeDivision($total_average_points, 0);
				    }
				    ?>
				    <td><?= $div ?></td>
				    <td></td>
				    <td></td>
				    <td></td>
				</tr>
				<tr>
				    <td style="font-weight: bolder">POINTS</td>
				    <?php
				    for ($i = 0; $i < $total_i; $i++) {
					?>
	    			    <td><?= $division[$i][1] ?></td>
					<?php
				    }
				    ?>
				    <td><?= $total_average_points ?></td>
				    <td></td>
				    <td></td>
				    <td></td>
				</tr>
			    </tbody>
			</table>


			<h2 class="heading2">SUMMARY</h2>


			<table class="table table-striped table-bordered">
			    <thead>
				<tr>
				    <th style="font-weight: bolder">Total Marks</th>
				    <th style="font-weight: bolder">Performance Average</th>
				    <th style="font-weight: bolder">Number of Students</th>
				    <th style="font-weight: bolder">Position In <?= isset($section_id) && $section_id == '' ? 'Class' : 'Section' ?></th>

				</tr>
			    </thead>
			    <tbody>
				<tr>
				    <td><?= $total_marks ?></td>
				    <td><?= round($total_marks / count($subjects), 1) ?></td>
				    <td><?= count($students) ?></td>
				    <td><?= $rank ?></td>

				</tr>
			    </tbody>
			</table>


			<table class="table table-striped table-bordered">
			    <thead>
				<tr>
				    <th style="font-weight: bolder">GRADES</th>
				    <?php foreach ($grades as $grade) { ?>
	    			    <th><?= $grade->grade ?> = (<?= $grade->gradefrom ?>-<?= $grade->gradeupto ?>)</th>
	<?php } ?>
				</tr>
			    </thead>
			</table>
		    </div>
	    <?php } ?>
    	</div>

<?php } ?>
	<div style="padding-left:2em;">


	    <p>Class Teacher’s Signature: ________________</p>

	    <p>General Remarks: ___________________________________________________________________________________________________________
		_____________________________________________________________________</p>


	    <div>
		<div>Signature of Headmistress: ____________</div>
	    </div>		               	 
	    <br/>

	</div>
    </section>
</div>
