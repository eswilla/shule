<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class role extends MY_Model {

    protected $_table_name = 'role';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'intval';
    protected $_order_by = "name";

    function __construct() {
	parent::__construct();
    }

    function all($array = NULL, $signal = FALSE) {
	$query = parent::get($array, $signal);
	return $query;
    }

}
