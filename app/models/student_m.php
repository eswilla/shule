<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class student_m extends MY_Model {

    public $_table_name = 'student_info';
    /* infinite code starts here */
    public $_primary_key = 'studentID';
    protected $_primary_filter = 'intval';
    protected $_order_by = "roll asc";

    function __construct() {
	parent::__construct();
	$this->load->database();
    }

    function get_username($table, $data = NULL) {
	$query = $this->db->get_where($table, $data);
	return $query->result();
    }

    function get_class($id = NULL) {
	$query = $this->db->get_where('classes', array('classesID' => $id));
	return $query->row();
    }

    function get_classes() {
	$this->db->select('*')->from('classes')->order_by('classes_numeric asc');
	$query = $this->db->get();
	return $query->result();
    }

    function get_parent($parent_id = NULL) {
	$query = $this->db->get_where('parent', array('parentID' => $parent_id));
	return $query->row();
    }

    function get_parent_info($username = NULL) {
	$query = $this->db->get_where('parent', array('username' => $username));
	return $query->row();
    }

    function get_order_by_roll($array = NULL) {
	$query = parent::get_order_by($array);
	return $query;
    }

    function get_student($array = NULL, $signal = FALSE) {
	$query = parent::get($array, $signal);
	return $query;
    }

    function get_single_student($array) {
	$query = parent::get_single($array);
	return $query;
    }

    function get_order_by_student($array = NULL) {

	$query = parent::get_order_by($array);
	return $query;
    }

    function get_order_by_student_year($classesID) {

	$query = $this->db->query('SELECT * FROM ' . set_schema_name() . $this->_table_name . ' WHERE year = (SELECT MIN(year) FROM ' . set_schema_name() . $this->_table_name . ') AND "classesID" = ' . $classesID . ' order by roll asc');
	return $query->result();
    }

    function get_order_by_student_single_year($classesID) {

	$query = $this->db->query('SELECT year FROM ' . set_schema_name() . $this->_table_name . ' WHERE year = (SELECT MIN(year) FROM ' . set_schema_name() . $this->_table_name . ') AND "classesID" = ' . $classesID . ' order by roll asc');
	return $query->row();
    }

    function get_order_by_student_single_max_year($classesID) {

	$query = $this->db->query('SELECT year FROM ' . set_schema_name() . $this->_table_name . ' WHERE year = (SELECT MAX(year) FROM ' . set_schema_name() . $this->_table_name . ') AND "classesID" = ' . $classesID . ' order by roll asc');
	return $query->row();
    }

    function get_order_by_studen_with_section_and_classes($classesID) {
	$this->db->select('*');
	$this->db->from($this->_table_name);
	$this->db->join('classes', 'student.classesID = classes.classesID', 'LEFT');
	$this->db->join('section', 'student.sectionID = section.sectionID', 'LEFT');
	$this->db->where('student.classesID', $classesID);
	$query = $this->db->get();
	return $query->result();
    }

    function get_order_by_studen_with_section($classesID, $sectionID) {
	$this->db->select('*');
	$this->db->from($this->_table_name);
	$this->db->join('classes', 'student.classesID = classes.classesID', 'LEFT');
	$this->db->join('section', 'student.sectionID = section.sectionID', 'LEFT');
	$this->db->where('student.classesID', $classesID);
	$this->db->where('student.sectionID', $sectionID);
	$query = $this->db->get();
	return $query->result();
    }

    function getStudentByClass($id) {
	$this->db->select('*');
	$this->db->from($this->_table_name);
	$this->db->where('classesID', $id);
	$query = $this->db->get();
	return $query->result();
    }

    function getStudentBySection($id) {
	$this->db->select('*');
	$this->db->from('student');
	$this->db->where('sectionID', $id);
	$query = $this->db->get();
	return $query->result();
    }

    function getParentId($phone) {
	$this->db->select('parentID');
	$this->db->from('parent');
	$this->db->where('phone', "$phone");
	$this->db->or_where('other_phone', "$phone");
	$query = $this->db->get();
	return $query->row();
    }
    
    function getStudentParentInfo($classes_id) {
	$query = $this->db->query('SELECT a.name as student_name, a.dob as date_of_birth, a.section, a.roll, b.name as guardian, b.father_name, b.mother_name, b.father_profession, b.mother_profession, b.email, b.phone, b.email, b.address FROM ' . set_schema_name() . $this->_table_name . ' a  JOIN ' . set_schema_name() . 'parent b ON a."parentID"=b."parentID" where a."classesID"='.$classes_id);
	return $query->result_array();
    }

    function insert_student($array) {
	$this->_table_name = 'student';
	$error = parent::insert($array);
	return TRUE;
    }

    function insert_parent($array) {
	$this->db->insert('parent', $array);
	return TRUE;
    }

    function update_student($data, $id = NULL) {
	$this->_table_name = 'student';
	parent::update($data, $id);

	return $id;
    }

    function update_student_classes($data, $array = NULL) {
	$this->_table_name = 'student';
	$this->db->set($data);
	$this->db->where($array);
	$this->db->update($this->_table_name);
    }

    function delete_student($id) {
	$this->_table_name = 'student';
	parent::delete($id);
    }

    function delete_parent($id) {
	$this->db->delete('parent', array('studentID' => $id));
    }

    function hash($string) {
	return parent::hash($string);
    }

    /* infinite code starts here */
}

/* End of file student_m.php */
/* Location: .//D/xampp/htdocs/school/mvc/models/student_m.php */