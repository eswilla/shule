<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Setting_m extends MY_Model
{

    protected $_table_name = 'setting';
    protected $_primary_key = 'settingID';
    protected $_primary_filter = 'intval';
    protected $_order_by = "settingID asc";

    function __construct()
    {
        parent::__construct();
    }

    function get_setting($array = NULL, $signal = FALSE)
    {
        $query = parent::get($array, $signal);
        return $query;
    }

    function get_order_by_setting($array = NULL)
    {
        $query = parent::get_order_by($array);
        return $query;
    }

    function insert_setting($array)
    {
        $error = parent::insert($array);
        return TRUE;
    }

    function update_setting($data, $id = NULL)
    {
        parent::update($data, $id);
        return $id;
    }

    public function delete_setting($id)
    {
        parent::delete_data($id);
    }

    public function get_sms_api($schema) {
	return $this->db->query('select api_key, api_secret FROM '.$schema.'.setting limit 1')->row();
    }
    public function save_signature($signature,$file_name)
    {
        $user_type = $this->session->userdata('usertype');
        $username = $this->session->userdata('username');


        if ($user_type == 'Admin' || $user_type == 'Accountant') {

            $this->db->update('user', ['signature' => $signature, 'signature_path' => $file_name], ['usertype' => $user_type, 'username' => $username]);


        } elseif ($user_type == 'Teacher') {

            $this->db->update('teacher', ['signature' => $signature, 'signature_path' => $file_name], ['usertype' => $user_type, 'username' => $username]);

        }
        return TRUE;
    }

    public function get_signature()
    {
        $usertype = $this->session->userdata("usertype");
        $username = $this->session->userdata("username");


            if ($usertype == 'Admin' || $usertype == 'Accountant') {
                $signature = $this->db->query("SELECT signature FROM " . set_schema_name() . "user WHERE usertype='" . $usertype . "' AND username= '" . $username . "'")->row();
            } elseif ($usertype == 'Teacher') {
                $signature = $this->db->query("SELECT signature FROM " . set_schema_name() . "teacher WHERE usertype='" . $usertype . "' AND username= '" . $username . "'")->row();
            }
        if(!empty($user))
            return $user->row()->signature;
        return '';



    }
}

/* End of file setting_m.php */
/* Location: .//D/xampp/htdocs/school/mvc/models/setting_m.php */