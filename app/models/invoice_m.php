<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Invoice_m extends MY_Model {

    protected $_table_name = 'invoice';
    protected $_primary_key = 'invoiceID';
    protected $_primary_filter = 'intval';
    protected $_order_by = "invoiceID desc";

    function __construct() {
	parent::__construct();
	$this->load->database();
    }

    function get_join_receipts($classesID, $feetypeID) {
	$this->db->select('*');
	$this->db->from('invoice');
	$this->db->join('payment_history', 'payment_history.invoiceID = invoice.invoiceID', 'RIGHT');
	$this->db->join('receipt', 'receipt.paymentID = payment_history.paymentID', 'LEFT');
	$this->db->where("invoice.classesID", $classesID);
	$this->db->where("invoice.feetypeID", $feetypeID);
	$query = $this->db->get();
	return $query->result();
    }

    function get_join_where_classes($studentID, $classesID) {
	$this->db->select('*');
	$this->db->from('invoice');
	$this->db->join('classes', 'classes.classesID = invoice.classesID', 'LEFT');
	$this->db->where("invoice.classesID", $classesID);
	$this->db->where("invoice.studentID", $studentID);
	$query = $this->db->get();
	return $query->result();
    }

    function get_invoice_byid($id) {
	$this->db->select('*');
	$this->db->from('invoice');
	$this->db->join('feetype', 'feetype.feetypeID = invoice.feetypeID', 'RIGHT');
	$this->db->join('bank_account', 'bank_account.bank_account_id = feetype.bank_account_id', 'RIGHT');
	$this->db->where("invoice.invoiceID", $id);
	$query = $this->db->get();
	return $query->row();
        
         // $result = $this->db->query('SELECT a.* ,b.*,c.* from ' . set_schema_name() . 'invoice a join ' . set_schema_name() . 'feetype b on a."feetypeID"=b."feetypeID" JOIN ' . set_schema_name() . 'bank_account c on b."bank_account_id"=c."bank_account_id" WHERE a."invoiceID"=' . $id .'')->get(); 
    }
    
        function get_invoice_bystudentID($id,$classesID) {
      $result = $this->db->query('SELECT a.* ,b.*,c.* from ' . set_schema_name() . 'invoice a join ' . set_schema_name() . 'feetype b on a."feetypeID"=b."feetypeID" JOIN ' . set_schema_name() . 'bank_account c on b."bank_account_id"=c."bank_account_id" WHERE a."studentID"=' . $id . 'and a."classesID"=' . $classesID . '')->result();
	return $result;      
//            
//            
//            
//            
//	$this->db->select('*');
//	$this->db->from('invoice');
//	$this->db->join('feetype', 'feetype.feetypeID = invoice.feetypeID');
//	$this->db->join('bank_account', 'bank_account.bank_account_id = feetype.bank_account_id');
//	$this->db->where("invoice.studentID", $id);
//	$query = $this->db->get();
//	return $query->row();
    }

    function get_payment_bystatus($classesID, $status) {
	$result = $this->db->query('SELECT a.* ,b.*,c.* from ' . set_schema_name() . 'payment a join ' . set_schema_name() . 'invoice b on a."invoiceID"=b."invoiceID" JOIN ' . set_schema_name() . 'student c on a."studentID"=c."studentID" WHERE b."classesID"=' . $classesID . ' and a."approved"=' . $status . '')->result();
	return $result;
    }
     function get_paymentinfo($classesID) {
	$result = $this->db->query('SELECT a.* ,b.*,c.* from ' . set_schema_name() . 'payment a join ' . set_schema_name() . 'invoice b on a."invoiceID"=b."invoiceID" JOIN ' . set_schema_name() . 'student c on a."studentID"=c."studentID" WHERE b."classesID"=' . $classesID . '')->result();
	return $result;
    }

    function get_invoice($array = NULL, $signal = FALSE) {
	$query = parent::get($array, $signal);
	return $query;
    }

    function get_combined_invoice_by_student($classesID) {
	$result = $this->db->query('select distinct feetype,student,amount,paidamount,(amount-paidamount) as dueamount,(select sum(amount) from ' . set_schema_name() . 'invoice where invoice."studentID"=' . $classesID . ') as amounttotal,(select sum(paidamount) from ' . set_schema_name() . 'invoice where invoice."studentID"=' . $classesID . ') as paidamounttotal from ' . set_schema_name() . 'invoice  where invoice."studentID"=' . $classesID . 'AND ')->result();
	return $result;
    }

    function get_invoice_by_class($classesID) {
	$this->db->select('*');
	$this->db->from('invoice');
	$this->db->where("invoice.classesID", $classesID);
	$query = $this->db->get();
	return $query->result();
    }

    function get_distinctinvoice_by_class($classesID) {
	$query = $this->db->query('SELECT * from ' . set_schema_name() . 'feetype where "feetypeID" in(select DISTINCT("feetypeID") from ' . set_schema_name() . 'invoice where "classesID"=' . $classesID . ')')->result();
	return $query;
    }

    function get_order_by_invoice($array = NULL) {
	$query = parent::get_order_by($array);
	return $query;
    }

    function get_single_invoice($array = NULL) {
	$query = parent::get_single($array);
	return $query;
    }

    function insert_invoice($array) {
	//print_r($array);
	return $this->db->insert('invoice', $array);
//	$error = parent::insert($array);
//	return $error;
    }

    function update_invoice($data, $id = NULL) {
	parent::update($data, $id);
	return $id;
    }

    public function delete_invoice($id) {
	parent::delete($id);
    }

    function get_classes() {
	$this->db->select('*')->from('classes')->order_by('classes_numeric asc');
	$query = $this->db->get();
	return $query->result();
    }

    function get_allinvoice_by_class2($classesID, $feetypeID) {
	$query = $this->db->query('select "invoiceNO","paiddate","roll","classes",feetype,student,amount,paidamount,(amount-paidamount) as dueamount,(select sum(amount) from ' . set_schema_name() . 'invoice where invoice."classesID"=' . $classesID . ') as amounttotal,(select sum(paidamount) from ' . set_schema_name() . 'invoice where invoice."classesID"=' . $classesID . ') as paidamounttotal from ' . set_schema_name() . 'invoice  where invoice."classesID"=' . $classesID . ' AND invoice."feetypeID"=' . $feetypeID . 'AND invoice.amount<>invoice.paidamount')->result();
	return $query;
    }

    function get_allinvoice_by_class($classesID, $feetypeID) {
	$query = $this->db->query('select a."invoiceNO",c."email",a."paiddate",a."roll",a."classes",b."feetype",c."name",a.amount,d.*,a.paidamount,(a.amount-a.paidamount) as dueamount,(select sum(amount) from ' . set_schema_name() . 'invoice  where invoice."classesID"=' . $classesID . ') as amounttotal,(select sum(paidamount) from ' . set_schema_name() . 'invoice   where invoice."classesID"=' . $classesID . ') as paidamounttotal from ' . set_schema_name() . 'invoice a JOIN ' . set_schema_name() . 'student c on a."studentID"=c."studentID" RIGHT JOIN ' . set_schema_name() . 'feetype b on a."feetypeID"= b."feetypeID" RIGHT JOIN ' . set_schema_name() . 'bank_account d on d."bank_account_id"=b."bank_account_id" where a."classesID"=' . $classesID . ' AND a."feetypeID"=' . $feetypeID . ' AND (a.amount-a.paidamount)>0');
	return $query->result();
    }

    public function get_parent($studentID) {
	 $sql='select a.name, a.email, a.phone, b.name as sname FROM ' . set_schema_name() . 'student b JOIN ' . set_schema_name() . 'parent a ON b."parentID"=a."parentID" WHERE b."studentID"=' . $studentID;
	return $this->db->query($sql)->row();
    }

}

/* End of file invoice_m.php */
/* Location: .//D/xampp/htdocs/school/mvc/models/invoice_m.php */