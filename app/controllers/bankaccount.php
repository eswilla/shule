<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of bankaccount
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
class Bankaccount extends Admin_Controller {

    /**
     * -----------------------------------------
     * 
     * ******* Address****************
     * INETS COMPANY LIMITED
     * P.O BOX 32258, DAR ES SALAAM
     * TANZANIA
     * 
     * 
     * *******Office Location *********
     * 11th block, Bima Road, Mikocheni B, Kinondoni, Dar es salaam
     * 
     * 
     * ********Contacts***************
     * Email: <info@inetstz.com>
     * Website: <www.inetstz.com>
     * Mobile: <+255 655 406 004>
     * Tel:    <+255 22 278 0228>
     * -----------------------------------------
     */
    function __construct() {
	parent::__construct();
	$language = $this->session->userdata('lang');
	$this->lang->load('bankaccount', $language);
    }

    public function index() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin" || $usertype == "Accountant") {
	    $this->data['bankaccounts'] = $this->bankaccount_m->get_banks();
	    $this->data["subview"] = "bankaccount/index";
	    $this->load->view('_layout_main', $this->data);
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    protected function rules() {
	$rules = array(
	    array(
		'field' => 'bankaccount_name',
		'label' => $this->lang->line("bankaccount_name"),
		'rules' => 'trim|required|xss_clean|max_length[60]'
	    ),
	    array(
		'field' => 'bankaccount_branch',
		'label' => $this->lang->line("bankaccount_branch"),
		'rules' => 'trim|required|xss_clean|max_length[250]'
	    ),
	    array(
		'field' => 'bankaccount_account',
		'label' => $this->lang->line("bankaccount_account"),
		'rules' => 'trim|required|xss_clean|max_length[250]|callback_unique_bankaccount'
	    ),
	    array(
		'field' => 'account_name',
		'label' => $this->lang->line("account_name"),
		'rules' => 'trim|required|xss_clean|max_length[250]'
	    ),
	    array(
		'field' => 'bankaccount_swiftcode',
		'label' => $this->lang->line("bankaccount_swiftcode"),
		'rules' => 'trim|required|xss_clean|max_length[10]'
	    ),
	    array(
		'field' => 'bankaccount_currency',
		'label' => $this->lang->line("bankaccount_currency"),
		'rules' => 'trim|required|xss_clean|max_length[10]|callback_unique_notzero'
	    ),
	    array(
		'field' => 'bankaccount_note',
		'label' => $this->lang->line("bankaccount_note"),
		'rules' => 'trim|xss_clean|max_length[250]'
	    )
	);
	return $rules;
    }

    public function add() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin" || $usertype == "Accountant") {
	    if ($_POST) {
		$rules = $this->rules();
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE) {
		    $this->data["subview"] = "bankaccount/add";
		    $this->load->view('_layout_main', $this->data);
		} else {
		    $array = array(
			"bank_name" => $this->input->post("bankaccount_name"),
			"account_number" => $this->input->post("bankaccount_account"),
			"account_name"=>  $this->input->post('account_name'),
			"note" => $this->input->post("note"),
			'bank_branch' => $this->input->post("bankaccount_branch"),
			"currency" => $this->input->post("bankaccount_currency"),
			"bank_swiftcode" => $this->input->post("bankaccount_swiftcode")
		    );
		    $this->bankaccount_m->insert_bankaccount($array);
		    $this->session->set_flashdata('success', $this->lang->line('menu_success'));
		    redirect(base_url("bankaccount/index"));
		}
	    } else {
		$this->data["subview"] = "bankaccount/add";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function edit() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin" || $usertype == "Accountant") {
	    $id = htmlentities(($this->uri->segment(3)));
	    if ((int) $id) {
		$this->data['bankaccount'] = $this->bankaccount_m->get_banks($id);
		if ($this->data['bankaccount']) {
		    if ($_POST) {
			$rules = $this->rules();
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == FALSE) {
			    $this->data["subview"] = "bankaccount/edit";
			    $this->load->view('_layout_main', $this->data);
			} else {
			    $array = array(
				"bank_name" => $this->input->post("bankaccount_name"),
				"account_number" => $this->input->post("bankaccount_account"),
				"account_name"=>  $this->input->post('account_name'),
				"note" => $this->input->post("note"),
				'bank_branch' => $this->input->post("bankaccount_branch"),
				"currency" => $this->input->post("bankaccount_currency"),
				"bank_swiftcode" => $this->input->post("bankaccount_swiftcode"));                            
			    $this->bankaccount_m->update_bankaccount($array, $id);
			    $this->session->set_flashdata('success', $this->lang->line('menu_success'));
			    redirect(base_url("bankaccount/index"));
			}
		    } else {
			$this->data["subview"] = "bankaccount/edit";
			$this->load->view('_layout_main', $this->data);
		    }
		} else {
		    $this->data["subview"] = "error";
		    $this->load->view('_layout_main', $this->data);
		}
	    } else {
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function delete() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin" || $usertype == "Accountant") {
	    $id = htmlentities(($this->uri->segment(3)));
	    if ((int) $id) {
		$this->bankaccount_m->delete_bankaccount($id);
		$this->session->set_flashdata('success', $this->lang->line('menu_success'));
		redirect(base_url("bankaccount/index"));
	    } else {
		redirect(base_url("bankaccount/index"));
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function unique_notzero() {
	if ($this->input->post("bankaccount_currency") == "0") {
	    $this->form_validation->set_message("unique_notzero", "%s is required");
	    return FALSE;
	} else {
	    return TRUE;
	}
    }
    

    public function unique_bankaccount() {
	$id = htmlentities(($this->uri->segment(3)));
	if ((int) $id) {
	    $bankaccount = $this->bankaccount_m->get_order_by_bankaccount(array("account_number" => $this->input->post("bankaccount_account"), "bank_account_id !=" => $id));
	    if (count($bankaccount)) {
		$this->form_validation->set_message("unique_bankaccount", "%s already exists");
		return FALSE;
	    }
	    return TRUE;
	} else {
	    $bankaccount = $this->bankaccount_m->get_order_by_bankaccount(array("account_number" => $this->input->post("bankaccount_account")));

	    if (count($bankaccount)) {
		$this->form_validation->set_message("unique_bankaccount", "%s already exists");
		return FALSE;
	    }
	    return TRUE;
	}
    }

}

/* End of file feetype.php */
/* Location: .//D/xampp/htdocs/school/mvc/controllers/feetype.php */