<?php

/**
 * Description of examReport
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class ExamReport extends Admin_Controller {

    /**
     * -----------------------------------------
     * 
     * ******* Address****************
     * INETS COMPANY LIMITED
     * P.O BOX 32258, DAR ES SALAAM
     * TANZANIA
     * 
     * 
     * *******Office Location *********
     * 11th block, Bima Road, Mikocheni B, Kinondoni, Dar es salaam
     * 
     * 
     * ********Contacts***************
     * Email: <info@inetstz.com>
     * Website: <www.inetstz.com>
     * Mobile: <+255 655 406 004>
     * Tel:    <+255 22 278 0228>
     * -----------------------------------------
     */
    function __construct() {
	parent::__construct();
	$language = $this->session->userdata('lang');
	$this->lang->load('examreport', $language);
    }

    public function getReportList() {
	$examreport_id = $this->input->post('id');
	if ((int) $examreport_id) {
	    $string = base_url("examreport/index/$examreport_id");
	    echo $string;
	} else {
	    redirect(base_url("examreport/index"));
	}
    }

    public function getExamName($exam_id) {
	$exam = $this->db->query('select exam FROM ' . set_schema_name() . 'exam WHERE "examID"=' . $exam_id)->row();
	return $exam->exam;
    }

    public function index() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin") {
	    $this->data['classes'] = $this->classes_m->get_classes();
	    $id = htmlentities($this->uri->segment(3));
	    $this->data['set'] = $id;
	    if ((int) $id) {
		$this->data['set'] = $id;
		$this->data['classes'] = $this->student_m->get_classes();
		$this->data['examreports'] = $this->examreport_m->get_examreport_by_class($id);
		$this->data["subview"] = "examreport/index";
		$this->load->view('_layout_main', $this->data);
	    } else {
		$this->data["subview"] = "examreport/index";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function getReport() {
	
    }

    public function edit() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin") {
	    $id = htmlentities(($this->uri->segment(3)));
	    if ((int) $id) {
		$this->data['classlevel'] = $this->classlevel_m->get_classlevel($id);
		if ($this->data['classlevel']) {
		    if ($_POST) {
			$rules = $this->rules();
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == FALSE) {
			    $this->data["subview"] = "classlevel/edit";
			    $this->load->view('_layout_main', $this->data);
			} else {


			    $this->classlevel_m->update_classlevel($_POST, $id);
			    $this->session->set_flashdata('success', $this->lang->line('menu_success'));
			    redirect(base_url("classlevel/index"));
			}
		    } else {
			$this->data["subview"] = "classlevel/edit";
			$this->load->view('_layout_main', $this->data);
		    }
		} else {
		    $this->data["subview"] = "error";
		    $this->load->view('_layout_main', $this->data);
		}
	    } else {
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function delete() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin") {
	    $id = htmlentities(($this->uri->segment(3)));
	    if ((int) $id) {
		$this->classlevel_m->delete_classlevel($id);
		$this->session->set_flashdata('success', $this->lang->line('menu_success'));
		redirect(base_url("classlevel/index"));
	    } else {
		redirect(base_url("classlevel/index"));
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function unique_classlevel() {
	$id = htmlentities(($this->uri->segment(3)));
	if ((int) $id) {
	    $classlevel = $this->classlevel_m->get_order_by_classlevel(array("name" => $this->input->post("name"), "classlevel_id !=" => $id));
	    if (count($classlevel)) {
		$this->form_validation->set_message("unique_classlevel", "%s already exists");
		return FALSE;
	    }
	    return TRUE;
	} else {
	    $classlevel = $this->classlevel_m->get_order_by_classlevel(array("classlevel" => $this->input->post("name")));

	    if (count($classlevel)) {
		$this->form_validation->set_message("unique_classlevel", "%s already exists");
		return FALSE;
	    }
	    return TRUE;
	}
    }

    public function unique_point() {
	$id = htmlentities(($this->uri->segment(3)));
	if ((int) $id) {
	    $classlevel = $this->classlevel_m->get_order_by_classlevel(array("point" => $this->input->post("point"), "classlevel_id !=" => $id));
	    if (count($classlevel)) {
		$this->form_validation->set_message("unique_point", "%s already exists");
		return FALSE;
	    }
	    return TRUE;
	} else {
	    $classlevel = $this->classlevel_m->get_order_by_classlevel(array("point" => $this->input->post("point")));

	    if (count($classlevel)) {
		$this->form_validation->set_message("unique_point", "%s already exists");
		return FALSE;
	    }
	    return TRUE;
	}
    }

   

}
