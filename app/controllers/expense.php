<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Expense extends Admin_Controller {

    /**
     * -----------------------------------------
     * 
     * ******* Address****************
     * INETS COMPANY LIMITED
     * P.O BOX 32258, DAR ES SALAAM
     * TANZANIA
     * 
     * 
     * *******Office Location *********
     * 11th block, Bima Road, Mikocheni B, Kinondoni, Dar es salaam
     * 
     * 
     * ********Contacts***************
     * Email: <info@inetstz.com>
     * Website: <www.inetstz.com>
     * Mobile: <+255 655 406 004>
     * Tel:    <+255 22 278 0228>
     * -----------------------------------------
     */
    function __construct() {

	parent::__construct();
	$this->load->model("expense_m");
	$language = $this->session->userdata('lang');
	$this->lang->load('expense', $language);
    }

    public function index() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin" || $usertype == "Accountant") {
	    $this->data['expenses'] = $this->expense_m->get_expense();
	    $this->data["subview"] = "expense/index";
	    $this->load->view('_layout_main', $this->data);
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    protected function rules() {
	 $rules = array(
	    array(
		'field' => 'expense',
		'label' => $this->lang->line("expense_expense"),
		'rules' => 'trim|required|xss_clean|max_length[128]'
	    ),
	    array(
		'field' => 'date',
		'label' => $this->lang->line("expense_date"),
		'rules' => 'trim|required|max_length[20]|xss_clean'
	    ),
	    array(
		'field' => 'amount',
		'label' => $this->lang->line("expense_amount"),
		'rules' => 'trim|required|numeric|max_length[11]|xss_clean|callback_valid_number'
	    ),
              array(
		'field' => 'depreciation',
		'label' => $this->lang->line("depreciation"),
		'rules' => 'trim|required|max_length[1]|xss_clean'
	    ),
	    array(
		'field' => 'note',
		'label' => $this->lang->line("expense_note"),
		'rules' => 'trim|max_length[200]|xss_clean'
	    )
	);
	return $rules;
    }

    public function add() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin" || $usertype == "Accountant") {
	    if ($_POST) {
		$rules = $this->rules();
		$this->form_validation->set_rules($rules);
		//this is forging, we are not sure, but we have to recheck this function
		if ($this->form_validation->run() == false) {
		    $this->data["subview"] = "expense/add";
		    $this->load->view('_layout_main', $this->data);
		} else {
		    $uname = $this->session->userdata("name");
		    $username = $this->session->userdata("username");
		    $email = $this->session->userdata("email");
		    $table = "";
		    $userid = $this->session->userdata("userID");
		    

		    $array = array(
			"create_date" => date("Y-m-d"),
			"date" => date("Y-m-d", strtotime($this->input->post("date"))),
			"expense" => $this->input->post("expense"),
			"amount" => $this->input->post("amount"),
                        "is_depreciation" => $this->input->post("depreciation"),
			"note" => $this->input->post("note"),
                        "categoryID" => $this->input->post("categoryID"),
			"expenseyear" => date("Y"),
			'userID'=>$userid,
			'uname'=>$uname,
			'usertype'=>$usertype
		    );

		    if ($usertype == "Admin") {
			$table = "setting";
			$userid = "settingID";
		    } elseif ($usertype == "Accountant") {
			$table = "user";
			$userid = "userID";
		    }
		    $this->db->insert('expense', $array);
		    $user = $this->expense_m->user_expense($table,$username, $email);
//		    $array['userID'] = $user->$userid;
//		    $array['uname'] = $user->name;
//		    $array['usertype'] = $user->usertype;


		    $this->session->set_flashdata('success', $this->lang->line('menu_success'));
		    redirect(base_url("expense/index"));
		}
	    } else { 
                $this->data["category"]=$this->expense_m->get_category();
		$this->data["subview"] = "expense/add";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }
        protected function category_rules() {
	 $rules = array(
	    array(
		'field' => 'subcategory',
		'label' => $this->lang->line("subcategory"),
		'rules' => 'trim|required|xss_clean|max_length[128]'
	    ),
             array(
		'field' => 'financialID',
		'label' => $this->lang->line("financialID"),
		'rules' => 'trim|required|xss_clean|'
	    ),
	    array(
		'field' => 'date',
		'label' => $this->lang->line("date"),
		'rules' => 'trim|required|max_length[20]|xss_clean'
	    ),
	    array(
		'field' => 'note',
		'label' => $this->lang->line("expense_note"),
		'rules' => 'trim|max_length[200]|xss_clean'
	    )
	);
	return $rules;
    }
    public function financial_category() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin" || $usertype == "Accountant") {
        $this->data["category"]=$this->expense_m->get_financialstatement();
	    if ($_POST) {
		$rules = $this->category_rules();
		$this->form_validation->set_rules($rules);

		//this is forging, we are not sure, but we have to recheck this function
		if ($this->form_validation->run() == FALSE) {
		    $this->data["subview"] = "expense/add_category";
		    $this->load->view('_layout_main', $this->data);
		} else {
		    $array = array(
			"create_date" => date("Y-m-d"),
			"name" => $this->input->post("subcategory"),
			"financialID" => $this->input->post("financialID"),
			"note" => $this->input->post("note"),
		    );
		   // $this->db->insert('financial_category', $array);
		    $this->session->set_flashdata('success', $this->lang->line('menu_success'));
		    redirect(base_url("expense/financial_category"));
		}
	    } else {
           
		$this->data["subview"] = "expense/add_category";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function financial_index(){
         $id = htmlentities(($this->uri->segment(3)));
           if ($_POST) {
        $from_date= date("Y-m-d", strtotime($this->input->post("from_date")));
         $to_date= date("Y-m-d", strtotime($this->input->post("to_date"))); 
        if ($from_date=='1970-01-01' || $to_date=='1970-01-01') {
         $this->data["type"]=$id;
        $this->data["subview"] = "expense/accountreport/financial_index";
        $this->load->view('_layout_main', $this->data);
		}else{
        $this->data['expense_info']=$this->expense_m->get_account_category($id);
//        $output = [];
//        foreach ($expenses as $expense) {
//             $output[] = $this->expense_m->get_account_report($expense->categoryID,'2015-12-6',$to_date);
//        
//        }
         $this->data['from_date'] =$from_date; 
         $this->data['to_date'] = $to_date; 
       if($id==1){
       
        $this->data["subview"] = "expense/accountreport/profit_loss";
        $this->load->view('_layout_main', $this->data);      
         }  else {
     $this->data["subview"] = "expense/accountreport/balance_sheet";
        $this->load->view('_layout_main', $this->data);
}         
              
                } } else {
        $this->data["type"]=$id;
        $this->data["subview"] = "expense/accountreport/financial_index";
        $this->load->view('_layout_main', $this->data);  
           } 
    }
        public function financial_expense($id,$from_date,$to_date){
         
       return $this->expense_m->get_account_report($id,$from_date,$to_date);    
            
    }
    
    
    public function edit() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin") {
	    $id = htmlentities(($this->uri->segment(3)));
	    if ((int) $id) {
		$this->data['expense'] = $this->expense_m->get_expense($id);
		if ($this->data['expense']) {
		    if ($_POST) {
			$rules = $this->rules();
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == FALSE) {
			    $this->data["subview"] = "expense/edit";
			    $this->load->view('_layout_main', $this->data);
			} else {

			    $uname = $this->session->userdata("name");
			    $username = $this->session->userdata("username");
			    $email = $this->session->userdata("email");
			    $table = "";
			    $userid = "";
			    $array = array();
			    $array = array(
				"date" => date("Y-m-d", strtotime($this->input->post("date"))),
				"expense" => $this->input->post("expense"),
				"amount" => $this->input->post("amount"),
				"note" => $this->input->post("note"),
			    );

			    if ($usertype == "Admin") {
				$table = "setting";
				$userid = "settingID";
			    } elseif ($usertype == "Accountant") {
				$table = "user";
				$userid = "userID";
			    }

			    $user = $this->expense_m->user_expense($table, $username, $email);
			    $array['userID'] = $user->$userid;

			    $array['uname'] = $user->$uname;
			    $array['usertype'] = $user->$usertype;

			    $this->expense_m->update_expense($array, $id);
			    $this->session->set_flashdata('success', $this->lang->line('menu_success'));
			    redirect(base_url("expense/index"));
			}
		    } else {
			$this->data["subview"] = "expense/edit";
			$this->load->view('_layout_main', $this->data);
		    }
		} else {
		    $this->data["subview"] = "error";
		    $this->load->view('_layout_main', $this->data);
		}
	    } else {
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function delete() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin") {
	    $id = htmlentities(($this->uri->segment(3)));
	    if ((int) $id) {
		$this->expense_m->delete_expense($id);
		$this->session->set_flashdata('success', $this->lang->line('menu_success'));
		redirect(base_url("expense/index"));
	    } else {
		redirect(base_url("expense/index"));
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    function date_valid($date) {
	if (strlen($date) < 10) {
	    $this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");
	    return FALSE;
	} else {
	    $arr = explode("-", $date);
	    $dd = $arr[0];
	    $mm = $arr[1];
	    $yyyy = $arr[2];
	    if (checkdate($mm, $dd, $yyyy)) {
		return TRUE;
	    } else {
		$this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");
		return FALSE;
	    }
	}
    }

    function valid_number() {
	if ($this->input->post('amount') && $this->input->post('amount') < 0) {
	    $this->form_validation->set_message("valid_number", "%s is invalid number");
	    return FALSE;
	}
	return TRUE;
    }

}

/* End of file expense.php */
/* Location: .//D/xampp/htdocs/school/mvc/controllers/expense.php */