<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class manual_help extends Admin_Controller{
    
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
    }
            function  index(){

        $usertype = $this->session->userdata("usertype");
        if($usertype=="Teacher"){
            $this->load->view('manual_help/teacher_manual_help');
        }
        elseif($usertype=="Admin"){
            $this->load->view('manual_help/admin_manual_help');

        }
        elseif($usertype=="Student"){
            $this->load->view('manual_help/student_manual_help');

        }
        elseif($usertype=="Accountant"){
            $this->load->view('manual_help/accountant_manual_help');

        }
        else{
            $this->load->view('manual_help/librarian_manual_help');
        }



    }
}