<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Promotion extends Admin_Controller {

    /**
     * -----------------------------------------
     * 
     * ******* Address****************
     * INETS COMPANY LIMITED
     * P.O BOX 32258, DAR ES SALAAM
     * TANZANIA
     * 
     * 
     * *******Office Location *********
     * 11th block, Bima Road, Mikocheni B, Kinondoni, Dar es salaam
     * 
     * 
     * ********Contacts***************
     * Email: <info@inetstz.com>
     * Website: <www.inetstz.com>
     * Mobile: <+255 655 406 004>
     * Tel:    <+255 22 278 0228>
     * -----------------------------------------
     */
    function __construct() {
	parent::__construct();
	$language = $this->session->userdata('lang');
	$this->lang->load('promotion', $language);
    }

    public function index() {

	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin" || $usertype == "Teacher") {
	    $id = htmlentities(($this->uri->segment(3)));
	    if ((int) $id) {
		$this->data['set'] = $id;
		$this->data['classes'] = $this->student_m->get_classes();
		$this->data['students'] = $this->student_m->get_order_by_student(array('classesID' => $id));
		$this->data['subjects'] = $this->subject_m->get_order_by_subject(array('classesID' => $id));
		$this->data['promotionsubjects'] = $this->promotionsubject_m->get_order_by_promotionsubject(array("classesID" => $id));

		foreach ($this->data['subjects'] as $key => $subject) {
		    $subjectinfo = $this->promotionsubject_m->get_order_by_promotionsubject(array('classesID' => $id, 'subjectID' => $subject->subjectID));

		    if (count($subjectinfo)) {
			$this->db->insert('promotionsubject', array('classesID' => $id, 'subjectID' => $subject->subjectID, 'subjectCode' => $subject->subject_code));
		    }
		}

		$rules = array();
		$array = array();
		if ($_POST) {

		    $prom_status = $this->input->get_post('status');

		    if ($prom_status == 'avg') {

			$pass_mark = $this->input->get_post('pass_mark');
			unset($_POST['pass_mark']);
			unset($_POST['status']);


			$combine_exams_array = array_flip($_POST);
			if (!array_filter($_POST)) {
			    $this->session->set_flashdata('error', 'You need to select at least one exam');
			    redirect(base_url("promotion/index/" . $id));
			}
			redirect("promotion/add/$id/$pass_mark/?t=" . urlencode(json_encode($combine_exams_array)));
		    } else {
			//promote all students to the next class
			$students = $this->student_m->get_order_by_student_year($id);
			$student_ids = '';
			foreach ($students as $student) {
			    $student_ids .=$student->studentID . ',';
			}
			$student_info = rtrim($student_ids, ',');

			$this->promotion_to_next_class($student_info, $id);

			redirect("promotion/index");
		    }
		} else {
		    $this->data["subview"] = "promotion/index";
		    $this->load->view('_layout_main', $this->data);
		}
	    } else {
		$this->data['classes'] = $this->student_m->get_classes();
		$this->data["subview"] = "promotion/search";
		$this->load->view('_layout_main', $this->data);
	    }
	}
    }

    public function getExamsList() {
	$class_id = $this->input->post('id');
	$exams = $this->exam_m->get_exam_by_class($class_id);
	$list = '';
	foreach ($exams as $value) {
	    $list.= "<div class='form-group' >";
	    $list.= "<label for=\"$value->exam\" class=\"col-sm-2 col-sm-offset-2  control-label\">
                 $value->exam
                  </label>";
	    $list.= " <input type=\"checkbox\" class='check' name=\"$value->exam\" value=\"$value->examID\">";
	    $list.= "</div>";
	}
	echo $list;
    }

    public function promotion_list() {
	$classID = $this->input->post('id');
	if ((int) $classID) {
	    $string = base_url("promotion/index/$classID");
	    echo $string;
	} else {
	    redirect(base_url("promotion/index"));
	}
    }

    public function add() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin" || $usertype == "Teacher") {

	    $id = htmlentities(($this->uri->segment(3)));
	    if (isset($_POST) && $this->input->post('prm') != '') {
		//here we deal with someone fail
		$this->promote_repeat_class();
	    }
	    if ((int) $id) {
		$classes = $this->classes_m->get_classes($id);
		if (count($classes)) {
		    $this->data['set'] = $id;
		    $this->data['pass_mark'] = htmlentities(($this->uri->segment(4)));
		    $exams = (array) json_decode($this->input->get('t'));
		    $this->data['students'] = $this->examreport_m->combinesql($id, $exams);

		    $this->data['classes'] = $this->student_m->get_classes();
		    // $this->data['students'] = $this->student_m->get_order_by_student_year($id);
		    if (count($this->data['students'])) {

			/*
			 * select * students and compare the results 
			 * if pass_mark for a student is greater than
			 */

			$student_result = array();

			$i = 0;
			foreach ($this->data['students'] as $key => $student) {
			    $student = (object) $student;
			    $special_promoted = $this->db->query('SELECT * FROM ' . set_schema_name() . 'special_promotion WHERE student_id=' . $student->studentID)->row();
			    if (!empty($special_promoted)) {
				unset($this->data['students'][$i]);
			    } else {
				$f = 0;
				if ($student->average > $this->data['pass_mark']) {
				    $f = 1;
				}
				$student_result[$student->studentID] = $f;
			    }
			    $i++;
			}

			$this->data['student_result'] = $student_result;

			$this->data["subview"] = "promotion/add";
			$this->load->view('_layout_main', $this->data);
		    } else {
			$this->session->set_flashdata('error', $this->lang->line('promotion_emstudent'));
			redirect(base_url('promotion/index'));
		    }
		} else {
		    $this->data["subview"] = "error";
		    $this->load->view('_layout_main', $this->data);
		}
	    } else {
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    function promote_repeat_class() {
	$status = $this->input->post('prm');
	$student_id = $this->input->post('student_id');
	if ($status == 0) {
	    //student transferred to other school
	    $this->student_m->update_student(array('status' => 0), $student_id);
	} else if ($status == 3) {
	    //student repeat in the same class
	    $student = $this->student_m->get_student($student_id);

	    $this->db->insert('special_promotion', array('student_id' => $student_id,
		'from_academic_year_id' => $student->academic_year_id,
		'to_academic_year_id' => $student->academic_year_id,
		'pass_mark' => $student_id = $this->input->post('pass_mark')
	    ));
	}
    }

    function promotion_to_next_class($student_ids = null, $current_class_id = null) {

	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin" || $usertype == "Teacher") {

	    $studentIDs = $student_ids == NULL ? $this->input->post("studentIDs") : $student_ids;
	    $js_classeID = $current_class_id == NULL ? $this->input->post("classesID") : $current_class_id;
	    $explode = explode(",", $studentIDs);

	    /**
	     * specify the current classesID and sectionID
	     * 
	     * Get next class numeric from classesID specified with the same classlevelID
	     * IF yes, take, classesID
	     * 
	     */
	    $classes = $this->classes_m->get_classes($js_classeID);
	    $next_class = $this->classes_m->get_single_class(array('classes_numeric' => $classes->classes_numeric + 1, 'classlevel_id' => $classes->classlevel_id));


	    if (!empty($next_class)) {
		//loop through all students and update them to a next class
		foreach ($explode as $student_id) {

		    //If we want to match section name, then we need to get section name for that student
		    //and select it below
		    $student_info = $this->student_m->get_single_student(array('studentID' => $student_id));
		    if (!empty($student_info)) {
			$next_section = $this->section_m->get_single_section(array('classesID' => $next_class->classesID));

			//$next_academic = $this->academic_year_m->get_next_academic($student_info->academic_year_id);
			$class = $this->classes_m->get_classes($student_info->classesID);
			$academic = $this->academic_year_m->get_current_year($class->classlevel_id);
			if (!empty($academic) && ($academic->id != $student_info->academic_year_id)) {
			    $array = array(
				'classesID' => $next_class->classesID,
				'sectionID' => $next_section->sectionID,
				'academic_year_id' => $academic->id,
			    );

			    $this->student_m->update_student($array, $student_id);
			    // After updating student info, then you need to add records in student_archive
			    $archive_data = array(
				'student_id' => $student_id,
				'academic_year_id' => $student_info->academic_year_id,
				'section_id' => $student_info->sectionID
			    );

			    $this->db->insert('student_archive', $archive_data);
			    $this->session->set_flashdata('success', $this->lang->line('menu_success'));
			    echo 'success';
			}
		    }
		}
	    } else {
		//This shows there is no further class for students to be promoted, thus, its a final class
		// if this is the case, then set that these students have finished the school already

		$this->promoteToFinal($explode);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function promoteToFinal($student_list) {
	if (!empty($student_list)) {
	    foreach ($student_list as $student_id) {

		//If we want to match section name, then we need to get section name for that student
		//and select it below
		$student_info = $this->student_m->get_single_student(array('studentID' => $student_id));
		if (!empty($student_info)) {

		    $array = array(
			'status' => 2
		    );

		    $this->student_m->update_student($array, $student_id);
		    //change parent status into 0 also
		    $this->parentes_m->update_parentes(array('status' => 0), $student_info->parentID);
		}
	    }
	    $this->session->set_flashdata('success', 'Students has been promoted to Final Class');
	    redirect(base_url("promotion/index"));
	} else {
	    
	}
    }

    public function promotion_remark() {
	$special_promotion = array(
	    'remark' => $this->input->get_post('remark'),
	    'from_academic_year_id' => $this->input->get_post('from'),
	    'pass_mark' => $this->input->get_post('pass_mark'),
	    'status' => $this->input->get_post('status'),
	    'student_id' => $this->input->get_post('student_id')
	);
	$this->db->insert('special_promotion', $special_promotion);
	echo $this->input->get_post('status') == 1 ? 'Student has been booked ready for promotion' :
		'student has been placed ready to be shifted to lower class';
    }

    public function remove_promotion_remark() {
	$special_promotion = array(
	    'from_academic_year_id' => $this->input->get_post('from'),
	    'student_id' => $this->input->get_post('student_id')
	);
	$this->db->delete('special_promotion', $special_promotion);
	echo 'Student removed for promotion';
    }

}

/* End of file promotion.php */
/* Location: .//var/www/html/school/mvc/controllers/promotion.php */