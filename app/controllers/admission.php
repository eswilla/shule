<?php

/**
 * Description of admission
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admission extends Admin_Controller {

    /**
     * -----------------------------------------
     * 
     * ******* Address****************
     * INETS COMPANY LIMITED
     * P.O BOX 32258, DAR ES SALAAM
     * TANZANIA
     * 
     * 
     * *******Office Location *********
     * 11th block, Bima Road, Mikocheni B, Kinondoni, Dar es salaam
     * 
     * 
     * ********Contacts***************
     * Email: <info@inetstz.com>
     * Website: <www.inetstz.com>
     * Mobile: <+255 655 406 004>
     * Tel:    <+255 22 278 0228>
     * -----------------------------------------
     */
    private $language;

    function __construct() {
	parent::__construct();
	$this->language = $this->session->userdata('lang');
    }

  
    public function index() {
	//$id = htmlentities(($this->uri->segment(3)));
	$id = $this->input->get_post('next');
	if (empty($id)) {
	    $view = 'admission/register_parent';
	    $this->lang->load('parentes', $this->language);
	} else {
	    $view = 'admission/register_student';
	    $this->data['classes'] = $this->student_m->get_classes();
	    $this->lang->load('student', $this->language);
	    $this->data['parent_id']=$id;
	}
	$this->data["subview"] = $view;
	$this->load->view('_layout_admission', $this->data);
    }
    
    public function status() { 
	$this->data['status']= $this->input->get_post('student_result');
	$this->data["subview"] = 'admission/final_status';
	$this->load->view('_layout_admission', $this->data);
    }

    protected function parentRules() {
	$rules = array(
	    array(
		'field' => 'name',
		'label' => $this->lang->line("parentes_guargian_name"),
		'rules' => 'trim|required|xss_clean|max_length[60]'
	    ),
	    array(
		'field' => 'father_name',
		'label' => $this->lang->line("parentes_father_name"),
		'rules' => 'trim|required|xss_clean|max_length[60]'
	    ),
	    array(
		'field' => 'mother_name',
		'label' => $this->lang->line("parentes_mother_name"),
		'rules' => 'trim|required|xss_clean|max_length[60]'
	    ),
	    array(
		'field' => 'father_profession',
		'label' => $this->lang->line("parentes_father_name"),
		'rules' => 'trim|required|xss_clean|max_length[40]'
	    ),
	    array(
		'field' => 'mother_profession',
		'label' => $this->lang->line("parentes_mother_name"),
		'rules' => 'trim|required|xss_clean|max_length[40]'
	    ),
	    array(
		'field' => 'email',
		'label' => $this->lang->line("parentes_email"),
		'rules' => 'trim|required|max_length[40]|valid_email|xss_clean|callback_unique_email'
	    ),
	    array(
		'field' => 'phone',
		'label' => $this->lang->line("parentes_phone"),
		'rules' => 'trim|min_length[5]|max_length[25]|xss_clean'
	    ),
	    array(
		'field' => 'address',
		'label' => $this->lang->line("parentes_address"),
		'rules' => 'trim|max_length[200]|xss_clean'
	    ),
	    array(
		'field' => 'photo',
		'label' => $this->lang->line("parentes_photo"),
		'rules' => 'trim|max_length[200]|xss_clean'
	    )
	);
	return $rules;
    }

    protected function studentRules() {
	$rules = array(
	    array(
		'field' => 'name',
		'label' => $this->lang->line("student_name"),
		'rules' => 'trim|required|xss_clean|max_length[60]'
	    ),
	    array(
		'field' => 'dob',
		'label' => $this->lang->line("student_dob"),
		'rules' => 'trim|required|max_length[10]|callback_date_valid|xss_clean'
	    ),
	    array(
		'field' => 'sex',
		'label' => $this->lang->line("student_sex"),
		'rules' => 'trim|required|max_length[10]|xss_clean'
	    ),
	    array(
		'field' => 'religion',
		'label' => $this->lang->line("student_religion"),
		'rules' => 'trim|max_length[25]|xss_clean'
	    ),
//			array(
//				'field' => 'email', 
//				'label' => $this->lang->line("student_email"), 
//				'rules' => 'trim|required|max_length[40]|valid_email|xss_clean|callback_unique_email'
//			),
//	    array(
//		'field' => 'phone',
//		'label' => $this->lang->line("student_phone"),
//		'rules' => 'trim|max_length[25]|min_length[5]|xss_clean'
//	    ),
	    array(
		'field' => 'address',
		'label' => $this->lang->line("student_address"),
		'rules' => 'trim|max_length[200]|xss_clean'
	    ),
	    array(
		'field' => 'classesID',
		'label' => $this->lang->line("student_classes"),
		'rules' => 'trim|required|numeric|max_length[11]|xss_clean|callback_unique_classesID'
	    ),
	    array(
		'field' => 'sectionID',
		'label' => $this->lang->line("student_section"),
		'rules' => 'trim|required|numeric|max_length[11]|xss_clean|callback_unique_sectionID'
	    ),
	    array(
		'field' => 'roll',
		'label' => $this->lang->line("student_roll"),
		'rules' => 'trim|required|max_length[40]|callback_unique_roll|xss_clean'
	    ),
	    array(
		'field' => 'guargianID',
		'label' => $this->lang->line("student_guargian"),
		'rules' => 'trim|required|max_length[11]|xss_clean|numeric'
	    ),
	    array(
		'field' => 'photo',
		'label' => $this->lang->line("student_photo"),
		'rules' => 'trim|max_length[200]|xss_clean'
	    ),
//			array(
//			'field' => 'username', 
//			'label' => $this->lang->line("student_username"), 
//			'rules' => 'trim|required|min_length[4]|max_length[40]|xss_clean|callback_lol_username'
//			),
//			array(
//				'field' => 'password',
//				'label' => $this->lang->line("student_password"), 
//				'rules' => 'trim|required|min_length[4]|max_length[40]|xss_clean'
//			)
	);
	return $rules;
    }

    public function addParent() {
	$setting = $this->setting_m->get_setting(1);
	if ($_POST) {
	    $rules = $this->parentRules();
	    $array = array();
	    for ($i = 0; $i < count($rules); $i++) {
		$array[$rules[$i]['field']] = $this->input->post($rules[$i]['field']);
	    }
	    $array['username'] = $this->input->post("phone");
	    $array['password'] = $this->student_m->hash("123456");
	    $array['usertype'] = "Parent";

	    $new_file = "defualt.png";
	    if ($_FILES["image"]['name'] != "") {
		$file_name = $_FILES["image"]['name'];
		$file_name_rename = rand(1, 100000000000);
		$explode = explode('.', $file_name);
		if (count($explode) >= 2) {
		    $new_file = $file_name_rename . '.' . $explode[1];
		    $config['upload_path'] = "./uploads/images";
		    $config['allowed_types'] = "gif|jpg|png";
		    $config['file_name'] = $new_file;
		    $config['max_size'] = '1024';
		    $config['max_width'] = '3000';
		    $config['max_height'] = '3000';
		    $array['photo'] = $new_file;
		    $this->load->library('upload', $config);
		    if (!$this->upload->do_upload("image")) {
			$this->data["image"] = $this->upload->display_errors();
			$this->data["subview"] = "parentes/add";
			$this->load->view('_layout_main', $this->data);
		    } else {
			$data = array("upload_data" => $this->upload->data());
			$this->parentes_m->insert_parentes($array);
			$this->session->set_flashdata('success', $this->lang->line('menu_success'));

			//send email
			$message = 'Hello ' . $this->input->post("name") . ', you have been added as a Parent in ' . $setting->sname . '. Login  with username = ' . $array['username'] . ' and password = 123456 in this link www.' . $setting->sname . '.shulesoft.com';
			//send_email($this->input->post("email"), $setting->sname .' Online Login Information', $message);
			//send sms
			//$this->send_sms($this->input->post("phone"),$message);
			redirect(base_url("parentes/index"));
		    }
		} else {
		    $this->data["image"] = "Invalid file";
		    $this->data["subview"] = "parentes/add";
		    $this->load->view('_layout_main', $this->data);
		}
	    } else {
		$array["photo"] = $new_file;
		$this->parentes_m->insert_parentes($array);
		$this->session->set_flashdata('success', $this->lang->line('menu_success'));
//			
//			//send email
//			$message = 'Hello ' . $this->input->post("name") . ', you have been added as a Parent in ' . $setting->sname . '. Login  with username = ' . $array['username'] . ' and password = 123456 in this link www.' . $setting->sname . '.shulesoft.com';
//			
//			send_email($this->input->post("email"), $setting->sname .' Online Login Information', $message);
//
//			$this->send_sms($this->input->post("phone"), $message);
		$id = $this->db->insert_id();
		redirect(base_url("admission/index/?next=" . $id));
	    }
	} else {
	    redirect(base_url("admission/index"));
	}
    }

    public function addStudent() {

	$setting = $this->setting_m->get_setting(1);

	$classesID = $this->input->post("classesID");

	if ($classesID != 0) {
	    $this->data['sections'] = $this->section_m->get_order_by_section(array("classesID" => $classesID));
	} else {
	    $this->data['sections'] = "empty";
	}
	$this->data['sectionID'] = $this->input->post("sectionID");

	if ($_POST) {
	    $sectionID = $this->input->post("sectionID");
	    if ($sectionID == 0) {
		$this->data['sectionID'] = 0;
	    } else {
		$this->data['sections'] = $this->section_m->get_allsection($classesID);
		$this->data['sectionID'] = $this->input->post("sectionID");
	    }
	   
	    $dbmaxyear = $this->student_m->get_order_by_student_single_max_year($classesID);
	    $maxyear = "";
	    if (count($dbmaxyear)) {
		$maxyear = $dbmaxyear->year;
	    } else {
		$maxyear = date("Y");
	    }

	    $array = array();
	    $array["name"] = $this->input->post("name");
	    $array["dob"] = date("Y-m-d", strtotime($this->input->post("dob")));
	    $array["sex"] = $this->input->post("sex");
	    $array["religion"] = $this->input->post("religion");

	    $array["phone"] = $this->input->post("phone");
	    $array["address"] = $this->input->post("address");
	    $array["classesID"] = $this->input->post("classesID");
	    $array["sectionID"] = 1;
	    $array["section"] = '1';
	    $array["roll"] = time();
	    $array["username"] = str_replace(' ', '.', strtolower($this->input->post("name")));
	    $array['password'] = $this->student_m->hash("abc123456");
	    $array['usertype'] = "Student";
	    $array['parentID'] = $this->input->post('guargianID');
	    $array['library'] = 0;
	    $array['hostel'] = 0;
	    $array['transport'] = 0;
	    $array['create_date'] = date("Y-m-d");
	    $array['year'] = $maxyear;
	    $array['totalamount'] = 0;
	    $array['paidamount'] = 0;
	    $array["email"] = $this->input->post("email") == '' ?
		    $array['username'] . '@shulesoft.com' :
		    $this->input->post("email");
	    
	    $new_file = "defualt.png";
	    if ($_FILES["image"]['name'] != "") {
		$file_name = $_FILES["image"]['name'];
		$file_name_rename = rand(1, 100000000000);
		$explode = explode('.', $file_name);
		if (count($explode) >= 2) {

		    $new_file = $file_name_rename . '.' . $explode[1];
		    $config['upload_path'] = "./uploads/images";
		    $config['allowed_types'] = "gif|jpg|png";
		    $config['file_name'] = $new_file;
		    $config['max_size'] = '1024';
		    $config['max_width'] = '3000';
		    $config['max_height'] = '3000';
		    $array['photo'] = $new_file;
		    $this->load->library('upload', $config);
		    if (!$this->upload->do_upload("image")) {
			$this->data["image"] = $this->upload->display_errors();
			$this->data["subview"] = "student/add";
			$this->load->view('_layout_main', $this->data);
		    } else {
			$data = array("upload_data" => $this->upload->data());


			$this->db->insert('student', $array);
			$this->session->set_flashdata('success', $this->lang->line('menu_success'));
//				$this->send_sms($this->input->post("phone"), 'Hello ' . $this->input->post("name") . ', you have been added as a Student at ' . $setting->sname);
			redirect(base_url("admission/status?value=success"));
		    }
		} else {
		    $this->data["image"] = "Invalid file";
		    $this->data["subview"] = "student/add";
		    $this->load->view('_layout_main', $this->data);
		}
	    } else {
		$array["photo"] = $new_file;
		$this->db->insert('student', $array);
		$this->session->set_flashdata('success', $this->lang->line('menu_success'));
//			$this->send_sms($this->input->post("phone"), 'Hello ' . $this->input->post("name") . ', you have been added as a Student at ' . $setting->sname);
		redirect(base_url("admission/status?value=success"));
	    }
	} else {
	   redirect(base_url("admission/index"));
	}
    }

    public function edit() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin") {
	    $id = htmlentities(($this->uri->segment(3)));
	    $url = htmlentities(($this->uri->segment(4)));
	    if ((int) $id && (int) $url) {
		$this->data['classes'] = $this->student_m->get_classes();
		$this->data['student'] = $this->student_m->get_student($id);
		$this->data['parents'] = $this->parentes_m->get_parentes();
		$classesID = $this->data['student']->classesID;

		$this->data['sections'] = $this->section_m->get_order_by_section(array('classesID' => $classesID));
		$this->data['set'] = $url;
		if ($this->data['student']) {
		    if ($_POST) {
			$rules = $this->rules();
			unset($rules[11], $rules[12], $rules[13]);
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == FALSE) {
			    $this->data["subview"] = "student/edit";
			    $this->load->view('_layout_main', $this->data);
			} else {

			    $array = array();
			    $array["name"] = $this->input->post("name");
			    $array["dob"] = date("Y-m-d", strtotime($this->input->post("dob")));
			    $array["sex"] = $this->input->post("sex");
			    $array["religion"] = $this->input->post("religion");
			    $array["email"] = $this->input->post("email");
			    $array["phone"] = $this->input->post("phone");
			    $array["address"] = $this->input->post("address");
			    $array["classesID"] = $this->input->post("classesID");
			    $array["sectionID"] = $this->input->post("sectionID");
			    $section = $this->section_m->get_section($this->input->post("sectionID"));
			    $array["section"] = $section->section;
			    $array["roll"] = $this->input->post("roll");
			    $array["parentID"] = $this->input->post("guargianID");

			    if ($_FILES["image"]['name'] != "") {
				$file_name = $_FILES["image"]['name'];
				$file_name_rename = rand(1, 100000000000);
				$explode = explode('.', $file_name);
				if (count($explode) >= 2) {
				    $new_file = $file_name_rename . '.' . $explode[1];
				    $config['upload_path'] = "./uploads/images";
				    $config['allowed_types'] = "gif|jpg|png";
				    $config['file_name'] = $new_file;
				    $config['max_size'] = '1024';
				    $config['max_width'] = '3000';
				    $config['max_height'] = '3000';

				    $array['photo'] = $new_file;
				    $this->load->library('upload', $config);
				    if (!$this->upload->do_upload("image")) {
					$this->data["image"] = $this->upload->display_errors();
					$this->data["subview"] = "student/edit";
					$this->load->view('_layout_main', $this->data);
				    } else {
					$data = array("upload_data" => $this->upload->data());
					$this->student_m->update_student($array, $id);
					$this->session->set_flashdata('success', $this->lang->line('menu_success'));
					redirect(base_url("student/index/$url"));
				    }
				} else {
				    $this->data["image"] = "Invalid file";
				    $this->data["subview"] = "student/edit";
				    $this->load->view('_layout_main', $this->data);
				}
			    } else {
				$this->student_m->update_student($array, $id);
				$this->session->set_flashdata('success', $this->lang->line('menu_success'));
				redirect(base_url("student/index/$url"));
			    }
			}
		    } else {
			$this->data["subview"] = "student/edit";
			$this->load->view('_layout_main', $this->data);
		    }
		} else {
		    $this->data["subview"] = "error";
		    $this->load->view('_layout_main', $this->data);
		}
	    } else {
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function view() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin" || $usertype == "Teacher") {
	    $id = htmlentities(($this->uri->segment(3)));
	    $url = htmlentities(($this->uri->segment(4)));
	    if ((int) $id && (int) $url) {
		$this->data["student"] = $this->student_m->get_student($id);
		$this->data["class"] = $this->student_m->get_class($url);
		if ($this->data["student"] && $this->data["class"]) {
		    $this->data["section"] = $this->section_m->get_section($this->data['student']->sectionID);
		    $this->data['set'] = $url;
		    $this->data["parent"] = $this->parentes_m->get_parentes($this->data["student"]->parentID);

		    $this->data["subview"] = "student/view";
		    $this->load->view('_layout_main', $this->data);
		} else {
		    $this->data["subview"] = "error";
		    $this->load->view('_layout_main', $this->data);
		}
	    } else {
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function print_preview() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin") {
	    $id = htmlentities(($this->uri->segment(3)));
	    $url = htmlentities(($this->uri->segment(4)));

	    if ((int) $id && (int) $url) {
		$this->data["student"] = $this->student_m->get_student($id);
		$this->data["class"] = $this->student_m->get_class($url);
		if ($this->data["student"] && $this->data["class"]) {
		    $this->data["section"] = $this->section_m->get_section($this->data['student']->sectionID);
		    $this->load->library('html2pdf');
		    $this->html2pdf->folder('./assets/pdfs/');
		    $this->html2pdf->filename('Report.pdf');
		    $this->html2pdf->paper('a4', 'portrait');
		    $this->data['panel_title'] = $this->lang->line('panel_title');
		    $this->data["parent"] = $this->parentes_m->get_parentes($this->data["student"]->parentID);
		    $html = $this->load->view('student/print_preview', $this->data, true);
		    $this->html2pdf->html($html);
		    $this->html2pdf->create();
		} else {
		    $this->data["subview"] = "error";
		    $this->load->view('_layout_main', $this->data);
		}
	    } else {
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function send_mail() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin") {
	    $id = $this->input->post('id');
	    $url = $this->input->post('set');
	    if ((int) $id && (int) $url) {
		$this->data["student"] = $this->student_m->get_student($id);
		$this->data["class"] = $this->student_m->get_class($url);
		if ($this->data["student"] && $this->data["class"]) {
		    $this->data["section"] = $this->section_m->get_section($this->data['student']->sectionID);
		    $this->load->library('html2pdf');
		    $this->html2pdf->folder('uploads/report');
		    $this->html2pdf->filename('Report.pdf');
		    $this->html2pdf->paper('a4', 'portrait');
		    $this->data['panel_title'] = $this->lang->line('panel_title');
		    $this->data["parent"] = $this->parentes_m->get_parentes($this->data["student"]->parentID);
		    $html = $this->load->view('student/print_preview', $this->data, true);
		    $this->html2pdf->html($html);
		    $this->html2pdf->create('save');

		    if ($path = $this->html2pdf->create('save')) {
			$this->load->library('email');
			$this->email->set_mailtype("html");
			$this->email->from($this->data["siteinfos"]->email, $this->data['siteinfos']->sname);
			$this->email->to($this->input->post('to'));
			$this->email->subject($this->input->post('subject'));
			$this->email->message($this->input->post('message'));
			$this->email->attach($path);
			if ($this->email->send()) {
			    $this->session->set_flashdata('success', $this->lang->line('mail_success'));
			} else {
			    $this->session->set_flashdata('error', $this->lang->line('mail_error'));
			}
		    }
		} else {
		    $this->data["subview"] = "error";
		    $this->load->view('_layout_main', $this->data);
		}
	    } else {
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function delete() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin") {
	    $id = htmlentities(($this->uri->segment(3)));
	    $url = htmlentities(($this->uri->segment(4)));
	    if ((int) $id && (int) $url) {
		$this->student_m->delete_student($id);
		$this->session->set_flashdata('success', $this->lang->line('menu_success'));
		redirect(base_url("student/index/$url"));
	    } else {
		redirect(base_url("student/index/$url"));
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function unique_roll() {
	$id = htmlentities(($this->uri->segment(3)));
	if ((int) $id) {
	    $student = $this->student_m->get_order_by_roll(array("roll" => $this->input->post("roll"), "studentID !=" => $id, "classesID" => $this->input->post('classesID')));
	    if (count($student)) {
		$this->form_validation->set_message("unique_roll", "%s already exists");
		return FALSE;
	    }
	    return TRUE;
	} else {
	    $student = $this->student_m->get_order_by_roll(array("roll" => $this->input->post("roll"), "classesID" => $this->input->post('classesID')));

	    if (count($student)) {
		$this->form_validation->set_message("unique_roll", "%s already exists");
		return FALSE;
	    }
	    return TRUE;
	}
    }

    public function lol_username() {
	$id = htmlentities(($this->uri->segment(3)));
	if ((int) $id) {
	    $tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'setting' => 'setting');
	    $array = array();
	    $i = 0;
	    foreach ($tables as $table) {
		$user = $this->student_m->get_username($table, array("username" => $this->input->post('username'), $table . "ID !=" => $id));
		if (count($user)) {
		    $this->form_validation->set_message("lol_username", "%s already exists");
		    $array['permition'][$i] = 'no';
		} else {
		    $array['permition'][$i] = 'yes';
		}
		$i++;
	    }
	    if (in_array('no', $array['permition'])) {
		return FALSE;
	    } else {
		return TRUE;
	    }
	} else {
	    $tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'setting' => 'setting');
	    $array = array();
	    $i = 0;
	    foreach ($tables as $table) {
		$user = $this->student_m->get_username($table, array("username" => $this->input->post('username')));
		if (count($user)) {
		    $this->form_validation->set_message("lol_username", "%s already exists");
		    $array['permition'][$i] = 'no';
		} else {
		    $array['permition'][$i] = 'yes';
		}
		$i++;
	    }

	    if (in_array('no', $array['permition'])) {
		return FALSE;
	    } else {
		return TRUE;
	    }
	}
    }

    public function unique_classesID() {
	if ($this->input->post('classesID') == 0) {
	    $this->form_validation->set_message("unique_classesID", "The %s field is required");
	    return FALSE;
	}
	return TRUE;
    }

    public function unique_sectionID() {
	if ($this->input->post('sectionID') == 0) {
	    $this->form_validation->set_message("unique_sectionID", "The %s field is required");
	    return FALSE;
	}
	return TRUE;
    }

    public function student_list() {
	$classID = $this->input->post('id');
	if ((int) $classID) {
	    $string = base_url("student/index/$classID");
	    echo $string;
	} else {
	    redirect(base_url("student/index"));
	}
    }

    public function unique_email() {
	$id = htmlentities(($this->uri->segment(3)));
	if ((int) $id) {
	    $user_info = $this->student_m->get_single_student(array('studentID' => $id));
	    $tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'setting' => 'setting');
	    $array = array();
	    $i = 0;
	    foreach ($tables as $table) {
		$user = $this->student_m->get_username($table, array("email" => $this->input->post('email'), 'username !=' => $user_info->username));
		if (count($user)) {
		    $this->form_validation->set_message("unique_email", "%s already exists");
		    $array['permition'][$i] = 'no';
		} else {
		    $array['permition'][$i] = 'yes';
		}
		$i++;
	    }
	    if (in_array('no', $array['permition'])) {
		return FALSE;
	    } else {
		return TRUE;
	    }
	} else {
	    $tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'setting' => 'setting');
	    $array = array();
	    $i = 0;
	    foreach ($tables as $table) {
		$user = $this->student_m->get_username($table, array("email" => $this->input->post('email')));
		if (count($user)) {
		    $this->form_validation->set_message("unique_email", "%s already exists");
		    $array['permition'][$i] = 'no';
		} else {
		    $array['permition'][$i] = 'yes';
		}
		$i++;
	    }

	    if (in_array('no', $array['permition'])) {
		return FALSE;
	    } else {
		return TRUE;
	    }
	}
    }

    function sectioncall() {
	$classesID = $this->input->post('id');

	if ((int) $classesID) {
	    $allsection = $this->section_m->get_order_by_section(array('classesID' => $classesID));
	    echo "<option value='0'>", $this->lang->line("student_select_section"), "</option>";
	    foreach ($allsection as $value) {
		echo "<option value=\"$value->sectionID\">", $value->section, "</option>";
	    }
	}
    }

}

/* End of file student_help.php */
/* Location: .//D/xampp/htdocs/school/mvc/controllers/student_help.php */
