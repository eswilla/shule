<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mark extends Admin_Controller {

    /**
     * -----------------------------------------
     * 
     * ******* Address****************
     * INETS COMPANY LIMITED
     * P.O BOX 32258, DAR ES SALAAM
     * TANZANIA
     * *******Office Location *********
     * 11th block, Bima Road, Mikocheni B, Kinondoni, Dar es salaam
     * 
     * 
     * ********Contacts***************
     * Email: <info@inetstz.com>
     * Website: <www.inetstz.com>
     * Mobile: <+255 655 406 004>
     * Tel:    <+255 22 278 0228>
     * -----------------------------------------
     */
    function __construct() {
	parent::__construct();
	$language = $this->session->userdata('lang');
	$this->lang->load('mark', $language);
    }

    protected function rules() {
	$rules = array(
	    array(
		'field' => 'examID',
		'label' => $this->lang->line("mark_exam"),
		'rules' => 'trim|required|xss_clean|max_length[11]|callback_check_exam'
	    ),
	    array(
		'field' => 'classesID',
		'label' => $this->lang->line("mark_classes"),
		'rules' => 'trim|required|xss_clean|max_length[11]|callback_check_classes'
	    ),
	    array(
		'field' => 'subjectID',
		'label' => $this->lang->line("mark_subject"),
		'rules' => 'trim|required|xss_clean|max_length[11]|callback_check_subject'
	    )
	);
	return $rules;
    }

    public function get_rank_per_all_subjects($examID, $classesID, $studentID, $sectionID = null) {
	if ($sectionID == NULL) {
	    $sql = 'SELECT sum,rank FROM (
select "examID", "classesID", "studentID", sum(mark), rank() over (order by sum(mark) desc) from ' . set_schema_name() . 'mark where "examID" = ' . $examID . ' and "classesID" = ' . $classesID . ' and mark is not null group by "examID", "classesID", "studentID" ) a WHERE a."studentID"=' . $studentID . '';
	} else {
	    $sql = 'SELECT sum,rank FROM (
select a."examID", a."classesID", a."studentID", sum(a.mark), rank() over (order by sum(a.mark) desc) from ' . set_schema_name() . 'mark a where a."examID" = ' . $examID . ' and a."classesID" = ' . $classesID . ' and a."studentID" in (select "studentID" from ' . set_schema_name() . 'student where "sectionID" = ' . $sectionID . ')  and mark is not null group by a."examID", a."classesID", a."studentID") a WHERE a."studentID"=' . $studentID . '';
	}
	return $total = $this->db->query($sql)->row();
    }

    public function index() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin" || $usertype == "Teacher") {
	    $id = htmlentities(($this->uri->segment(3)));
	    if ((int) $id) {
		$this->data['set'] = $id;
		$this->data['classes'] = $this->student_m->get_classes();
		$this->data['student'] = $this->student_m->get_order_by_student(array('classesID' => $id));
		$this->data["subview"] = "mark/index";
		$this->load->view('_layout_main', $this->data);
	    } else {
		$this->data['classes'] = $this->student_m->get_classes();
		$this->data["subview"] = "mark/search";
		$this->load->view('_layout_main', $this->data);
	    }
	} elseif ($usertype == 'Parent') {
	    $username = $this->session->userdata("username");
	    $parent = $this->parentes_m->get_single_parentes(array('username' => $username));
	    $this->data['allstudents'] = $this->student_m->get_order_by_student(array('parentID' => $parent->parentID));
	    $id = htmlentities(($this->uri->segment(3)));

	    if ((int) $id) {
		$checkstudent = $this->student_m->get_single_student(array('studentID' => $id));
		if (count($checkstudent)) {
		    $classesID = $checkstudent->classesID;
		    $studentID = $checkstudent->studentID;
		    $student = $checkstudent;
		    $this->data['set'] = $id;
		    $this->data["student"] = $this->student_m->get_student($studentID);
		    $this->data["classes"] = $this->student_m->get_class($classesID);

		    if ($this->data["student"] && $this->data["classes"]) {

			$this->data["exams"] = $this->db->query('SELECT a.exam, a."examID", a.date, a.note FROM ' . set_schema_name() . 'exam a WHERE a."examID" IN (SELECT "examID"  FROM ' . set_schema_name() . 'mark m WHERE m."studentID"=' . $studentID . ')')->result();
			$this->data["grades"] = $this->grade_m->get_order_by_grade(array('classlevel_id' => $this->data['classes']->classlevel_id));
			$this->data["marks"] = $this->mark_m->get_order_by_mark(array("studentID" => $student->studentID, "classesID" => $student->classesID));
			$this->data["section"] = $this->section_m->get_section($this->data['student']->sectionID);
			$this->data["exams_report"] = $this->db->query("SELECT * from " . set_schema_name() . 'exam_report WHERE "classesID"=' . $classesID . ' AND student_id=' . $student->studentID)->result();

			$this->data["subview"] = "mark/index_parent";
			$this->load->view('_layout_main', $this->data);
		    } else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		    }
		} else {
		    $this->data["subview"] = "error";
		    $this->load->view('_layout_main', $this->data);
		}
	    } else {
		$this->data["subview"] = "mark/search_parent";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function add() {
	$usertype = $this->data['usertype'] = $this->session->userdata("usertype");
	if ($usertype == "Admin" || $usertype == "Teacher") {
	    $this->data['students'] = array();
	    $this->data['set_exam'] = 0;
	    $this->data['set_classes'] = 0;
	    $this->data['set_subject'] = 0;
	    $classesID = $this->input->post("classesID");


	    if ($classesID != 0) {

		if ($usertype == "Admin") {
		    $this->data['subjects'] = $this->subject_m->get_subject_call($classesID);
		} elseif ($usertype == "Teacher") {
		    $username = $this->session->userdata("username");
		    $teacher = $this->user_m->get_username_row("teacher", array("username" => $username));
		    $this->data['subjects'] = $this->subject_m->get_order_by_subject(array("classesID" => $classesID, "teacherID" => $teacher->teacherID));
		}
	    } else {
		$this->data['subjects'] = 0;
	    }
	    $this->data['exams'] = $this->exam_m->get_exam();
	    $this->data['classes'] = $this->classes_m->get_classes();
	    if ($_POST) {

		$rules = $this->rules();
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE) {
		    $this->data["subview"] = "mark/add";
		    $this->load->view('_layout_main', $this->data);
		} else {
		    $examID = $this->input->post('examID');
		    $classesID = $this->input->post('classesID');
		    $subjectID = $this->input->post('subjectID');
		    $sectionID = $this->input->post('sectionID');

		    $this->data['set_exam'] = $examID;
		    $this->data['set_classes'] = $classesID;
		    $this->data['set_subject'] = $subjectID;

		    $exam = $this->exam_m->get_exam($examID);
		    $subject = $this->subject_m->get_subject($subjectID);
		    $year = date("Y");
		    $section = $sectionID == 0 ? array("classesID" => $classesID) :
			    array("classesID" => $classesID, 'sectionID' => $sectionID);

		    /**
		     * Make sure you save marks only for students who study this subject
		     */
		    $students = $this->mark_m->get_student_subject($subjectID);

		    if (count($students)) {
			foreach ($students as $student) {
			    $studentID = $student->studentID;
			    $in_student = $this->mark_m->get_order_by_mark(array("examID" => $examID, "classesID" => $classesID, "subjectID" => $subjectID, "studentID" => $studentID));
			    if (!count($in_student)) {
				$data = array(
				    "examID" => $examID,
				    "exam" => $exam->exam,
				    "studentID" => $studentID,
				    "classesID" => $classesID,
				    "subjectID" => $subjectID,
				    "subject" => $subject->subject,
				    "year" => $year
				);
				$this->mark_m->insert_mark($data);
			    }
			}
			$this->data['students'] = $students;
			$all_student = $this->mark_m->get_order_by_mark(array("examID" => $examID, "classesID" => $classesID, "subjectID" => $subjectID));
			$this->data['marks'] = $all_student;
		    }

		    $this->data["subview"] = "mark/add";
		    $this->load->view('_layout_main', $this->data);
		}
	    } else {
		$this->data["subview"] = "mark/add";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    function mark_send() {
	$examID = $this->input->post("examID");
	$classesID = $this->input->post("classesID");
	$subjectID = $this->input->post("subjectID");
	$inputs = $this->input->post("inputs");
	$exploade = explode("$", $inputs);
	$ex_array = array();
	foreach ($exploade as $key => $value) {
	    if ($value == "") {
		break;
	    } else {
		$ar_exp = explode(":", $value);
		$ex_array[$ar_exp[0]] = $ar_exp[1];
	    }
	}

	$students = $this->student_m->get_order_by_student(array("classesID" => $classesID));
	foreach ($students as $student) {
	    foreach ($ex_array as $key => $mark) {
		if ($key == $student->studentID) {
		    $array = array("mark" => $mark);
		    $this->mark_m->update_mark_classes($array, array("examID" => $examID, "classesID" => $classesID, "subjectID" => $subjectID, "studentID" => $student->studentID));
		    break;
		}
	    }
	}
	echo $this->lang->line('mark_success');
    }

    public function getExamByClass() {

	$classId = $this->input->post('id');
	if ((int) $classId) {
	    $exams = $this->db->query('SELECT a.exam, a."examID" from ' . set_schema_name() . 'exam a join ' . set_schema_name() . 'class_exam b ON b.exam_id=a."examID" where  b.class_id=' . $classId)->result();
	    echo "<option value='0'>Select Exam</option>";
	    foreach ($exams as $value) {
		echo "<option value=\"$value->examID\">", $value->exam, "</option>";
	    }
	}
    }

    public function view() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin" || $usertype == "Teacher") {
	    $id = htmlentities(($this->uri->segment(3)));
	    $url = htmlentities(($this->uri->segment(4)));

	    if ((int) $id && (int) $url) {
		$this->data["student"] = $this->student_m->get_student($id);
		$this->data["classes"] = $this->student_m->get_class($url);
		if ($this->data["student"] && $this->data["classes"]) {
		    $this->data['set'] = $url;
		    $this->data["exams"] = $this->exam_m->get_exam();
		    $this->data["grades"] = $this->grade_m->get_grade();
		    $this->data["marks"] = $this->mark_m->get_order_by_mark(array("studentID" => $id, "classesID" => $url));
		    $this->data["section"] = $this->section_m->get_section($this->data['student']->sectionID);



		    $this->data["subview"] = "mark/view";
		    $this->load->view('_layout_main', $this->data);
		} else {
		    $this->data["subview"] = "error";
		    $this->load->view('_layout_main', $this->data);
		}
	    } else {
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	    }
	} elseif ($usertype == "Student") {

	    $username = $this->session->userdata("username");
	    $student = "";
	    if ($usertype == "Student") {
		$student = $this->user_m->get_username_row("student", array("username" => $username));
	    } elseif ($usertype == "Parent") {
		$user = $this->user_m->get_username_row("parent", array("username" => $username));
		$student = $this->student_m->get_student($user->studentID);
	    }

	    $this->data["student"] = $this->student_m->get_student($student->studentID);
	    $this->data["classes"] = $this->student_m->get_class($student->classesID);
	    if ($this->data["student"] && $this->data["classes"]) {
		$this->data["exams"] = $this->exam_m->get_exam();
		$this->data["grades"] = $this->grade_m->get_grade();
		$this->data["marks"] = $this->mark_m->get_order_by_mark(array("studentID" => $student->studentID, "classesID" => $student->classesID));

		$this->data["section"] = $this->section_m->get_section($this->data['student']->sectionID);
		$this->data["subview"] = "mark/view";
		$this->load->view('_layout_main', $this->data);
	    } else {
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    function print_preview() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin" || $usertype == "Teacher") {
	    $id = htmlentities(($this->uri->segment(3)));
	    $url = htmlentities(($this->uri->segment(4)));

	    if ((int) $id && (int) $url) {
		$this->data["student"] = $this->student_m->get_student($id);
		$this->data["classes"] = $this->student_m->get_class($url);
		if ($this->data["student"] && $this->data["classes"]) {
		    $this->data['set'] = $url;
		    $this->data["exams"] = $this->exam_m->get_exam();
		    $this->data["grades"] = $this->grade_m->get_grade();
		    $this->data["marks"] = $this->mark_m->get_order_by_mark(array("studentID" => $id, "classesID" => $url));
		    $this->data["section"] = $this->section_m->get_section($this->data['student']->sectionID);

		    $this->load->library('html2pdf');
		    $this->html2pdf->folder('./assets/pdfs/');
		    $this->html2pdf->filename('Report.pdf');
		    $this->html2pdf->paper('a4', 'portrait');
		    $this->data['panel_title'] = $this->lang->line('panel_title');
		    $html = $this->load->view('mark/print_preview', $this->data, true);
		    $this->html2pdf->html($html);
		    $this->html2pdf->create();
		} else {
		    $this->data["subview"] = "error";
		    $this->load->view('_layout_main', $this->data);
		}
	    } else {
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function send_mail() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin" || $usertype == "Teacher") {
	    $id = $this->input->post('id');
	    $url = $this->input->post('set');
	    if ((int) $id && (int) $url) {
		$this->data["student"] = $this->student_m->get_student($id);
		$this->data["classes"] = $this->student_m->get_class($url);
		if ($this->data["student"] && $this->data["classes"]) {

		    $this->data['set'] = $url;
		    $this->data["exams"] = $this->exam_m->get_exam();
		    $this->data["grades"] = $this->grade_m->get_grade();
		    $this->data["marks"] = $this->mark_m->get_order_by_mark(array("studentID" => $id, "classesID" => $url));
		    $this->data["section"] = $this->section_m->get_section($this->data['student']->sectionID);

		    $this->load->library('html2pdf');
		    $this->html2pdf->folder('uploads/report');
		    $this->html2pdf->filename('Report.pdf');
		    $this->html2pdf->paper('a4', 'portrait');
		    $this->data['panel_title'] = $this->lang->line('panel_title');
		    $html = $this->load->view('mark/print_preview', $this->data, true);
		    $this->html2pdf->html($html);
		    $this->html2pdf->create('save');

		    if ($path = $this->html2pdf->create('save')) {
			$this->load->library('email');
			$this->email->set_mailtype("html");
			$this->email->from($this->data["siteinfos"]->email, $this->data['siteinfos']->sname);
			$this->email->to($this->input->post('to'));
			$this->email->subject($this->input->post('subject'));
			$this->email->message($this->input->post('message'));
			$this->email->attach($path);
			if ($this->email->send()) {
			    $this->session->set_flashdata('success', $this->lang->line('mail_success'));
			} else {
			    $this->session->set_flashdata('error', $this->lang->line('mail_error'));
			}
		    }
		} else {
		    $this->data["subview"] = "error";
		    $this->load->view('_layout_main', $this->data);
		}
	    } else {
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    function mark_list() {
	$classID = $this->input->post('id');
	if ((int) $classID) {
	    $string = base_url("mark/index/$classID");
	    echo $string;
	} else {
	    redirect(base_url("mark/index"));
	}
    }

    function student_list() {
	$studentID = $this->input->post('id');
	if ((int) $studentID) {
	    $string = base_url("mark/index/$studentID");
	    echo $string;
	} else {
	    redirect(base_url("mark/index"));
	}
    }

    function subjectcall() {
	$usertype = $this->session->userdata("usertype");
	$id = $this->input->post('id');
	if ((int) $id) {
	    if ($usertype == "Admin") {
		$allsubject = $this->subject_m->get_order_by_subject(array("classesID" => $id));
		echo "<option value='0'>", $this->lang->line("mark_select_subject"), "</option>";
		foreach ($allsubject as $value) {
		    echo "<option value=\"$value->subjectID\">", $value->subject, "</option>";
		}
	    } elseif ($usertype == "Teacher") {
		$username = $this->session->userdata("username");
		$teacher = $this->user_m->get_username_row("teacher", array("username" => $username));
		$allsubject = $this->subject_m->get_order_by_subject(array("classesID" => $id, "teacherID" => $teacher->teacherID));
		echo "<option value='0'>", $this->lang->line("mark_select_subject"), "</option>";
		foreach ($allsubject as $value) {
		    echo "<option value=\"$value->subjectID\">", $value->subject, "</option>";
		}
	    }
	}
    }

    public function getSubjectBySection() {
	$id = $this->input->post('id');
	if ((int) $id) {
	    $allsubject = $this->db->query('SELECT * FROM ' . set_schema_name() . 'subject WHERE "subjectID" IN (SELECT subject_id FROM ' . set_schema_name() . 'subject_section WHERE section_id=' . $id . ' )')->result();
	    echo "<option value='0'>", $this->lang->line("mark_select_subject"), "</option>";
	    foreach ($allsubject as $value) {
		echo "<option value=\"$value->subjectID\">", $value->subject, "</option>";
	    }
	}
    }

    function check_exam() {
	if ($this->input->post('examID') == 0) {
	    $this->form_validation->set_message("check_exam", "The %s field is required");
	    return FALSE;
	}
	return TRUE;
    }

    function check_classes() {
	if ($this->input->post('classesID') == 0) {
	    $this->form_validation->set_message("check_classes", "The %s field is required");
	    return FALSE;
	}
	return TRUE;
    }

    function check_subject() {
	if ($this->input->post('subjectID') == 0) {
	    $this->form_validation->set_message("check_subject", "The %s field is required");
	    return FALSE;
	}
	return TRUE;
    }

    public function get_position($student_id, $examID, $subject_id, $classesID, $sectionID = null) {
	if ($sectionID == NULL) {

	    $sql = 'select subject, mark, rank FROM (
select a."examID", a."subject", a."classesID", a."studentID", a.mark, rank() over (partition by a."subjectID" order by a.mark desc) from ' . set_schema_name() . 'mark a where a."examID" = ' . $examID . ' and a."classesID" =' . $classesID . ' and "subjectID" = ' . $subject_id . '  and a.mark is not null 
) a where "studentID"=' . $student_id;
	} else {
	    $sql = 'select subject, mark, rank FROM ( select subject, a."examID", a."classesID", a."studentID", a.mark, rank() over (partition by a."subjectID" order by a.mark desc) from ' . set_schema_name() . 'mark a where a."examID" = ' . $examID . ' and a."classesID" = ' . $classesID . ' and "subjectID" =' . $subject_id . ' and "studentID" in (select "studentID" from ' . set_schema_name() . 'student where "sectionID" = ' . $sectionID . ') and a.mark is not null) a where "studentID"=' . $student_id;
	}
	return $this->db->query($sql)->row();
    }

    public function get_general_report($student_id, $exam_id) {
	$sql = 'select a.total, a.rank from  ' . set_schema_name() . 'mark_grade a  where a."examID"=' . $exam_id . ' AND  a."studentID"=' . $student_id;
	return $total = $this->db->query($sql)->row();
    }

    public function get_student_per_class($classesID, $sectionID = NULL) {
	if ($sectionID == NULL) {
	    $sql = 'select COUNT(*) as total_student from  ' . set_schema_name() . 'student a  where a."classesID"=' . $classesID;
	} else {
	    $sql = 'select COUNT(*) as total_student from  ' . set_schema_name() . 'student a  where a."classesID"=' . $classesID . ' AND a."sectionID"=' . $sectionID;
	}
	return $total = $this->db->query($sql)->row();
    }

    public function uploadMark($examID = null, $classesID = null, $subjectID = null) {
	/*
	 *   $data = array(
	  array(
	  'title' => 'My title' ,
	  'name' => 'My Name' ,
	  'date' => 'My date'
	  ),
	  array(
	  'title' => 'Another title' ,
	  'name' => 'Another Name' ,
	  'date' => 'Another date'
	  )
	  );

	  $this->db->insert_batch('mytable', $data);
	 */


	$data = $this->uploadExcel();
	$exam_id = $examID == NULL ? $this->input->get_post('examID') : $examID;
	$class_id = $classesID == NULL ? $this->input->get_post('classesID') : $classesID;


	$exam = $this->exam_m->get_exam($exam_id);

	$status = '';

	foreach ($data as $k => $val) {
	    $value = array_change_key_case($val, CASE_LOWER);

	    $student = $this->student_m->get_single_student(array('roll' => '' . $value['roll'] . '','classesID'=>$class_id));

	    if (!empty($student)) {
//check available subject for this student and loop through all subjects
		$subjects = $this->mark_m->get_subject_for_student($student->studentID);

		foreach ($subjects as $subject) {

			$data_values = array(
			    'examID' => $exam->examID,
			    'exam' => $exam->exam,
			    'studentID' => $student->studentID,
			    'classesID' => $class_id,
			    'subjectID' => $subject->subject_id,
			    'subject' => $subject->subject,
			    'year' => date('Y'),
			    'mark' => $value[strtolower($subject->subject)]
			);
			$where = array(
			    'examID' => $exam->examID,
			    'exam' => $exam->exam,
			    'studentID' => $student->studentID,
			    'classesID' => $class_id,
			    'subjectID' => $subject->subject_id,
			    'subject' => $subject->subject,
			    'year' => date('Y')
			);
			$marks = $this->mark_m->get_order_by_mark($where);
			$mark_all = $this->mark_m->get_order_by_mark($data_values);
			if (empty($marks)) {
			    $this->mark_m->insert_mark($data_values);
			    $status.='<p class="alert alert-success">' . $student->name . ', ' . $subject . ' marks uploaded successful</p>';
			} else {
			    if (empty($mark_all)) {
				$array = array("mark" => $value[strtolower($subject->subject)]);
				$this->mark_m->update_mark_classes($array, $where);
				$status.='<p class="alert alert-info">' . $student->name . ', ' . $subject->subject . ' marks updated successful</p>';
			    }
			}
		    
		}
	    } else {
		$status.= '<p class="alert alert-danger"> Student roll ' . $value['roll'] . '  does not exist</p>';
	    }
	}
	$this->data['status'] = $status;
	$this->data["subview"] = "mark/upload_status";
	$this->load->view('_layout_main', $this->data);
    }

}

/* End of file class.php */
/* Location: .//D/xampp/htdocs/school/mvc/controllers/class.php */