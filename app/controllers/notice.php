<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Notice extends Admin_Controller {

    /**
     * -----------------------------------------
     * 
     * ******* Address****************
     * INETS COMPANY LIMITED
     * P.O BOX 32258, DAR ES SALAAM
     * TANZANIA
     * 
     * 
     * *******Office Location *********
     * 11th block, Bima Road, Mikocheni B, Kinondoni, Dar es salaam
     * 
     * 
     * ********Contacts***************
     * Email: <info@inetstz.com>
     * Website: <www.inetstz.com>
     * Mobile: <+255 655 406 004>
     * Tel:    <+255 22 278 0228>
     * -----------------------------------------
     */
    function __construct() {
	parent::__construct();
	$this->load->model("notice_m");
	$this->load->model("alert_m");
	$language = $this->session->userdata('lang');
	$this->lang->load('notice', $language);
    }

    public function index() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype) {
	    $year = date("Y");
	    $this->data['notices'] = $this->notice_m->get_order_by_notice(array('year' => $year));
	    $this->data["subview"] = "notice/index";
	    $this->load->view('_layout_main', $this->data);
	} else {
	    $this->data["subview"] = "notice/add";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    protected function rules() {
	$rules = array(
	    array(
		'field' => 'title',
		'label' => $this->lang->line("notice_title"),
		'rules' => 'trim|required|xss_clean|max_length[128]'
	    ),
	    array(
		'field' => 'date',
		'label' => $this->lang->line("notice_date"),
		'rules' => 'trim|required|max_length[20]|xss_clean'
	    ),
	    array(
		'field' => 'notice',
		'label' => $this->lang->line("notice_notice"),
		'rules' => 'trim|required|xss_clean'
	    )
	);
	return $rules;
    }

    public function add() {
	$usertype = $this->session->userdata("usertype");
	$year = date("Y");
	if ($usertype == "Admin") {
	    $this->data['classes']=  $this->classes_m->get_classes();
	    if ($_POST) {
		$rules = $this->rules();
		$this->form_validation->set_rules($rules);
		if($this->form_validation->run() == FALSE) {
		    $this->data['form_validation'] = validation_errors();
		    $this->data["subview"] = "notice/add";
		    $this->load->view('_layout_main', $this->data);
		} else {
		    $array = array(
			"title" => $this->input->post("title"),
			"notice" => $this->input->post("notice"),
			"notice_for"=>  implode(',', $this->input->post('notice_for')),
			"year" => $year,
			"date" => date("Y-m-d", strtotime($this->input->post("date")))
		    );
		    $this->db->insert('notice',$array);
		    $this->session->set_flashdata('success', $this->lang->line('menu_success'));
		    redirect(base_url("notice/index"));
		}
	    } else {
		$this->data["subview"] = "notice/add";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function edit() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin") {
	    $id = htmlentities(($this->uri->segment(3)));
	    if ((int) $id) {
		$this->data['notice'] = $this->notice_m->get_notice($id);
		if ($this->data['notice']) {
		    if ($_POST) {
			$rules = $this->rules();
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == FALSE) {
			    $this->data["subview"] = "notice/edit";
			    $this->load->view('_layout_main', $this->data);
			} else {
			    $array = array(
				"title" => $this->input->post("title"),
				"notice" => $this->input->post("notice"),
				"date" => date("Y-m-d", strtotime($this->input->post("date")))
			    );

			    $this->notice_m->update_notice($array, $id);
			    $this->session->set_flashdata('success', $this->lang->line('menu_success'));
			    redirect(base_url("notice/index"));
			}
		    } else {
			$this->data["subview"] = "notice/edit";
			$this->load->view('_layout_main', $this->data);
		    }
		} else {
		    $this->data["subview"] = "error";
		    $this->load->view('_layout_main', $this->data);
		}
	    } else {
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function view() {
	$id = htmlentities(($this->uri->segment(3)));
	if ((int) $id) {
	    $this->data['notice'] = $this->notice_m->get_notice($id);
	    if ($this->data['notice']) {
		$alert = $this->alert_m->get_alert_by_notic($id);
		if (!count($alert)) {
		    $array = array(
			"noticeID" => $id,
			"username" => $this->session->userdata("username"),
			"usertype" => $this->session->userdata("usertype")
		    );
		    $this->alert_m->insert_alert($array);
		    $this->data["subview"] = "notice/view";
		    $this->load->view('_layout_main', $this->data);
		} else {
		    $this->data["subview"] = "notice/view";
		    $this->load->view('_layout_main', $this->data);
		}
	    } else {
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function delete() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin") {
	    $id = htmlentities(($this->uri->segment(3)));
	    if ((int) $id) {
		$this->notice_m->delete_notice($id);
		$this->session->set_flashdata('success', $this->lang->line('menu_success'));
		redirect(base_url("notice/index"));
	    } else {
		redirect(base_url("notice/index"));
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    function date_valid($date) {
	if (strlen($date) < 10) {
	    $this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");
	    return FALSE;
	} else {
	    $arr = explode("/", $date);
	    $dd = $arr[1];
	    $mm = $arr[0];
	    $yyyy = $arr[2];
	    if (checkdate($mm, $dd, $yyyy)) {
		return TRUE;
	    } else {
		$this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");
		return FALSE;
	    }
	}
    }

    public function print_preview() {
	$id = htmlentities(($this->uri->segment(3)));
	if ((int) $id) {
	    $this->data['notice'] = $this->notice_m->get_notice($id);
	    if ($this->data['notice']) {

		$this->load->library('html2pdf');
		$this->html2pdf->folder('./assets/pdfs/');
		$this->html2pdf->filename('Report.pdf');
		$this->html2pdf->paper('a4', 'portrait');
		$this->data['panel_title'] = $this->lang->line('panel_title');
		$html = $this->load->view('notice/print_preview', $this->data, true);
		$this->html2pdf->html($html);
		$this->html2pdf->create();
	    } else {
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function send_mail() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin") {
	    $id = $this->input->post('id');
	    if ((int) $id) {
		$this->data['notice'] = $this->notice_m->get_notice($id);
		if ($this->data['notice']) {

		    $this->load->library('html2pdf');
		    $this->html2pdf->folder('uploads/report');
		    $this->html2pdf->filename('Report.pdf');
		    $this->html2pdf->paper('a4', 'portrait');
		    $this->data['panel_title'] = $this->lang->line('panel_title');
		    $html = $this->load->view('notice/print_preview', $this->data, true);
		    $this->html2pdf->html($html);
		    $this->html2pdf->create('save');

		    if ($path = $this->html2pdf->create('save')) {
			$this->load->library('email');
			$this->email->set_mailtype("html");
			$this->email->from($this->data["siteinfos"]->email, $this->data['siteinfos']->sname);
			$this->email->to($this->input->post('to'));
			$this->email->subject($this->input->post('subject'));
			$this->email->message($this->input->post('message'));
			$this->email->attach($path);
			if ($this->email->send()) {
			    $this->session->set_flashdata('success', $this->lang->line('mail_success'));
			} else {
			    $this->session->set_flashdata('error', $this->lang->line('mail_error'));
			}
		    }
		} else {
		    $this->data["subview"] = "error";
		    $this->load->view('_layout_main', $this->data);
		}
	    } else {
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    private function checkKeysExists($value) {
	$required = array('date', 'notice', 'notice_for','title');
	$data = array_shift($value);
	if (count(array_intersect_key(array_flip($required), $data)) === count($required)) {
	    //All required keys exist! 
	    $status = 1;
	} else {
	    $missing = array_intersect_key(array_flip($required), $data);
	    $data_miss = array_diff(array_flip($required), $missing);
	    $status = ' Column with title <b>' . implode(', ', array_keys($data_miss)) . '</b>  miss from Excel file. Please make sure file is in the same format as a sample file';
	}
	return $status;
    }

    public function uploadByFile() {
	$data = $this->uploadExcel();

	$status = $this->checkKeysExists($data);
	if ((int) $status == 1) {
	    $status = '';
	    foreach ($data as $value) {

		/*
		 * To avoid any duplicate, lets check first if this  name & phone
		 */
		
		    $array = array(
			"title" => $value["title"],
			"notice" => $value["notice"],
			"notice_for"=>  implode(',', $value['notice_for']),
			"year" => date('Y'),
			"date" => date("Y-m-d", strtotime($value['date']))
		    );
		    $this->notice_m->insert_notice($array);

		    $status .= '<p class="alert alert-success">Notice "' . $value["title"] .$value['date'].'" uploaded successfully</p>';
		
	    }
	}
	$this->session->set_flashdata('success', $this->lang->line('menu_success'));
	$this->data['status'] = $status;
	$this->data["subview"] = "mark/upload_status";
	$this->load->view('_layout_main', $this->data);
    }
}

/* End of file notice.php */
/* Location: .//D/xampp/htdocs/school/mvc/controllers/notice.php */