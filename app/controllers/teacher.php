<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Teacher extends Admin_Controller {

    /**
     * -----------------------------------------
     * 
     * ******* Address****************
     * INETS COMPANY LIMITED
     * P.O BOX 32258, DAR ES SALAAM
     * TANZANIA
     * 
     * 
     * *******Office Location *********
     * 11th block, Bima Road, Mikocheni B, Kinondoni, Dar es salaam
     * 
     * 
     * ********Contacts***************
     * Email: <info@inetstz.com>
     * Website: <www.inetstz.com>
     * Mobile: <+255 655 406 004>
     * Tel:    <+255 22 278 0228>
     * -----------------------------------------
     */
    function __construct() {
	parent::__construct();
	$this->load->model("teacher_m");
	$this->load->model("setting_m");
	$language = $this->session->userdata('lang');
	$this->lang->load('teacher', $language);
	$this->lang->load('email', $language);
    }

    public function index() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype) {
	    $this->data['teachers'] = $this->teacher_m->get_teacher();
	    $this->data["subview"] = "teacher/index";
	    $this->load->view('_layout_main', $this->data);
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    protected function rules() {
	$rules = array(
	    array(
		'field' => 'name',
		'label' => $this->lang->line("teacher_name"),
		'rules' => 'trim|required|xss_clean|max_length[60]'
	    ),
	    array(
		'field' => 'designation',
		'label' => $this->lang->line("teacher_designation"),
		'rules' => 'trim|required|max_length[128]|xss_clean'
	    ),
	    array(
		'field' => 'dob',
		'label' => $this->lang->line("teacher_dob"),
		'rules' => 'trim|required|max_length[20]|xss_clean'
	    ),
	    array(
		'field' => 'sex',
		'label' => $this->lang->line("teacher_sex"),
		'rules' => 'trim|required|max_length[10]|xss_clean'
	    ),
	    array(
		'field' => 'religion',
		'label' => $this->lang->line("teacher_religion"),
		'rules' => 'trim|max_length[25]|xss_clean'
	    ),
	    array(
		'field' => 'id',
		'label' => $this->lang->line("teacher_id"),
		'rules' => 'trim|max_length[10]|xss_clean'
	    ),
	    array(
		'field' => 'email',
		'label' => $this->lang->line("teacher_email"),
		'rules' => 'trim|required|max_length[40]|valid_email|xss_clean|callback_unique_email'
	    ),
	    array(
		'field' => 'phone',
		'label' => $this->lang->line("teacher_phone"),
		'rules' => 'trim|min_length[5]|max_length[25]|xss_clean'
	    ),
	    array(
		'field' => 'address',
		'label' => $this->lang->line("teacher_address"),
		'rules' => 'trim|max_length[200]|xss_clean'
	    ),
	    array(
		'field' => 'jod',
		'label' => $this->lang->line("teacher_jod"),
		'rules' => 'trim|required|max_length[20]|xss_clean'
	    ),
	    array(
		'field' => 'photo',
		'label' => $this->lang->line("teacher_photo"),
		'rules' => 'trim|max_length[200]|xss_clean'
	    )
	);
	return $rules;
    }

    public function add() {
	$setting = $this->setting_m->get_setting(1);
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin") {
	    if ($_POST) {

		$id = $this->input->post('id');


		$rules = $this->rules();
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE) {
		    $this->data['form_validation'] = validation_errors();
		    $this->data["subview"] = "teacher/add";
		    $this->load->view('_layout_main', $this->data);
		} else {

		    $array = array();
		    $array['name'] = $this->input->post("name");
		    $array['designation'] = $this->input->post("designation");
		    $array['id_number'] = $this->input->post("id");
		    $array["dob"] = date("Y-m-d", strtotime($this->input->post("dob")));
		    $array["sex"] = $this->input->post("sex");
		    $array['religion'] = $this->input->post("religion");
		    $array['email'] = $this->input->post("email");
		    $array['phone'] = $this->input->post("phone");
		    $array['address'] = $this->input->post("address");
		    $array['jod'] = date("Y-m-d", strtotime($this->input->post("jod")));
		    $array['username'] = $this->input->post("phone");
		    $array['employment_type'] = $this->input->post("employment_type");
		    $array['password'] = $this->teacher_m->hash('teacher123');
		    $array['usertype'] = "Teacher";

		    $new_file = "defualt.png";
		    if ($_FILES["image"]['name'] != "") {
			$file_name = $_FILES["image"]['name'];
			$file_name_rename = rand(1, 100000000000);
			$explode = explode('.', $file_name);
			if (count($explode) >= 2) {
			    $upload = $this->uploadImage();
			    $array['photo'] = $upload;
			    if ($upload == FALSE) {
				$this->data["image"] = $this->upload->display_errors();
				$this->data["subview"] = "teacher/add";
				$this->load->view('_layout_main', $this->data);
			    } else {

				$data = array("upload_data" => $this->upload->data());

				$this->teacher_m->insert_teacher($array);
				$this->session->set_flashdata('success', $this->lang->line('menu_success'));

				$message = sprintf($this->lang->line('add_new_teacher'), $this->input->post('name'), $setting->sname, $this->input->post('phone'), '123456');
				$this->send_sms($this->input->post("phone"), $message);
				$this->send_email($this->input->post('email'), $setting->sname . ' Online Login Information', $message);
				redirect(base_url("teacher/index"));
			    }
			} else {
			    $this->data["image"] = "Invalid file";
			    $this->data["subview"] = "teacher/add";
			    $this->load->view('_layout_main', $this->data);
			}
		    } else {
			$array["photo"] = $new_file;
			$this->teacher_m->insert_teacher($array);
			$this->session->set_flashdata('success', $this->lang->line('menu_success'));

			//send email
			$message = sprintf($this->lang->line('add_new_teacher'), $this->input->post('name'), $setting->sname, $this->input->post('phone'), '123456');

			//$this->send_sms($this->input->post("phone"), $message);
			$this->send_email($this->input->post('email'), $setting->sname . ' Online Login Information', $message);
			redirect(base_url("teacher/index"));
		    }
		}
	    } else {
		$this->data["subview"] = "teacher/add";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function view() {
	$usertype = $this->session->userdata('usertype');
	if ($usertype) {
	    $id = htmlentities(($this->uri->segment(3)));
	    if ((int) $id) {
		$this->data['teacher'] = $this->teacher_m->get_teacher($id);
		if ($this->data['teacher']) {
		    $this->data["subview"] = "teacher/view";
		    $this->load->view('_layout_main', $this->data);
		} else {
		    $this->data["subview"] = "error";
		    $this->load->view('_layout_main', $this->data);
		}
	    } else {
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function edit() {
	$usertype = $this->session->userdata("usertype");
	$setting = $this->setting_m->get_setting(1);
	if ($usertype == "Admin") {
	    $id = htmlentities(($this->uri->segment(3)));
	    if ((int) $id) {
		$this->data['teacher'] = $this->teacher_m->get_teacher($id);
		if ($this->data['teacher']) {
		    if ($_POST) {
			$rules = $this->rules();
			unset($rules[9], $rules[10], $rules[11]);
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == FALSE) {
			    $this->data["subview"] = "teacher/edit";
			    $this->load->view('_layout_main', $this->data);
			} else {
			    $array = array();
			    $array['name'] = $this->input->post("name");
			    $array['designation'] = $this->input->post("designation");
			    $array["dob"] = date("Y-m-d", strtotime($this->input->post("dob")));
			    $array["sex"] = $this->input->post("sex");
			    $array['religion'] = $this->input->post("religion");
			    $array['email'] = $this->input->post("email");
			    $array['phone'] = $this->input->post("phone");
			    $array['employment_type'] = $this->input->post("employment_type");
			    $array['address'] = $this->input->post("address");
			    $array['jod'] = date("Y-m-d", strtotime($this->input->post("jod")));

			    if ($_FILES["image"]['name'] != "") {
				$file_name = $_FILES["image"]['name'];
				$file_name_rename = rand(1, 100000000000);
				$explode = explode('.', $file_name);
				if (count($explode) >= 2) {
				    $upload = $this->uploadImage();
				    $array['photo'] = $upload;
				    if ($upload == FALSE) {
					$this->data["image"] = $this->upload->display_errors();
					$this->data["subview"] = "teacher/edit";
					$this->load->view('_layout_main', $this->data);
				    } else {
					$data = array("upload_data" => $this->upload->data());
					$this->teacher_m->update_teacher($array, $id);
					$this->session->set_flashdata('success', $this->lang->line('menu_success'));
					//$message = 'Hello ' . $this->input->post("name") . ', Your informations have been modified ' . $setting->sname . '. Login with username =' . $this->input->post("phone") . ' and password =teacher123 at www.' . $setting->sname . '.shulesoft.com';
					$message = sprintf($this->lang->line('edit_teacher'), $this->input->post("name"), $this->input->post("phone"), 'teacher123');

					//$this->send_sms($this->input->post("phone"), $message);
					$this->send_email($this->input->post('email'), $setting->sname . ' Online Login Information', $message);
					redirect(base_url("teacher/index"));
				    }
				} else {
				    $this->data["image"] = "Invalid file";
				    $this->data["subview"] = "teacher/edit";
				    $this->load->view('_layout_main', $this->data);
				}
			    } else {
				$this->teacher_m->update_teacher($array, $id);
				$this->session->set_flashdata('success', $this->lang->line('menu_success'));
				$message = sprintf($this->lang->line('edit_teacher'), $this->input->post("name"), $this->input->post("phone"), 'teacher123');
				//$message = 'Hello ' . $this->input->post("name") . ', Your informations have been modified ' . $setting->sname . '. Login with username =' . $this->input->post("phone") . ' and password =teacher123 at www.' . $setting->sname . '.shulesoft.com';
				//$this->send_sms($this->input->post("phone"), $message);
				$this->send_email($this->input->post('email'), $setting->sname . ' Online Login Information', $message);
				redirect(base_url("teacher/index"));
			    }
			}
		    } else {
			$this->data["subview"] = "teacher/edit";
			$this->load->view('_layout_main', $this->data);
		    }
		} else {
		    $this->data["subview"] = "error";
		    $this->load->view('_layout_main', $this->data);
		}
	    } else {
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function delete() {
	$usertype = $this->session->userdata("usertype");
	$setting = $this->setting_m->get_setting(1);
	if ($usertype == "Admin") {
	    $id = htmlentities(($this->uri->segment(3)));
	    if ((int) $id) {
		$teacher_id = $this->teacher_m->get_single_teacher(array("teacherID" => $id));

		$this->teacher_m->delete_teacher($id);
		$this->session->set_flashdata('success', $this->lang->line('menu_success'));
		//$message="Hello".$teacher_id->name."You have been deleted in shulesoft";
		$message = sprintf($this->lang->line('delete_teacher'), $teacher_id->name, $setting->sname, $setting->sname);
		$this->send_email($teacher_id->email, $setting->sname, $message);
		redirect(base_url("teacher/index"));
	    } else {
		redirect(base_url("teacher/index"));
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function lol_username() {
	$id = htmlentities(($this->uri->segment(3)));
	if ((int) $id) {
	    $tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'setting' => 'setting');
	    $array = array();
	    $i = 0;
	    foreach ($tables as $table) {
		$user = $this->teacher_m->get_username($table, array("username" => $this->input->post('username'), $table . "ID !=" => $id));
		if (count($user)) {
		    $this->form_validation->set_message("lol_username", "%s already exists");
		    $array['permition'][$i] = 'no';
		} else {
		    $array['permition'][$i] = 'yes';
		}
		$i++;
	    }
	    if (in_array('no', $array['permition'])) {
		return FALSE;
	    } else {
		return TRUE;
	    }
	} else {
	    $tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'setting' => 'setting');
	    $array = array();
	    $i = 0;
	    foreach ($tables as $table) {
		$user = $this->teacher_m->get_username($table, array("username" => $this->input->post('username')));
		if (count($user)) {
		    $this->form_validation->set_message("lol_username", "%s already exists");
		    $array['permition'][$i] = 'no';
		} else {
		    $array['permition'][$i] = 'yes';
		}
		$i++;
	    }

	    if (in_array('no', $array['permition'])) {
		return FALSE;
	    } else {
		return TRUE;
	    }
	}
    }

    public function date_valid($date) {
	if (strlen($date) < 10) {
	    $this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");
	    return FALSE;
	} else {
	    $arr = explode("-", $date);
	    $dd = $arr[0];
	    $mm = $arr[1];
	    $yyyy = $arr[2];
	    if (checkdate($mm, $dd, $yyyy)) {
		return TRUE;
	    } else {
		$this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");
		return FALSE;
	    }
	}
    }

    public function print_preview() {
	$id = htmlentities(($this->uri->segment(3)));
	if ((int) $id) {
	    $usertype = $this->session->userdata('usertype');
	    if ($usertype == "Admin") {
		$this->load->library('html2pdf');
		$this->html2pdf->folder('./assets/pdfs/');
		$this->html2pdf->filename('Report.pdf');
		$this->html2pdf->paper('a4', 'portrait');

		$this->data["teacher"] = $this->teacher_m->get_teacher($id);
		if ($this->data["teacher"]) {
		    $this->data['panel_title'] = $this->lang->line('panel_title');
		    $html = $this->load->view('teacher/print_preview', $this->data, true);
		    $this->html2pdf->html($html);
		    $this->html2pdf->create();
		} else {
		    $this->data["subview"] = "error";
		    $this->load->view('_layout_main', $this->data);
		}
	    } else {
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function send_mail() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin") {
	    $id = $this->input->post('id');
	    if ((int) $id) {
		$this->load->library('html2pdf');
		$this->html2pdf->folder('uploads/report');
		$this->html2pdf->filename('Report.pdf');
		$this->html2pdf->paper('a4', 'portrait');

		$this->data["teacher"] = $this->teacher_m->get_teacher($id);
		if ($this->data["teacher"]) {
		    $this->data['panel_title'] = $this->lang->line('panel_title');
		    $html = $this->load->view('teacher/print_preview', $this->data, true);
		    $this->html2pdf->html($html);
		    $this->html2pdf->create('save');

		    if ($path = $this->html2pdf->create('save')) {
			$this->load->library('email');
			$this->email->set_mailtype("html");
			$this->email->from($this->data["siteinfos"]->email, $this->data['siteinfos']->sname);
			$this->email->to($this->input->post('to'));
			$this->email->subject($this->input->post('subject'));
			$this->email->message($this->input->post('message'));
			$this->email->attach($path);
			if ($this->email->send()) {
			    $this->session->set_flashdata('success', $this->lang->line('mail_success'));
			} else {
			    $this->session->set_flashdata('error', $this->lang->line('mail_error'));
			}
		    }
		} else {
		    $this->data["subview"] = "error";
		    $this->load->view('_layout_main', $this->data);
		}
	    } else {
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function unique_email() {
	$id = htmlentities(($this->uri->segment(3)));
	if ((int) $id) {
	    $user_info = $this->teacher_m->get_single_teacher(array('teacherID' => $id));
	    $tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'setting' => 'setting');
	    $array = array();
	    $i = 0;
	    foreach ($tables as $table) {
		$user = $this->teacher_m->get_username($table, array("email" => $this->input->post('email'), 'username !=' => $user_info->username));
		if (count($user)) {
		    $this->form_validation->set_message("unique_email", "%s already exists");
		    $array['permition'][$i] = 'no';
		} else {
		    $array['permition'][$i] = 'yes';
		}
		$i++;
	    }
	    if (in_array('no', $array['permition'])) {
		return FALSE;
	    } else {
		return TRUE;
	    }
	} else {
	    $tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'setting' => 'setting');
	    $array = array();
	    $i = 0;
	    foreach ($tables as $table) {
		$user = $this->teacher_m->get_username($table, array("email" => $this->input->post('email')));
		if (count($user)) {
		    $this->form_validation->set_message("unique_email", "%s already exists");
		    $array['permition'][$i] = 'no';
		} else {
		    $array['permition'][$i] = 'yes';
		}
		$i++;
	    }

	    if (in_array('no', $array['permition'])) {
		return FALSE;
	    } else {
		return TRUE;
	    }
	}
    }

    public function export() {

	// get all users in array format
	$users = $this->db->query('SELECT name,designation,dob,sex,religion,email,phone,address FROM ' . set_schema_name() . 'teacher')->result_array();
	$title = array(' Name', 'designation', 'Date of birth', 'Gender', 'Religion', 'email', 'phone,address');
	$this->exportExcel($users, $title, 'teachers');
    }

    private function checkKeysExists($value) {
	$required = array('phone', 'name', 'designation', 'dob', 'sex',
	    'religion', 'joining_date', 'email', 'employment_type', 'address');
	$data = array_shift($value);
	if (count(array_intersect_key(array_flip($required), $data)) === count($required)) {
	    //All required keys exist! 
	    $status = 1;
	} else {
	    $missing = array_intersect_key(array_flip($required), $data);
	    $data_miss = array_diff(array_flip($required), $missing);
	    $status = ' Column with title <b>' . implode(', ', array_keys($data_miss)) . '</b>  miss from Excel file. Please make sure file is in the same format as a sample file';
	}
	return $status;
    }

    public function uploadByFile() {
	$data = $this->uploadExcel();

	$status = $this->checkKeysExists($data);
	if ((int) $status == 1) {
	    $status = '';
	    $password = $this->student_m->hash("teacher123");
	    foreach ($data as $value) {

		/*
		 * To avoid any duplicate, lets check first if this  name & phone
		 */
		$name = preg_replace('/[^a-zA-Z0-9 ]/', '', $value['name']);
		$phone = preg_replace('/[^a-zA-Z0-9 ]/', '', $value['phone']);

		$teacher_info = $this->teacher_m->get_single_teacher(array('name' => $name, 'phone' => $phone));
		if ($name == '' || $phone == '') {
		    $status .= '<p class="alert alert-danger">Teacher name and phone cannot be empty. Record failed to be uploaded</p>';
		} else if (empty($teacher_info)) {

		    $array = array(
			"name" => $name,
			"designation" => $value['designation'],
			"dob" => date("Y-m-d", strtotime($value['dob'])),
			"sex" => $value['sex'],
			"religion" => $value['religion'],
			"email" => $value['email'],
			"phone" => $value['phone'],
			"jod" => date("Y-m-d", strtotime($value['joining_date'])),
			"address" => $value['address'],
			"password" => $password,
			"username" => $phone,
			"employment_type" => $value['employment_type'],
			"usertype" => 'Teacher',
			"photo" => "defualt.png"
		    );
		    $this->teacher_m->insert_teacher($array);

		    $status .= '<p class="alert alert-success">Teacher "' . $name . '" uploaded successfully</p>';
		} else {
		    $status .= '<p class="alert alert-info">Teacher "' . $name . '" exist</p>';
		}
	    }
	}
	$this->session->set_flashdata('success', $this->lang->line('menu_success'));
	$this->data['status'] = $status;
	$this->data["subview"] = "mark/upload_status";
	$this->load->view('_layout_main', $this->data);
    }

}

/* End of file teacher.php */
/* Location: .//D/xampp/htdocs/school/mvc/controllers/teacher.php */