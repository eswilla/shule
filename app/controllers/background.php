<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once 'app/traits/mailList.php';
require_once 'app/traits/SmsBackground.php';

class Background extends Admin_Controller {

    use mailList;

use SmsBackground;

    /**
     * Description of background
     *
     *  -----------------------------------------------------
     *  Copyright: INETS COMPANY LIMITED
     *  Website: www.inetstz.com
     *  Email: info@inetstz.com
     *  -----------------------------------------------------
     * @author Ephraim Swilla
     */
    public function __construct() {
	parent::__construct();
    }

    public function updateSchema() {
	$db_file = file_get_contents('app/config/development/db.txt');
	$schemas = explode(',', $db_file);
	$Conn = new PDO('pgsql:host='.$this->db->hostname.';port='.$this->db->port.';dbname='.$this->db->database,$this->db->username, $this->db->password);

	foreach ($schemas as $schema) {
	    
	    $Conn->exec('ALTER TABLE '.$schema.'.notice ADD COLUMN notice_for character varying;');
	}
    }
    public function test() {
	$data = $this->db->query('select "studentID","subjectID",subject,"classesID" from ' . set_schema_name() . 'mark where "subjectID" in (SELECT "subjectID" FROM ' . set_schema_name() . 'subject WHERE subject_author=\'Option\' AND mark <>0 AND mark is not NULL) group by "studentID","subjectID",subject,"classesID"')->result();
	$i = 0;
	foreach ($data as $mark) {
	    $sub = $this->db->query('select * FROM ' . set_schema_name() . 'subject_student WHERE subject_id=' . $mark->subjectID . ' AND student_id=' . $mark->studentID)->result();
	    if (empty($sub)) {
		$this->db->insert('subject_student', array(
		    'subject_id' => $mark->subjectID,
		    'student_id' => $mark->studentID
		));
		echo $i . '=record inserted successfully<br/>';
		$i++;
	    }
	}

	exit;
	$data = $this->db->query('select phone, name from ' . set_schema_name() . 'parent WHERE "parentID" in (select "parentID" from ' . set_schema_name() . 'student WHERE "classesID" in (SELECT "classesID" from ' . set_schema_name() . 'classes where classes_numeric=0 ) )')->result();
	$i = 1;
	foreach ($data as $val) {
	    $this->db->insert('sms', array(
		'body' => 'Habari ' . $val->name . ', tunapenda kukutaarifu kua tarehe 28 Jan 2017 (Jumapili) saa 2:30 asubuhi kutakua na  PARENTS MEETING hapa CANOSSA. kama mzazi/mlezi tunakusihi usikose',
		'phone_number' => $val->phone
	    ));
	    echo $i . '=sms sent to parent ' . $val->name . '<br/>';
	    $i++;
	}
    }

    public function index() {
	return $this->sendSmsToBackground();
    }

    public function sendSmsToBackground() {
	$schemas = array('canossa');
	foreach ($schemas as $schema) {
	    $app = $this->setting_m->get_sms_api($schema);
	    define('API_KEY', $app->api_key);
	    define('API_SECRET', $app->api_secret);
	    $this->load->library("karibusms");

	    $data = $this->db->query('select * from ' . $schema . '.sms where status IS NULL or status <>1 ORDER BY RANDOM() limit 10')->result();

	    if (!empty($data)) {
		foreach ($data as $sms) {
		    $karibusms = new karibusms();
		    $karibusms->set_name(strtoupper($schema));
		    $karibusms->karibuSMSpro = TRUE;
		    $result = (object) json_decode($karibusms->send_sms($sms->phone_number, $sms->body));
		    if ($result->success == 1) {
			$this->db->query('update ' . $schema . '.sms set status=1 WHERE sms_id=' . $sms->sms_id);
		    } else {
			$this->db->query('update ' . $schema . '.sms set status=0 WHERE sms_id=' . $sms->sms_id);
		    }
		}
	    }
	}
    }

    public function maillist() {
	$command = $_POST['command'];
	$email_addr = $_POST['email_addr'];
	$list_name = 'your-list';
	$to_addr = ' Majordomo@yourdomain.com';
	$body = $command . ' ' . $list_name;
	mail($to_addr, '', $body, "From: $email_addr");
	echo 'Your request to ' . $command . ' to the list ' . $list_name . ' has been processed.';
    }

}
